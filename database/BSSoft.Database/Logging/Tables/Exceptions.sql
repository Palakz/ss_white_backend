﻿CREATE TABLE [Logging].[Exceptions] (
    [Id]             INT              IDENTITY (1, 1) NOT NULL,
    [SubscriberId]   INT              NULL,
    [RequestId]      UNIQUEIDENTIFIER NULL,
    [Source]         VARCHAR (MAX)    NULL,
    [StackTrace]     VARCHAR (MAX)    NULL,
    [InnerException] VARCHAR (MAX)    NULL,
    [Message]        VARCHAR (MAX)    NULL,
    [Data]           VARCHAR (MAX)    NULL,
    [FileName]       VARCHAR (MAX)    NULL,
    [IpAddress]      VARCHAR (45)     NULL,
    [Stamp]          SMALLDATETIME    NULL,
    CONSTRAINT [PK__Exceptio__3214EC079033EA88] PRIMARY KEY CLUSTERED ([Id] ASC)
);

