﻿CREATE TABLE [document].[SalesDocumentHeaders] (
    [Id]              INT             IDENTITY (1, 1) NOT NULL,
    [SalesDocumentId] INT             NOT NULL,
    [HeaderId]        INT             NOT NULL,
    [CalculationType] TINYINT         NOT NULL,
    [Percentage]      DECIMAL (18, 2) NOT NULL,
    [Value]           DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__SalesDoc__3214EC07FF11040C] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__SalesDocu__Heade__310335E5] FOREIGN KEY ([HeaderId]) REFERENCES [master].[Headers] ([Id]),
    CONSTRAINT [FK__SalesDocu__Sales__300F11AC] FOREIGN KEY ([SalesDocumentId]) REFERENCES [document].[SalesDocuments] ([Id])
);

