﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetLotNumbers
Comments		: 20-04-2021 | Amit Khanna | This procedure is used to get All lot numbers for common calls.

Test Execution	: EXEC document.GetLotNumbers
					@SubscriberId = 1,
					@IsEditRequest = 0,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetLotNumbers]
(
	@SubscriberId INT,
	@IsEditRequest BIT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[GetLotNumbers]','
										@SubscriberId =',@SubscriberId,',
										@IsEditRequest =',@IsEditRequest,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].[GetLotNumbers]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
	SELECT 
		doc.LotNumber,
		it.[Name]
	FROM
		document.PurchaseDocumentItems AS doc
		INNER JOIN document.AllocatedLotNumbers AS al ON doc.PurchaseDocumentId = al.PurchaseDocumentId AND doc.LotNumber = al.LotNumber
		INNER JOIN [master].Items AS it ON doc.ItemId = it.Id
	WHERE
		it.SubscriberId = @SubscriberId
		AND 
		(
			al.Quantity > 0 
			OR al.[Weight] > 0
			OR @IsEditRequest = 1
		)
			
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
