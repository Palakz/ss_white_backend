﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllJournalVouchers
Comments		: 11-05-2021 | Amit Khanna | This procedure is used to get All Journal Vouchers.

Test Execution	: EXEC [document].[GetAllJournalVouchers]
						@SubscriberId =1,
						@CompanyId = NULL,
						@ClientId =  NULL,
						@Month = NULL,
						@FinancialYear = NULL,
						@FromDate = NULL,
						@ToDate = NULL,
						@Status = NULL,
						@Start = 0,
						@Length = 10,
						@SearchKeyword = NULL,
						@SortExpression = NULL,
						@IpAddress = NULL,
						@TotalRecords = 0 
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetAllJournalVouchers]
(
	@SubscriberId INT,
	@CompanyId INT,
	@ClientId INT,
	@Month SMALLINT,
	@FinancialYear INT,
	@FromDate SMALLDATETIME,
	@ToDate SMALLDATETIME,
	@Status TINYINT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[GetAllJournalVouchers]','
										@SubscriberId =',@SubscriberId,',
										@CompanyId = ',@CompanyId,',
										@ClientId = ',@ClientId,',
										@Month = ',@Month,',
										@FinancialYear = ',@FinancialYear,',
										@FromDate = ',@FromDate,',
										@ToDate = ',@ToDate,',
										@Status = ',@Status,',
										@Start = ',@Start,',
										@Length = ',@Length,',
										@SearchKeyword = ','',@SearchKeyword,'',',
										@SortExpression = ','',@SortExpression,'',',
										@IpAddress = ','',@IpAddress,'',',
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[document].[GetAllJournalVouchers]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT); 
    BEGIN TRY
		
		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[document].JournalVouchers AS us
			INNER JOIN [master].Clients AS ct ON us.CreditedClientId = ct.Id
			INNER JOIN [master].Clients AS dct ON us.DebitedClientId = dct.Id
		WHERE 
			us.SubscriberId = @SubscriberId
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.CompanyId = ISNULL(@CompanyId,us.CompanyId)
			AND
			(
				us.CreditedClientId = ISNULL(@ClientId,us.CreditedClientId)
				OR us.DebitedClientId = ISNULL(@ClientId,us.DebitedClientId)
			)
			AND us.[Date] BETWEEN ISNULL(@FromDate,us.[Date]) AND ISNULL(@ToDate,us.[Date])
			AND us.[Month]  = ISNULL(@Month,us.[Month])
			AND us.FinancialYear = ISNULL(@FinancialYear,us.FinancialYear)
			AND us.EndDate IS NULL
			AND 
			(
					us.VoucherNumber LIKE '%' + ISNULL(@SearchKeyword,us.[VoucherNumber]) + '%'
					OR ct.[CompanyName] LIKE '%' + ISNULL(@SearchKeyword,ct.[CompanyName]) + '%'
					OR dct.[CompanyName] LIKE '%' + ISNULL(@SearchKeyword,dct.[CompanyName]) + '%'
					OR us.CreditedReferenceNumber LIKE '%' + ISNULL(@SearchKeyword,us.[CreditedReferenceNumber]) + '%'
					OR us.DebitedReferenceNumber LIKE '%' + ISNULL(@SearchKeyword,us.[DebitedReferenceNumber]) + '%'
			);

		SELECT @TotalRecords = COUNT(Id)  FROM #TempTable; 

		SELECT
			us.Id,
			us.VoucherNumber,
			us.[Date],
			ct.[CompanyName] AS CreditedClientName,
			dct.[CompanyName] AS DebitedClientName,
			us.TotalAmount,
			us.Remarks,
			us.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN document.JournalVouchers AS us ON tmp.Id = us.Id
			INNER JOIN [master].Clients AS ct ON us.CreditedClientId = ct.Id
			INNER JOIN [master].Clients AS dct ON us.DebitedClientId = dct.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.[ActualVoucherNumber] END DESC,
			CASE WHEN @SortExpression = 'voucherNumber asc' THEN us.[ActualVoucherNumber] END ASC,
			CASE WHEN @SortExpression = 'voucherNumber desc' THEN us.[ActualVoucherNumber] END DESC,
			CASE WHEN @SortExpression = 'date asc' THEN us.[Date] END ASC,
			CASE WHEN @SortExpression = 'date desc' THEN us.[Date] END DESC,
			CASE WHEN @SortExpression = 'totalAmount asc' THEN us.[TotalAmount] END ASC,
			CASE WHEN @SortExpression = 'totalAmount desc' THEN us.[TotalAmount] END DESC,
			CASE WHEN @SortExpression = 'creditedClientName asc' THEN ct.[CompanyName] END ASC,
			CASE WHEN @SortExpression = 'creditedClientName desc' THEN ct.[CompanyName] END DESC,
			CASE WHEN @SortExpression = 'debitedClientName asc' THEN dct.[CompanyName] END ASC,
			CASE WHEN @SortExpression = 'debitedClientName desc' THEN dct.[CompanyName] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
