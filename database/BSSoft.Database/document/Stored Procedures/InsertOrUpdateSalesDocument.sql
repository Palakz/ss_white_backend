﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateSalesDocument
Comments		: 09-04-2021 | Amit Khanna | This procedure is used to insert or update Sales document.

Test Execution	: EXEC document.InsertOrUpdateSalesDocument
						@SubscriberId = 1,
						@FinancialYear = 2,
						@Id = 1
						@ClientId = 1,
						@VoucherNumber = 1,
						@Date = '2022-01-01',
						@VoucherType = 1,
						@Month = 4,
						@ChallanNumber = NULL,
						@ChallanDate = NULL,
						@MarkNumber = NULL,
						@ReceivingPerson = NULL,
						@TotalAmount = 410.05,
						@Remarks = NULL,
						@Prefix = NULL,
						@Suffix = NULL,
						@IpAddress = NULL,
						@CreatedBy = 1,
						@CreatedDate  = '2021-01-01',
						@StatusTypeActive = 1,
						@DocumentTypeSales = 1,
						@DocumentTypePurchase = 2,
						@ReturnValue = 1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[InsertOrUpdateSalesDocument]
(
	@SubscriberId INT,
	@FinancialYear INT,
	@Id INT,
	@CompanyId INT,
	@ClientId INT,
	@VoucherNumber VARCHAR(MAX),
	@Date DATE,
	@VoucherType TINYINT,
	@Month TINYINT,
	@ReceivedBy VARCHAR(50),
	@TotalAmount DECIMAL(18,2),
	@AdvanceReceivedBy VARCHAR(50),
	@AdvanceAmount DECIMAL(18,2),
	@Remarks VARCHAR(100),
	@Prefix VARCHAR(MAX),
	@Suffix VARCHAR(MAX),
	@DocumentItems [document].SalesDocumentItemType READONLY,
	@DocumentHeaders [document].[DocumentHeaderTableType] READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT,
	@DocumentTypeSales TINYINT,
	@DocumentTypePurchase TINYINT,
	@ReturnValue SMALLINT OUTPUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC document.InsertOrUpdateSalesDocument','
										@SubscriberId =',@SubscriberId,',
										@FinancialYear =',@FinancialYear,',
										@Id =',@Id,',
										@CompanyId =',@CompanyId,',
										@ClientId =',@ClientId,',
										@VoucherNumber =''',@VoucherNumber,''',
										@Date =',@Date,',
										@VoucherType =',@VoucherType,',
										@Month =',@Month,',
										@ReceivedBy =''',@ReceivedBy,''',
										@TotalAmount =',@TotalAmount,',
										@AdvanceReceivedBy =''',@AdvanceReceivedBy,''',
										@AdvanceAmount =',@AdvanceAmount,',
										@Remarks =''',@Remarks,''',
										@Prefix = ','',@Prefix,'','
										@Suffix = ','',@Suffix,'','
										@IpAddress = ,','',@IpAddress,'','
										@CreatedBy = ,',@CreatedBy,',
										@CreatedDate = ,',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive,',
										@DocumnetTypeSales = ,',@DocumentTypeSales,',
										@DocumentTypePurchase =,',@DocumentTypePurchase,',
										@ReturnValue = ',@ReturnValue
									  ),

			@ProcedureName = 'document.InsertOrUpdateSalesDocument',
			@ExecutionTime = GETDATE()	
			
	DECLARE @SalesDocumentId INT,@LastNumber INT;
	SET @ReturnValue = 1;
    BEGIN TRY
		EXEC @LastNumber = [subscriber].GetLastNumber
									@SubscriberId = @SubscriberId,
									@EntityId = @CompanyId,
									@FinancialYear = @FinancialYear,
									@DocumentType = @DocumentTypeSales,
									@ModuleType = NULL,
									@Status = @StatusTypeActive,
									@IpAddress = @IpAddress;
		
		SET @VoucherNumber = CONCAT(@Prefix,@LastNumber,@Suffix);
		CREATE TABLE #TempSalesDocumentItems
		(
			ItemId			INT					NOT NULL, 
			UnitId			INT					NOT NULL,
			[Description]	VARCHAR(150)		NULL,
			Quantity		DECIMAL(18,2)		NOT NULL,
			[Weight]		DECIMAL(18,2)		NULL,
			FreeQuantity	DECIMAL(18,2)		NULL,
			Rate			DECIMAL(18,2)		NULL,
			TotalAmount		DECIMAL(18,2)		NULL,
			LotNumber		INT					NOT NULL
		);

		CREATE TABLE #TempSalesAllocatedLotNumbers
		(
			LotNumber INT NOT NULL,
			Quantity DECIMAL(18,2),
			[Weight] DECIMAL(18,2)
		);

		CREATE TABLE #TempErrorLotNumbers
		(
			Id INT IDENTITY(1,1),
			LotNumber INT NOT NULL,
			Quantity DECIMAL(18,2),
			[Weight] DECIMAL(18,2)
		);

		INSERT INTO #TempSalesDocumentItems
		(
			ItemId,
			UnitId,
			[Description],
			Quantity,
			[Weight],
			FreeQuantity,
			Rate,
			TotalAmount,
			LotNumber
		)
		SELECT
			di.ItemId,
			di.UnitId,
			di.[Description],
			di.Quantity,
			di.[Weight],
			di.FreeQuantity,
			di.Rate,
			di.TotalAmount,
			di.LotNumber
		FROM
			@DocumentItems AS di;

		SELECT
			dh.HeaderId,
			dh.[Percentage],
			dh.CalculationType,
			dh.[Value]
		INTO
			#DocumentHeaders
		FROM
			@DocumentHeaders AS dh
			
		IF(@Id IS NULL)
		BEGIN
			INSERT INTO #TempSalesAllocatedLotNumbers
			(
				LotNumber,
				Quantity,
				[Weight]
			)
			SELECT
				aln.LotNumber,
				aln.Quantity,
				aln.[Weight]
			FROM
				document.AllocatedLotNumbers AS aln
				INNER JOIN #TempSalesDocumentItems As sdi ON aln.LotNumber = sdi.LotNumber
			WHERE
				aln.PurchaseDocumentId IS NOT NULL
				AND aln.SubscriberId = @SubscriberId;
		END
		ELSE
		BEGIN
			INSERT INTO #TempSalesAllocatedLotNumbers
			(
				LotNumber,
				Quantity,
				[Weight]
			)
			SELECT
				aln.LotNumber,
				SUM(aln.Quantity),
				SUM(aln.[Weight])
			FROM
				document.AllocatedLotNumbers AS aln
				INNER JOIN #TempSalesDocumentItems As sdi ON aln.LotNumber = sdi.LotNumber
			WHERE
				aln.SubscriberId = @SubscriberId
				AND 
				(
					aln.PurchaseDocumentId IS NOT NULL
					OR
					aln.SalesDocumentId = @Id
				)
			GROUP BY
				aln.LotNumber;
		END
		
		INSERT INTO #TempErrorLotNumbers
		(
			LotNumber,
			Quantity,
			[Weight]
		)
		SELECT
			aln.LotNumber,
			aln.Quantity,
			aln.[Weight]
		FROM
			#TempSalesAllocatedLotNumbers  AS aln
			INNER JOIN #TempSalesDocumentItems AS tsdi ON aln.LotNumber = tsdi.LotNumber
		WHERE
			(
				tsdi.Quantity > aln.Quantity
				OR
				tsdi.[Weight] > aln.[Weight]
			);

		IF EXISTS(SELECT Id FROM #TempErrorLotNumbers)
		BEGIN
			SET @ReturnValue = -3;
		END
		ELSE
		BEGIN
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [document].SalesDocuments WHERE SubscriberId = @SubscriberId AND [VoucherNumber] = @VoucherNumber AND CompanyId = @CompanyId AND FinancialYear = @FinancialYear AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				BEGIN TRAN
				INSERT INTO [document].SalesDocuments
				(
					SubscriberId,
					CompanyId,
					ClientId,
					VoucherNumber,
					[Date],
					VoucherType,
					[Month],
					FinancialYear,
					ReceivedBy,
					TotalAmount,
					AdvanceReceivedBy,
					AdvanceAmount,
					Remarks,
					ActualDocumentNumber,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@CompanyId,
					@ClientId,
					@VoucherNumber,
					@Date,
					@VoucherType,
					@Month,
					@FinancialYear,
					@ReceivedBy,
					@TotalAmount,
					@AdvanceReceivedBy,
					@AdvanceAmount,
					@Remarks,
					@LastNumber,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);

				SET @SalesDocumentId = SCOPE_IDENTITY();

				INSERT INTO [document].SalesDocumentItems
				(
					SalesDocumentId,
					ItemId,
					UnitId,
					[Description],
					Quantity,
					[Weight],
					FreeQuantity,
					Rate,
					Amount,
					LotNumber
				)
				SELECT
					@SalesDocumentId,
					di.ItemId,
					di.UnitId,
					di.[Description],
					di.Quantity,
					di.[Weight],
					di.FreeQuantity,
					di.Rate,
					di.TotalAmount,
					di.LotNumber
				FROM
					#TempSalesDocumentItems AS di;

				INSERT INTO document.SalesDocumentHeaders
				(
					SalesDocumentId,
					HeaderId,
					CalculationType,
					[Percentage],
					[Value]
				)
				SELECT
					@SalesDocumentId,
					thd.HeaderId,
					thd.CalculationType,
					thd.[Percentage],
					thd.[Value]
				FROM
					#DocumentHeaders AS thd;

				INSERT INTO document.AllocatedLotNumbers
				(
					SubscriberId,
					SalesDocumentId,
					DocumentType,
					LotNumber,
					Quantity,
					[Weight]
				)
				SELECT
					@SubscriberId,
					@SalesDocumentId,
					@DocumentTypeSales,
					pdi.LotNumber,
					pdi.Quantity,
					pdi.[Weight]
				FROM
					#TempSalesDocumentItems AS pdi;

				UPDATE
					aln
				SET
					aln.Quantity = aln.Quantity - it.Quantity,
					aln.[Weight] = ISNULL(aln.[Weight],0) - ISNULL(it.[Weight],0)
				FROM
					document.AllocatedLotNumbers AS aln 
					INNER JOIN #TempSalesDocumentItems AS it ON aln.LotNumber = it.LotNumber
				WHERE
					aln.DocumentType = @DocumentTypePurchase
					AND aln.SubscriberId = @SubscriberId;

				UPDATE
					subscriber.NumberConfigurations
				SET
					LastNumber = LastNumber + 1,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					SubscriberId = @SubscriberId
					AND EntityId = @CompanyId
					AND DocumentType = @DocumentTypeSales
					AND FinancialYear = @FinancialYear;

				COMMIT TRAN;
			END
		END
		ELSE
		BEGIN
			BEGIN TRAN
			UPDATE
				[document].Salesdocuments
			SET
				ClientId = @ClientId,
				[Date] = @Date,
				VoucherType = @VoucherType,
				[Month] = @Month,
				ReceivedBy = @ReceivedBy,
				TotalAmount = @TotalAmount,
				AdvanceReceivedBy = @AdvanceReceivedBy,
				AdvanceAmount = @AdvanceAmount,
				Remarks = @Remarks,
				IpAddress = @IpAddress,
				UpdatedBy = @CreatedBy,
				UpdatedDate = @CreatedDate
			WHERE
				Id = @Id;

			UPDATE
				aln
			SET
				aln.Quantity = aln.Quantity + it.Quantity,
				aln.[Weight] = ISNULL(aln.[Weight],0) + ISNULL(it.[Weight],0)
			FROM
				document.AllocatedLotNumbers AS aln 
				INNER JOIN document.AllocatedLotNumbers AS it ON aln.LotNumber = it.LotNumber AND it.SalesDocumentId = @Id
			WHERE
				aln.DocumentType = @DocumentTypePurchase
				AND aln.SubscriberId = @SubscriberId;

			DELETE FROM
					[document].SalesDocumentItems
				WHERE
					SalesDocumentId = @Id;

			DELETE FROM
					[document].SalesDocumentHeaders
				WHERE
					SalesDocumentId = @Id;

			DELETE
				aln
			FROM 
				document.AllocatedLotNumbers AS aln
			WHERE
				NOT EXISTS
				(
					SELECT
						LotNumber
					FROM
						#TempSalesDocumentItems
					WHERE
						LotNumber = aln.LotNumber
				)
				AND aln.SubscriberId = @SubscriberId
				AND aln.SalesDocumentId = @Id;

			INSERT INTO [document].SalesDocumentItems
			(
				SalesDocumentId,
				ItemId,
				UnitId,
				[Description],
				Quantity,
				[Weight],
				FreeQuantity,
				Rate,
				Amount,
				LotNumber
			)
			SELECT
				@Id,
				di.ItemId,
				di.UnitId,
				di.[Description],
				di.Quantity,
				di.[Weight],
				di.FreeQuantity,
				di.Rate,
				di.TotalAmount,
				di.LotNumber
			FROM
				#TempSalesDocumentItems AS di;

			INSERT INTO document.SalesDocumentHeaders
			(
				SalesDocumentId,
				HeaderId,
				CalculationType,
				[Percentage],
				[Value]
			)
			SELECT
				@Id,
				thd.HeaderId,
				thd.CalculationType,
				thd.[Percentage],
				thd.[Value]
			FROM
				#DocumentHeaders AS thd;

			MERGE
				document.AllocatedLotNumbers AS trgt
			USING
				#TempSalesDocumentItems AS src
			ON
			(
				trgt.SalesDocumentId = @Id
				AND trgt.DocumentType = @DocumentTypeSales
				AND trgt.LotNumber = src.LotNumber
			)
			WHEN MATCHED THEN UPDATE
			SET
				trgt.Quantity = src.Quantity,
				trgt.[Weight] = src.[Weight]
			WHEN NOT MATCHED THEN
			INSERT
			(
				SubscriberId,
				SalesDocumentId,
				DocumentType,
				LotNumber,
				Quantity,
				[Weight]
			)
			VALUES
			(
				@SubscriberId,
				@Id,
				@DocumentTypeSales,
				src.LotNumber,
				src.Quantity,
				src.[Weight]
			);

			UPDATE
				aln
			SET
				aln.Quantity = aln.Quantity - it.Quantity,
				aln.[Weight] = ISNULL(aln.[Weight],0) - ISNULL(it.[Weight],0)
			FROM
				document.AllocatedLotNumbers AS aln 
				INNER JOIN #TempSalesDocumentItems AS it ON aln.LotNumber = it.LotNumber
			WHERE
				aln.DocumentType = @DocumentTypePurchase
				AND aln.SubscriberId = @SubscriberId;

			COMMIT TRAN;
		END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	SELECT
		@ReturnValue AS ReturnValue,
		ln.LotNumber,
		ln.Quantity,
		ln.[Weight]
	FROM
		#TempErrorLotNumbers AS ln;
			


	DROP TABLE #TempSalesDocumentItems,#DocumentHeaders,#TempErrorLotNumbers,#TempSalesAllocatedLotNumbers;
