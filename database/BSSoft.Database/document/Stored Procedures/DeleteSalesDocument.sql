﻿/*------------------------------------------------------------------------------------------------------------
Name			: DeleteSalesDocument
Comments		: 23-04-2021 | Amit Khanna | This procedure is used to delete sales document by Id.

Test Execution	: EXEC [document].[DeleteSalesDocument]
						@SubscriberId =  1,
						@Id =  5,
						@IpAddress =  1,
						@DocumentTypePurchase = 2
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[DeleteSalesDocument]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45),
	@DocumentTypePurchase TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[DeleteSalesDocument]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,'','
										@DocumentTypePurchase = ',@DocumentTypePurchase
									  ),
			@ProcedureName = '[document].[DeleteSalesDocument]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY
		BEGIN TRAN

		UPDATE
			aln
		SET
			aln.Quantity = aln.Quantity + it.Quantity,
			aln.[Weight] = ISNULL(aln.[Weight],0) + ISNULL(it.[Weight],0)
		FROM
			document.AllocatedLotNumbers AS aln 
			INNER JOIN document.AllocatedLotNumbers AS it ON aln.LotNumber = it.LotNumber AND it.SalesDocumentId = @Id
		WHERE
			aln.DocumentType = @DocumentTypePurchase
			AND aln.SubscriberId = @SubscriberId;

		DELETE FROM
			document.AllocatedLotNumbers
		WHERE
			SalesDocumentId = @Id;

		DELETE FROM
			document.SalesDocumentItems
		WHERE
			SalesDocumentId = @Id;

		DELETE
			document.SalesDocumentHeaders
		WHERE
			SalesDocumentId = @Id;

		DELETE FROM
			document.SalesDocuments
		WHERE
			Id = @Id
			AND SubscriberId = @SubscriberId;
			
		COMMIT TRAN;
		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
