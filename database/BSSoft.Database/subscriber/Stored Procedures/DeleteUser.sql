﻿/*------------------------------------------------------------------------------------------------------------
Name			: DeleteUser
Comments		: 30-03-2021 | Amit Khanna | This procedure is used to delete user by Id. Status of user is set to 3 and enddate is set.

Test Execution	: EXEC subscriber.DeleteUser
					@SubscriberId =  1,
					@Id =  5,
					@IpAddress =  1,
					@StatusTypeDeleted = 3;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[DeleteUser]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45),
	@StatusTypeDeleted TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[DeleteUser]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ''',@IpAddress,'','
										@StatusTypeDelete = ',@StatusTypeDeleted
									  ),
			@ProcedureName = '[subscriber].[DeleteUser]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		UPDATE
			subscriber.users
		SET
			[status]  = @StatusTypeDeleted,
			EndDate = GETDATE()
		WHERE
			Id = @Id
			AND SubscriberId = @SubscriberId;

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
