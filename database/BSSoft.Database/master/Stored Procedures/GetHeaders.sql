﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetHeaders
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to get All headers for common calls.

Test Execution	: EXEC master.GetHeaders
					@SubscriberId = 1,
					@Status = NULL,
					@IpAddress = NULL
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetHeaders]
(
	@SubscriberId INT,
	@Status TINYINT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetHeaders]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,'
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetHeaders]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
		
		SELECT
			hdr.HeaderId,
			hd.[Name],
			hdr.CalculationType,
			mdl.[Name] AS Module,
			hdr.ModuleId
		FROM 
			[master].Headers AS hd
			INNER JOIN [master].HeaderDetails AS hdr ON hd.Id = hdr.HeaderId
			INNER JOIN subscriber.Modules AS mdl ON hdr.ModuleId = mdl.Id
		WHERE
			hd.SubscriberId = ISNULL(@SubscriberId,hd.SubscriberId)
			AND hd.[Status] = ISNULL(@Status,hd.[Status])
		ORDER BY
			hd.[Name] ASC;
		
		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
