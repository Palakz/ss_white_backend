﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateUqc
Comments		: 25-03-2021 | Amit Khanna | This procedure is used to insert uqc or update uqc by Id.

Test Execution	: EXEC master.InsertOrUpdateUqc
						@SubscriberId = 1,
						@Id = NULL,
						@Name = 'demo',
						@Code = 'demo',
						@PackingTypeId = 1,
						@SubUnitId = 1,
						@ConversionUnit = NULL,
						@IpAddress = '',
						@CreatedBy = 1,
						@CreatedDate = '2021-02-28',
						@StatusTypeActive = 1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateUqc]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(100),
	@Code VARCHAR(200),
	@PackingTypeId INT,
	@SubUnitId INT,
	@ConversionUnit DECIMAL(18,2),
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateUqc]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =,'',@Name,'',
										@Code =,'',@Code,'',
										@PackingTypeId =,',@PackingTypeId,',
										@SubUnitId =,',@SubUnitId,',
										@ConversionUnit =,',@ConversionUnit,',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateUqc]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1;
    BEGIN TRY
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].UQCS WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND Code = @Code AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO [master].UQCS
				(
					SubscriberId,
					[Name],
					Code,
					PackingTypeId,
					SubUnitId,
					ConversionUnit,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@Name,
					@Code,
					@PackingTypeId,
					@SubUnitId,
					@ConversionUnit,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].UQCS WHERE SubscriberId = @SubscriberId  AND [Name] = @Name AND Code = @Code AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				UPDATE
					[master].UQCS
				SET
					[Name] = @Name,
					[Code] = @Code,
					PackingTypeId = @PackingTypeId,
					SubUnitId = @SubUnitId,
					ConversionUnit  = @ConversionUnit,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id
					AND SubscriberId = @SubscriberId;
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
