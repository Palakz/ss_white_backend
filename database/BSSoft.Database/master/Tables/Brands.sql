﻿CREATE TABLE [master].[Brands] (
    [Id]           INT PRIMARY KEY IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT REFERENCES subscriber.subscribers(Id) NOT NULL,
    [Name]         VARCHAR(100) NOT NULL,
    [Status]       TINYINT NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT REFERENCES subscriber.Users(Id) NOT NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT REFERENCES subscriber.Users(Id) NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
);

