﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetLedgerGroupById
Comments		: 30-03-2021 | Vikas Patel | This procedure is used to get ledger group by Id.

Test Execution	: EXEC master.GetLedgerGroupById
					@SubscriberId =  1,
					@Id =  1,
					@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetLedgerGroupById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetLedgerGroupById]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetLedgerGroupById]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			tx.Id,
			tx.[Name],
			tx.[Code],
			tx.[Type] AS LedgerGroupType,
			tx.[AccountType]
		FROM
			[master].LedgerGroups AS tx
		WHERE
			tx.Id = @Id;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
