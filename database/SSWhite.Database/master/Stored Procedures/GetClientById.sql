﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetClientById
Comments		: 31-03-2021 | Amit Khanna | This procedure is used to get client by Id.

	Test Execution	: EXEC master.GetClientById
						@SubscriberId =  1,
						@Id =  13,
						@IpAddress =  1;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetClientById]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetClientById]','
										@SubscriberId = ',@SubscriberId,',
										@Id = ',@Id,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[master].[GetCompanyById]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY

		SELECT
			cmp.Id,
			cmp.CompanyName,
			cmp.Code,
			cmp.Gstin,
			cmp.Pan,
			cmp.[Address],
			cmp.CityId,
			cmp.StateId,
			cmp.EmailAddress,
			cmp.MobileNumber,
			cmp.Website,
			cmp.CreditLimit,
			cmp.OpeningBalance,
			cmp.BalanceType,
			cmp.LedgerGroupId,
			cmp.ZoneId
		FROM
			[master].Clients AS cmp
		WHERE
			cmp.Id = @Id;

		SELECT
			bdt.[Name],
			bdt.MobileNumber,
			bdt.Phone,
			bdt.EmailAddress
		FROM
			[master].ClientContactDetails AS bdt
		WHERE
			bdt.ClientId = @Id;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
