﻿/*------------------------------------------------------------------------------------------------------------
Name			: [master].[GetAllExtraOrdinaryAboutNominee]
Comments		: 04-07-2023 | Amit | This procedure is used to get Get All Extra Ordinary About Nominee.

Test Execution	: EXEC [master].[GetAllExtraOrdinaryAboutNominee]
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetAllExtraOrdinaryAboutNominee]
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		ms.Id,
		ms.[Name]
	FROM
		[master].ExtraOrdinaryAboutNominee AS ms
	WHERE
		ms.Status = 1
END

