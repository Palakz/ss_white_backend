﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [master].[GetSSWhiteTeamDetails]
Comments	: 22-04-2022 | Kartik Bariya | This procedure is used to get SSWhiteTeam details.

Test Execution : EXEC [master].[GetSSWhiteTeamDetails] 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetSSWhiteTeamDetails]
(
	@Status int
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		Id,
		Name,
		Designation,
		Email,
		Image
	FROM 
		master.SSWhiteTeam
	WHERE 
		Status = @Status
End;
