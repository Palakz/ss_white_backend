﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateItem
Comments		: 22-06-2021 | Tanvi Pathak | This procedure is used to insert item or update item by Id.

Test Execution	: DECLARE @ReturnValue INT = 1;
					EXEC @ReturnValue = [master].[InsertOrUpdateItem]
					@SubscriberId =1,
					@Id = NULL,
					@Name = 'demo',
					@Code = '1',
					@ItemTypeId = 1,
					@SizeId = 1,
					@BaseId = 1,
					@TaxId = 1,
					@BrandId = 1,
					@UnitId = 1,
					@ItemCategoryId = 1,
					@ItemSubCategoryId = 1,
					@Weight= 100.00,
					@Description = '',
					@ActualCodeNumber = 1,
					@IpAddress =NULL,
					@CreatedBy = 1,
					@CreatedDate = '2021-06-22',
					@StatusTypeActive =1
					SELECT @ReturnValue;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[InsertOrUpdateItem]
(
	@SubscriberId INT,
	@Id INT,
	@Name VARCHAR(100),
	@Code VARCHAR(100),
	@ItemTypeId INT,
	@SizeId INT,
	@BaseId INT,
	@TaxId INT,
	@BrandId INT,
	@UnitId INT,
	@ItemCategoryId INT,
	@ItemSubCategoryId INT,
	@Weight DECIMAL(18,2),
	@Description VARCHAR(500),
	@ActualCodeNumber INT,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[InsertOrUpdateItem]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@Name =''',@Name,''',
										@Code =''',@Code,''',
										@ItemTypeId =',@ItemTypeId,',
										@SizeId =',@SizeId,',
										@BaseId =',@BaseId,',
										@TaxId = ',@TaxId,',
										@BrandId = ',@BrandId,',
										@UnitId = ',@UnitId,',
										@ItemCategoryId =',@ItemCategoryId,',
										@ItemSubCategoryId =',@ItemSubCategoryId,',
										@Weight = ',@Weight,'
										@Description = ''',@Description,'''
										@ActualCodeNumber = ',@ActualCodeNumber,'
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive
									  ),

			@ProcedureName = '[master].[InsertOrUpdateItem]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1
		
    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Items WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO [master].Items
				(
					SubscriberId,
					[Name],
					Code,
					ItemTypeId,
					SizeId,
					BaseId,
					TaxId,
					BrandId,
					UnitId,
					ItemCategoryId,
					ItemSubCategoryId,
					[Weight],
					[Description],
					ActualCodeNumber,
					[Status],
					IpAddress,
					CreatedBy,
					CreatedDate
				)
				VALUES
				(
					@SubscriberId,
					@Name,
					@Code,
					@ItemTypeId,
					@SizeId,
					@BaseId,
					@TaxId,
					@BrandId,
					@UnitId,
					@ItemCategoryId,
					@ItemSubCategoryId,
					@Weight,
					@Description,
					@ActualCodeNumber,
					@StatusTypeActive,
					@IpAddress,
					@CreatedBy,
					@CreatedDate
				);
			END
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT Id FROM [master].Items WHERE SubscriberId = @SubscriberId AND [Name] = @Name AND EndDate IS NULL AND Id <> @Id)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				UPDATE
					[master].Items
				SET
					[Name] = @Name,
					ItemTypeId = @ItemTypeId,
					SizeId = @SizeId,
					BaseId = @BaseId,
					TaxId = @TaxId,
					BrandId = @BrandId,
					UnitId = @UnitId,
					ItemCategoryId = @ItemCategoryId,
					ItemSubCategoryId = @ItemSubCategoryId,
					[Weight] = @Weight,
					[Description] = @Description,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					Id = @Id;
			END
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
