﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllSizes
Comments		: 04-06-2021 | Tanvi Pathak | This procedure is used to get All Sizes.

Test Execution	: EXEC master.GetAllSizes
					@SubscriberId = 1,
					@Status = NULL,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@IpAddress = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetAllSizes]
(
	@SubscriberId INT,
	@Status TINYINT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [master].[GetAllSizes]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,',
										@Start = ',@Start,',
										@Length = ',@Length,',
										@SearchKeyword = ','',@SearchKeyword,'',',
										@SortExpression = ','',@SortExpression,'',',
										@IpAddress = ','',@IpAddress,'',',
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[master].[GetAllSizes]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT); 
    BEGIN TRY
		
		INSERT INTO #TempTable(Id)
		SELECT  
			us.Id
		FROM 
			[master].[Sizes] AS us
		WHERE 
			us.SubscriberId = @SubscriberId
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.EndDate IS NULL
			AND 
			(
					us.[SizeName] LIKE '%' + ISNULL(@SearchKeyword,us.[SizeName]) +'%'
			);

		SELECT @TotalRecords =COUNT(Id)  FROM #TempTable 

		SELECT
			us.Id,
			us.[SizeName],
			us.[PackPerBox],  
			us.[WeightPerBox], 
			us.[IsInch],       
			us.[LInch],        
			us.[BInch],       
			us.[IsCM],      
			us.[LCM],    
			us.[BCM],       
			us.[IsMM],       
			us.[LMM],      
			us.[BMM],    
			us.[SqureFeet],  
			us.[SqureMeter],
			us.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN [master].Sizes us ON tmp.Id = us.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.[SizeName] END ASC,
			CASE WHEN @SortExpression = 'Name asc' THEN us.[SizeName] END ASC,
			CASE WHEN @SortExpression = 'Name desc' THEN us.[SizeName] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
