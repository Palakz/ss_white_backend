﻿/*------------------------------------------------------------------------------------------------------------
Name			:  [master].[GetAllEmployeeTypes]
Comments		: 04-07-2023 | Amit | This procedure is used to get Get All Employee Types.

Test Execution	: EXEC [master].[GetAllEmployeeTypes]
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [master].[GetAllEmployeeTypes]
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		gn.Id,
		gn.[Name]
	FROM
		[master].EmployeeTypes AS gn
	WHERE
		gn.Status = 1
END
