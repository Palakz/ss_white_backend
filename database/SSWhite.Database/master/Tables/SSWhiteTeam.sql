﻿CREATE TABLE [master].[SSWhiteTeam] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (255)  NOT NULL,
    [Designation]  VARCHAR (255)  NOT NULL,
    [Image]        NVARCHAR (255) NULL,
    [Email]        VARCHAR (255)  NOT NULL,
    [Status]       TINYINT        NOT NULL,
    [CreatedBy]    INT            NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [ModifiedBy]   INT            NULL,
    [ModifiedDate] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

