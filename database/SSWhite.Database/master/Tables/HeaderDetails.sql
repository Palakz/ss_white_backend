﻿CREATE TABLE [master].[HeaderDetails] (
    [Id]              INT     IDENTITY (1, 1) NOT NULL,
    [HeaderId]        INT     NOT NULL,
    [ModuleId]        INT     NOT NULL,
    [CalculationType] TINYINT NOT NULL,
    CONSTRAINT [PK__HeaderDe__3214EC07C6BA7580] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__HeaderDet__Heade__5C37ACAD] FOREIGN KEY ([HeaderId]) REFERENCES [master].[Headers] ([Id]),
    CONSTRAINT [FK__HeaderDet__Modul__5D2BD0E6] FOREIGN KEY ([ModuleId]) REFERENCES [subscriber].[Modules] ([Id])
);

