﻿CREATE TABLE [master].[ClientSites] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [ClientId] INT           NOT NULL,
    [Name]     VARCHAR (100) NOT NULL,
    [Address]  VARCHAR (500) NULL,
    CONSTRAINT [PK__ClientSi__3214EC074D774B28] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ClientSit__Clien__08F5448B] FOREIGN KEY ([ClientId]) REFERENCES [master].[Clients] ([Id])
);

