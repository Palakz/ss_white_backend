﻿CREATE TABLE [master].[ClientItemDiscounts] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [ClientId]     INT           NOT NULL,
    [Status]       TINYINT       NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT           NOT NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT           NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__ClientIt__3214EC07910B2784] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ClientIte__Clien__469D7149] FOREIGN KEY ([ClientId]) REFERENCES [master].[Clients] ([Id]),
    CONSTRAINT [FK__ClientIte__Creat__47919582] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__ClientIte__Subsc__45A94D10] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__ClientIte__Updat__4885B9BB] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

