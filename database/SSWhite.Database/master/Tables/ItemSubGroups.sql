﻿CREATE TABLE [master].[ItemSubGroups] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [ItemGroupId]  INT           NOT NULL,
    [Name]         VARCHAR (50)  NOT NULL,
    [Status]       TINYINT       NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT           NOT NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT           NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__ItemSubG__3214EC076FE67C7F] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ItemSubGr__Creat__40F9A68C] FOREIGN KEY ([CreatedBy]) REFERENCES [subscriber].[Users] ([Id]),
    CONSTRAINT [FK__ItemSubGr__ItemG__40058253] FOREIGN KEY ([ItemGroupId]) REFERENCES [master].[ItemGroups] ([Id]),
    CONSTRAINT [FK__ItemSubGr__Subsc__3F115E1A] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id]),
    CONSTRAINT [FK__ItemSubGr__Updat__41EDCAC5] FOREIGN KEY ([UpdatedBy]) REFERENCES [subscriber].[Users] ([Id])
);

