﻿CREATE TYPE [master].[BankDetailTableType] AS TABLE (
    [Name]          VARCHAR (100) NOT NULL,
    [AccountNumber] VARCHAR (18)  NOT NULL,
    [Branch]        VARCHAR (50)  NOT NULL,
    [IfscCode]      VARCHAR (11)  NOT NULL,
    [IsDefault]     BIT           NOT NULL);

