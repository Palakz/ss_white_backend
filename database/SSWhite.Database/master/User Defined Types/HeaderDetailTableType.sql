﻿CREATE TYPE [master].[HeaderDetailTableType] AS TABLE (
    [ModuleId]        INT     NOT NULL,
    [CalculationType] TINYINT NOT NULL);

