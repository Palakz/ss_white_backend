﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.GetAllEpos
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: EXEC [emp].[GetRoles]
					@StatusTypeActive = 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetRoles]
(
	@StatusTypeActive INT
)
AS
BEGIN

	SET NOCOUNT ON;
	SELECT 
		RoleID,
		RoleName	
	FROM 
		dbo.Roles
	WHERE 
		IsActive = @StatusTypeActive
END
