﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[GetRecruiteeById]
Comments	: 15-06-2023 | Amit | This procedure is used to Add New Recruitee.

Test Execution :	
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetRecruiteeById]
(
	@Id INT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
		SELECT 
			rt.Id,
			rt.[FirstName],
			rt.[MiddleName],
			rt.[SurName],
			rt.[DOB],
			rt.[GendersId],
			rt.[MaritalStatusesId],
			rt.[ImagePath],
			rt.[Status],
			rt.[DoAnyRelative],
			rt.RelativeName,
			rt.RelationWithRelative,
			(SELECT STRING_AGG(Name,',')  FROM [master].Identities WHERE Id IN (select * from STRING_SPLIT(rt.IdentityProofs, ','))) AS IdentityProofsValue,
			rt.[IsDisabilityOrSickness],
			rt.[DisabilityDescription],
			rt.[IsWorkExperience],
			rt.[IsFromReference],
			rt.[CreatedDate],
			rt.[CreatedBy],
			gn.[Name] AS Gender,
			ms.[Name] AS MaritalStatus
		 FROM
			emp.Recruitees  AS rt
			INNER JOIN [master].Genders AS gn ON rt.GendersId = gn.Id
			INNER JOIN [master].MaritalStatuses AS ms ON rt.MaritalStatusesId = ms.Id
		WHERE
			rt.Id = @Id;
		  
		SELECT
			rd.Id,
			rd.RecruiteeId,
			rd.AddressType,
			rd.IsSameAsCurrentAddress,
			rd.AddressLine1,
			rd.AddressLine2,
			rd.City,
			rd.Taluka,
			rd.District,
			rd.State,
			rd.Pincode,
			rd.MobileNumber1,
			rd.MobileNumber2,
			rd.Email
		FROM
			[emp].RecruiteeAddresses AS rd
		WHERE
			rd.RecruiteeId = @Id
    
		SELECT
			rd.Id,
			rd.RecruiteeId,
			rd.AcademicYear,
			rd.Course,
			rd.University,
			rd.MainSubject,
			rd.Percentage
		FROM
			emp.RecruiteeEducations AS rd
		WHERE
			rd.RecruiteeId = @Id;

		SELECT
			re.Id,
			re.RecruiteeId,
			re.CompanyName,
			re.CompanyAddress,
			re.Designation,
			re.FromDate,
			re.ToDate,
			re.MonthlySalary
		FROM
			emp.RecruiteeExperiences AS re
		WHERE
			re.RecruiteeId = @Id;

		SELECT
			rf.Id,
			rf.RecruiteeId,
			rf.RelationTypesId,
			rf.[Name],
			rf.Occupation,
			rf.OrganizationName,
			rt.[Name] AS Relation
		FROM
			emp.RecruiteeFamilyDetails AS rf
			INNER JOIN [master].RelationTypes AS rt ON rf.RelationTypesId = rt.Id
		WHERE
			rf.RecruiteeId = @Id;

		SELECT
			rq.Id,
			rq.RecruiteeId,
			rq.QuestionsId,
			rq.Answer,
			qt.[Name] AS Question,
			qt.QuestionTypesId,
			qt.QuestionCategoriesId
 		FROM
			emp.RecruiteeQuestions AS rq
			INNER JOIN [master].Questions AS qt ON rq.QuestionsId = qt.Id
		WHERE
			rq.RecruiteeId = @Id
		ORDER BY 
			rq.QuestionsId ASC;

		SELECT
			rr.Id,
			rr.RecruiteeId,
			rr.[Name],
			rr.Designation,
			rr.Address,
			rr.ContactNumber
		FROM
			emp.RecruiteeReferences AS rr
		WHERE
			rr.RecruiteeId = @Id;
		
    END TRY
	BEGIN CATCH
		DECLARE
		@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
End;
