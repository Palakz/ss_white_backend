﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC [emp].[ChangeEmployeeStatus]
Comments	: 23-05-2023 | Amit | This procedure is used to change Employee status.
USE [SSWhiteHR]
 TEst Execution :	 
					  [emp].[ChangeEmployeeStatus] 
					 @EmployeeId = '9999',
					 @UpdatedBy = 12,
					 @UpdatedDate = '2021-03-03',
					 @InActiveReasonType = 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[ChangeEmployeeStatus]
(
	@EmployeeId VARCHAR(MAX),
	@UpdatedBy INT,
	@UpdatedDate DATETIME,
	@InActiveReasonType INT
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Status AS INT;

	SELECT @Status = [Status] FROM emp.UserMaster
	WHERE PersonId = @EmployeeId

	IF(@Status = 1 AND @InActiveReasonType > 0 )
	BEGIN

		UPDATE emp.VMSEmployeeDetails
		SET [InActiveReasonTypesId] = @InActiveReasonType
		WHERE PersonId = @EmployeeId

		UPDATE emp.UserMaster
		SET [Status] = 0,
		UpdatedBy = @UpdatedBy,
		UpdatedDate = @UpdatedDate
		WHERE PersonId = @EmployeeId

	END
	ELSE
	BEGIN
		
		UPDATE emp.VMSEmployeeDetails
		SET [InActiveReasonTypesId] = null
		WHERE PersonId = @EmployeeId

		UPDATE emp.UserMaster
		SET [Status] = 1,
		UpdatedBy = @UpdatedBy,
		UpdatedDate = @UpdatedDate
		WHERE PersonId = @EmployeeId

	END
END
