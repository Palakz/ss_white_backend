﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.MpcVoteForEpo
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	:   41 for epo count userid 
						60 for mpc count, 41 for epo
				EXEC [emp].[GetModulesCounts]
					@UserId = 41,
					@ApprovalTypeApproved = 1,
					@StatusTypeActive = 1
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetModulesCounts]
(
	@UserId INT,
	@ApprovalTypeApproved INT,
	@StatusTypeActive INT
)
AS
BEGIN

	SET NOCOUNT ON;

	CREATE TABLE #EpoCreationId(Id INT);
	CREATE TABLE #EpoCreationIdForAllMpc(Id INT);
	DECLARE @MPCMember varchar(100),@MPCNumber varchar(100), @EPOVoteIds varchar(max),@MpcVoteForEpoCount INT, 
			@MpcAllVoteForEpoCount int,@EpoApprovalCount int, @FtoApprovalCount int, 
			@DocumentApprovalCount int,@Department VARCHAR(100)  ;
	SELECT @MPCMember = MPCMember, @MPCNumber = MPCNumber from [emp].[emplimit] WHere UserId = @UserId
	IF (@MPCMember = 'Y')
	BEGIN
		IF(@MPCNumber = 'MPC1') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc1Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC2') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc2Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC3') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc3Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC4') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc4Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC5') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc5Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC6') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc6Vote = 'NO'
		END

		INSERT INTO #EpoCreationIdForAllMpc SELECT EpoCreationId from emp.EpoMpcVote 
					WHERE (Mpc1Vote = 'NO' OR Mpc2Vote = 'NO' OR Mpc3Vote = 'NO' OR Mpc4Vote = 'NO' OR Mpc5Vote = 'NO' OR Mpc6Vote = 'NO')
	END

	------------------mpc count
	SELECT  
		@MpcVoteForEpoCount = COUNT(ec.Id)
	FROM 
		[emp].[EpoCreation] ec
	WHERE
		ec.Id in (select Id from #EpoCreationId) AND Status = 1


	--Epo approval count
	SELECT  
			@EpoApprovalCount = Count(ec.Id)
		FROM [emp].[EpoCreation] ec
	INNER JOIN 
			emp.EpoApprovalList ev ON ev.EpoNo = ec.Id
	INNER JOIN 
		emp.UserMaster um ON um.id = ec.CreatedBy
	INNER JOIN 
		[emp].[VMSEmployeeDetails] vms ON vms.PersonID = um.PersonId
	WHERE
			--ec.CreatedBy IN (select userid from emp.EmpLimit where MPCMember = 'Y')
			 ec.[Status] = 1
			 AND ev.UserId  = @UserId
			 AND ApprovalType != @ApprovalTypeApproved
			AND vms.Department = (SELECT  [Department] FROM emp.[VMSEmployeeDetails] WHERE UserId = @userid)
			
	-----fto approval
	SELECT Top 1
		@Department = department
	FROM 
		emp.VMSEmployeeDetails 
	WHERE 
		UserId = @UserId
		
	SELECT  
		@FtoApprovalCount =count(ef.Id)
	FROM [emp].[AttendanceFto] ef
	
	WHERE
		ef.[Status] = 1
		AND ef.SupervisorUserId =  CASE WHEN @Department = 'HR' THEN ef.SupervisorUserId ELSE @UserId  END
	

	--doc count 
	SELECT  
		@DocumentApprovalCount = COUNT(d.Id)
	FROM [document].[Documents] d
		 INNER JOIN [document].[ApprovalNotification] a ON a.DocumentId = d.Id
	WHERE
		d.[Status] = @StatusTypeActive
		AND a.UserId = @UserId
		AND d.IsLatest = 1

	SELECT	
		@MpcVoteForEpoCount AS MpcVoteForEpoCount,
		@EpoApprovalCount AS EpoApprovalCount,
		@FtoApprovalCount AS FtoApprovalCount,
		@DocumentApprovalCount AS DocumentApprovalCount

	DROP TABLE #EpoCreationId;
	END

