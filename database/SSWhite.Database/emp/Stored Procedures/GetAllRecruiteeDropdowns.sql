﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.GetAllRecruiteeDropdowns
Comments		: 15-06-2023 | Amit | This procedure is used to get Get All Recruitees Dropdown.

Test Execution	: EXEC [emp].[GetAllRecruiteeDropdowns]
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetAllRecruiteeDropdowns]
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		gn.Id,
		gn.[Name]
	FROM
		[master].Genders AS gn
	WHERE
		gn.Status = 1

	SELECT
		gn.Id,
		gn.[Name]
	FROM
		[master].Identities AS gn
	WHERE
		gn.Status = 1

	SELECT
		gn.Id,
		gn.[Name]
	FROM
		[master].MaritalStatuses AS gn
	WHERE
		gn.Status = 1

	SELECT
		gn.Id,
		gn.[Name]
	FROM
		[master].RelationTypes AS gn
	WHERE
		gn.Status = 1

	SELECT
		gn.Id,
		gn.[Name],
		gn.QuestionCategoriesId,
		gn.QuestionTypesId,
		gn.Options,
		gn.Attachment,
		gn.Answer
	FROM
		[master].Questions AS gn
	WHERE
		gn.Status = 1
END