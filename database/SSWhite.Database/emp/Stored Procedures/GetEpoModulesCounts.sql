﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.MpcVoteForEpo
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	:  use 42 & 41 userid 
				EXEC [emp].[GetEpoModulesCounts]
					@UserId = 60
					
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE [emp].[GetEpoModulesCounts]
(
	@UserId INT
)
AS
BEGIN

	SET NOCOUNT ON;

	CREATE TABLE #EpoCreationId(Id INT);
	CREATE TABLE #EpoCreationIdForAllMpc(Id INT);
	DECLARE @MPCMember varchar(100),@MPCNumber varchar(100), @EPOVoteIds varchar(max),@MpcVoteForEpoCount INT, 
			@MpcAllVoteForEpoCount int,@MySubordinateEpoCount int  ;
	SELECT @MPCMember = MPCMember, @MPCNumber = MPCNumber from [emp].[emplimit] WHere UserId = @UserId
	IF (@MPCMember = 'Y')
	BEGIN
		IF(@MPCNumber = 'MPC1') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc1Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC2') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc2Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC3') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc3Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC4') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc4Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC5') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc5Vote = 'NO'
		END
		ELSE IF(@MPCNumber = 'MPC6') 
		BEGIN
			INSERT into #EpoCreationId SELECT EpoCreationId from emp.EpoMpcVote WHERE Mpc6Vote = 'NO'
		END

		INSERT INTO #EpoCreationIdForAllMpc SELECT EpoCreationId from emp.EpoMpcVote 
					WHERE (Mpc1Vote = 'NO' OR Mpc2Vote = 'NO' OR Mpc3Vote = 'NO' OR Mpc4Vote = 'NO' OR Mpc5Vote = 'NO' OR Mpc6Vote = 'NO')
	END

	SELECT  
		@MpcVoteForEpoCount = COUNT(ec.Id)
	FROM 
		[emp].[EpoCreation] ec
	WHERE
		ec.Id in (select Id from #EpoCreationId) AND Status = 1

	SELECT  
		@MpcAllVoteForEpoCount = COUNT(ec.Id)
	FROM 
		[emp].[EpoCreation] ec
	WHERE
		ec.Id in (select Id from #EpoCreationIdForAllMpc) AND Status = 1

	SELECT  
		@MySubordinateEpoCount = COUNT(ec.Id)
	FROM [emp].[EpoCreation] ec
	INNER JOIN 
		emp.UserMaster um ON um.id = ec.CreatedBy
	INNER JOIN 
		[emp].[VMSEmployeeDetails] vms ON vms.PersonID = um.PersonId
	WHERE
			 ec.[Status] = 1
			 AND ec.CreatedBy  != @UserId
			AND vms.Department = (SELECT  [Department] FROM emp.[VMSEmployeeDetails] WHERE UserId = @userid)

	SELECT	
		@MpcVoteForEpoCount AS MpcVoteForEpoCount,
		@MpcAllVoteForEpoCount AS MpcAllVoteForEpoCount,
		@MySubordinateEpoCount AS MySubordinateEpoCount

	DROP TABLE #EpoCreationId,#EpoCreationIdForAllMpc;
	END

