﻿/*------------------------------------------------------------------------------------------------------------
Name			: [emp].[GetAllEmployeeDetailsDropdowns]
Comments		: 04-07-2023 | Amit | This procedure is used to get Get All Employee Dropdowns.

Test Execution	: EXEC [emp].[GetAllEmployeeDetailsDropdowns] 
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetAllEmployeeDetailsDropdowns]
AS
BEGIN
	SET NOCOUNT ON;
-------------------------------------------------------------------------------------------
	SELECT
		gn.Id,
		gn.[Name] AS Gender
	FROM
		[master].Genders AS gn
	WHERE
		gn.Status = 1
-------------------------------------------------------------------------------------------
	SELECT
		ms.Id,
		ms.[Name] AS MaritalStatus
	FROM
		[master].MaritalStatuses AS ms
	WHERE
		ms.Status = 1
-------------------------------------------------------------------------------------------
	SELECT
		dt.Id,
		dt.[Name] AS DocumentType
	FROM
		[emp].DocumentTypes AS dt
-------------------------------------------------------------------------------------------
	SELECT 
		DISTINCT(departmentsupervisorname) AS DepartmentSupervisorName
	FROM 
		emp.Department 
	WHERE 
		mid=1
-------------------------------------------------------------------------------------------
	SELECT 
		vme.[userID],
		vme.[PersonName] AS BuddyName
	FROM 
		[emp].[VMSEmployeeDetails] AS vme
-------------------------------------------------------------------------------------------
	SELECT 
		Id,
		Name AS Organization
	FROM
		[master].[Organization]
	WHERE 
		Status = 1	

-------------------------------------------------------------------------------------------
END

