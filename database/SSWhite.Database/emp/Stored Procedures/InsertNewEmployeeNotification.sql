﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[InsertNewEmployeeNotification]
Comments	: 26-06-2023 | Amit | This procedure is used to Insert New Employee Notification.

Test Execution : EXEC [emp].[InsertNewEmployeeNotification]
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[InsertNewEmployeeNotification]
(
	@Name VARCHAR(80),
	@Education VARCHAR(80),
	@Experience VARCHAR(20),
	@Hobbies VARCHAR(MAX),
	@JoiningDate DATETIME,
	@ImagePath VARCHAR(MAX),
	@IsMailSent BIT,
	@CreatedBy INT NULL
)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		INSERT INTO emp.NewEmployeeNotifications
		(
			Name,
			Education,
			Experience,
			Hobbies,
			JoiningDate,
			ImagePath,
			IsMailSent,
			CreatedBy
		)
		VALUES
		(
			@Name,
			@Education,
			@Experience,
			@Hobbies,
			@JoiningDate,
			@ImagePath,
			@IsMailSent,
			@CreatedBy
		)
	END TRY
	BEGIN CATCH
		DECLARE
		@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
End;
