﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[AddOrEditRecruitee]
Comments	: 15-06-2023 | Amit | This procedure is used to Add New Recruitee.

Test Execution :	
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[AddOrEditRecruitee]
(
	@Id INT,
	@UserId INT,
	@FirstName VARCHAR(50),
	@MiddleName VARCHAR(50),
	@SurName VARCHAR(50),
	@Dob SMALLDATETIME,
	@GenderId INT,
	@MaritalStatusId INT,
	@ImagePath VARCHAR(MAX),
	@DoAnyRelative BIT,
	@RelativeName VARCHAR(80),
	@RelationWithRelative VARCHAR(80),
	@IsDisabilityOrSickness BIT,
	@DisabilityDescription VARCHAR(500),
	@IsWorkExperience BIT,
	@IsFromReference BIT,
	@IdentityProofs VARCHAR(50),
	@IsSameAsCurrentAddress BIT,
	@RecruiteeAddresses [emp].[RecruiteeAddressType] READONLY,
	@RecruiteeEducations [emp].[RecruiteeEducationType] READONLY,
	@RecruiteeFamilyDetails [emp].[RecruiteeFamilyType] READONLY,
	@RecruiteeExperiences [emp].[RecruiteeExperienceType] READONLY,
	@RecruiteeQuestions [emp].[RecruiteeQuestionType] READONLY,
	@RecruiteeReferences [emp].[RecruiteeReferenceType] READONLY,
	@StatusTypeActive SMALLINT,
	@CreatedBy INT,
	@InsertedId INT OUTPUT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
		BEGIN TRAN

		DECLARE @CreatedDate DATETIME = GETDATE();
		IF(@Id IS NULL)
		BEGIN
			INSERT INTO [emp].[Recruitees]
			(
			  [FirstName]
			 ,[MiddleName]
			 ,[SurName]
			 ,[DOB]
			 ,[GendersId]
			 ,[MaritalStatusesId]
			 ,[ImagePath]
			 ,[Status]
			 ,[DoAnyRelative]
			 ,RelativeName
			 ,RelationWithRelative
			 ,IdentityProofs
			 ,[IsDisabilityOrSickness]
			 ,[DisabilityDescription]
			 ,[IsWorkExperience]
			 ,[IsFromReference]
			 ,[CreatedDate]
			 ,[CreatedBy]
			)
			VALUES
			(
				@FirstName,
				@MiddleName,
				@SurName,
				@Dob,
				@GenderId,
				@MaritalStatusId,
				@ImagePath,
				@StatusTypeActive,
				@DoAnyRelative,
				@RelativeName,
				@RelationWithRelative,
				@IdentityProofs,
				@IsDisabilityOrSickness,
				@DisabilityDescription,
				@IsWorkExperience,
				@IsFromReference,
				@CreatedDate,
				@CreatedBy
			)	
		   
			SET @InsertedId = SCOPE_IDENTITY();

			INSERT INTO [emp].[RecruiteeAddresses]
			(
				[RecruiteeId]
			   ,[AddressType]
			   ,[IsSameAsCurrentAddress]
			   ,[AddressLine1]
			   ,[AddressLine2]
			   ,[City]
			   ,[Taluka]
			   ,[District]
			   ,[State]
			   ,[PinCode]
			   ,[MobileNumber1]
			   ,[MobileNumber2]
			   ,[Email]
			)
			SELECT
				@InsertedId,
				rd.AddressType,
				rd.IsSameAsCurrentAddress,
				rd.AddressLine1,
				rd.AddressLine2,
				rd.City,
				rd.Taluka,
				rd.District,
				rd.State,
				rd.Pincode,
				rd.MobileNumber1,
				rd.MobileNumber2,
				rd.Email
			FROM
				@RecruiteeAddresses AS rd
     
			INSERT INTO [emp].[RecruiteeEducations]
			(
				[RecruiteeId]
				,[AcademicYear]
				,[Course]
				,[University]
				,[MainSubject]
				,[Percentage]
			)
			SELECT
				@InsertedId,
				rd.AcademicYear,
				rd.Course,
				rd.University,
				rd.MainSubject,
				rd.Percentage
			FROM
				@RecruiteeEducations AS rd

			INSERT INTO [emp].[RecruiteeExperiences]
			(
				[RecruiteeId]
			   ,[CompanyName]
			   ,[CompanyAddress]
			   ,[Designation]
			   ,[FromDate]
			   ,[ToDate]
			   ,[MonthlySalary]
			)
			SELECT
				@InsertedId,
				re.CompanyName,
				re.CompanyAddress,
				re.Designation,
				re.FromDate,
				re.ToDate,
				re.MonthlySalary
			FROM
				@RecruiteeExperiences AS re


			INSERT INTO [emp].[RecruiteeFamilyDetails]
			(
				 [RecruiteeId]
				,[RelationTypesId]
				,[Name]
				,[Occupation]
				,[OrganizationName]
			)
			SELECT
				@InsertedId,
				rf.RelationTypesId,
				rf.[Name],
				rf.Occupation,
				rf.OrganizationName
			FROM
				@RecruiteeFamilyDetails AS rf

			INSERT INTO [emp].[RecruiteeQuestions]
			(
				[RecruiteeId]
			   ,[QuestionsId]
			   ,[Answer]
			)
			SELECT
				@InsertedId,
				rq.QuestionsId,
				rq.Answer
			FROM
				@RecruiteeQuestions AS rq

			INSERT INTO [emp].[RecruiteeReferences]
			(
				[RecruiteeId]
			   ,[Name]
			   ,[Designation]
			   ,[Address]
			   ,[ContactNumber]
			)
			SELECT
				@InsertedId,
				rr.[Name],
				rr.Designation,
				rr.Address,
				rr.ContactNumber
			FROM
				@RecruiteeReferences AS rr
			END
		
			COMMIT TRAN
---------------------------------------------------UPDATE -------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		DECLARE
		@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
End;
