﻿/*------------------------------------------------------------------------------------------------------------
Procedure	:[emp].[AddOrEditEmployee]
Comments	: 26-04-2022 | Kartik Bariya | This procedure is used to Add New Employee.
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[AddOrEditEmployee]  
(  
 @Id INT,  
 @PersonID nvarchar(50),	 
 @Organization nvarchar(500),  
 @FirstName nvarchar(500),  
 @MiddleName nvarchar(500),
 @LastName nvarchar(500),  
 @Gender nvarchar(50),  
 @Cast nvarchar(20),  
 @MaritalStatusesId INT,
 @DOB date,  
 @DOJ date,  
 @DOI date,  
 @Address1 nvarchar(50),  
 @Address2 nvarchar(50),  
 @BloodGroup nvarchar(50),  
 @EmailAddress nvarchar(500),  
 @EmpPosition nvarchar(500),  
 @ReportingUnder nvarchar(500),  
 @Department nvarchar(500),  
 @Signature nvarchar(200),  
 @Picture nvarchar(500),  
 @Password  nvarchar(150),  
 @CurrentDate datetime,  
 @PhoneNo nvarchar(50),  
 @WorkEmailAddress nvarchar(500),  
 @WorkPhoneNo nvarchar(50),
 @PFNumber nvarchar(30),
 @UANNunber nvarchar(30),
 @BankAccountNumber nvarchar(20),
 @Buddy VARCHAR(50),
 @EmergencyContactName nvarchar(500),
 @EmergencyMobileNumber nvarchar(50),
 @EmergencyPhoneNumber nvarchar(50),
 @EmergencyAddress nvarchar(50),
 @EmergencyEmailAddress nvarchar(500),
 @CreatedBy int,  
 @StatusActive smallint,  
 @Role int,
 --@InActiveReasonTypes [master].[InActiveReasonTypesType] READONLY,
 @Documents [emp].[DocumentsType] READONLY
)  

AS  
BEGIN  
 SET NOCOUNT ON;  
  
   
  DECLARE @IsSignature AS int,  @InsertedId int, @LoginIdTemp VARCHAR(250) ;  
  
  IF(@Signature is NULL)  
   set @IsSignature = 0;  
  ELSE  
   set @IsSignature = 1;  
   
  BEGIN TRY  
  IF(@Id = 0)  
  BEGIN  
  BEGIN TRAN  
  
  INSERT INTO [emp].[UserMaster]  
      ([TenentId]  
      ,[LocationId]  
      ,[CRUPId]  
      ,[FirstName]  
      ,[LastName]  
      ,[LoginId]  
      ,[Password]  
      ,[UserType]  
      ,[Remarks]  
      ,[ActiveFlag]  
      ,[LastLoginDt]  
      --,[UserDetailId]  
      ,[AccLock]  
      ,[FirstTime]  
      ,[PasswordChng]  
      --,[ThemeName]  
      --,[ApprovalDt]  
      --,[VerificationCd]  
      ,[EmailAddress]  
      ,[TillDt]  
      ,[IsSignature]  
      ,[SignatureImage]  
      ,[Avtar]  
      ,[CompId]  
      --,[EmpId]  
      --,[CheckInSwitch]  
      ,[LoginActive]  
      ,[ActiveUser]  
      ,[UserDate]  
      ,[Language1]  
      ,[Language2]  
      ,[Language3]  
      ,[DateofBirth]  
      ,[EmployeePosition]  
      ,[Salary]  
      ,[DateOfJoining]  
      ,[PhoneNo]  
      ,[PinCode]  
      ,[Address1]  
      ,[Address2]  
      ,[Img]  
      ,[PersonId]		 
      --,[LibraryMenu]  
      --,[LibraryReader]  
      --,[LibraryMenuDept  
    ,CreatedBy  
    ,CreatedDate   
    ,Status   
    ,EmpDept  
    ,RoleId  
	,MiddleName
      )  
   VALUES  
      (  
      10  
      ,1  
      ,0  
      ,@FirstName  
      ,@LastName  
      ,CONCAT(trim(@FirstName),'.', trim(@LastName))  
      ,@Password  
      ,3  
      ,'SSWHITE'  
      ,'Y'  
      ,@CurrentDate  
      --,<UserDetailId, int,>  
      ,'Y'  
      ,'Y'  
      ,'SSWHITE'  
      --,<ThemeName, nvarchar(50),>  
      --,<ApprovalDt, datetime,>  
      --,<VerificationCd, nvarchar(40),>  
      ,@EmailAddress  
      ,'2025-12-04 00:00:00.000'  
      ,@IsSignature    
      ,@Signature  
      ,123  
      ,12  
      --,<EmpId, int,>  
      --,<CheckInSwitch, bit,>  
    ,1  
      ,1  
      ,'2025-12-31'  
      ,''  
      ,'0'  
      ,'1'  
      ,@DOB  
      ,@EmpPosition  
      ,''  
      ,@DOJ  
      ,@PhoneNo  
      ,'363030'  
      ,@Address1  
      ,@Address2  
      ,@Picture  
      ,@PersonID		 
      --,<LibraryMenu, nvarchar(50),>  
      --,<LibraryReader, nvarchar(max),>  
      --,<LibraryMenuDept, nvarchar(500),>  
      ,@CreatedBy  
      ,@CurrentDate  
      ,@StatusActive  
      ,@Department  
      ,@Role
	  ,@MiddleName
      )  
    
  SET @InsertedId = SCOPE_IDENTITY();  
  SELECT @LoginIdTemp = LoginId FROM [emp].[UserMaster] WHERE Id = @InsertedId;  
   
  IF Exists(Select 1 FROM [emp].[UserMaster] WHERE LoginId = @LoginIdTemp and id not in (@InsertedId))  
  BEGIN  
  --print @loginIdTemp;  
   UPDATE [emp].[UserMaster]  SET LoginId = CONCAT(trim(LoginId),@InsertedId)   
   WHERE Id = @InsertedId;  
  END  
  
  INSERT INTO [emp].[VMSEmployeeDetails]  
      (
	  [PersonID],		   
      [Organization]  
      ,[PersonName]  
      ,[Gender]  
      ,[EffectiveTime]  
      ,[ExpiryTime]  
      ,[CardNo]  
      ,[RoomNo]  
      ,[FloorNo]  
      ,[Picture]  
      ,[Add1]  
      ,[Add2]  
      ,[DOB]  
      ,[DOJ]  
      ,[DOI]  
      ,[BloodGroup]  
      ,[signature]  
      ,[EmailAddress]  
      ,[EmpPosition]  
      ,[ReportingUnder]  
      ,[ApprovalForDocument]  
      ,[UserId]  
      ,[Department]  
	  ,[WorkPhoneNumber]
	  ,[WorkEmail]
	  ,[PFNumber]
	  ,[UANNumber]
	  ,[BankAccountNumber]
	  ,[Buddy]
	  --,[PreviousDepartment]
	  ,[MiddleName]
	  ,[Cast]
	  ,[MaritalStatusesId]
	  --,[InActiveReasonTypesId]
	  ,[EmergencyContactName]
	  ,[EmergencyMobileNumber]
	  ,[EmergencyPhoneNumber]
	  ,[EmergencyAddress]
	  ,[EmergencyEmail]
      )  
   VALUES  
      (  
      @PersonID,  
      @Organization,  
      CONCAT(trim(@FirstName),' ',trim(@MiddleName),' ',trim(@LastName)),  
      @Gender,  
      '2000-01-01 00:00:00.000',  
      '2037-12-31 23:59:59.000',  
      '',  
      '',  
      '',  
      @Picture,  
      @Address1,  
      @Address2,  
      @DOB,  
      @DOJ,  
      @DOI,  
      @BloodGroup,  
      @signature,  
      @EmailAddress,  
      @EmpPosition,  
      @ReportingUnder,  
      0,  
      @InsertedId,  
      @Department,
	  @WorkPhoneNo,
	  @WorkEmailAddress,
	  @PFNumber,
	  @UANNunber,
	  @BankAccountNumber,
	  @Buddy,
	  @MiddleName,
	  @Cast,
	  @MaritalStatusesId,
	  @EmergencyContactName,
	  @EmergencyMobileNumber,
	  @EmergencyPhoneNumber,
	  @EmergencyAddress,
	  @EmergencyEmailAddress
      )  
  
   INSERT INTO [emp].[EmpLimit]  
      ([PId]  
      ,[EmployeeId]  
      ,[EmpLimitNew]  
      ,[PreviousEmpLimit_no]  
      --,[PreviousEmpLimit1]  
      --,[PreviousEmpLimit2]  
      --,[PreviousEmpLimit3]  
      --,[PreviousEmpLimit4]  
      --,[PreviousEmpLimit5]  
      --,[RevisedDate1]  
      --,[RevisedDate2]  
      --,[RevisedDate3]  
      --,[RevisedDate4]  
      --,[RevisedDate5]  
      ,[MPCMember]  
      --,[MPCNumber]  
      ,[CreatedBy]  
      ,[CreatedDate]  
      --,[UpdatedBy]  
      --,[UpdatedDate]  
      --,[IsDeleted]  
      ,UserId  
      )  
   VALUES  
      (1  
      ,@PersonID	   
      ,0  
      ,1  
      --,<PreviousEmpLimit1, decimal(18,2),>  
      --,<PreviousEmpLimit2, decimal(18,2),>  
      --,<PreviousEmpLimit3, decimal(18,2),>  
      --,<PreviousEmpLimit4, decimal(18,2),>  
      --,<PreviousEmpLimit5, decimal(18,2),>  
      --,<RevisedDate1, datetime,>  
      --,<RevisedDate2, datetime,>  
      --,<RevisedDate3, datetime,>  
      --,<RevisedDate4, datetime,>  
      --,<RevisedDate5, datetime,>  
      ,'N'  
      --,<MPCNumber, nvarchar(50),>  
      ,@CreatedBy  
      ,@CurrentDate  
    ,@InsertedId)  
    
		INSERT INTO [emp].[Documents]
			(
				[Name],
				[UserMasterId],
				[DocumentTypeId],
				[Status],
				[CreatedDate],
				[CreatedBy],
				[DocumentImage]	 
			)
			SELECT
				doc.Name,
				@InsertedId,
				doc.DocumentTypeId,
				 @StatusActive  
      ,@CurrentDate
				,@CreatedBy  
				,doc.DocumentImage		 
			FROM
				@Documents AS doc

  COMMIT TRAN  
  SELECT  
   Loginid,  
   EmailAddress  
  FROM   
   [emp].[UserMaster]  
  WHERE   
   Id = @InsertedId  
  
  END  
  ELSE   
  BEGIN  
  
   BEGIN TRAN  
   UPDATE   
    emp.UserMaster  
   SET   
    PersonId = @PersonId,		   
    FirstName = @FirstName,  
    LastName = @LastName,  
    LoginId = CONCAT(trim(@FirstName),'.', trim(@LastName)),  
    Password = @Password,  
    LastLoginDt = @CurrentDate,  
    EmailAddress = @EmailAddress,  
    SignatureImage = CASE WHEN @signature IS NOT NULL THEN @signature ELSE SignatureImage END,  
    DateofBirth = @DOB,  
    EmployeePosition = @EmpPosition,  
    DateOfJoining = @DOJ,  
    PhoneNo = @PhoneNo,  
    Address1 = @Address1,  
    Address2 = @Address2,  
    Img = CASE WHEN @Picture IS NOT NULL THEN @Picture ELSE Img END,  
    EmpDept = @Department,  
    RoleId = @Role,
	MiddleName = @MiddleName
   WHERE   
    Id = @Id  
  
   UPDATE emp.VMSEmployeeDetails  
   SET  [PersonID] = @PersonId     
        ,[Organization] = @Organization  
        ,[PersonName]=CONCAT(trim(@FirstName),' ',trim(@MiddleName),' ',trim(@LastName))  
        ,[Gender] = @Gender  
      ,Picture =  CASE WHEN @Picture IS NOT NULL THEN @Picture ELSE Picture END  
        ,[Add1] =  @Address1  
        ,[Add2] =  @Address2  
        ,[DOB] = @DOB  
        ,[DOJ] =  @DOJ  
        ,[DOI] = @CurrentDate  
        ,[BloodGroup] = @BloodGroup  
        ,[signature] =  CASE WHEN @signature IS NOT NULL THEN @signature ELSE signature END  
        ,[EmailAddress] = @EmailAddress  
        ,[EmpPosition] = @EmpPosition  
        ,[ReportingUnder] = @ReportingUnder 
		,PreviousDepartment = Department
        ,Department = @Department
		,[WorkPhoneNumber] = @WorkPhoneNo
	  ,[WorkEmail] =  @WorkEmailAddress
	  ,[PFNumber] = @PFNumber
	  ,[UANNumber] = @UANNunber
	  ,[BankAccountNumber] = @BankAccountNumber
	  ,[Buddy] = @Buddy
	  ,[MiddleName] = @MiddleName
	  ,[Cast] = @Cast
	  ,[MaritalStatusesId] = @MaritalStatusesId
	  ,[EmergencyContactName] = @EmergencyContactName
	  ,[EmergencyMobileNumber] = @EmergencyMobileNumber
	  ,[EmergencyPhoneNumber] = @EmergencyPhoneNumber
	  ,[EmergencyAddress] = @EmergencyAddress
	  ,[EmergencyEmail] = @EmergencyEmailAddress
   WHERE userID = @Id  
  
   UPDATE emp.EmpLimit   
   SET EmployeeId = @PersonId  		  
   WHERE UserId = @Id;  

 --  UPDATE [emp].[Documents]  
 --  SET 
	--[Name] = @Documents.Name,
	--[DocumentTypeId] = @Documents.DocumentTypeId,  
 --   [Status] = @StatusActive  
 --   WHERE UserId = @Id
  
   COMMIT TRAN  
  END  
  END TRY  
  BEGIN CATCH  
   Rollback  
   DECLARE  
   @stringmsg VARCHAR(MAX),  
    @ErrorNumber VARCHAR(MAX),  
    @ErrorSeverity VARCHAR(MAX),  
    @ErrorState VARCHAR(MAX),  
    @ErrorLine VARCHAR(MAX),  
    @ErrorMessage VARCHAR(MAX)  
    set  @ErrorNumber =  ERROR_NUMBER();  
    set  @ErrorSeverity= ERROR_SEVERITY();  
    set  @ErrorState =  ERROR_STATE();  
    set  @ErrorMessage =   ERROR_MESSAGE();  
    set  @ErrorLine =  ERROR_LINE();  
     set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;  
  
     THROW 51000,@stringmsg , 1;   
  END CATCH;  
 END  
 
