﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[GetAllRecruitees]
Comments	: 15-06-2023 | Amit | This procedure is used to Add New Recruitee.

Test Execution :	
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetAllRecruitees]
(
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@TotalRecords INT OUT
)
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
		SELECT 
			DISTINCT
			rt.Id
		INTO
			#TempIds
		 FROM
			emp.Recruitees  AS rt
			INNER JOIN [master].Genders AS gn ON rt.GendersId = gn.Id
			INNER JOIN [master].MaritalStatuses AS ms ON rt.MaritalStatusesId = ms.Id
			INNER JOIN [emp].RecruiteeAddresses AS rd ON rt.Id = rd.RecruiteeId
		WHERE
			rt.[Status] = 1
			AND 
			(
					rt.Id LIKE '%' + ISNULL(@SearchKeyword, rt.Id) +'%'
					OR rt.[FirstName] LIKE '%' + ISNULL(@SearchKeyword,rt.[FirstName]) +'%'
					OR rt.SurName  LIKE '%' + ISNULL(@SearchKeyword,rt.SurName) +'%'
					OR rt.MiddleName  LIKE '%' + ISNULL(@SearchKeyword,rt.MiddleName) +'%'
					OR rd.MobileNumber1  LIKE '%' + ISNULL(@SearchKeyword,rd.MobileNumber1) +'%'
			);
		  
		SELECT @TotalRecords = COUNT(Id) FROM #TempIds
		
		SELECT 
			rd.Id,
			rd.[FirstName],
			rd.[MiddleName],
			rd.[SurName],
			rd.[DOB],
			rd.[GendersId],
			rd.[MaritalStatusesId],
			rd.[ImagePath],
			rd.[Status],
			rd.[DoAnyRelative],
			rd.[IsDisabilityOrSickness],
			rd.[DisabilityDescription],
			rd.[IsWorkExperience],
			rd.[IsFromReference],
			rd.[CreatedDate],
			rd.[CreatedBy],
			gn.[Name] AS Gender,
			ms.[Name] AS MaritalStatus
		 FROM
			emp.Recruitees  AS rd
			INNER JOIN [master].Genders AS gn ON rd.GendersId = gn.Id
			INNER JOIN [master].MaritalStatuses AS ms ON rd.MaritalStatusesId = ms.Id
			INNER JOIN #TempIds AS tid ON rd.Id = tid.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN rd.Id END DESC,
			CASE WHEN @SortExpression = 'Id asc' THEN Id END ASC,
			CASE WHEN @SortExpression = 'Id desc' THEN Id END DESC,
			CASE WHEN @SortExpression = 'CreatedDate asc' THEN rd.CreatedDate  END ASC,
			CASE WHEN @SortExpression = 'CreatedDate desc' THEN rd.CreatedDate  END DESC,
			CASE WHEN @SortExpression = 'FirstName asc' THEN FirstName END ASC,
			CASE WHEN @SortExpression = 'FirstName desc' THEN FirstName END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

	DROP TABLE #TempIds;
    END TRY
	BEGIN CATCH
		DECLARE
		@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
End;
