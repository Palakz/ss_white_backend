﻿/*------------------------------------------------------------------------------------------------------------
Name			: emp.GetAllEpos
Comments		: 26-03-2021 | Amit | This procedure is used to get Get All Employee Limit.

Test Execution	: EXEC [emp].[GetUserRights]
					@UserId = 66
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[GetUserRights]
(
	@UserId INT
)
AS
BEGIN
SET NOCOUNT ON;
	DECLARE @UserRoleId INT;

	SELECT 
		@UserRoleId = RoleId 
	FROM 
		emp.Usermaster 
	WHERE 
		Id = @UserId;

	SELECT 
		UniqueCode,
		IsAccess 
	FROM 
		Modules_PagesAccess
	WHERE
		RoleId = @UserRoleId
END
