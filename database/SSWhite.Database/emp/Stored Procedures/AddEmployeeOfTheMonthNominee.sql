﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [emp].[AddEmployeeOfTheMonthNominee]
Comments	: 04-07-2022 | Amit | This procedure is used to Add Employee of the Month Nominee.

Test Execution : EXEC [emp].[AddEmployeeOfTheMonthNominee]
					@Id = NULL,
					@EmployeeId = '82'
					,@EmployeeTypeId= 1
					,@DepartmentId = 2
					,@Supervisor = 'Goel'
					,@AdditionalDetails = 'Aditional details of employee'
					,@NominatedBy  = 18
					,@NominatedDetailsId  = '2022-04-26 13:34:25.903'
					,@CurrentDate  = '2022-04-26 13:34:25.903'
					,@CreatedBy = 18
					,@StatusActive = 1
					
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [emp].[AddEmployeeOfTheMonthNominee]  
(  
 @Id INT NULL,
 @EmployeeId INT, 
 @EmployeeTypeId INT,
 @DepartmentId INT,
 @Supervisor VARCHAR(70),	 
 @AdditionalDetails VARCHAR(500),  
 @NominatedBy INT,  
 @NominatedDate SMALLDATETIME,
 @NominatedDetailsId VARCHAR(500),  
 @CurrentDate SMALLDATETIME,  
 @CreatedBy INT,  
 @StatusActive SMALLINT 
)  

AS  
BEGIN  
 SET NOCOUNT ON;  
 
  IF(@Id IS NULL)  
  BEGIN  
  
  INSERT INTO [emp].EmployeeOfTheMonth  
    (
	    [EmployeeId],
        [EmployeeTypeId],
	    [DepartmentId],
		[Supervisor],
		[AdditionalDetails],
        [NominatedBy],
        [NominatedDate],
        [NominatedDetailsId],
		[IsNotificationSent],
	[IsEmployeeOfTheMonth]
        ,CreatedBy  
	    ,CreatedDate   
        ,Status   
      )  
   VALUES  
      (  
      @EmployeeId,
	  @EmployeeTypeId,
	  @DepartmentId,
	  @Supervisor,
	  @AdditionalDetails,
	  @NominatedBy,
	  CAST(@NominatedDate AS SMALLDATETIME),
	  @NominatedDetailsId
	  ,0
	  ,0
      ,@CreatedBy  
      , CAST(@CurrentDate AS SMALLDATETIME)  
      ,@StatusActive  
      )  
  
  END  
 END  
 
