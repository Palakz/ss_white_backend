﻿CREATE TABLE [emp].[Tax] (
    [Id]           INT             IDENTITY (1, 1) NOT NULL,
    [Rate]         DECIMAL (18, 2) NULL,
    [Status]       TINYINT         NOT NULL,
    [CreatedBy]    INT             NULL,
    [CreatedDate]  DATETIME        NOT NULL,
    [ModifiedBy]   INT             NULL,
    [ModifiedDate] DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

