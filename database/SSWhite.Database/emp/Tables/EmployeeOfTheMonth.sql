﻿CREATE TABLE [emp].[EmployeeOfTheMonth](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[EmployeeTypeId] [int] NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[Supervisor] [varchar](70) NOT NULL,
	[AdditionalDetails] [varchar](500) NULL,
	[NominatedBy] [int] NOT NULL,
	[NominatedDate] [smalldatetime] NOT NULL,
	[NominatedDetailsId] [varchar](500) NULL,
	[IsNotificationSent] [bit] NOT NULL,
	[IsEmployeeOfTheMonth] [bit] NOT NULL,
	[Status] [smallint] NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[UpdatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [emp].[EmployeeOfTheMonth]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeOfTheMonth__EmployeeTypes] FOREIGN KEY([EmployeeTypeId])
REFERENCES [master].[EmployeeTypes] ([Id])
GO

ALTER TABLE [emp].[EmployeeOfTheMonth] CHECK CONSTRAINT [FK__EmployeeOfTheMonth__EmployeeTypes]
GO

ALTER TABLE [emp].[EmployeeOfTheMonth]  WITH CHECK ADD  CONSTRAINT [FK__EmployeeOfTheMonth__UserMaster] FOREIGN KEY([EmployeeId])
REFERENCES [emp].[UserMaster] ([Id])
GO

ALTER TABLE [emp].[EmployeeOfTheMonth] CHECK CONSTRAINT [FK__EmployeeOfTheMonth__UserMaster]
GO