﻿CREATE TABLE [emp].RecruiteeFamilyDetails(
Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
RecruiteeId INT FOREIGN KEY REFERENCES [emp].Recruitees(Id) NOT NULL,
RelationTypesId INT FOREIGN KEY REFERENCES [master].RelationTypes(Id) NOT NULL,
[Name] VARCHAR(80) NOT NULL,
Occupation VARCHAR(80) NULL,
OrganizationName VARCHAR(100) NULL
);