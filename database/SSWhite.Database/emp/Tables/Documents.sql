﻿CREATE TABLE [emp].[Documents](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[UserMasterId] [int] NOT NULL,
	[DocumentTypeId] [int] NOT NULL,
	[Status] [smallint] NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[UpdatedBy] [int] NULL,
	[DocumentImage] [varchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [emp].[Documents]  WITH CHECK ADD FOREIGN KEY([DocumentTypeId])
REFERENCES [emp].[DocumentTypes] ([Id])
GO

ALTER TABLE [emp].[Documents]  WITH CHECK ADD FOREIGN KEY([UserMasterId])
REFERENCES [emp].[UserMaster] ([Id])
GO
