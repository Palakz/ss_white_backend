﻿CREATE TABLE [emp].[VMSEmployeeDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonID] [nvarchar](50) NOT NULL,
	[Organization] [nvarchar](500) NULL,
	[PersonName] [nvarchar](500) NULL,
	[Gender] [nvarchar](50) NULL,
	[EffectiveTime] [datetime] NULL,
	[ExpiryTime] [datetime] NULL,
	[CardNo] [nvarchar](500) NULL,
	[RoomNo] [nvarchar](500) NULL,
	[FloorNo] [nvarchar](500) NULL,
	[Picture] [nvarchar](500) NULL,
	[Add1] [nvarchar](50) NULL,
	[Add2] [nvarchar](50) NULL,
	[DOB] [date] NULL,
	[DOJ] [date] NULL,
	[DOI] [date] NULL,
	[BloodGroup] [nvarchar](50) NULL,
	[signature] [nvarchar](200) NULL,
	[EmailAddress] [nvarchar](500) NULL,
	[EmpPosition] [nvarchar](500) NULL,
	[ReportingUnder] [nvarchar](500) NULL,
	[BdayCards] [nvarchar](500) NULL,
	[ApprovalForDocument] [bit] NULL,
	[userID] [int] NULL,
	[Department] [nvarchar](200) NULL,
	[WorkingStatus] [int] NULL,
	[Cast] [varchar](20) NULL,
	[InActiveReasonTypesId] [int] NULL,
	[EmergencyContactName] [varchar](50) NULL,
	[EmergencyMobileNumber] [varchar](20) NULL,
	[EmergencyPhoneNumber] [varchar](20) NULL,
	[EmergencyAddress] [varchar](50) NULL,
	[EmergencyEmail] [varchar](500) NULL,
	[WorkPhoneNumber] [varchar](20) NULL,
	[WorkEmail] [varchar](500) NULL,
	[PFNumber] [varchar](30) NULL,
	[UANNumber] [varchar](20) NULL,
	[BankAccountNumber] [varchar](20) NULL,
	[Buddy] [varchar](50) NULL,
	[MaritalStatusesId] [int] NULL,
	[PreviousDepartment] [varchar](200) NULL,
	[MiddleName] [varchar](50) NULL,
 CONSTRAINT [PK__VMSEmplo__3214EC07AAFDAB7D] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
 CONSTRAINT [uniquekeyvmsonPersonId] UNIQUE NONCLUSTERED 
(
	[PersonID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [emp].[VMSEmployeeDetails] ADD  CONSTRAINT [vms_WorkingStatus]  DEFAULT ((0)) FOR [WorkingStatus]
GO

ALTER TABLE [emp].[VMSEmployeeDetails]  WITH CHECK ADD FOREIGN KEY([InActiveReasonTypesId])
REFERENCES [master].[InActiveReasonTypes] ([Id])
GO

ALTER TABLE [emp].[VMSEmployeeDetails]  WITH CHECK ADD  CONSTRAINT [FK__VMSEmploy__userI__1FF8A574] FOREIGN KEY([userID])
REFERENCES [emp].[UserMaster] ([Id])
GO

ALTER TABLE [emp].[VMSEmployeeDetails] CHECK CONSTRAINT [FK__VMSEmploy__userI__1FF8A574]
GO
