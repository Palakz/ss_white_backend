﻿CREATE TABLE [emp].[UOM] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [MId]          INT           NOT NULL,
    [UOMName]      NVARCHAR (50) NULL,
    [Status]       TINYINT       NOT NULL,
    [CreatedBy]    INT           NULL,
    [CreatedDate]  DATETIME      NOT NULL,
    [ModifiedBy]   INT           NULL,
    [ModifiedDate] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

