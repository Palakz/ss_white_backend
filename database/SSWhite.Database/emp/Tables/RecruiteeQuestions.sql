﻿CREATE TABLE [emp].RecruiteeQuestions(
Id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
RecruiteeId INT FOREIGN KEY REFERENCES [emp].Recruitees(Id) NOT NULL,
QuestionsId INT FOREIGN KEY REFERENCES [master].Questions(Id) NOT NULL,
Answer VARCHAR(50) NULL,
);