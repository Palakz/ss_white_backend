﻿CREATE TYPE [emp].[RecruiteeExperienceType] AS TABLE (
    CompanyName VARCHAR(60) NOT NULL,
	CompanyAddress VARCHAR(100) NOT NULL,
	Designation VARCHAR(20) NOT NULL,
	FromDate SMALLDATETIME NOT NULL,
	ToDate SMALLDATETIME NULL,
	MonthlySalary DECIMAL(18,2) NOT NULL
);