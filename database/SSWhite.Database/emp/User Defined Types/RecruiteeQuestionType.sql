﻿CREATE TYPE [emp].[RecruiteeQuestionType] AS TABLE (
	QuestionsId INT NOT NULL,
	Answer VARCHAR(50) NULL
);