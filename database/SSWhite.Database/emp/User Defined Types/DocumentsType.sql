﻿CREATE TYPE [emp].[DocumentsType] AS TABLE(
	[Name] [varchar](50) NOT NULL,
	[DocumentTypeId] [tinyint] NOT NULL,
	[DocumentImage] [varchar](1000) NOT NULL
)
GO
