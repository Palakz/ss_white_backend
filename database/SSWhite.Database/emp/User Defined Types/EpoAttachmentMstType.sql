﻿CREATE TYPE [emp].[EpoAttachmentMstType] AS TABLE (
    [AttachedFileName] NVARCHAR (MAX) NULL,
    [Approval]         BIT            NULL);

