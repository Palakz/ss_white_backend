﻿CREATE TYPE [emp].[RecruiteeFamilyType] AS TABLE (
    RelationTypesId INT NULL,
	[Name] VARCHAR(80) NOT NULL,
	Occupation VARCHAR(80) NULL,
	OrganizationName VARCHAR(100) NULL
);