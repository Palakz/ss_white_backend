﻿CREATE TYPE [emp].[RecruiteeEducationType] AS TABLE (
    AcademicYear DATE NOT NULL,
	Course VARCHAR(80) NOT NULL,
	University VARCHAR(80) NOT NULL,
	MainSubject VARCHAR(80) NOT NULL,
	Percentage DECIMAL(18,2) NOT NULL
);