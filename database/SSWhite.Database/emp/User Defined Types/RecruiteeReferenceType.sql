﻿CREATE TYPE [emp].[RecruiteeReferenceType] AS TABLE (
    Name VARCHAR(60) NOT NULL,
	Designation VARCHAR(20) NOT NULL,
	Address VARCHAR(100) NOT NULL,
	ContactNumber VARCHAR(20) NULL
);