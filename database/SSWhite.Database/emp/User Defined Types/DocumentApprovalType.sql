﻿CREATE TYPE [emp].[DocumentApprovalType] AS TABLE (
    [Id]                  INT NOT NULL,
    [Approvalfordocument] BIT NOT NULL);

