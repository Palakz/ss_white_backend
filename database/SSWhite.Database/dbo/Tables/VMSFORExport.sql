﻿CREATE TABLE [dbo].[VMSFORExport] (
    [ID]             INT            NOT NULL,
    [personid]       NVARCHAR (50)  NULL,
    [name]           NVARCHAR (50)  NULL,
    [status]         NVARCHAR (50)  NULL,
    [late]           NVARCHAR (50)  NULL,
    [early]          NVARCHAR (50)  NULL,
    [chckin]         NVARCHAR (50)  NULL,
    [checkout]       NVARCHAR (50)  NULL,
    [hours]          NVARCHAR (50)  NULL,
    [gender]         NVARCHAR (10)  NULL,
    [typeofemployee] NVARCHAR (500) NULL,
    [PID]            NVARCHAR (50)  NULL,
    [date]           DATE           NULL,
    CONSTRAINT [PK_VMSFORExport] PRIMARY KEY CLUSTERED ([ID] ASC)
);

