﻿CREATE TABLE [dbo].[TBLBIN] (
    [TenentID]         INT           NOT NULL,
    [BIN_ID]           INT           NOT NULL,
    [MyComLocID]       INT           NULL,
    [BINDesc1]         VARCHAR (50)  NOT NULL,
    [BINDesc2]         VARCHAR (50)  NULL,
    [BINRemarks]       VARCHAR (200) NULL,
    [BinReqMaintenace] BIT           NULL,
    [ACTIVE]           CHAR (1)      NULL,
    [CRUP_ID]          BIGINT        NULL
);

