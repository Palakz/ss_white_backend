﻿CREATE TABLE [dbo].[who_Is_In_the_Building] (
    [ID]          INT            NOT NULL,
    [PID]         INT            NOT NULL,
    [PersonID]    INT            NOT NULL,
    [datetime]    DATETIME       NULL,
    [date]        DATE           NULL,
    [time]        NVARCHAR (50)  NULL,
    [Status]      NVARCHAR (500) NULL,
    [personname]  NVARCHAR (500) NULL,
    [CID]         INT            NULL,
    [picture_emp] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_who_Is_In_the_Building] PRIMARY KEY CLUSTERED ([ID] ASC, [PID] ASC, [PersonID] ASC)
);

