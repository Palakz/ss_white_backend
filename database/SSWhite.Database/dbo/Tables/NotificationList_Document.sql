﻿CREATE TABLE [dbo].[NotificationList_Document] (
    [ID]                INT            NOT NULL,
    [MID]               INT            NOT NULL,
    [PID]               INT            NOT NULL,
    [document_author]   NVARCHAR (500) NULL,
    [document_manager]  NVARCHAR (500) NULL,
    [Emp_Name]          NVARCHAR (MAX) NULL,
    [Emp_ID]            NVARCHAR (50)  NULL,
    [list_type]         NVARCHAR (500) NULL,
    [approval]          NVARCHAR (50)  NULL,
    [approval_date]     DATETIME       NULL,
    [notification_date] DATETIME       NULL,
    CONSTRAINT [PK_NotificationList_Document] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC, [PID] ASC)
);

