﻿CREATE TABLE [dbo].[MODULE_MAP] (
    [TenentID]      INT            NOT NULL,
    [PRIVILEGE_ID]  INT            NOT NULL,
    [LOCATION_ID]   INT            NOT NULL,
    [MODULE_ID]     INT            NOT NULL,
    [MODULE_MAP_ID] INT            NOT NULL,
    [UserID]        INT            NOT NULL,
    [ACTIVE_FLAG]   VARCHAR (1)    NULL,
    [TenantID]      INT            NULL,
    [CRUP_ID]       BIGINT         NULL,
    [MySerial]      INT            NULL,
    [ALL_FLAG]      NVARCHAR (1)   NULL,
    [ADD_FLAG]      NVARCHAR (1)   NULL,
    [MODIFY_FLAG]   NVARCHAR (1)   NULL,
    [DELETE_FLAG]   NVARCHAR (1)   NULL,
    [VIEW_FLAG]     NVARCHAR (1)   NULL,
    [SP1]           INT            NULL,
    [SP2]           INT            NULL,
    [SP3]           INT            NULL,
    [SP4]           INT            NULL,
    [SP5]           INT            NULL,
    [SP1Name]       NVARCHAR (150) NULL,
    [SP2Name]       NVARCHAR (150) NULL,
    [SP3Name]       NVARCHAR (150) NULL,
    [SP4Name]       NVARCHAR (150) NULL,
    [SP5Name]       NVARCHAR (150) NULL,
    [AppVer]        NVARCHAR (50)  NULL,
    [AppVerDate]    DATETIME       NULL,
    [DBVer]         NVARCHAR (50)  NULL,
    [DBVerDate]     DATETIME       NULL,
    CONSTRAINT [PK_ACM_MODULE_MAP] PRIMARY KEY CLUSTERED ([TenentID] ASC, [LOCATION_ID] ASC, [MODULE_ID] ASC, [MODULE_MAP_ID] ASC)
);

