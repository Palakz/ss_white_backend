﻿CREATE TABLE [dbo].[monthly_attendance_data] (
    [personID]   INT            NOT NULL,
    [PersonName] NVARCHAR (500) NULL,
    [day1]       NVARCHAR (50)  NULL,
    [day2]       NVARCHAR (50)  NULL,
    [day3]       NVARCHAR (50)  NULL,
    [day4]       NVARCHAR (50)  NULL,
    [day5]       NVARCHAR (50)  NULL,
    [day6]       NVARCHAR (50)  NULL,
    [day7]       NVARCHAR (50)  NULL,
    [day8]       NVARCHAR (50)  NULL,
    [day9]       NVARCHAR (50)  NULL,
    [day10]      NVARCHAR (50)  NULL,
    [day11]      NVARCHAR (50)  NULL,
    [day12]      NVARCHAR (50)  NULL,
    [day13]      NVARCHAR (50)  NULL,
    [day14]      NVARCHAR (50)  NULL,
    [day15]      NVARCHAR (50)  NULL,
    [day16]      NVARCHAR (50)  NULL,
    [day17]      NVARCHAR (50)  NULL,
    [day18]      NVARCHAR (50)  NULL,
    [day19]      NVARCHAR (50)  NULL,
    [day20]      NVARCHAR (50)  NULL,
    [day21]      NVARCHAR (50)  NULL,
    [day22]      NVARCHAR (50)  NULL,
    [day23]      NVARCHAR (50)  NULL,
    [day24]      NVARCHAR (50)  NULL,
    [day25]      NVARCHAR (50)  NULL,
    [day26]      NVARCHAR (50)  NULL,
    [day27]      NVARCHAR (50)  NULL,
    [day28]      NVARCHAR (50)  NULL,
    [day29]      NVARCHAR (50)  NULL,
    [day30]      NVARCHAR (50)  NULL,
    [day31]      NVARCHAR (50)  NULL,
    [total]      NVARCHAR (50)  NULL,
    CONSTRAINT [PK_monthly_attendance_data] PRIMARY KEY CLUSTERED ([personID] ASC)
);

