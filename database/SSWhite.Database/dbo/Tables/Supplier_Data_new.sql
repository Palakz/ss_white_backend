﻿CREATE TABLE [dbo].[Supplier_Data_new] (
    [ID]         INT            NOT NULL,
    [MID]        INT            NOT NULL,
    [SID]        NVARCHAR (50)  NOT NULL,
    [Supp_Name]  NVARCHAR (MAX) NULL,
    [Supp_Add]   NVARCHAR (MAX) NULL,
    [Supp_mob]   NVARCHAR (50)  NULL,
    [Supp_email] NVARCHAR (500) NULL,
    [Supp_gst]   NVARCHAR (500) NULL,
    CONSTRAINT [PK_Supplier_Data_new] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC, [SID] ASC)
);

