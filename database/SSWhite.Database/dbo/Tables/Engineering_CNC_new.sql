﻿CREATE TABLE [dbo].[Engineering_CNC_new] (
    [ID]                             INT             NOT NULL,
    [PO_Number]                      NVARCHAR (50)   NOT NULL,
    [Part_Number]                    NVARCHAR (25)   NULL,
    [PID]                            INT             NOT NULL,
    [Line_Number]                    INT             NULL,
    [Previously_Ava_Or_New]          NVARCHAR (50)   NULL,
    [PO_Date]                        DATETIME        NULL,
    [PO_Received_Date_Entry]         DATETIME        NULL,
    [Category]                       NVARCHAR (50)   NULL,
    [SubPart]                        NVARCHAR (50)   NULL,
    [Revision_Number]                INT             NULL,
    [SQRM_Code]                      NVARCHAR (50)   NULL,
    [PO_Qty]                         INT             NULL,
    [Part_maximum_DIA_HEIGHT]        NVARCHAR (100)  NULL,
    [Material_Part_Name]             NVARCHAR (100)  NULL,
    [Bar_Plate_Diameter_Dimensions]  NVARCHAR (50)   NULL,
    [Quantity_Parent]                DECIMAL (18, 3) NULL,
    [Unit]                           NVARCHAR (50)   NULL,
    [Special_Tool_Detail]            NVARCHAR (MAX)  NULL,
    [PO_DRAWING_Related_Remarks]     NVARCHAR (MAX)  NULL,
    [Material_Related_Remark]        NVARCHAR (MAX)  NULL,
    [Drawing_Available_Date]         DATETIME        NULL,
    [Drawing_Verify_Date]            DATETIME        NULL,
    [Part_Entry_Date]                DATETIME        NULL,
    [Material_Suggestion_Email_Date] DATETIME        NULL,
    [PTD_Date]                       DATETIME        NULL,
    [OP_Seq_Sheet_complete_Date]     DATETIME        NULL,
    [Mom_Complete_Date]              DATETIME        NULL,
    [SN_Made_out_Source]             NVARCHAR (50)   NULL,
    [PTD_Form_Required]              NVARCHAR (10)   NULL,
    [PTD_Form_Status]                NVARCHAR (50)   NULL,
    [Stage_Drawing_Required]         NVARCHAR (10)   NULL,
    [Stage_Drawing_Available_Date]   DATETIME        NULL,
    [op10]                           NVARCHAR (500)  NULL,
    [op20]                           NVARCHAR (500)  NULL,
    [op30]                           NVARCHAR (500)  NULL,
    [op40]                           NVARCHAR (500)  NULL,
    [op50]                           NVARCHAR (500)  NULL,
    [op60]                           NVARCHAR (500)  NULL,
    [op70]                           NVARCHAR (500)  NULL,
    [op80]                           NVARCHAR (500)  NULL,
    [op90]                           NVARCHAR (500)  NULL,
    [op100]                          NVARCHAR (500)  NULL,
    [op110]                          NVARCHAR (500)  NULL,
    [op120]                          NVARCHAR (500)  NULL,
    [op130]                          NVARCHAR (500)  NULL,
    [op140]                          NVARCHAR (500)  NULL,
    [op150]                          NVARCHAR (500)  NULL,
    [op160]                          NVARCHAR (500)  NULL,
    [op170]                          NVARCHAR (500)  NULL,
    [op180]                          NVARCHAR (500)  NULL,
    [op190]                          NVARCHAR (500)  NULL,
    [op200]                          NVARCHAR (500)  NULL,
    [op210]                          NVARCHAR (500)  NULL,
    [op220]                          NVARCHAR (500)  NULL,
    [Process_Detail]                 NVARCHAR (MAX)  NULL,
    [Process_Remark]                 NVARCHAR (MAX)  NULL,
    [Template]                       NVARCHAR (10)   NULL,
    [SN_AVA_NOTA]                    NVARCHAR (10)   NULL,
    [NJ_AVA_NOTA]                    NVARCHAR (10)   NULL,
    [Mafia_Made]                     NVARCHAR (10)   NULL,
    [Mafia_Released]                 NVARCHAR (10)   NULL,
    [PIR_MAFIA_Status]               NVARCHAR (50)   NULL,
    [COMP_Date_Mafia_Status_Check]   DATETIME        NULL,
    [Mafia_Made_Date]                DATETIME        NULL,
    [Correction_Date]                DATETIME        NULL,
    [Released_Date]                  DATETIME        NULL,
    [Made_By]                        NVARCHAR (500)  NULL,
    [Mafia_Remark]                   NVARCHAR (MAX)  NULL,
    [KPI_Drawing_Verify]             NVARCHAR (50)   NULL,
    [MOM_KPI]                        NVARCHAR (50)   NULL,
    [Material_Suggestion_KPI]        NVARCHAR (50)   NULL,
    [Wrong_Drawing_In_Part_Library]  NVARCHAR (50)   NULL,
    [po_entry_date]                  DATETIME        NULL,
    [partdate]                       DATETIME        NULL,
    [part_final_entry]               INT             NULL,
    [Tool_Category]                  NVARCHAR (50)   NULL,
    [Tool_type]                      NVARCHAR (50)   NULL,
    [mafia_update_by]                NVARCHAR (500)  NULL,
    [mom_Creation_Date]              DATETIME        NULL,
    [Tool_Type_New]                  NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_Engineering_CNC_new] PRIMARY KEY CLUSTERED ([ID] ASC, [PO_Number] ASC, [PID] ASC)
);

