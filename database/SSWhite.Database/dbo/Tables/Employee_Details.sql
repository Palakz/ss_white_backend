﻿CREATE TABLE [dbo].[Employee_Details] (
    [ID]            INT            NOT NULL,
    [employee_Id]   INT            NULL,
    [employee_name] NVARCHAR (500) NULL,
    [mobile_no]     NVARCHAR (50)  NULL,
    [status]        NVARCHAR (10)  NULL,
    CONSTRAINT [PK_Employee_Table] PRIMARY KEY CLUSTERED ([ID] ASC)
);

