﻿CREATE TABLE [dbo].[EPO_MPC_VOTE] (
    [ID]                         INT           NOT NULL,
    [PID]                        INT           NOT NULL,
    [EPO]                        INT           NOT NULL,
    [MPC1_vote]                  NVARCHAR (10) NULL,
    [MPC2_vote]                  NVARCHAR (10) NULL,
    [MPC3_vote]                  NVARCHAR (10) NULL,
    [MPC4_vote]                  NVARCHAR (10) NULL,
    [MPC5_vote]                  NVARCHAR (10) NULL,
    [MPC6_vote]                  NVARCHAR (10) NULL,
    [countvote]                  INT           NULL,
    [status]                     NVARCHAR (50) NULL,
    [need_Higher_Authority]      NVARCHAR (50) NULL,
    [Higher_authority_vote]      NVARCHAR (50) NULL,
    [MPC1_vote_date]             DATETIME      NULL,
    [MPC2_vote_date]             DATETIME      NULL,
    [MPC3_vote_date]             DATETIME      NULL,
    [MPC4_vote_date]             DATETIME      NULL,
    [MPC5_vote_date]             DATETIME      NULL,
    [MPC6_vote_date]             DATETIME      NULL,
    [Higher_authority_vote_date] DATETIME      NULL,
    CONSTRAINT [PK_EPO_MPC_VOTE] PRIMARY KEY CLUSTERED ([ID] ASC, [PID] ASC, [EPO] ASC)
);

