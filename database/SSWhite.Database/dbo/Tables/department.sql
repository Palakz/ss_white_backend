﻿CREATE TABLE [dbo].[department] (
    [id]                       INT            NOT NULL,
    [mid]                      INT            NOT NULL,
    [departmentname]           NVARCHAR (MAX) NULL,
    [departmentsupervisor]     NVARCHAR (MAX) NULL,
    [departmentsupervisorname] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_department] PRIMARY KEY CLUSTERED ([id] ASC, [mid] ASC)
);

