﻿CREATE TABLE [dbo].[backup_logs] (
    [ID]           INT           NOT NULL,
    [PID]          INT           NOT NULL,
    [filename]     NVARCHAR (50) NULL,
    [created_date] DATETIME      NULL,
    CONSTRAINT [PK_backup_logs] PRIMARY KEY CLUSTERED ([ID] ASC, [PID] ASC)
);

