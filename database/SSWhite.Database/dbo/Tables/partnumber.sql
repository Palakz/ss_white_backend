﻿CREATE TABLE [dbo].[partnumber] (
    [id]     INT            NOT NULL,
    [partno] NVARCHAR (500) NULL,
    CONSTRAINT [PK_partnumber] PRIMARY KEY CLUSTERED ([id] ASC)
);

