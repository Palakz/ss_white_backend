﻿CREATE TABLE [dbo].[nonreturnablegatepass] (
    [gatepassno]         NVARCHAR (50)   NOT NULL,
    [id]                 INT             NOT NULL,
    [gid]                INT             NOT NULL,
    [descriptionofgoods] NVARCHAR (MAX)  NULL,
    [qty]                DECIMAL (18, 3) NULL,
    [remarks]            NVARCHAR (MAX)  NULL,
    [authorisedperson]   NVARCHAR (100)  NULL,
    [status]             NVARCHAR (50)   NULL,
    [deliverydate]       DATE            NULL,
    [cancelleddate]      DATE            NULL,
    [reasonfordelete]    NVARCHAR (MAX)  NULL,
    [supplieraddress]    NVARCHAR (500)  NULL,
    CONSTRAINT [PK_nonreturnablegatepass] PRIMARY KEY CLUSTERED ([gatepassno] ASC, [id] ASC, [gid] ASC)
);

