﻿CREATE TABLE [dbo].[gatepasspreviousdaydetail] (
    [pid]              INT            NOT NULL,
    [gpno]             NVARCHAR (50)  NOT NULL,
    [authorizedperson] NVARCHAR (500) NULL,
    [supplierName]     NVARCHAR (500) NULL,
    [Date]             DATETIME       NULL,
    [Type]             NVARCHAR (50)  NULL,
    CONSTRAINT [PK_gatepasspreviousdaydetail] PRIMARY KEY CLUSTERED ([pid] ASC, [gpno] ASC)
);

