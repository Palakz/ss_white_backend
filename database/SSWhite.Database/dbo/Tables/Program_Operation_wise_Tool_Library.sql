﻿CREATE TABLE [dbo].[Program_Operation_wise_Tool_Library] (
    [ID]                INT            NOT NULL,
    [MID]               INT            NOT NULL,
    [PID]               INT            NOT NULL,
    [Part_Name]         NVARCHAR (MAX) NULL,
    [Op_No]             NVARCHAR (50)  NULL,
    [Type_of_Operation] NVARCHAR (500) NULL,
    [Tool_Type]         NVARCHAR (500) NULL,
    [Tool_Name]         NVARCHAR (MAX) NULL,
    [Machine_Name]      NVARCHAR (500) NULL,
    [Program_ID]        INT            NULL,
    [Machine_No]        NVARCHAR (500) NULL,
    CONSTRAINT [PK_Program_Operation_wise_Tool_Library] PRIMARY KEY CLUSTERED ([ID] ASC, [MID] ASC, [PID] ASC)
);

