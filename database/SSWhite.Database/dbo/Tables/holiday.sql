﻿CREATE TABLE [dbo].[holiday] (
    [id]     INT           NOT NULL,
    [day]    NVARCHAR (50) NULL,
    [date]   DATE          NULL,
    [name]   NVARCHAR (50) NULL,
    [active] NVARCHAR (10) NULL,
    CONSTRAINT [PK_holiday] PRIMARY KEY CLUSTERED ([id] ASC)
);

