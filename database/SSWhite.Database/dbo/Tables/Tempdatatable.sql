﻿CREATE TABLE [dbo].[Tempdatatable] (
    [TenentID]   INT             NOT NULL,
    [Type]       INT             NOT NULL,
    [ID]         INT             NOT NULL,
    [UserId]     INT             NULL,
    [Name]       NVARCHAR (MAX)  NULL,
    [Count]      DECIMAL (18, 2) NULL,
    [TotalCount] DECIMAL (18, 2) NULL,
    [percentage] DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_Tempdatatable] PRIMARY KEY CLUSTERED ([TenentID] ASC, [Type] ASC, [ID] ASC)
);

