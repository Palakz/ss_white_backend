﻿CREATE TABLE [dbo].[Issue_Material] (
    [reqno]       NVARCHAR (50)  NOT NULL,
    [id]          INT            NOT NULL,
    [issue_id]    INT            NOT NULL,
    [receiptno]   INT            NOT NULL,
    [mid]         INT            NOT NULL,
    [item_name]   NVARCHAR (MAX) NULL,
    [onhandqty]   INT            NULL,
    [receivedqty] INT            NULL,
    [totalqty]    INT            NULL,
    CONSTRAINT [PK_Issue_Material] PRIMARY KEY CLUSTERED ([reqno] ASC, [id] ASC, [issue_id] ASC, [receiptno] ASC, [mid] ASC)
);

