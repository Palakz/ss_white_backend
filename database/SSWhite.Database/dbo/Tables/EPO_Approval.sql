﻿CREATE TABLE [dbo].[EPO_Approval] (
    [ID]                       INT             NOT NULL,
    [PID]                      INT             NOT NULL,
    [epo]                      INT             NOT NULL,
    [Employee]                 NVARCHAR (MAX)  NULL,
    [Manager]                  NVARCHAR (MAX)  NULL,
    [MPC]                      NVARCHAR (MAX)  NULL,
    [Higher_authority]         NVARCHAR (MAX)  NULL,
    [Employee_Limit]           DECIMAL (18, 2) NULL,
    [Manager_Limit]            DECIMAL (18, 2) NULL,
    [MPC_Limit]                DECIMAL (18, 2) NULL,
    [Self_approval]            NVARCHAR (10)   NULL,
    [Manager_approval]         NVARCHAR (10)   NULL,
    [MPC_approval]             NVARCHAR (10)   NULL,
    [MPC_Higher_approval]      NVARCHAR (10)   NULL,
    [active]                   NVARCHAR (10)   NULL,
    [Employee_approval_Date]   DATETIME        NULL,
    [Manager_approval_date]    DATETIME        NULL,
    [MPC_Approval_date]        DATETIME        NULL,
    [MPC_Higher_approval_date] DATETIME        NULL,
    CONSTRAINT [PK_EPO_Approval] PRIMARY KEY CLUSTERED ([ID] ASC, [PID] ASC, [epo] ASC)
);

