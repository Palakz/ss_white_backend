﻿CREATE TABLE [dbo].[Material_Library] (
    [ID]         INT            NOT NULL,
    [RefID]      INT            NOT NULL,
    [MID]        INT            NOT NULL,
    [Refname]    NVARCHAR (MAX) NULL,
    [SubRefName] NVARCHAR (MAX) NULL,
    [Refname1]   NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Material_Library] PRIMARY KEY CLUSTERED ([ID] ASC, [RefID] ASC, [MID] ASC)
);

