﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [dbo].[GetUserDetailsById]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution : EXEC [dbo].[GetUserDetailsById]
					@Id =43
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetUserDetailsById]
(
	@Id int
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
	Top 1
        FirstName,
        LastName,
        LoginId as UserName,
        UserType,
        [Status],
        EmailAddress,
        Language1,
        DateOfBirth,
        EmployeePosition as department,
        DateOfJoining,
        PhoneNo,
        Pincode,
        Address1,
        Address2,
		a.InOutStatus AS InOutStatus,
		a.PunchDateTime AS PunchTime,
		Img as picture,
		u.PersonId
	FROM
		[emp].[UserMaster] u
	LEFT JOIN emp.VMSDataFromDashboard a on u.Id = a.EmployeeId
	WHERE u.Id = @Id order by a.PunchDateTime  desc 
End;


select * from [emp].[UserMaster] order by 1 desc

select * from  emp.VMSDataFromDashboard 

SELECT 
	Top 1
        FirstName,
        LastName,
        LoginId as UserName,
        UserType,
        [Status],
        EmailAddress,
        Language1,
        DateOfBirth,
        EmployeePosition as department,
        DateOfJoining,
        PhoneNo,
        Pincode,
        Address1,
        Address2,
		a.InOutStatus AS InOutStatus,
		a.PunchDateTime AS PunchTime,
		Img as picture,
		u.PersonId
	FROM
		[emp].[UserMaster] u
	left JOIN emp.VMSDataFromDashboard a on u.Id = a.EmployeeId
	WHERE u.Id = @Id order by a.PunchDateTime  desc 