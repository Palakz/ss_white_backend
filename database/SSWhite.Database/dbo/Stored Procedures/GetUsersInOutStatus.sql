﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: [dbo].[GetUsersInOutStatus]
Comments	: 13-04-2022 | Kartik Bariya | This procedure is used to validate user while login into the application.

Test Execution : EXEC [dbo].[GetUsersInOutStatus]
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[GetUsersInOutStatus]
AS
BEGIN
	SET NOCOUNT ON;
	--select max(id) as ID, MAX(PunchTime) as PunchTime,UserId as UserId into #tempUserAttendance  
	--from UserAttendance  
	--group By UserId  

	--select  ad.UserId,u.UserName,u.Department,u.EmailAddress,u.PhoneNo, ad.PunchTime,ad.AttendanceInOutStatusId,
	--u.picture
	--from #tempUserAttendance tua inner join UserAttendance ad on ad.id = tua.id
	--inner join master.Users u on u.id=ad.UserId
	--drop table #tempUserAttendance
	
select max(id) as ID, MAX(PunchDateTime) as PunchTime,EmployeeId as UserId into #tempUserAttendance  
	from emp.VMSDataFromDashboard  
	group By EmployeeId  

	select  ad.EmployeeId as UserId,u.LoginId as UserName, u.EmpDept asDepartment,u.EmailAddress,u.PhoneNo, ad.PunchDateTime as PunchTime,ad.InOutStatus as AttendanceInOutStatusId,
	u.Img as picture
	from #tempUserAttendance tua inner join emp.VMSDataFromDashboard ad on ad.id = tua.id
	inner join emp.UserMaster u on u.id=ad.EmployeeId
	drop table #tempUserAttendance
End;

