﻿CREATE TABLE [document].[PurchaseDocumentItems] (
    [Id]                 INT             IDENTITY (1, 1) NOT NULL,
    [PurchaseDocumentId] INT             NOT NULL,
    [ItemId]             INT             NOT NULL,
    [ItemCategoryId]     INT             NOT NULL,
    [ItemSubCategoryId]  INT             NULL,
    [SizeId]             INT             NOT NULL,
    [BrandId]            INT             NOT NULL,
    [GradeId]            INT             NOT NULL,
    [TaxId]              INT             NOT NULL,
    [Qty]                DECIMAL (18, 2) NOT NULL,
    [Rate]               DECIMAL (18, 2) NOT NULL,
    [Total]              DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__Purchase__3214EC0723054C82] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__PurchaseD__Brand__5E3FF0B0] FOREIGN KEY ([BrandId]) REFERENCES [master].[Brands] ([Id]),
    CONSTRAINT [FK__PurchaseD__Grade__5F3414E9] FOREIGN KEY ([GradeId]) REFERENCES [master].[Grades] ([Id]),
    CONSTRAINT [FK__PurchaseD__ItemC__5B638405] FOREIGN KEY ([ItemCategoryId]) REFERENCES [master].[ItemCategories] ([Id]),
    CONSTRAINT [FK__PurchaseD__ItemI__5A6F5FCC] FOREIGN KEY ([ItemId]) REFERENCES [master].[Items] ([Id]),
    CONSTRAINT [FK__PurchaseD__ItemS__5C57A83E] FOREIGN KEY ([ItemSubCategoryId]) REFERENCES [master].[ItemSubCategories] ([Id]),
    CONSTRAINT [FK__PurchaseD__Purch__597B3B93] FOREIGN KEY ([PurchaseDocumentId]) REFERENCES [document].[PurchaseDocuments] ([Id]),
    CONSTRAINT [FK__PurchaseD__SizeI__5D4BCC77] FOREIGN KEY ([SizeId]) REFERENCES [master].[Sizes] ([Id]),
    CONSTRAINT [FK__PurchaseD__TaxId__60283922] FOREIGN KEY ([TaxId]) REFERENCES [master].[Taxes] ([Id])
);

