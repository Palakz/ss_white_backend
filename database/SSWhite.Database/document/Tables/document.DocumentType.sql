﻿
CREATE TABLE document.DocumentType
(
 Id INT IDENTITY(1,1) PRIMARY KEY,
 Name varchar(255) NOT NULL,
 Status tinyint NOT NULL,
 CreatedBy  int ,
 CreatedDate  DateTime ,
 ModifiedBy  int,
 ModifiedDate  DateTime
)


select * from  document.DocumentType

insert into document.DocumentType values('ECN',1,41,getdate(),NULL,NULL);
insert into document.DocumentType values('FCD',1,41,getdate(),NULL,NULL);
insert into document.DocumentType values('LIT',1,41,getdate(),NULL,NULL);
insert into document.DocumentType values('SOP',1,41,getdate(),NULL,NULL);
insert into document.DocumentType values('WI',1,41,getdate(),NULL,NULL);