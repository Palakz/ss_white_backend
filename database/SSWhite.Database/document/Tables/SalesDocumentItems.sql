﻿CREATE TABLE [document].[SalesDocumentItems] (
    [Id]              INT             IDENTITY (1, 1) NOT NULL,
    [SalesDocumentId] INT             NOT NULL,
    [ItemId]          INT             NOT NULL,
    [UnitId]          INT             NOT NULL,
    [LotNumber]       INT             NOT NULL,
    [Description]     VARCHAR (150)   NULL,
    [Quantity]        DECIMAL (18, 2) NOT NULL,
    [Weight]          DECIMAL (18, 2) NULL,
    [FreeQuantity]    DECIMAL (18, 2) NULL,
    [Rate]            DECIMAL (18, 2) NULL,
    [Amount]          DECIMAL (18, 2) NULL,
    [GroupId]         INT             NULL,
    CONSTRAINT [PK__SalesDoc__3214EC07D659B912] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__SalesDocu__ItemI__286DEFE4] FOREIGN KEY ([ItemId]) REFERENCES [master].[Items] ([Id]),
    CONSTRAINT [FK__SalesDocu__Sales__2779CBAB] FOREIGN KEY ([SalesDocumentId]) REFERENCES [document].[SalesDocuments] ([Id]),
    CONSTRAINT [FK__SalesDocu__UnitI__2962141D] FOREIGN KEY ([UnitId]) REFERENCES [master].[UQCS] ([Id])
);

