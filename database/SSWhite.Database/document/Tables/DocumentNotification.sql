﻿
CREATE TABLE document.DocumentNotification
(
 Id INT IDENTITY(1,1) PRIMARY KEY,
 DocumentId INT FOREIGN KEY REFERENCES document.Documents(Id),
 Name VARCHAR(255),
 UserId INT,
 PersonId VARCHAR(255),
 Email VARCHAR(255)
)


