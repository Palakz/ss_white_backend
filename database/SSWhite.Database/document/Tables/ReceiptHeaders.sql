﻿CREATE TABLE [document].[ReceiptHeaders] (
    [Id]              INT             IDENTITY (1, 1) NOT NULL,
    [ReceiptId]       INT             NOT NULL,
    [HeaderId]        INT             NOT NULL,
    [CalculationType] TINYINT         NOT NULL,
    [Percentage]      DECIMAL (18, 2) NOT NULL,
    [Value]           DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__ReceiptH__3214EC0771ECC7E9] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__ReceiptHe__Heade__6CE315C2] FOREIGN KEY ([HeaderId]) REFERENCES [master].[Headers] ([Id]),
    CONSTRAINT [FK__ReceiptHe__Recei__6BEEF189] FOREIGN KEY ([ReceiptId]) REFERENCES [document].[Receipts] ([Id])
);

