﻿CREATE TABLE [document].[OrderDetails] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [OrderId]           INT             NOT NULL,
    [ItemId]            INT             NOT NULL,
    [ItemCategoryId]    INT             NOT NULL,
    [ItemSubCategoryId] INT             NULL,
    [SizeId]            INT             NOT NULL,
    [BrandId]           INT             NOT NULL,
    [GradeId]           INT             NOT NULL,
    [Qty]               DECIMAL (18, 2) NOT NULL,
    [Rate]              DECIMAL (18, 2) NOT NULL,
    [Amount]            DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK__OrderDet__3214EC0722BE3CF9] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__OrderDeta__Brand__1B7E091A] FOREIGN KEY ([BrandId]) REFERENCES [master].[Brands] ([Id]),
    CONSTRAINT [FK__OrderDeta__Grade__1C722D53] FOREIGN KEY ([GradeId]) REFERENCES [master].[Grades] ([Id]),
    CONSTRAINT [FK__OrderDeta__ItemC__18A19C6F] FOREIGN KEY ([ItemCategoryId]) REFERENCES [master].[ItemCategories] ([Id]),
    CONSTRAINT [FK__OrderDeta__ItemI__17AD7836] FOREIGN KEY ([ItemId]) REFERENCES [master].[Items] ([Id]),
    CONSTRAINT [FK__OrderDeta__ItemS__1995C0A8] FOREIGN KEY ([ItemSubCategoryId]) REFERENCES [master].[ItemSubCategories] ([Id]),
    CONSTRAINT [FK__OrderDeta__Order__16B953FD] FOREIGN KEY ([OrderId]) REFERENCES [document].[Orders] ([Id]),
    CONSTRAINT [FK__OrderDeta__SizeI__1A89E4E1] FOREIGN KEY ([SizeId]) REFERENCES [master].[Sizes] ([Id])
);

