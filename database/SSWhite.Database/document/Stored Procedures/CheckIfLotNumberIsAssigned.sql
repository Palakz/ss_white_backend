﻿/*------------------------------------------------------------------------------------------------------------
Name			: CheckIfLotNumberIsAssigned
Comments		: 22-04-2021 | Amit Khanna | This procedure is used to check if lot number is assigned to sales document.

Test Execution	:
				DECLARE @ReturnValue BIT;
				EXEC @ReturnValue =  document.CheckIfLotNumberIsAssigned
					@SubscriberId = 1,
					@LotNumber = 13,
					@IpAddress = NULL
					SELECT @ReturnValue;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[CheckIfLotNumberIsAssigned]
(
	@SubscriberId INT,
	@LotNumber INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[CheckIfLotNumberIsAssigned]','
										@SubscriberId =',@SubscriberId,',
										@LotNumber =',@LotNumber,',
										@IpAddress = ','',@IpAddress,''
									  ),
			@ProcedureName = '[document].[CheckIfLotNumberIsAssigned]',
			@ExecutionTime = GETDATE()	
			
    BEGIN TRY
	DECLARE @ReturnValue BIT = 0;

	IF EXISTS(SELECT Id FROM document.AllocatedLotNumbers WHERE SubscriberId = @SubscriberId AND LotNumber = @LotNumber and SalesDocumentId IS NOT NULL)
	BEGIN
		SET @ReturnValue = 1;
	END
	
	EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
	RETURN @ReturnValue;
