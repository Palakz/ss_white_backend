﻿/*------------------------------------------------------------------------------------------------------------
Name			: GetAllOrders
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to get All Orders.

Test Execution	: EXEC document.GetAllOrders
					@SubscriberId = 1,
					@Status = NULL,
					@ClientId = NULL,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL,
					@IpAddress = NULL,
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[GetAllOrders]
(
	@SubscriberId INT,
	@Status TINYINT,
	@ClientId INT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@IpAddress VARCHAR(45),
	@TotalRecords INT OUT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[GetAllOrders]','
										@SubscriberId =',@SubscriberId,',
										@Status = ',@Status,',
										@ClientId = ',@ClientId,',
										@Start = ',@Start,',
										@Length = ',@Length,',
										@SearchKeyword = ','',@SearchKeyword,'',',
										@SortExpression = ','',@SortExpression,'',',
										@IpAddress = ','',@IpAddress,'',',
										@TotalRecords = ',@TotalRecords
									  ),
			@ProcedureName = '[document].[GetAllOrders]',
			@ExecutionTime = GETDATE()	
			
	CREATE TABLE #TempTable(Id INT, ResourcePersonId INT, ReferencePartyId INT, OrderStatusId INT); 
    BEGIN TRY
		
		INSERT INTO #TempTable(Id, OrderStatusId, ResourcePersonId, ReferencePartyId)
		SELECT  
			us.Id,
			us.ResourcePersonId,
			us.ReferencePartyId,
			us.OrderStatusId
		FROM 
			[document].[Orders] AS us
			INNER JOIN [master].Clients AS ct ON us.ClientId = ct.Id
			LEFT JOIN [master].ReferencePersons AS rp ON us.ResourcePersonId = rp.Id
			LEFT JOIN [master].ReferencePersons AS r ON us.ReferencePartyId = r.Id
			INNER JOIN [master].OrderStatuses AS os ON us.OrderStatusId = os.Id
		WHERE 
			us.SubscriberId = @SubscriberId
			AND us.[Status] = ISNULL(@Status,us.[Status])
			AND us.ClientId = ISNULL(@ClientId,us.ClientId)
			AND us.EndDate IS NULL
			AND 
			(
					ct.[CompanyName] LIKE '%' + ISNULL(@SearchKeyword,ct.[CompanyName]) +'%'
			);

		SELECT @TotalRecords =COUNT(Id)  FROM #TempTable 

		SELECT
			cr.Id,
			cr.ClientId,
			us.CompanyName AS ClientName,
			cr.OrderNo,
			cr.[Date],
			cr.ResourcePersonId,
			rp.[Name] AS ResourcePersonName,
			cr.ReferencePartyId,
			r.[Name] AS ReferencePartyName,
			cr.DeliveryDate,
			cr.OrderStatusId,
			os.[Name] AS OrderStatusName,
			cr.TotalAmount,
			cr.ActualOrderNumber,
			us.[Status]
		FROM 
			#TempTable AS tmp
			INNER JOIN [document].[Orders] AS cr ON tmp.Id = cr.Id
			INNER JOIN [master].Clients us ON cr.ClientId = us.Id
			INNER JOIN [master].OrderStatuses AS os ON cr.OrderStatusId = os.Id
			LEFT JOIN [master].ReferencePersons AS rp ON cr.ResourcePersonId = rp.Id
			LEFT JOIN [master].ReferencePersons AS r ON cr.ReferencePartyId = r.Id
		ORDER BY 
			CASE WHEN @SortExpression IS NULL THEN us.[Name] END ASC,
			CASE WHEN @SortExpression = 'companyName asc' THEN us.[CompanyName] END ASC,
			CASE WHEN @SortExpression = 'companyName desc' THEN us.[CompanyName] END DESC
		OFFSET 
			@Start ROWS
		FETCH NEXT 
			@Length ROWS ONLY;

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempTable;
