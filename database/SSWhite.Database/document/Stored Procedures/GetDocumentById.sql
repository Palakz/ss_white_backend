﻿/*------------------------------------------------------------------------------------------------------------
Name			: [document].[GetDocumentById] 
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	--HR userid =68
					[document].[GetDocumentById] 
					@Id = 108,
					@StatusTypeActive = 1,
					@UserId = 42 
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [document].[GetDocumentById] 
(
	@Id INT,
	@StatusTypeActive INT,
	@UserId INT
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @AwaitingApprovalStatus INT;
	SELECT 
		@AwaitingApprovalStatus = Approved
	FROM 
		document.ApprovalNotification
	WHERE
		DocumentId = @Id
		AND Userid = @UserId

	SELECT 
		Id,
		DocumentId,
		DocumentTitle,
		DocumentType,
		Rev,
		EcnNumber,
		EcnTitle,
		EcnDate,
		CustomerApprovalRequired,
		CustomerNotificationRequired,
		PartDescriptionName,
		PartNumber,
		DrawingNumber,
		CustomerPartNumber,
		CurrentRev,
		NewRev,
		DescriptionOfChange,
		ReasonForChange,
		InstructionForEcnImplementation,
		EcnChange,
		Purpose,
		Scope,
		Responsibility,
		Defination,
		Form,
		Notes,
		WorkStation,
		AttachmentApproval,
		Status,
		IsLatest,
		OriginalDocumentId ,
		ApprovalStatus,
		@AwaitingApprovalStatus AS AwaitingApprovalStatus
	FROM 
		document.Documents
	WHERE	
		Id = @Id
		AND Status = @StatusTypeActive
		--AND IsLatest = @StatusTypeActive

	SELECT 
		Id,
		LocalFileName,
		Title,
		AttachmentPath
	FROM 
		document.Attachment
	WHERE
		DocumentId = @Id

	SELECT 
		UserId,
		Name,
		PersonId
	FROM 
		document.DocumentNotification
	WHERE
		DocumentId = @Id

	SELECT 
		UserId,
		Name,
		PersonId,
		ApprovalTime
	FROM 
		document.ApprovalNotification
	WHERE
		DocumentId = @Id 

	SELECT  
		AssociatedDocumentId,
		Rev,
		Title
	FROM 
		document.AssociatedDocument
	WHERE
		DocumentId = @Id 
END