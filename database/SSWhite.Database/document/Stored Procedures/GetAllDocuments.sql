﻿/*------------------------------------------------------------------------------------------------------------
Name			: [document].[GetAllDocuments] 
Comments		: 03-05-2022 | Amit | This procedure is used to get Get All EPO DD values.

Test Execution	:	--HR userid =68
					[document].[GetAllDocuments] 
					@UserId = 42,
					@StatusTypeActive = 1,
					@DocumentType = NULL,
					@DocumentStageStatus = NULL,
					@ApprovalData = NULL,
					@ApprovalTypeApprovedAndReleased = 10,
					@Start =  0,
					@Length = 100,
					@SearchKeyword = NULL,
					@SortExpression = NULL, 
					@TotalRecords = 10
--------------------------------------------------------------------------------------------------------------*/
alter PROCEDURE [document].[GetAllDocuments] 
(
	@UserId INT,
	@StatusTypeActive INT,
	@DocumentType INT,
	@DocumentStageStatus INT,
	@ApprovalData BIT,
	@ApprovalTypeApprovedAndReleased INT,
	@Start INT,
	@Length INT,
	@SearchKeyword VARCHAR(100),
	@SortExpression VARCHAR(50),
	@TotalRecords INT OUT
)
AS
BEGIN
	SET NOCOUNT ON;
	CREATE TABLE #TempTable(Id INT,DocumentId VARCHAR(500),DocumentTitle VARCHAR(500), Rev VARCHAR(500),PersonName varchar(500),
		CreatedDate DATETIME,ApprovalStatus INT, DocumentType INT,CanEdit BIT); 
	
	DECLARE @Department VARCHAR(100);

	SELECT Top 1
		@Department = department
	FROM 
		emp.VMSEmployeeDetails 
	WHERE 
		UserId = @UserId

	IF(@ApprovalData = 1)
	BEGIN
		INSERT INTO #TempTable
		SELECT  
			d.Id,
			d.DocumentId,
			d.DocumentTitle,
			d.Rev,
			vms.PersonName,
			d.CreatedDate,
			d.ApprovalStatus,
			d.DocumentType,
			CanEdit = NULL 
		FROM [document].[Documents] d
		INNER JOIN 
			[emp].[VMSEmployeeDetails] vms ON vms.UserId = d.CreatedBY
		INNER JOIN
			[document].[ApprovalNotification] a ON a.DocumentId = d.Id
		WHERE
			d.[Status] = @StatusTypeActive
			AND a.UserId = @UserId
			AND d.IsLatest = 1
			AND d.DocumentType = ISNULL(@DocumentType,d.DocumentType)
			AND 
				(
						d.DocumentId LIKE '%' + ISNULL(@SearchKeyword, d.DocumentId) +'%'
						OR d.DocumentTitle LIKE '%' + ISNULL(@SearchKeyword, d.DocumentTitle) +'%'
						OR d.Rev  LIKE '%' + ISNULL(@SearchKeyword, d.Rev) +'%'
						OR d.CreatedBy  LIKE '%' + ISNULL(@SearchKeyword, d.CreatedBy) +'%'
						OR d.CreatedDate  LIKE '%' + ISNULL(@SearchKeyword, d.CreatedDate) +'%'
				);
	END
	ELSE
	BEGIN
		INSERT INTO #TempTable
		SELECT  
			d.Id,
			d.DocumentId,
			d.DocumentTitle,
			d.Rev,
			vms.PersonName,
			d.CreatedDate,
			d.ApprovalStatus,
			d.DocumentType,
			CanEdit = CASE WHEN d.CreatedBY = @UserId  THEN 1 ELSE 0 END 
		FROM [document].[Documents] d
		INNER JOIN 
			[emp].[VMSEmployeeDetails] vms ON vms.UserId = d.CreatedBY
		WHERE
			d.[Status] = @StatusTypeActive
			--AND d.IsLatest = 1
			AND d.DocumentType = ISNULL(@DocumentType,d.DocumentType)
			AND ISNULL(d.ApprovalStatus,1) =  CASE WHEN @DocumentStageStatus = 1 THEN @ApprovalTypeApprovedAndReleased ELSE ISNULL(d.ApprovalStatus,1)  END
			AND d.IsLatest =  CASE WHEN @DocumentStageStatus = 2 THEN 0 ELSE d.IsLatest END
			AND d.CreatedBy =  CASE WHEN @DocumentStageStatus = 3 THEN @UserId ELSE d.CreatedBy END
			AND 
				(
						d.DocumentId LIKE '%' + ISNULL(@SearchKeyword, d.DocumentId) +'%'
						OR d.DocumentTitle LIKE '%' + ISNULL(@SearchKeyword, d.DocumentTitle) +'%'
						OR d.Rev  LIKE '%' + ISNULL(@SearchKeyword, d.Rev) +'%'
						OR d.CreatedBy  LIKE '%' + ISNULL(@SearchKeyword, d.CreatedBy) +'%'
						OR d.CreatedDate  LIKE '%' + ISNULL(@SearchKeyword, d.CreatedDate) +'%'
				);
	END
	
	SELECT @TotalRecords = COUNT(Id)  FROM #TempTable 

	SELECT
		Id,
		DocumentId,
		DocumentTitle,
		Rev,
		PersonName,
		CreatedDate,
		ApprovalStatus,
		DocumentType,
		CanEdit
	FROM 
		#TempTable AS tmp
	ORDER BY 
		CASE WHEN @SortExpression IS NULL THEN Id END DESC,
		CASE WHEN @SortExpression = 'Id asc' THEN Id END ASC,
		CASE WHEN @SortExpression = 'Id desc' THEN Id END DESC,
		CASE WHEN @SortExpression = 'DocumentId asc' THEN DocumentId END ASC,
		CASE WHEN @SortExpression = 'DocumentId desc' THEN DocumentId END DESC,
		CASE WHEN @SortExpression = 'DocumentTitle asc' THEN DocumentTitle  END ASC,
		CASE WHEN @SortExpression = 'DocumentTitle desc' THEN DocumentTitle  END DESC,
		CASE WHEN @SortExpression = 'Rev asc' THEN Rev END ASC,
		CASE WHEN @SortExpression = 'Rev desc' THEN Rev END DESC,
		CASE WHEN @SortExpression = 'PersonName asc' THEN PersonName END ASC,
		CASE WHEN @SortExpression = 'PersonName desc' THEN PersonName END DESC,
		CASE WHEN @SortExpression = 'CreatedDate asc' THEN CreatedDate END ASC,
		CASE WHEN @SortExpression = 'CreatedDate desc' THEN CreatedDate END DESC
	OFFSET 
		@Start ROWS
	FETCH NEXT 
		@Length ROWS ONLY;

	DROP TABLE #TempTable;
END