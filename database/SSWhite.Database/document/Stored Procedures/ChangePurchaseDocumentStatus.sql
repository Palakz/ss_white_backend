﻿/*------------------------------------------------------------------------------------------------------------
Name			: ChangePurchaseDocumentStatus
Comments		: 01-07-2021 | Tanvi Pathak | This procedure is used to change purchaseDocument status.

Test Execution	: EXEC document.ChangePurchaseDocumentStatus
					@SubscriberId = 1,
					@Id =  1,
					@UpdatedBy = 1,
					@UpdatedDate = '2021-07-02',
					@IpAddress = NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[ChangePurchaseDocumentStatus]
(
	@SubscriberId INT,
	@Id INT,
	@UpdatedBy INT,
	@UpdatedDate SMALLDATETIME,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[ChangePurchaseDocumentStatus]','
										@SubscriberId =',@SubscriberId,',
										@Id = ',@Id,',
										@UpdateBy = ',@UpdatedBy,',
										@UpdateDate = ',@UpdatedDate,',
										@IpAddress = ','',@IpAddress
									  ),
			@ProcedureName = '[document].[ChangePurchaseDocumentStatus]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY
		UPDATE
			[document].PurchaseDocuments
		SET
			[Status] = CASE WHEN [Status] = 1 THEN 0 ELSE 1 END,
			UpdatedBy = @UpdatedBy,
			UpdatedDate = @UpdatedDate,
			IpAddress = @IpAddress
		WHERE
			SubscriberId = @SubscriberId
			AND Id = @Id;
			
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

