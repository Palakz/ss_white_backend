﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdateOrder
Comments		: 29-06-2021 | Tanvi Pathak | This procedure is used to insert order or update order by Id.

Test Execution	: DECLARE @ReturnValue INT;
					EXEC @ReturnValue =  [document].[InsertOrUpdateOrder]
						 @SubscriberId =1,
						 @Id =NULL,
						 @ClientId = 4,
						 @OrderNo = '5',
						 @Date = '2021-06-30',
						 @ResourcePersonId = 10,
						 @ReferencePartyId = 10,
						 @DeliveryDate = '2021-07-01',
						 @OrderStatusId = 2,
						 @TotalAmount = 200.00, 
						 @IpAddress =NULL,
						 @CreatedBy = 1,
						 @CreatedDate = '2021-06-30',
						 @StatusTypeActive = 1,
						 @ModuleTypeOrder = 9
					SELECT @ReturnValue
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[InsertOrUpdateOrder]
(
	@SubscriberId INT,
	@Id INT,
	@ClientId INT,
	@Orders [document].OrderDetailTableType READONLY,
	@OrderNo VARCHAR(100),
	@Date DATE,
	@ResourcePersonId INT,
	@ReferencePartyId INT,
	@DeliveryDate DATE,
	@OrderStatusId INT,
	@TotalAmount DECIMAL(18,2),
	--@ActualOrderNumber INT,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT,
	@ModuleTypeOrder TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[InsertOrUpdateOrder]','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@ClientId =',@ClientId,',
										@OrderNo = ''',@OrderNo,''',
										@Date = ',@Date,',
										@ResourcePersonId = ',@ResourcePersonId,',
										@ReferencePartyId = ',@ReferencePartyId,',
										@DeliveryDate = ',@DeliveryDate,',
										@OrderStatusId = ',@OrderStatusId,',
										@TotalAmount = ',@TotalAmount,',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive,',
										@ModuleTypeOrder =',@ModuleTypeOrder
									  ),

			@ProcedureName = '[document].[InsertOrUpdateOrder]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@OrderId INT,@OrderNumber INT;

	EXEC @OrderNumber = [subscriber].GetLastNumber
									@SubscriberId = @SubscriberId,
									@EntityId = NULL,
									@FinancialYear = NULL,
									@DocumentType = NULL,
									@ModuleType = @ModuleTypeOrder,
									@Status = @StatusTypeActive,
									@IpAddress = @IpAddress;

	SET @OrderNo = @OrderNumber;

	SELECT
		bdt.ItemId,
		bdt.ItemCategoryId,
		bdt.ItemSubCategoryId,
		bdt.SizeId,
		bdt.BrandId,
		bdt.GradeId,
		bdt.Qty,
		bdt.Rate,
		bdt.Amount
	INTO
		#TempRateDetails
	FROM
		@Orders AS bdt;

    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			BEGIN TRAN
			INSERT INTO [document].Orders
			(
				SubscriberId,
				ClientId,
				OrderNo,
				[Date],
				ResourcePersonId,
				ReferencePartyId,
				DeliveryDate,
				OrderStatusId,
				TotalAmount,
				ActualOrderNumber, 
				[Status],
				IpAddress,
				CreatedBy,
				CreatedDate
			)
			VALUES
			(
				@SubscriberId,
				@ClientId,
				@OrderNo,
				@Date,
				@ResourcePersonId,
				@ReferencePartyId,
				@DeliveryDate,
				@OrderStatusId,
				@TotalAmount,
				@OrderNo, 
				@StatusTypeActive,
				@IpAddress,
				@CreatedBy,
				@CreatedDate
			);
			
			SET @OrderId = SCOPE_IDENTITY();

			INSERT INTO [document].OrderDetails
			(
				OrderId,
				ItemId,
				ItemCategoryId,
				ItemSubCategoryId,
				SizeId,
				BrandId,
				GradeId,
				Qty,
				Rate,
				Amount
			)
			SELECT
				@OrderId,
				bdt.ItemId,
				bdt.ItemCategoryId,
				bdt.ItemSubCategoryId,
				bdt.SizeId,
				bdt.BrandId,
				bdt.GradeId,
				bdt.Qty,
				bdt.Rate,
				bdt.Amount
			FROM
				#TempRateDetails AS bdt;

				UPDATE
				subscriber.NumberConfigurations
				SET
					LastNumber = LastNumber + 1,
					ActualLastNumber = ActualLastNumber + 1,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					SubscriberId = @SubscriberId
					AND ModuleType = @ModuleTypeOrder;
			COMMIT TRAN

		END
		ELSE
		BEGIN
			BEGIN TRAN
			UPDATE
				[document].Orders
			SET
				ClientId = @ClientId,
				[Date] = @Date,
				ResourcePersonId = @ResourcePersonId,
				ReferencePartyId = @ReferencePartyId,
				OrderStatusId = @OrderStatusId,
				DeliveryDate = @DeliveryDate,
				TotalAmount = @TotalAmount,
				IpAddress = @IpAddress,
				UpdatedBy = @CreatedBy,
				UpdatedDate = @CreatedDate
			WHERE
				Id = @Id;

			DELETE FROM 
				[document].OrderDetails
			WHERE
				OrderId = @Id;

			INSERT INTO [document].OrderDetails
			(
				OrderId,
				ItemId,
				ItemCategoryId,
				ItemSubCategoryId,
				SizeId,
				BrandId,
				GradeId,
				Qty,
				Rate,
				Amount
			)
			SELECT
				@Id,
				bdt.ItemId,
				bdt.ItemCategoryId,
				bdt.ItemSubCategoryId,
				bdt.SizeId,
				bdt.BrandId,
				bdt.GradeId,
				bdt.Qty,
				bdt.Rate,
				bdt.Amount
			FROM
				#TempRateDetails AS bdt;

			COMMIT TRAN
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempRateDetails;
	RETURN @ReturnValue;
