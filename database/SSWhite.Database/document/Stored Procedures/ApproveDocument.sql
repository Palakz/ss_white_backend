﻿/*------------------------------------------------------------------------------------------------------------
Procedure	: EXEC  [emp].[GetEpoDetailsByEpoId]
Comments	: 01-05-2022 | Amit | This procedure is used to get Employee Limit details.
USE [db_a5dc4b_sswhite]  
Test Execution :	--hr id  = 68
					DECLARE @Currentdate1 datetime =getdate()
					EXEC [document].[ApproveDocument]
					@UserId = 41,
					@Id = 29,
					@ApprovalType = 10,
					@CurrentDate = @Currentdate1
--------------------------------------------------------------------------------------------------------------*/
ALTER PROCEDURE  [document].[ApproveDocument]
(
	@UserId INT,
	@Id INT,
	@ApprovalType INT,
	@CurrentDate DATETIME
)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	
	DECLARE @ApprovalCount INT;

	UPDATE 
		[document].[ApprovalNotification] 
	Set 
		Approved = @ApprovalType,
		ApprovalTime = @CurrentDate
	WHERE 
		DocumentId = @Id 
		AND UserId = @UserId

	SELECT 
		@ApprovalCount = COUNT(DISTINCT ISNULL(Approved,0)) 
	FROM
		[document].[ApprovalNotification] 
	WHERE 
		DocumentId = @Id;

	IF(@ApprovalCount = 1)
	BEGIN
		UPDATE 
			[document].[Documents] 
		Set 
			ApprovalStatus = (SELECT top 1 Approved FROM [document].[ApprovalNotification]  WHERE DocumentId = @Id)
		WHERE 
			Id = @Id 
	END
	ELSE
	BEGIN
		UPDATE 
			[document].[Documents] 
		Set 
			ApprovalStatus = NULL
		WHERE 
			Id = @Id 
	END

	SELECT 
		[Name] AS ApprovedPerson,
		d.DocumentId,
		DocumentTitle,
		vms.EmailAddress
	FROM
		[document].[ApprovalNotification] af
		INNER JOIN document.Documents d ON af.DocumentId = d.Id 
		INNER JOIN emp.VMSEmployeeDetails vms ON vms.userID = d.CreatedBy
	WHERE 
		af.DocumentId = @Id 
		AND af.UserId = @UserId

	END TRY
	BEGIN CATCH
		DECLARE
		@stringmsg VARCHAR(MAX),
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
			set  @ErrorNumber =  ERROR_NUMBER();
			set  @ErrorSeverity= ERROR_SEVERITY();
			set  @ErrorState =  ERROR_STATE();
			set  @ErrorMessage =   ERROR_MESSAGE();
			set  @ErrorLine =  ERROR_LINE();
			 set @stringmsg = concat(@ErrorMessage, ' : Line no',@ErrorLine) ;

			 THROW 51000,@stringmsg , 1; 
	END CATCH;
END 


