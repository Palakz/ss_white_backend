﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertOrUpdatePurchaseDocument
Comments		: 01-07-2021 | Tanvi Pathak | This procedure is used to insert or update purchase document.

Test Execution	:  DECLARE @ReturnValue INT;
					EXEC @ReturnValue =  [document].[InsertOrUpdatePurchaseDocument]
						 @SubscriberId =1,
						 @Id =NULL,
						 @ClientId = 4,
						 @VoucherNumber = '1',
						 @DocumentNumber = '1',
						 @Date = '2021-07-02',
						 @Remarks = NULL,
						 @GrossTotal = 100.00,
						 @TaxAmount = NULL,
						 @OtherAmount = NULL,
						 @Discount = NULL,
						 @GrandAmount = 100.00,
						 @BillAmount = NULL,
						 @CashAmount = NULL,
						 @IpAddress = NULL,
						 @CreatedBy = 1,
						 @CreatedDate = '2021-07-01',
						 @StatusTypeActive = 1,
						 @ModuleTypePurchase = 4
					SELECT @ReturnValue
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[InsertOrUpdatePurchaseDocument]
(
	@SubscriberId INT,
	@Id INT,
	@ClientId INT,
	@VoucherNumber VARCHAR(100),
	@DocumentNumber VARCHAR(100),
	@Date DATE,
	@Remarks VARCHAR(100),
	@GrossTotal DECIMAL(18,2),
	@TaxAmount DECIMAL(18,2),
	@OtherAmount DECIMAL(18,2),
	@Discount DECIMAL(18,2),
	@GrandAmount DECIMAL(18,2),
	@BillAmount DECIMAL(18,2),
	@CashAmount DECIMAL(18,2),
	@PurchaseDocuments [document].PurchaseDocumentItemType READONLY,
	@IpAddress VARCHAR(45),
	@CreatedBy INT,
	@CreatedDate SMALLDATETIME,
	@StatusTypeActive TINYINT,
	@ModuleTypePurchase TINYINT
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC document.InsertOrUpdatePurchaseDocument','
										@SubscriberId =',@SubscriberId,',
										@Id =',@Id,',
										@ClientId =',@ClientId,',
										@VoucherNumber =''',@VoucherNumber,''',
										@DocumentNumber = ''',@DocumentNumber,''',
										@Date =',@Date,',
										@Remarks =''',@Remarks,''',
										@GrossTotal = ',@GrossTotal,',
									    @TaxAmount = ',@TaxAmount,',
									    @OtherAmount = ',@OtherAmount,',
									    @Discount = ',@Discount,',
									    @GrandAmount = ',@GrandAmount,',
									    @BillAmount = ',@BillAmount,',
									    @CashAmount = ',@CashAmount,',
										@IpAddress = ','',@IpAddress,'','
										@CreatedBy = ',@CreatedBy,',
										@CreatedDate = ',@CreatedDate,',
										@StatusTypeActive =',@StatusTypeActive,',
										@ModuleTypePurchase =',@ModuleTypePurchase
									  ),

			@ProcedureName = 'document.InsertOrUpdatePurchaseDocument',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@PurchaseDocumentId INT,@PurchaseNo INT;

	EXEC @PurchaseNo = [subscriber].GetLastNumber
									@SubscriberId = @SubscriberId,
									@EntityId = NULL,
									@FinancialYear = NULL,
									@DocumentType = NULL,
									@ModuleType = @ModuleTypePurchase,
									@Status = @StatusTypeActive,
									@IpAddress = @IpAddress;

	SET @VoucherNumber = @PurchaseNo;

	SELECT
		bdt.ItemId,
		bdt.ItemCategoryId,
		bdt.ItemSubCategoryId,
		bdt.SizeId,
		bdt.BrandId,
		bdt.GradeId,
		bdt.TaxId,
		bdt.Qty,
		bdt.Rate,
		bdt.Total
	INTO
		#TempRateDetails
	FROM
		@PurchaseDocuments AS bdt;

    BEGIN TRY
	
		IF(@Id IS NULL)
		BEGIN
			BEGIN TRAN
			INSERT INTO [document].PurchaseDocuments
			(
				SubscriberId,
				ClientId,
				VoucherNumber,
				DocumentNumber,
				[Date],
				Remarks,
				GrossTotal,
				TaxAmount,
				OtherAmount,
				Discount,
				GrandAmount,
				BillAmount,
				CashAmount,
				ActualVoucherNumber, 
				[Status],
				IpAddress,
				CreatedBy,
				CreatedDate
			)
			VALUES
			(
				@SubscriberId,
				@ClientId,
				@VoucherNumber,
				@DocumentNumber,
				@Date,
				@Remarks,
				@GrossTotal,
				@TaxAmount,
				@OtherAmount,
				@Discount,
				@GrandAmount,
				@BillAmount,
				@CashAmount,
				@VoucherNumber, 
				@StatusTypeActive,
				@IpAddress,
				@CreatedBy,
				@CreatedDate
			);
			
			SET @PurchaseDocumentId = SCOPE_IDENTITY();

			INSERT INTO [document].PurchaseDocumentItems
			(
				PurchaseDocumentId,
				ItemId,
				ItemCategoryId,
				ItemSubCategoryId,
				SizeId,
				BrandId,
				GradeId,
				TaxId,
				Qty,
				Rate,
				Total
			)
			SELECT
				@PurchaseDocumentId,
				bdt.ItemId,
				bdt.ItemCategoryId,
				bdt.ItemSubCategoryId,
				bdt.SizeId,
				bdt.BrandId,
				bdt.GradeId,
				bdt.TaxId,
				bdt.Qty,
				bdt.Rate,
				bdt.Total
			FROM
				#TempRateDetails AS bdt;

				UPDATE
				subscriber.NumberConfigurations
				SET
					LastNumber = LastNumber + 1,
					ActualLastNumber = ActualLastNumber + 1,
					IpAddress = @IpAddress,
					UpdatedBy = @CreatedBy,
					UpdatedDate = @CreatedDate
				WHERE
					SubscriberId = @SubscriberId
					AND ModuleType = @ModuleTypePurchase;
			COMMIT TRAN
		END
		ELSE
		BEGIN
			BEGIN TRAN
			UPDATE
				[document].PurchaseDocuments
			SET
				ClientId = @ClientId,
				[Date] = @Date,
				Remarks = @Remarks,
				GrossTotal = @GrossTotal,
				TaxAmount = @TaxAmount,
				OtherAmount = @OtherAmount,
				Discount = @Discount,
				GrandAmount = @GrandAmount,
				BillAmount = @BillAmount,
				CashAmount = @CashAmount,
				IpAddress = @IpAddress,
				UpdatedBy = @CreatedBy,
				UpdatedDate = @CreatedDate
			WHERE
				Id = @Id;

			DELETE FROM 
				[document].[PurchaseDocumentItems]
			WHERE
				PurchaseDocumentId = @Id;

			INSERT INTO [document].[PurchaseDocumentItems]
			(
				PurchaseDocumentId,
				ItemId,
				ItemCategoryId,
				ItemSubCategoryId,
				SizeId,
				BrandId,
				GradeId,
				TaxId,
				Qty,
				Rate,
				Total
			)
			SELECT
				@Id,
				bdt.ItemId,
				bdt.ItemCategoryId,
				bdt.ItemSubCategoryId,
				bdt.SizeId,
				bdt.BrandId,
				bdt.GradeId,
				bdt.TaxId,
				bdt.Qty,
				bdt.Rate,
				bdt.Total
			FROM
				#TempRateDetails AS bdt;

			COMMIT TRAN
		END

		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

	DROP TABLE #TempRateDetails;
	RETURN @ReturnValue;
