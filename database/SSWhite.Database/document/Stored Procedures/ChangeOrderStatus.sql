﻿/*------------------------------------------------------------------------------------------------------------
Name			: ChangeOrderStatus
Comments		: 30-06-2021 | Tanvi Pathak | This procedure is used to change order status.

Test Execution	: EXEC document.ChangeOrderStatus
					@SubscriberId = 1,
					@Id =  16,
					@OrderStatusId = 2,
					@UpdatedBy = 1,
					@UpdatedDate = '2021-06-30',
					@IpAddress = NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [document].[ChangeOrderStatus]
(
	@SubscriberId INT,
	@Id INT,
	@OrderStatusId INT,
	@UpdatedBy INT,
	@UpdatedDate SMALLDATETIME,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [document].[ChangeOrderStatus]','
										@SubscriberId =',@SubscriberId,',
										@Id = ',@Id,',
										@OrderStatusId = ',@OrderStatusId,',
										@UpdateBy = ',@UpdatedBy,',
										@UpdateDate = ',@UpdatedDate,',
										@IpAddress = ','',@IpAddress
									  ),
			@ProcedureName = '[master].[document].[ChangeOrderStatus]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY
		UPDATE
			[document].Orders
		SET
			OrderStatusId = @OrderStatusId,
			UpdatedBy = @UpdatedBy,
			UpdatedDate = @UpdatedDate,
			IpAddress = @IpAddress
		WHERE
			SubscriberId = @SubscriberId
			AND Id = @Id;
			
		EXEC [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		EXEC [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;

