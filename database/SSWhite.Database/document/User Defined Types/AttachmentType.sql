﻿CREATE TYPE document.Attachment AS TABLE (
    [Id] INT NULL,
	LocalFileName VARCHAR(1000),
	Title VARCHAR(255),
	AttachmentPath VARCHAR(1000)
    );