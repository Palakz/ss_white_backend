﻿CREATE TYPE [document].[PurchaseDocumentItemType] AS TABLE (
    [ItemId]            INT             NOT NULL,
    [ItemCategoryId]    INT             NOT NULL,
    [ItemSubCategoryId] INT             NULL,
    [SizeId]            INT             NOT NULL,
    [BrandId]           INT             NOT NULL,
    [GradeId]           INT             NOT NULL,
    [TaxId]             INT             NOT NULL,
    [Qty]               DECIMAL (18, 2) NOT NULL,
    [Rate]              DECIMAL (18, 2) NOT NULL,
    [Total]             DECIMAL (18, 2) NOT NULL);

