﻿CREATE TYPE document.AssociatedDocument AS TABLE (
    [Id] INT NULL,
	 AssociatedDocumentId varchar(255),
 Rev VARCHAR(255),
 Title VARCHAR(1000)
    );
