﻿CREATE TABLE [subscriber].[Modules] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [Name]         VARCHAR (50)  NOT NULL,
    [ModuleType]   TINYINT       NULL,
    [Status]       TINYINT       CONSTRAINT [DF__Modules__Status__51BA1E3A] DEFAULT ((1)) NOT NULL,
    [EndDate]      SMALLDATETIME NULL,
    [IpAddress]    VARCHAR (45)  NULL,
    [CreatedBy]    INT           NULL,
    [CreatedDate]  SMALLDATETIME NOT NULL,
    [UpdatedBy]    INT           NULL,
    [UpdatedDate]  SMALLDATETIME NULL,
    CONSTRAINT [PK__Modules__3214EC0790B3D21F] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Modules__Subscri__50C5FA01] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id])
);

