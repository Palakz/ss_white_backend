﻿CREATE TABLE [subscriber].[SubscriberRightsGroup] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId] INT           NOT NULL,
    [RightGroupId] INT           NULL,
    [TrialExpiry]  SMALLDATETIME NULL,
    [Status]       BIT           CONSTRAINT [DF__Subscribe__Statu__673F4B05] DEFAULT ((1)) NULL,
    [CreateDate]   SMALLDATETIME NULL,
    CONSTRAINT [PK__Subscrib__3214EC07B1EB9D2D] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK__Subscribe__Right__664B26CC] FOREIGN KEY ([RightGroupId]) REFERENCES [subscriber].[RightsGroup] ([Id]),
    CONSTRAINT [FK__Subscribe__Subsc__65570293] FOREIGN KEY ([SubscriberId]) REFERENCES [subscriber].[Subscribers] ([Id])
);

