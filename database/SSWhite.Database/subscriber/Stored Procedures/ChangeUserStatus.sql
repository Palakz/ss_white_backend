﻿/*------------------------------------------------------------------------------------------------------------
Name			: ChangeUserStatus
Comments		: 30-03-2021 | Amit Khanna | This procedure is used to change user status.

Test Execution	: EXEC subscriber.ChangeUserStatus
					@SubscriberId = 1,
					@Id =  1,
					@IpAddress = NULL;
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[ChangeUserStatus]
(
	@SubscriberId INT,
	@Id INT,
	@IpAddress VARCHAR(45)
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[ChangeUserStatus]','
										@SubscriberId =',@SubscriberId,',
										@Id = ',@Id,'
										@IpAddress = ','',@IpAddress
									  ),
			@ProcedureName = '[subscriber].[ChangeUserStatus]',
			@ExecutionTime = GETDATE()	
	
    BEGIN TRY
		UPDATE
			subscriber.Users
		SET
			[Status] = CASE WHEN [Status] = 1 THEN 0 ELSE 1 END
		WHERE
			Id = @Id
			AND SubscriberId = @SubscriberId;
			
		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
	END TRY
	BEGIN CATCH
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
	END CATCH;
