﻿/*------------------------------------------------------------------------------------------------------------
Name			: InsertSubscriber
Comments		: 23-03-2021 | Amit Khanna | This procedure is used to insert new subscriber in the application.

Test Execution	: DECLARE @ReturnValue INT = 0;
					EXEC @ReturnValue =  subscriber.InsertSubscriber
						@FirstName = 'Amit',
						@LastName = 'Khanna',
						@EmailAddress = 'amitkhanna2201@gmail.com',
						@SubscriptionType  = 4,
						@SubscriptionStartDate = '2021-03-24',
						@UserName = 'amitkhanna',
						@Password  = '6G94qKPK8LYNjnTllCqm2G3BUM08AzOK7yW30tfjrMc=',
						@UserTypeAdmin = 1,
						@MobileNumber = '9727466371',
						@Address = 'Address',
						@Status  = 1,
						@SubscriptionTypeMonthly = 1,
						@SubscriptionTypeQuaterly = 2,
						@SubscriptionTypeSixMonths = 3,
						@SubscriptionTypeYearly = 4,
						@SubscriptionTypeTrial = 5,
						@IpAddress = NULL,
						@CreatedDate = '2021-03-24'
					SELECT @ReturnValue
--------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE [subscriber].[InsertSubscriber]
(
	@FirstName VARCHAR(100),
	@LastName VARCHAR(100),
	@EmailAddress VARCHAR(100),
	@SubscriptionType TINYINT,
	@SubscriptionStartDate SMALLDATETIME,
	@UserName VARCHAR(100),
	@Password VARCHAR(MAX),
	@UserTypeAdmin TINYINT,
	@MobileNumber VARCHAR(20),
	@Address VARCHAR(200),
	@Status TINYINT,
	@SubscriptionTypeMonthly TINYINT,
	@SubscriptionTypeQuaterly TINYINT,
	@SubscriptionTypeSixMonths TINYINT,
	@SubscriptionTypeYearly TINYINT,
	@SubscriptionTypeTrial TINYINT,
	@IpAddress VARCHAR(45),
	@CreatedDate SMALLDATETIME
)
AS
	SET NOCOUNT ON;
	
	DECLARE @ExecutionCommand VARCHAR(MAX), @ProcedureName  VARCHAR(MAX),@ExecutionTime SMALLDATETIME;
	SELECT	@ExecutionCommand= CONCAT('EXEC [subscriber].[InsertSubscriber]','
										@FirstName =''',@FirstName,''',
										@LastName =''',@LastName,''',
										@EmailAddress =''',@EmailAddress,''',
										@Address = ','',@Address,'','
										@SubscriptionType = ',@SubscriptionType,'
										@SubscriptionStartDate = ',@SubscriptionStartDate,'
										@UserName =''',@UserName,''',
										@Password =''',@Password,''',
										@UserTypeAdmin = ',@UserTypeAdmin,'
										@MobileNumber =''',@MobileNumber,''',
										@Address = ','',@Address,'','
										@Status = ',@Status,'
										@SubscriptionTypeMonthly =',@SubscriptionTypeMonthly,',
										@SubscriptionTypeQuaterly =',@SubscriptionTypeQuaterly,',
										@SubscriptionTypeSixMonths =',@SubscriptionTypeSixMonths,',
										@SubscriptionTypeYearly =',@SubscriptionTypeYearly,',
										@SubscriptionTypeTrial =',@SubscriptionTypeTrial,',
										@IpAddress = ','',@IpAddress,'','
										@CreatedDate = ',@CreatedDate
									  ),
			@ProcedureName = '[subscriber].[InsertSubscriber]',
			@ExecutionTime = GETDATE()	
			
	DECLARE @ReturnValue SMALLINT = 1,@SubscriberId INT =  NULL;
    BEGIN TRY
		BEGIN
			IF EXISTS(SELECT Id FROM subscriber.Users WHERE UserName = @UserName AND EndDate IS NULL)
			BEGIN
				SET @ReturnValue = -1;
			END
			ELSE
			BEGIN
				INSERT INTO subscriber.Subscribers
				(
					[FirstName],
					[LastName],
					[EmailAddress],
					[SubscriptionType],
					[SubscriptionStartDate],
					[SubscriptionEndDate],
					[Status]
				)
				VALUES
				(
					@FirstName,
					@LastName,
					@EmailAddress,
					@SubscriptionType,
					@SubscriptionStartDate,
					CASE WHEN @SubscriptionType = @SubscriptionTypeMonthly THEN DATEADD(MONTH,1,GETDATE())
						WHEN @SubscriptionType = @SubscriptionTypeQuaterly THEN DATEADD(MONTH,3,GETDATE()) 
						WHEN @SubscriptionType = @SubscriptionTypeSixMonths THEN DATEADD(MONTH,6,GETDATE())
						WHEN @SubscriptionType = @SubscriptionTypeYearly THEN DATEADD(YEAR,1,GETDATE())
						WHEN @SubscriptionType = @SubscriptionTypeTrial THEN DATEADD(MONTH,2,GETDATE()) 
						ELSE GETDATE() END,
					@Status
				);

				SET  @SubscriberId =  SCOPE_IDENTITY();

				INSERT INTO [subscriber].[Users]
				(
					[SubscriberId],
					[Type],
					[UserName],
					[Password],
					[FirstName],
					[LastName],
					[EmailAddress],
					[MobileNumber],
					[Address],
					[IsPasswordReset],
					[IsApproved],
					[ApprovedDate],
					[PasswordExpiry],
					[Status],
					[CreatedDate]		
				)
				VALUES
				(
					@SubscriberId,
					@UserTypeAdmin,
					@UserName,
					@Password,
					@FirstName,
					@LastName,
					@EmailAddress,
					@MobileNumber,
					@Address,
					1,
					1,
					GETDATE(),
					DATEADD(MONTH,6,GETDATE()),
					@Status,
					@CreatedDate
				)

			END
		END

		Exec [logging].[LogProcedure] @SubscriberId, @ProcedureName,@ExecutionCommand,@IpAddress, @ExecutionTime;
		RETURN @ReturnValue;
	END TRY
	BEGIN CATCH
		SET @ReturnValue = -2;
		DECLARE 
			@ErrorNumber VARCHAR(MAX),
			@ErrorSeverity VARCHAR(MAX),
			@ErrorState VARCHAR(MAX),
			@ErrorLine VARCHAR(MAX),
			@ErrorMessage VARCHAR(MAX)
		SELECT 
			 @ErrorNumber =  ERROR_NUMBER(),
			 @ErrorSeverity= ERROR_SEVERITY(),
			 @ErrorState =  ERROR_STATE(),
			 @ErrorMessage =   ERROR_MESSAGE(),
			 @ErrorLine =  ERROR_LINE();

		Exec [logging].[LogError] @SubscriberId,@IpAddress, @ErrorNumber ,@ErrorSeverity,@ErrorLine, @ProcedureName,@ExecutionCommand,@ErrorMessage,@ErrorState;
		RETURN @ReturnValue;
	END CATCH;
