﻿/*-------------------------------------------------------------------------------------
ProcedureName : LogProcedure
Comments      : Amit Khanna | 26-02-2021 | This procedure Logs Error Execution
--------------------------------------------------------------------------------------*/
CREATE PROCEDURE [Logging].[LogProcedure]
(
	@SubscriberId INT,
	@ProcedurName VARCHAR(200),
	@ExecutionCommand VARCHAR(MAX),
	@IpAddress VARCHAR(45),
	@ExecutionTime DATETIME
)

AS
	SET NOCOUNT ON;

     BEGIN TRY
         INSERT INTO [logging].[ProcedureLogs]
		(	
			SubscriberId,
			[Name],
			ExecutionCommand,
			IpAddress,
			Stamp
		)
		VALUES
		(
			@SubscriberId,
			@ProcedurName,
			@ExecutionCommand,
			@IpAddress,
			@ExecutionTime
		)

	END TRY
	BEGIN CATCH
		THROW 00002, 'Not able to insert procedure log.',1;
	END CATCH
	; 
