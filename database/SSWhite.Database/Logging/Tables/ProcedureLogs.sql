﻿CREATE TABLE [Logging].[ProcedureLogs] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [SubscriberId]     INT           NULL,
    [Name]             VARCHAR (200) NULL,
    [ExecutionCommand] VARCHAR (MAX) NULL,
    [IpAddress]        VARCHAR (45)  NULL,
    [Stamp]            SMALLDATETIME NULL,
    CONSTRAINT [PK__Procedur__3214EC077D1B0A2D] PRIMARY KEY CLUSTERED ([Id] ASC)
);

