
// const mutations = {
//     SET_USER(state,payload)
//     {
//         state.authToken = payload.authToken;
//     }
// }

export default {
    setAuthToken(state, payload) {
        state.authToken = payload.authToken;

        const date = new Date();
        const authDetails = {
            authToken: payload.authToken,
            expirationInMinutes: new Date(date.getTime() + (payload.expiringInMinutes * 60000)).toString("dd/MM/yyyy hh:mm:ss")
        };

        window.localStorage.setItem("authDetails", JSON.stringify(authDetails));
    },

    deleteAuthToken(state) {
        state.authToken = null
        window.localStorage.removeItem("authDetails");
    }
};