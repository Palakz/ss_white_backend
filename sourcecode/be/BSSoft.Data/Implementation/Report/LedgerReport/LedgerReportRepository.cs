﻿namespace SSWhite.Data.Implementation.Report.LedgerReport
{
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Report.Ledger;
    using SSWhite.Domain.Common.Report.Ledger;
    using SSWhite.Domain.Enums;
    using SSWhite.Domain.Request.Report.LedgerReport;
    using SSWhite.Domain.Response.Report.Ledger;

    public class LedgerReportRepository : ILedgerReportRepository
    {
        private readonly IBaseRepository _baseRepository;

        public LedgerReportRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<ServiceResponse<GetLedgerReportResponse>> GetLedgerReport(ServiceRequest<GetLedgerReport> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("FinancialYear", request.Data.FromDate.ToFinancialYear(), DbType.Int32);
            parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("TransactionTypeCredit", TransactionType.Credit, DbType.Int16);
            parameters.Add("TransactionTypeDebit", TransactionType.Debit, DbType.Int16);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            var response = new ServiceResponse<GetLedgerReportResponse>
            {
                Data = new GetLedgerReportResponse
                {
                    LedgerDetails = (await _baseRepository.QueryAsync<LedgerDetail>(StoredProcedures.DOCUMENT_GET_LEDGER_REPORT, parameters, commandType: CommandType.StoredProcedure)).ToList()
                }
            };

            return response;
        }
    }
}
