﻿namespace SSWhite.Data.Implementation.Master.ItemCategory
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Master.ItemCategory;
    using SSWhite.Domain.Common.Master.ItemCategory;
    using SSWhite.Domain.Request.Master.ItemCategory;
    using SSWhite.Domain.Response.Master.ItemCategory;

    public class ItemCategoryRepository : IItemCategoryRepository
    {
        private readonly IBaseRepository _baseRepository;

        public ItemCategoryRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddEditItemCategory(ServiceRequest<ItemCategory> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_INSERT_OR_UPDATE_ITEM_CATEGORY, parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("ReturnValue");
        }

        public async Task ChangeItemCategoryStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_CHANGE_ITEM_CATEGORY_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteItemCategory(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("StatusTypeDeleted", StatusType.Deleted, DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_DELETE_ITEM_CATEGORY, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllItemCategoriesResponse>> GetAllItemCategories(ServiceSearchRequest<GetAllItemCategories> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllItemCategoriesResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllItemCategoriesResponse>(StoredProcedures.MASTER_GET_ALL_ITEM_CATEGORIES, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetItemCategoryByIdResponse> GetItemCategoryById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);

            return await _baseRepository.QueryFirstAsync<GetItemCategoryByIdResponse>(StoredProcedures.MASTER_GET_ITEM_CATEGORY_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
