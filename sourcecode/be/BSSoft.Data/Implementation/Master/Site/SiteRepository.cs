﻿namespace SSWhite.Data.Implementation.Master.Site
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Master.Site;
    using SSWhite.Domain.Common.Master.Site;
    using SSWhite.Domain.Request.Master.Site;
    using SSWhite.Domain.Response.Master.Site;

    public class SiteRepository : ISiteRepository
    {
        private readonly IBaseRepository _baseRepository;

        public SiteRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddEditSite(ServiceRequest<Site> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("Address", request.Data.Address, DbType.String);
            parameters.Add("CityId", request.Data.CityId, DbType.Int32);
            parameters.Add("StateId", request.Data.StateId, DbType.Int32);
            parameters.Add("FinancialYears", CommonMethods.GetFinancialYearList().ToDataTable(), DbType.Object);
            parameters.Add("DocumentTypes", CommonCalls.GetDocumentTypeList().ToDataTable(), DbType.Object);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);

            await _baseRepository.ExecuteAsync(StoredProcedures.SUBSCRIBER_INSERT_OR_UPDATE_SITE, parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("ReturnValue");
        }

        public async Task ChangeSiteStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.SUBSCRIBER_CHANGE_SITE_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteSite(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("StatusTypeDeleted", StatusType.Deleted, DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.SUBSCRIBER_DELETE_SITE, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllSitesResponse>> GetAllSites(ServiceSearchRequest<GetAllSites> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllSitesResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllSitesResponse>(StoredProcedures.SUBSCRIBER_GET_ALL_SITES, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetSiteByIdResponse> GetSiteById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);

            return await _baseRepository.QueryFirstAsync<GetSiteByIdResponse>(StoredProcedures.SUBSCRIBER_GET_SITE_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
