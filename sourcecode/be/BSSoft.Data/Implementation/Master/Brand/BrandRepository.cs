﻿namespace SSWhite.Data.Implementation.Master.Brand
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Master.Brand;
    using SSWhite.Domain.Common.Master.Brand;
    using SSWhite.Domain.Request.Master.Brand;
    using SSWhite.Domain.Response.Master.Brand;

    public class BrandRepository : IBrandRepository
    {
        private readonly IBaseRepository _baseRepository;

        public BrandRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddBrand(ServiceRequest<AddBrand> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", null, DbType.Int32);
            return await AddEditCommonParams(request.Data, request.Session, parameters);
        }

        public async Task ChangeBrandStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_CHANGE_BRAND_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteBrand(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("StatusTypeDeleted", StatusType.Deleted, DbType.Int16);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_DELETE_BRAND, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllBrandsResponse>> GetAllBrands(ServiceSearchRequest<GetAllBrands> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllBrandsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllBrandsResponse>(StoredProcedures.MASTER_GET_ALL_BRANDS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetBrandByIdResponse> GetBrandById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            return await _baseRepository.QueryFirstAsync<GetBrandByIdResponse>(StoredProcedures.MASTER_GET_BRAND_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<int> EditBrand(ServiceRequest<EditBrand> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            return await AddEditCommonParams(request.Data, request.Session, parameters);
        }

        private async Task<int> AddEditCommonParams(Brand request, SessionDetail sessionDetail, DynamicParameters parameters)
        {
            parameters.Add("SubscriberId", sessionDetail.SubscriberId, DbType.Int32);
            parameters.Add("Name", request.Name, DbType.String);
            parameters.Add("IpAddress", sessionDetail.IpAddress, DbType.String);
            parameters.Add("CreatedBy", sessionDetail.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);

            await _baseRepository.ExecuteAsync(StoredProcedures.MASTER_INSERT_OR_UPDATE_BRAND, parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("ReturnValue");
        }
    }
}
