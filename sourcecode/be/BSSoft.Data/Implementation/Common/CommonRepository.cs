﻿namespace SSWhite.Data.Implementation.Common
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Common;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.HumanResource;

    public class CommonRepository : ICommonRepository
    {
        private readonly IBaseRepository _baseRepository;

        public CommonRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<List<GetOrganizationResponse>> GetOrganization()
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("Status", StatusType.Active, DbType.Int32);
            return (await _baseRepository.QueryAsync<GetOrganizationResponse>(StoredProcedures.MASTER_GET_ORGANIZATION, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetEmployeeJobDescriptionResponse>> GetEmployeeJobDescription()
        {
            return (await _baseRepository.QueryAsync<GetEmployeeJobDescriptionResponse>(StoredProcedures.MASTER_GET_EMPLOYEE_JOB_DESCRIPTION, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetReportingUnderResponse>> GetReportingUnder()
        {
            return (await _baseRepository.QueryAsync<GetReportingUnderResponse>(StoredProcedures.MASTER_GET_REPORTING_UNDER, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<GetAllEmployeeDetailsDropdownsResponse> GetAllEmployeeDetailsDropdowns()
        {
            try
            {
                var getAllEmployeeDetailsDropDownsResponse = new GetAllEmployeeDetailsDropdownsResponse();
                using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.EMP_GET_ALL_EMPLOYEE_DETAILS_DROPDOWNS, commandType: CommandType.StoredProcedure))
                {
                    getAllEmployeeDetailsDropDownsResponse.GenderDropDown = (await multi.ReadAsync<GenderDropDown>()).ToList();
                    getAllEmployeeDetailsDropDownsResponse.MaritalStatusDropDown = (await multi.ReadAsync<MaritalStatusDropDown>()).ToList();
                    getAllEmployeeDetailsDropDownsResponse.DocumentTypesDropDown = (await multi.ReadAsync<DocumentTypesDropDown>()).ToList();
                    getAllEmployeeDetailsDropDownsResponse.DepartmentSupervisorDropDown = (await multi.ReadAsync<DepartmentSupervisorDropDown>()).ToList();
                    getAllEmployeeDetailsDropDownsResponse.BuddyDropDown = (await multi.ReadAsync<BuddyDropDown>()).ToList();
                    getAllEmployeeDetailsDropDownsResponse.OrganizationDropDown = (await multi.ReadAsync<OrganizationDropDown>()).ToList();
                    getAllEmployeeDetailsDropDownsResponse.EmployeeLeaveTypesDropDown = (await multi.ReadAsync<EmployeeLeaveTypesDropDown>()).ToList();
                    getAllEmployeeDetailsDropDownsResponse.DepartmentCoursesDropDown = (await multi.ReadAsync<DepartmentCoursesDropDown>()).ToList();
                }

                return getAllEmployeeDetailsDropDownsResponse;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetDepartmentByDepartmentSupervisorNameResponse>> GetDepartmentByDepartmentSupervisorName(GetDepartmentByDepartmentSupervisorNameRequest getDepartmentByDepartmentSupervisorNameRequest)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("DepartmentSupervisorName", getDepartmentByDepartmentSupervisorNameRequest.DepartmentSupervisorName, DbType.String);

            return (await _baseRepository.QueryAsync<GetDepartmentByDepartmentSupervisorNameResponse>(StoredProcedures.MASTER_GET_DEPARTMENT_BY_DEPARTMENT_SUPERVISOR_NAME, parameteres, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetAllEmployeeTypesResponse>> GetAllEmployeeTypes()
        {
            return (await _baseRepository.QueryAsync<GetAllEmployeeTypesResponse>(StoredProcedures.MASTER_GET_ALL_EMPLOYEE_TYPES, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<List<GetReportingUnderDetailsByNameResponse>> GetReportingUnderDetailsByName(string getReportingUnderDetailsByNameRequest)
        {
            try
            {
                var parameteres = new DynamicParameters();
                parameteres.Add("DepartmentSupervisorName", getReportingUnderDetailsByNameRequest, DbType.String);

                return (await _baseRepository.QueryAsync<GetReportingUnderDetailsByNameResponse>(StoredProcedures.MASTER_GET_REPORTING_UNDER_DETAILS_BY_NAME, parameteres, commandType: CommandType.StoredProcedure)).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
