﻿namespace SSWhite.Data.Implementation.Document.Purchase
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Document.Purchase;
    using SSWhite.Domain.Common.Document;
    using SSWhite.Domain.Common.Document.Purchase;
    using SSWhite.Domain.Enums.Document;
    using SSWhite.Domain.Request.Document.Purchase;
    using SSWhite.Domain.Response.Document.Purchase;

    public class PurchaseRepository : IPurchaseRepository
    {
        private readonly IBaseRepository _baseRepository;

        public PurchaseRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<List<InsertOrUpdatePurchaseDocumentResponse>> AddEditPurchaseDocument(ServiceRequest<PurchaseDocument> request)
        {
            var documentItems = (from item in request.Data.PurchaseDocumentItems
                                 select new
                                 {
                                     item.ItemId,
                                     item.CrateId,
                                     item.UnitId,
                                     item.Description,
                                     item.Quantity,
                                     item.Weight,
                                     item.FreeQuantity,
                                     item.Rate,
                                     item.Amount,
                                     item.LotNumber
                                 }).ToList();

            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("VoucherNumber", request.Data.VoucherNumber, DbType.String);
            parameters.Add("Date", request.Data.Date, DbType.Date);
            parameters.Add("VoucherType", request.Data.VoucherType, DbType.Int16);
            parameters.Add("Month", request.Data.Date?.Month, DbType.Int16);
            parameters.Add("ChallanNumber", request.Data.ChallanNumber, DbType.String);
            parameters.Add("ChallanDate", request.Data.ChallanDate, DbType.Date);
            parameters.Add("VehicleNumber", request.Data.VehicleNumber, DbType.String);
            parameters.Add("MarkNumber", request.Data.MarkNumber, DbType.String);
            parameters.Add("ReceivingPerson", request.Data.ReceivingPerson, DbType.String);
            parameters.Add("TotalAmount", request.Data.TotalAmount, DbType.Decimal);
            parameters.Add("Remarks", request.Data.Remarks, DbType.String);
            parameters.Add("Prefix", request.Data.Prefix, DbType.String);
            parameters.Add("Suffix", request.Data.Suffix, DbType.String);
            parameters.Add("DocumentItems", documentItems.ToDataTable(), DbType.Object);

            if (request.Data.DocumentHeaders?.Any() == true)
            {
                var documentHeaders = (from item in request.Data.DocumentHeaders
                                       select new
                                       {
                                           item.HeaderId,
                                           CalculationType = (int)item.CalculationType,
                                           item.Percentage,
                                           item.Value
                                       }).ToList();
                parameters.Add("DocumentHeaders", documentHeaders.ToDataTable(), DbType.Object);
            }

            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ModuleTypeLotNumber", ModuleType.LotNumber, DbType.Int16);
            parameters.Add("DocumentTypePurchase", DocumentType.Purchase, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.Output);

            var list = (await _baseRepository.QueryAsync<InsertOrUpdatePurchaseDocumentResponse>(StoredProcedures.DOCUMENT_INSERT_OR_UPDATE_PURCHASE_DOCUMENT, parameters, commandType: CommandType.StoredProcedure)).ToList();

            list = list.Any() ? list : new List<InsertOrUpdatePurchaseDocumentResponse>() { new InsertOrUpdatePurchaseDocumentResponse() { ReturnValue = parameters.Get<int>("ReturnValue") } };

            return list;
        }

        public async Task ChangePurchaseDocumentStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DOCUMENT_CHANGE_PURCHASE_DOCUMENT_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<int> DeletePurchaseDocument(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);

            await _baseRepository.ExecuteAsync(StoredProcedures.DOCUMENT_DELETE_PURCHASE_DOCUMENT, parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("ReturnValue");
        }

        public async Task<ServiceSearchResponse<GetAllPurchaseDocumentsResponse>> GetAllPendingRatePurchaseDocuments(ServiceSearchRequest<GetAllPurchaseDocuments> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("VoucherType", request.Data.VoucherType, DbType.Int16);
            parameters.Add("Month", request.Data.Month, DbType.Int16);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllPurchaseDocumentsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllPurchaseDocumentsResponse>(StoredProcedures.DOCUMENT_GET_ALL_PENDING_PURCHASE_DOCUMENTS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllPurchaseDocumentsResponse>> GetAllPurchaseDocuments(ServiceSearchRequest<GetAllPurchaseDocuments> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("VoucherType", request.Data.VoucherType, DbType.Int16);
            parameters.Add("Month", request.Data.Month, DbType.Int16);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllPurchaseDocumentsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllPurchaseDocumentsResponse>(StoredProcedures.DOCUMENT_GET_ALL_PURCHASE_DOCUMENTS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetPurchaseDocumentByIdResponse> GetPurchaseDocumentById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);
            var response = new GetPurchaseDocumentByIdResponse();
            var documentItems = new List<PurchaseDocumentItem>();
            var documentHeaders = new List<DocumentHeader>();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.DOCUMENT_GET_PURCHASE_DOCUMENT_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstAsync<GetPurchaseDocumentByIdResponse>();
                documentItems = (await multi.ReadAsync<PurchaseDocumentItem>()).ToList();
                documentHeaders = (await multi.ReadAsync<DocumentHeader>()).ToList();
            }

            if (response != null)
            {
                response.PurchaseDocumentItems = documentItems;
                response.DocumentHeaders = documentHeaders;
            }

            return response;
        }
    }
}
