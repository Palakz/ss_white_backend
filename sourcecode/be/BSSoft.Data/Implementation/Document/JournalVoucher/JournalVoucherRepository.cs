﻿namespace SSWhite.Data.Implementation.Document.JournalVoucher
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Document.JournalVoucher;
    using SSWhite.Domain.Common.Document.JournalVoucher;
    using SSWhite.Domain.Enums.Document;
    using SSWhite.Domain.Request.Document.JournalVoucher;
    using SSWhite.Domain.Response.Document.JournalVoucher;

    public class JournalVoucherRepository : IJournalVoucherRepository
    {
        private readonly IBaseRepository _baseRepository;

        public JournalVoucherRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddEditJournalVoucher(ServiceRequest<JournalVoucher> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("CreditedClientId", request.Data.CreditedClientId, DbType.Int32);
            parameters.Add("DebitedClientId", request.Data.DebitedClientId, DbType.Int32);
            parameters.Add("VoucherNumber", request.Data.VoucherNumber, DbType.String);
            parameters.Add("Date", request.Data.Date, DbType.Date);
            parameters.Add("TotalAmount", request.Data.TotalAmount, DbType.Decimal);
            parameters.Add("CreditedReferenceNumber", request.Data.CreditedReferenceNumber, DbType.String);
            parameters.Add("CreditedReferenceDate", request.Data.CreditedReferenceDate, DbType.Date);
            parameters.Add("DebitedReferenceNumber", request.Data.DebitedReferenceNumber, DbType.String);
            parameters.Add("DebitedReferenceDate", request.Data.DebitedReferenceDate, DbType.Date);
            parameters.Add("Month", request.Data.Date?.Month, DbType.Int16);
            parameters.Add("Remarks", request.Data.Remarks, DbType.String);
            parameters.Add("Prefix", request.Data.Prefix, DbType.String);
            parameters.Add("Suffix", request.Data.Suffix, DbType.String);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("DocumentTypeJournalVoucher", DocumentType.JournalVoucher, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);

            await _baseRepository.ExecuteAsync(StoredProcedures.DOCUMENT_INSERT_OR_UPDATE_JOURNAL_VOUCHER, parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("ReturnValue");
        }

        public async Task ChangeJournalVoucherStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DOCUMENT_CHANGE_JOURNAL_VOUCHER_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteJournalVoucher(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DOCUMENT_DELETE_JOURNAL_VOUCHER, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllJournalVouchersResponse>> GetAllJournalVouchers(ServiceSearchRequest<GetAllJournalVouchers> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("Month", request.Data.Month, DbType.Int16);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllJournalVouchersResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllJournalVouchersResponse>(StoredProcedures.DOCUMENT_GET_ALL_JOURNAL_VOUCHERS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetJournalVoucherByIdResponse> GetJournalVoucherById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);

            return await _baseRepository.QueryFirstOrDefaultAsync<GetJournalVoucherByIdResponse>(StoredProcedures.DOCUMENT_GET_JOURNAL_VOUCHER_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
