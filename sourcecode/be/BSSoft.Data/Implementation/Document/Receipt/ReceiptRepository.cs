﻿namespace SSWhite.Data.Implementation.Document.Receipt
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.Document.Receipt;
    using SSWhite.Domain.Common.Document;
    using SSWhite.Domain.Common.Document.Receipt;
    using SSWhite.Domain.Enums.Document;
    using SSWhite.Domain.Request.Document.Receipt;
    using SSWhite.Domain.Response.Document.Receipt;

    public class ReceiptRepository : IReceiptRepository
    {
        private readonly IBaseRepository _baseRepository;

        public ReceiptRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<int> AddEditReceipt(ServiceRequest<Receipt> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("Id", request.Data.Id, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("DayBookId", request.Data.DayBookId, DbType.Int32);
            parameters.Add("ReceiptNumber", request.Data.ReceiptNumber, DbType.String);
            parameters.Add("Date", request.Data.Date, DbType.Date);
            parameters.Add("ReceiptType", request.Data.ReceiptType, DbType.Int16);
            parameters.Add("Month", request.Data.Date?.Month, DbType.Int16);
            parameters.Add("ReceivedAmount", request.Data.ReceivedAmount, DbType.Decimal);
            parameters.Add("TotalAmount", request.Data.TotalAmount, DbType.Decimal);
            parameters.Add("BankId", request.Data.BankId, DbType.Int32);
            parameters.Add("ChequeNumber", request.Data.ChequeNumber, DbType.String);
            parameters.Add("Branch", request.Data.Branch, DbType.String);
            parameters.Add("IsReturnedCheque", request.Data.IsReturnedCheque, DbType.Boolean);
            parameters.Add("ChequeReturnedDate", request.Data.ChequeReturnedDate, DbType.DateTime);
            parameters.Add("Remarks", request.Data.Remarks, DbType.String);
            parameters.Add("Prefix", request.Data.Prefix, DbType.String);
            parameters.Add("Suffix", request.Data.Suffix, DbType.String);

            if (request.Data.ReceiptHeaders?.Any() == true)
            {
                var documentHeaders = (from item in request.Data.ReceiptHeaders
                                       select new
                                       {
                                           item.HeaderId,
                                           CalculationType = (int)item.CalculationType,
                                           item.Percentage,
                                           item.Value
                                       }).ToList();
                parameters.Add("ReceiptHeaders", documentHeaders.ToDataTable(), DbType.Object);
            }

            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
            parameters.Add("ReturnValue", DbType.Int16, direction: ParameterDirection.ReturnValue);

            await _baseRepository.ExecuteAsync(StoredProcedures.DOCUMENT_INSERT_OR_UPDATE_RECEIPT, parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("ReturnValue");
        }

        public async Task ChangeReceiptStatus(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.Date);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DOCUMENT_CHANGE_RECEIPT_STATUS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task DeleteReceipt(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("Id", request.Data, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);

            await _baseRepository.ExecuteAsync(StoredProcedures.DOCUMENT_DELETE_RECEIPT, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetAllReceiptsResponse>> GetAllReceipts(ServiceSearchRequest<GetAllReceipts> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("CompanyId", request.Session.CurrentEntityId, DbType.Int32);
            parameters.Add("ClientId", request.Data.ClientId, DbType.Int32);
            parameters.Add("ReceiptType", request.Data.ReceiptType, DbType.Int16);
            parameters.Add("Month", request.Data.Month, DbType.Int16);
            parameters.Add("FinancialYear", request.Session.CurrentFinancialYear, DbType.Int32);
            parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("Status", request.Data.Status, DbType.Int16);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("IpAddress", request.IpAddress, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);

            var response = new ServiceSearchResponse<GetAllReceiptsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllReceiptsResponse>(StoredProcedures.DOCUMENT_GET_ALL_RECEIPTS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };

            return response;
        }

        public async Task<GetReceiptByIdResponse> GetReceiptById(ServiceRequest<int> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@SubscriberId", request.Session.SubscriberId, DbType.Int32);
            parameters.Add("@Id", request.Data, DbType.Int32);
            parameters.Add("@IpAddress", request.IpAddress, DbType.String);
            var response = new GetReceiptByIdResponse();
            var receiptHeaders = new List<DocumentHeader>();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.DOCUMENT_GET_RECEIPT_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstAsync<GetReceiptByIdResponse>();
                receiptHeaders = (await multi.ReadAsync<DocumentHeader>()).ToList();
            }

            if (response != null)
            {
                response.ReceiptHeaders = receiptHeaders;
            }

            return response;
        }
    }
}
