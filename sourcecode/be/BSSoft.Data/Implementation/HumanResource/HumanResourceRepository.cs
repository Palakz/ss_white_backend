﻿namespace SSWhite.Data.Implementation.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Dapper;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Core.Utilities;
    using SSWhite.Data.Common;
    using SSWhite.Data.Interface.Base;
    using SSWhite.Data.Interface.HumanResource;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.HumanResource;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.HumanResource;

    public class HumanResourceRepository : IHumanResourceRepository
    {
        private readonly IBaseRepository _baseRepository;

        public HumanResourceRepository(IBaseRepository baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public async Task<GetAllRecruiteeDropdownsResponse> GetAllRecruiteeDropdowns()
        {
            try
            {
                var getAllRecruiteeDropDownsResponse = new GetAllRecruiteeDropdownsResponse();
                using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.EMP_GET_ALL_RECRUITEE_DROPDOWNS, commandType: CommandType.StoredProcedure))
                {
                    getAllRecruiteeDropDownsResponse.GendersDropDown = (await multi.ReadAsync<GendersDropDown>()).ToList();
                    getAllRecruiteeDropDownsResponse.IdentitiesDropDown = (await multi.ReadAsync<IdentitiesDropDown>()).ToList();
                    getAllRecruiteeDropDownsResponse.MaritalStatusesDropDown = (await multi.ReadAsync<MaritalStatusesDropDown>()).ToList();
                    getAllRecruiteeDropDownsResponse.RelationTypesDropDown = (await multi.ReadAsync<RelationTypesDropDown>()).ToList();
                    getAllRecruiteeDropDownsResponse.QuestionsDropDown = (await multi.ReadAsync<QuestionsDropDown>()).ToList();
                }

                return getAllRecruiteeDropDownsResponse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<GetRecruiteeByIdResponse> GetRecruiteeById(ServiceRequest<GetRecruiteeByIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", request.Data.Id, DbType.Int16);

            var response = new GetRecruiteeByIdResponse();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.EMP_GET_RECRUITEE_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstOrDefaultAsync<GetRecruiteeByIdResponse>();

                response.RecruiteeAddresses = (await multi.ReadAsync<RecruiteeAddresses>()).ToList();
                response.RecruiteeEducations = (await multi.ReadAsync<RecruiteeEducations>()).ToList();
                response.RecruiteeExperiences = (await multi.ReadAsync<RecruiteeExperiences>()).ToList();
                response.RecruiteeFamilyDetails = (await multi.ReadAsync<RecruiteeFamilyDetails>()).ToList();
                response.RecruiteeQuestions = (await multi.ReadAsync<RecruiteeQuestions>()).ToList();
                response.RecruiteeReferences = (await multi.ReadAsync<RecruiteeReferences>()).ToList();
            }

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllRecruiteesResponse>> GetAllRecruitees(ServiceSearchRequest<GetAllRecruiteesRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetAllRecruiteesResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllRecruiteesResponse>(StoredProcedures.EMP_GET_ALL_RECRUITEES, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<AddOrEditRecruiteeResponse> AddOrEditRecruitee(ServiceRequest<AddOrEditRecruiteeRequest> request)
        {
            try
            {
                var recruiteeAddresses = (from item in request.Data.RecruiteeAddresses
                                          select new RecruiteeAddresses()
                                          {
                                              AddressType = item.AddressType,
                                              IsSameAsCurrentAddress = item.IsSameAsCurrentAddress,
                                              AddressLine1 = item.AddressLine1,
                                              AddressLine2 = item.AddressLine2,
                                              City = item.City,
                                              Taluka = item.Taluka,
                                              District = item.District,
                                              State = item.State,
                                              PinCode = item.PinCode.Trim(),
                                              MobileNumber1 = item.MobileNumber1,
                                              MobileNumber2 = item.MobileNumber2,
                                              Email = item.Email,
                                          }).ToList();

                var recruiteeEducations = (from item in request.Data.RecruiteeEducations
                                           select new RecruiteeEducations()
                                           {
                                               AcademicYear = item.AcademicYear,
                                               Course = item.Course,
                                               University = item.University,
                                               MainSubject = item.MainSubject,
                                               Percentage = item.Percentage
                                           }).ToList();

                var recruiteeFamilyDetails = (from item in request.Data.RecruiteeFamilyDetails
                                              select new
                                              {
                                                  RelationTypesId = item.RelationTypesId,
                                                  Name = item.Name,
                                                  Occupation = item.Occupation,
                                                  OrganizationName = item.OrganizationName,
                                              }).ToList();

                var recruiteeQuestions = (from item in request.Data.RecruiteeQuestions
                                          select new
                                          {
                                              QuestionsId = item.QuestionsId,
                                              Answer = item.Answer
                                          }).ToList();

                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Data.Id, DbType.Int32);
                parameters.Add("UserId", request.Session.UserId, DbType.Int32);
                parameters.Add("FirstName", request.Data.FirstName, DbType.String);
                parameters.Add("MiddleName", request.Data.MiddleName, DbType.String);
                parameters.Add("SurName", request.Data.SurName, DbType.String);
                parameters.Add("Dob", request.Data.DOB, DbType.DateTime);
                parameters.Add("GenderId", request.Data.GenderId, DbType.Int16);
                parameters.Add("MaritalStatusId", request.Data.MaritalStatusId, DbType.Int16);
                parameters.Add("ImagePath", request.Data.ImagePath, DbType.String);
                parameters.Add("DoAnyRelative", request.Data.DoAnyRelative, DbType.Boolean);
                parameters.Add("RelativeName", request.Data.RelativeName, DbType.String);
                parameters.Add("RelationWithRelative", request.Data.RelationWithRelative, DbType.String);
                parameters.Add("IdentityProofs", string.Join(",", request.Data.IdentityProofs), DbType.String);
                parameters.Add("IsDisabilityOrSickness", request.Data.IsDisabilityOrSickness, DbType.Boolean);
                parameters.Add("DisabilityDescription", request.Data.DisabilityDescription, DbType.String);
                parameters.Add("IsWorkExperience", request.Data.IsWorkExperience, DbType.Boolean);
                parameters.Add("IsFromReference", request.Data.IsFromReference, DbType.Boolean);
                parameters.Add("IsSameAsCurrentAddress", request.Data.IsSameAsCurrentAddress, DbType.Boolean);
                parameters.Add("RecruiteeAddresses", recruiteeAddresses.ToDataTable(), DbType.Object);
                parameters.Add("RecruiteeEducations", recruiteeEducations.ToDataTable(), DbType.Object);
                parameters.Add("RecruiteeFamilyDetails", recruiteeFamilyDetails.ToDataTable(), DbType.Object);

                if (request.Data.RecruiteeExperiences?.Any() == true)
                {
                    var recruiteeExperiences = (from item in request.Data.RecruiteeExperiences
                                                select new RecruiteeExperiences()
                                                {
                                                    CompanyName = item.CompanyName,
                                                    CompanyAddress = item.CompanyAddress,
                                                    Designation = item.Designation,
                                                    FromDate = item.FromDate,
                                                    ToDate = item.ToDate,
                                                    MonthlySalary = item.MonthlySalary
                                                }).ToList();

                    parameters.Add("RecruiteeExperiences", recruiteeExperiences.ToDataTable(), DbType.Object);
                }

                parameters.Add("RecruiteeQuestions", recruiteeQuestions.ToDataTable(), DbType.Object);

                if (request.Data.RecruiteeReferences?.Any() == true)
                {
                    var recruiteeReferences = (from item in request.Data.RecruiteeReferences
                                               select new RecruiteeReferences()
                                               {
                                                   Name = item.Name,
                                                   Designation = item.Designation,
                                                   Address = item.Address,
                                                   ContactNumber = item.ContactNumber
                                               }).ToList();

                    parameters.Add("RecruiteeReferences", recruiteeReferences.ToDataTable(), DbType.Object);
                }

                parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
                parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
                parameters.Add("InsertedId", DbType.Int32, direction: ParameterDirection.Output);

                await _baseRepository.ExecuteAsync(StoredProcedures.EMP_ADD_OR_EDIT_RECRUITEE, parameters, commandType: CommandType.StoredProcedure);
                return new AddOrEditRecruiteeResponse()
                {
                    Id = parameters.Get<int>("InsertedId")
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SendNewEmployeeNotification(ServiceRequest<NewEmployeeNotificationRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Name", request.Data.Name, DbType.String);
            parameters.Add("Education", request.Data.Education, DbType.String);
            parameters.Add("Experience", request.Data.Experience, DbType.String);
            parameters.Add("Hobbies", request.Data.Hobbies, DbType.String);
            parameters.Add("JoiningDate", request.Data.JoiningDate, DbType.DateTime);
            parameters.Add("ImagePath", request.Data.ImagePath, DbType.String);
            parameters.Add("IsMailSent", true, DbType.Boolean);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.EMP_INSERT_NEW_EMPLOYEE_NOTIFICATION, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<List<GetAllExtraOrdinaryAboutNomineeResponse>> GetAllExtraOrdinaryAboutNominee()
        {
            return (await _baseRepository.QueryAsync<GetAllExtraOrdinaryAboutNomineeResponse>(StoredProcedures.MASTER_GET_ALL_EXTRA_ORDINARY_ABOUT_NOMINEE, commandType: CommandType.StoredProcedure)).ToList();
        }

        public async Task<AddEmployeeOfTheMonthNomineeResponse> AddEmployeeOfTheMonthNominee(ServiceRequest<AddEmployeeOfTheMonthNomineeRequest> request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Data.Id, DbType.Int32);
                parameters.Add("EmployeeId", request.Data.EmployeeId, DbType.Int32);
                parameters.Add("EmployeeTypeId", request.Data.EmployeeTypeId, DbType.Int16);
                parameters.Add("DepartmentId", request.Data.DepartmentId, DbType.Int16);
                parameters.Add("Supervisor", request.Data.Supervisor, DbType.String);
                parameters.Add("AdditionalDetails", request.Data.AdditionalDetails, DbType.String);
                parameters.Add("NominatedBy", request.Session.UserId, DbType.Int32);
                parameters.Add("NominatedDate", request.Data.NominatedDate, DbType.Date);
                parameters.Add("NominatedDetailsId", string.Join(",", request.Data.NominatedDetailsId), DbType.String);
                parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
                parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
                parameters.Add("StatusActive", StatusType.Active, DbType.Int16);

                return (await _baseRepository.QueryAsync<AddEmployeeOfTheMonthNomineeResponse>(StoredProcedures.EMP_ADD_EMPLOYEE_OF_THE_MONTH_NOMINEE, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllEmployeeOfTheMonthNomineesResponse>> GetAllEmployeeOfTheMonthNominees(ServiceSearchRequest<GetAllEmployeeOfTheMonthNomineesRequest> request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Start", request.Start, DbType.Int32);
                parameters.Add("Length", request.Length, DbType.Int32);
                parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
                parameters.Add("SortExpression", request.SortExpression, DbType.String);
                parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
                var response = new ServiceSearchResponse<GetAllEmployeeOfTheMonthNomineesResponse>()
                {
                    Result = (await _baseRepository.QueryAsync<GetAllEmployeeOfTheMonthNomineesResponse>(StoredProcedures.EMP_GET_ALL_EMPLOYEE_OF_THE_MONTH_NOMINEES, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                    TotalRecords = parameters.Get<int>("TotalRecords"),
                };
                return response;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetNomineeByNomineeIdResponse>> GetNomineeByNomineeId(GetNomineeByNomineeIdRequest request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("NomineeId", request.NomineeId, DbType.Int16);

                return (await _baseRepository.QueryAsync<GetNomineeByNomineeIdResponse>(StoredProcedures.EMP_GET_NOMINEE_BY_NOMINEE_ID, parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<AddLetsKnowEachOtherResponse> AddLetsKnowEachOther(ServiceRequest<AddLetsKnowEachOtherRequest> request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Data.Id, DbType.Int32);
                parameters.Add("UserId", request.Session.UserId, DbType.Int32);
                parameters.Add("SSWGoals", request.Data.SSWGoals, DbType.String);
                parameters.Add("InterestingFacts", request.Data.InterestingFacts, DbType.String);
                parameters.Add("Hobbies", request.Data.Hobbies, DbType.String);
                parameters.Add("FavouriteQuote", request.Data.FavouriteQuote, DbType.String);
                parameters.Add("FavouriteMovie", request.Data.FavouriteMovie, DbType.String);
                parameters.Add("FavouriteSong", request.Data.FavouriteSong, DbType.String);
                parameters.Add("FavouriteBook", request.Data.FavouriteBook, DbType.String);
                parameters.Add("PersonalGoals", request.Data.PersonalGoals, DbType.String);
                parameters.Add("FamilyIntroduction", request.Data.FamilyIntroduction, DbType.String);
                parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
                parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
                parameters.Add("StatusActive", StatusType.Active, DbType.Int16);

                return (await _baseRepository.QueryAsync<AddLetsKnowEachOtherResponse>(StoredProcedures.EMP_ADD_LETS_KNOW_EACH_OTHER, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllLetsKnowEachOtherResponse>> GetAllLetsKnowEachOther(ServiceSearchRequest<GetAllLetsKnowEachOtherRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetAllLetsKnowEachOtherResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllLetsKnowEachOtherResponse>(StoredProcedures.EMP_GET_ALL_LETS_KNOW_EACH_OTHER, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<List<GetLetsKnowEachOtherByIdResponse>> GetLetsKnowEachOtherById(GetLetsKnowEachOtherByIdRequest request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Id, DbType.Int16);

                return (await _baseRepository.QueryAsync<GetLetsKnowEachOtherByIdResponse>(StoredProcedures.EMP_GET_LETS_KNOW_EACH_OTHER_BY_ID, parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetGatepassByIdResponse>> GetGatepassById(GetGatepassByIdRequest request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Id, DbType.Int16);

                return (await _baseRepository.QueryAsync<GetGatepassByIdResponse>(StoredProcedures.EMP_GET_GATEPASS_BY_ID, parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<AddGatepassResponse> AddGatepass(ServiceRequest<AddGatepassRequest> request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Data.Id, DbType.Int32);
                parameters.Add("UserId", request.Data.UserId, DbType.Int32);
                parameters.Add("EmployeeTypeId", request.Data.EmployeeTypeId, DbType.Int32);
                parameters.Add("ContractAgency", request.Data.ContractAgency, DbType.String);
                parameters.Add("Purpose", request.Data.Purpose, DbType.String);
                parameters.Add("TimeIn", request.Data.TimeIn, DbType.DateTime);
                parameters.Add("TimeOut", request.Data.TimeOut, DbType.DateTime);
                parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
                parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
                parameters.Add("StatusActive", StatusType.Active, DbType.Int16);

                return (await _baseRepository.QueryAsync<AddGatepassResponse>(StoredProcedures.EMP_ADD_GATEPASS, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllGatepassesResponse>> GetAllGatepasses(ServiceSearchRequest<GetAllGatepassesRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetAllGatepassesResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllGatepassesResponse>(StoredProcedures.EMP_GET_ALL_GATEPASSES, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<CreateNewLeaveSheetResponse> CreateNewLeaveSheet(ServiceRequest<CreateNewLeaveSheetRequest> request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Data.Id, DbType.Int32);
                parameters.Add("UserId", request.Data.UserId, DbType.Int32);
                parameters.Add("EmployeeTypeId", request.Data.EmployeeTypeId, DbType.Int32);
                parameters.Add("ContractAgency", request.Data.ContractAgency, DbType.String);
                parameters.Add("LeaveTypeId", request.Data.LeaveTypeId, DbType.Int32);
                parameters.Add("EVL", request.Data.EVL, DbType.Int32);
                parameters.Add("EFL", request.Data.EFL, DbType.Int32);
                //parameters.Add("AlreadyTakenLeaveInCurrentMonth", request.Data.AlreadyTakenLeaveInCurrentMonth, DbType.Int32);
                parameters.Add("LeaveApplyDate", DateTime.Now, DbType.DateTime);
                parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
                parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
                parameters.Add("CountOfDays", request.Data.CountOfDays, DbType.Double);
                parameters.Add("LeavePurpose", request.Data.LeavePurpose, DbType.String);
                parameters.Add("AddressWhileOnLeave", request.Data.AddressWhileOnLeave, DbType.String);
                parameters.Add("Note", request.Data.Note, DbType.String);
                parameters.Add("ApprovedBY", request.Data.ApprovedBY, DbType.Int32);
                parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
                parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
                parameters.Add("StatusActive", StatusType.Active, DbType.Int16);

                return (await _baseRepository.QueryAsync<CreateNewLeaveSheetResponse>(StoredProcedures.EMP_CREATE_NEW_LEAVE_SHEET, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetEmployeeLeaveDetailsByIdResponse> GetEmployeeLeaveDetailsById(ServiceRequest<GetEmployeeLeaveDetailsByIdRequest> request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("UserId", request.Data.UserId, DbType.Int16);

                var response = new GetEmployeeLeaveDetailsByIdResponse();

                using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.EMP_GET_EMPLOYEE_LEAVE_DETAILS_BY_ID, parameters, commandType: CommandType.StoredProcedure))
                {
                    //response = await multi.ReadFirstOrDefaultAsync<GetEmployeeLeaveDetailsByIdResponse>();

                    response.EmployeeDetailsForLeaveSheet = (await multi.ReadAsync<EmployeeDetailsForLeaveSheet>()).ToList();
                    response.EmployeeCurrentMonthLeaveDetails = (await multi.ReadAsync<EmployeeCurrentMonthLeaveDetails>()).ToList();
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ServiceSearchResponse<GetAllLeaveSheetsResponse>> GetAllLeaveSheets(ServiceSearchRequest<GetAllLeaveSheetsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetAllLeaveSheetsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllLeaveSheetsResponse>(StoredProcedures.EMP_GET_ALL_LEAVE_SHEETS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<List<GetLeaveSheetDetailsByIdResponse>> GetLeaveSheetDetailsById(GetLeaveSheetDetailsByIdRequest request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Id, DbType.Int16);

                return (await _baseRepository.QueryAsync<GetLeaveSheetDetailsByIdResponse>(StoredProcedures.EMP_GET_LEAVE_SHEET_DETAILS_BY_ID, parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<AddNewDepartmentCourseResponse> AddNewDepartmentCourse(ServiceRequest<AddNewDepartmentCourseRequest> request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Data.Id, DbType.Int32);
                parameters.Add("Name", request.Data.Name, DbType.String);
                parameters.Add("DepartmentId", request.Data.DepartmentId, DbType.Int32);
                parameters.Add("SubDepartmentId", request.Data.SubDepartmentId, DbType.Int32);
                parameters.Add("SummaryOfContent", request.Data.SummaryOfContent, DbType.String);
                parameters.Add("Hours", request.Data.Hours, DbType.Double);
                parameters.Add("Location", request.Data.Location, DbType.String);
                parameters.Add("CurrentDate", DateTime.Now, DbType.DateTime);
                parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
                parameters.Add("StatusActive", StatusType.Active, DbType.Int16);

                return (await _baseRepository.QueryAsync<AddNewDepartmentCourseResponse>(StoredProcedures.MASTER_ADD_NEW_DEPARTMENT_COURSE, parameters, commandType: CommandType.StoredProcedure)).FirstOrDefault();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllDepartmentCoursesResponse>> GetAllDepartmentCourses(ServiceSearchRequest<GetAllDepartmentCoursesRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetAllDepartmentCoursesResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllDepartmentCoursesResponse>(StoredProcedures.MASTER_GET_ALL_DEPARTMENT_COURSES, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<List<GetDepartmentCourseDetailsByIdResponse>> GetDepartmentCourseDetailsById(GetDepartmentCourseDetailsByIdRequest request)
        {
            try
            {
                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Id, DbType.Int16);

                return (await _baseRepository.QueryAsync<GetDepartmentCourseDetailsByIdResponse>(StoredProcedures.MASTER_GET_DEPARTMENT_COURSE_DETAILS_BY_ID, parameters, commandType: CommandType.StoredProcedure)).ToList();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetDepartmentCoursesByDepartmentIdResponse> GetDepartmentCoursesByDepartmentId(GetDepartmentCoursesByDepartmentIdRequest request)
        {
            var parameteres = new DynamicParameters();
            parameteres.Add("DepartmentId", request.DepartmentId, DbType.String);

            var response = new GetDepartmentCoursesByDepartmentIdResponse();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.MASTER_GET_DEPARTMENT_COURSES_BY_DEPARTMENT_ID, parameteres, commandType: CommandType.StoredProcedure))
            {
                //response = await multi.ReadFirstOrDefaultAsync<GetDepartmentCoursesByDepartmentIdResponse>();

                response.DepartmentCoursesDetails = (await multi.ReadAsync<DepartmentCoursesDetails>()).ToList();
                response.BuddyDropDowns = (await multi.ReadAsync<BuddyDropDown>()).ToList();
            }

            return response;
        }

        public async Task<AddOrEditTrainingResponse> AddOrEditTraining(ServiceRequest<AddOrEditTrainingRequest> request)
        {
            try
            {
                var employeeTraining = (from item in request.Data.EmployeeTraining
                                        select new
                                        {
                                            EmployeeTrainingId = item.EmployeeTrainingId,
                                            TrainingTypeId = item.TrainingTypeId,
                                            UserId = item.UserId,
                                            IsEvaluated = item.IsEvaluated,
                                            EvaluationDate = item.EvaluationDate,
                                            EvaluationMethod = item.EvaluationMethod,
                                            HasAttachment = item.HasAttachment,
                                            JobDescriptionImage = item.JobDescriptionImage,
                                            TrainingHardCopyImage = item.TrainingHardCopyImage,
                                            IsDeleted = item.IsDeleted,
                                        }).ToList();

                var parameters = new DynamicParameters();
                parameters.Add("Id", request.Data.Id, DbType.Int32);
                parameters.Add("DepartmentId", request.Data.DepartmentId, DbType.Int16);
                parameters.Add("DepartmentCourseId", request.Data.DepartmentCourseId, DbType.Int16);
                parameters.Add("Instructor", request.Data.Instructor, DbType.String);
                parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
                parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
                parameters.Add("Author", request.Session.UserId, DbType.Int32);
                parameters.Add("EnteredBy", request.Data.EnteredBy, DbType.Int16);
                parameters.Add("StatusTypeActive", StatusType.Active, DbType.Int16);
                parameters.Add("CreatedDate", DateTime.Now, DbType.DateTime);
                parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
                parameters.Add("EmployeeTraining", employeeTraining.ToDataTable(), DbType.Object);
                parameters.Add("InsertedId", DbType.Int32, direction: ParameterDirection.Output);

                await _baseRepository.ExecuteAsync(StoredProcedures.EMP_ADD_OR_EDIT_TRAINING, parameters, commandType: CommandType.StoredProcedure);
                return new AddOrEditTrainingResponse()
                {
                    Id = parameters.Get<int>("InsertedId")
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ServiceSearchResponse<GetAllTrainingsResponse>> GetAllTrainings(ServiceSearchRequest<GetAllTrainingsRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetAllTrainingsResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetAllTrainingsResponse>(StoredProcedures.EMP_GET_ALL_TRAININGS, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<GetTrainingByIdResponse> GetTrainingById(ServiceRequest<GetTrainingByIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("TrainingId", request.Data.TrainingId, DbType.Int16);

            var response = new GetTrainingByIdResponse();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.EMP_GET_TRAINING_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstOrDefaultAsync<GetTrainingByIdResponse>();

                response.EmployeeTraining = (await multi.ReadAsync<EmployeeTrainingResponse>()).ToList();
            }

            return response;
        }

        public async Task DeleteTrainingById(ServiceRequest<DeleteTrainingByIdRequest> request)
        {
            var parameters = new DynamicParameters();

            parameters.Add("TrainingId", request.Data.TrainingId, DbType.Int16);
            parameters.Add("StatusTypeDelete", StatusType.Deleted, DbType.Int16);
            parameters.Add("UpdatedDate", DateTime.Now, DbType.DateTime);
            parameters.Add("UpdatedBy", request.Session.UserId, DbType.Int32);
            parameters.Add("IsDeleted", request.Data.IsDeleted, DbType.Boolean);

            await _baseRepository.ExecuteAsync(StoredProcedures.EMP_DELETE_TRAINING_BY_ID, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<ServiceSearchResponse<GetInstructorByTrainingDropDownListResponse>> GetInstructorByTrainingDropDownList(ServiceSearchRequest<GetInstructorByTrainingDropDownListRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetInstructorByTrainingDropDownListResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetInstructorByTrainingDropDownListResponse>(StoredProcedures.EMP_GET_INSTRUCTOR_BY_TRAINING_DROPDOWN_LIST, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<ServiceSearchResponse<GetEmployeeTrainingDropDownListResponse>> GetEmployeeTrainingDropDownList(ServiceSearchRequest<GetEmployeeTrainingDropDownListRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetEmployeeTrainingDropDownListResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetEmployeeTrainingDropDownListResponse>(StoredProcedures.EMP_GET_EMPLOYEE_BY_TRAINING_DROPDOWN_LIST, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<ServiceSearchResponse<GetCourseByTrainingDropDownListResponse>> GetCourseByTrainingDropDownList(ServiceSearchRequest<GetCourseByTrainingDropDownListRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetCourseByTrainingDropDownListResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetCourseByTrainingDropDownListResponse>(StoredProcedures.EMP_GET_COURSE_BY_TRAINING_DROPDOWN_LIST, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<ServiceSearchResponse<GetHoursByTrainingDropDownListResponse>> GetHoursByTrainingDropDownList(ServiceSearchRequest<GetHoursByTrainingDropDownListRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("FromDate", request.Data.FromDate, DbType.DateTime);
            parameters.Add("ToDate", request.Data.ToDate, DbType.DateTime);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetHoursByTrainingDropDownListResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetHoursByTrainingDropDownListResponse>(StoredProcedures.EMP_GET_HOURS_BY_TRAINING_DROPDOWN_LIST, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<ServiceSearchResponse<GetTrainingDetailsOfCourseByCourseIdResponse>> GetTrainingDetailsOfCourseByCourseId(ServiceSearchRequest<GetTrainingDetailsOfCourseByCourseIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("CourseId", request.Data.CourseId, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetTrainingDetailsOfCourseByCourseIdResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetTrainingDetailsOfCourseByCourseIdResponse>(StoredProcedures.EMP_GET_TRAININGDETAILS_OF_COURSE_BY_COURSEID, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<ServiceSearchResponse<GetTrainingDetailsOfInstructorByInstructorNameResponse>> GetTrainingDetailsOfInstructorByInstructorName(ServiceSearchRequest<GetTrainingDetailsOfInstructorByInstructorNameRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("InstructorName", request.Data.InstructorName, DbType.String);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetTrainingDetailsOfInstructorByInstructorNameResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetTrainingDetailsOfInstructorByInstructorNameResponse>(StoredProcedures.EMP_GET_TRAININGDETAILS_OF_INSTRUCTOR_BY_INSTRUCTORNAME, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task<ServiceSearchResponse<GetTrainingDetailsOfEmployeeByEmployeeIdResponse>> GetTrainingDetailsOfEmployeeByEmployeeId(ServiceSearchRequest<GetTrainingDetailsOfEmployeeByEmployeeIdRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("UserId", request.Data.UserId, DbType.Int32);
            parameters.Add("Start", request.Start, DbType.Int32);
            parameters.Add("Length", request.Length, DbType.Int32);
            parameters.Add("SearchKeyword", request.SearchKeyword, DbType.String);
            parameters.Add("SortExpression", request.SortExpression, DbType.String);
            parameters.Add("TotalRecords", DbType.Int32, direction: ParameterDirection.Output);
            var response = new ServiceSearchResponse<GetTrainingDetailsOfEmployeeByEmployeeIdResponse>()
            {
                Result = (await _baseRepository.QueryAsync<GetTrainingDetailsOfEmployeeByEmployeeIdResponse>(StoredProcedures.EMP_GET_TRAININGDETAILS_OF_EMPLOYEE_BY_EMPLOYEEID, parameters, commandType: CommandType.StoredProcedure)).ToList(),
                TotalRecords = parameters.Get<int>("TotalRecords"),
            };
            return response;
        }

        public async Task SendTrainingNotification(ServiceRequest<TrainingNotificationRequest> request)
        {
            var parameters = new DynamicParameters();
            parameters.Add("CourseId", request.Data.CourseId, DbType.Int32);
            parameters.Add("Location", request.Data.Location, DbType.String);
            parameters.Add("TrainingTypeId", request.Data.TrainingTypeId, DbType.Int32);
            parameters.Add("Instructor", request.Data.Instructor, DbType.String);
            parameters.Add("Date", request.Data.Date, DbType.Date);
            parameters.Add("Time", request.Data.Time, DbType.Time);
            parameters.Add("EmailAddress", string.Join(",", request.Data.EmailAddress), DbType.String);
            parameters.Add("Description", request.Data.Description, DbType.String);
            parameters.Add("IsMailSent", true, DbType.Boolean);
            parameters.Add("CreatedBy", request.Session.UserId, DbType.Int32);
            await _baseRepository.ExecuteAsync(StoredProcedures.EMP_INSERT_TRAINING_NOTIFICATIONS, parameters, commandType: CommandType.StoredProcedure);
        }

        public async Task<GetRecruiteeByIdResponse> GetRecruiteeDetailsById(int id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("Id", id, DbType.Int64);

            var response = new GetRecruiteeByIdResponse();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.EMP_GET_RECRUITEE_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstOrDefaultAsync<GetRecruiteeByIdResponse>();

                response.RecruiteeAddresses = (await multi.ReadAsync<RecruiteeAddresses>()).ToList();
                response.RecruiteeEducations = (await multi.ReadAsync<RecruiteeEducations>()).ToList();
                response.RecruiteeExperiences = (await multi.ReadAsync<RecruiteeExperiences>()).ToList();
                response.RecruiteeFamilyDetails = (await multi.ReadAsync<RecruiteeFamilyDetails>()).ToList();
                response.RecruiteeQuestions = (await multi.ReadAsync<RecruiteeQuestions>()).ToList();
                response.RecruiteeReferences = (await multi.ReadAsync<RecruiteeReferences>()).ToList();
            }

            return response;
        }

        public async Task<GetTrainingByIdResponse> GetTrainingDetailsByTrainingId(int id)
        {
            var parameters = new DynamicParameters();
            parameters.Add("TrainingId", id, DbType.Int16);

            var response = new GetTrainingByIdResponse();

            using (var multi = await _baseRepository.QueryMultipleAsync(StoredProcedures.EMP_GET_TRAINING_BY_ID, parameters, commandType: CommandType.StoredProcedure))
            {
                response = await multi.ReadFirstOrDefaultAsync<GetTrainingByIdResponse>();

                response.EmployeeTraining = (await multi.ReadAsync<EmployeeTrainingResponse>()).ToList();
            }

            return response;
        }
    }
}