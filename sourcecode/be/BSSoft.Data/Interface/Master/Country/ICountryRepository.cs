﻿namespace SSWhite.Data.Interface.Master.Country
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Country;
    using SSWhite.Domain.Request.Master.Country;
    using SSWhite.Domain.Response.Master.Country;

    public interface ICountryRepository
    {
        Task<ServiceSearchResponse<GetAllCountriesResponse>> GetAllCountries(ServiceSearchRequest<GetAllCountries> request);

        Task<int> AddEditCountry(ServiceRequest<Country> request);

        Task ChangeCountryStatus(ServiceRequest<int> request);

        Task<GetCountryByIdResponse> GetCountryById(ServiceRequest<int> request);

        Task DeleteCountry(ServiceRequest<int> request);
    }
}
