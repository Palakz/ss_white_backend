﻿namespace SSWhite.Data.Interface.Master.ItemSubCategory
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemSubCategory;
    using SSWhite.Domain.Request.Master.ItemSubCategory;
    using SSWhite.Domain.Response.Master.ItemSubCategory;

    public interface IItemSubCategoryRepository
    {
        Task<ServiceSearchResponse<GetAllItemSubCategoriesResponse>> GetAllItemSubCategories(ServiceSearchRequest<GetAllItemSubCategories> request);

        Task<int> AddEditItemSubCategory(ServiceRequest<ItemSubCategory> request);

        Task ChangeItemSubCategoryStatus(ServiceRequest<int> request);

        Task<GetItemSubCategoryByIdResponse> GetItemSubCategoryById(ServiceRequest<int> request);

        Task DeleteItemSubCategory(ServiceRequest<int> request);
    }
}
