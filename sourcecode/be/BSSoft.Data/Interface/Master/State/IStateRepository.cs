﻿namespace SSWhite.Data.Interface.Master.State
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.State;
    using SSWhite.Domain.Request.Master.State;
    using SSWhite.Domain.Response.Master.State;

    public interface IStateRepository
    {
        Task<ServiceSearchResponse<GetAllStatesResponse>> GetAllStates(ServiceSearchRequest<GetAllStates> request);

        Task<int> AddEditState(ServiceRequest<State> request);

        Task ChangeStateStatus(ServiceRequest<int> request);

        Task<GetStateByIdResponse> GetStateById(ServiceRequest<int> request);

        Task DeleteState(ServiceRequest<int> request);
    }
}
