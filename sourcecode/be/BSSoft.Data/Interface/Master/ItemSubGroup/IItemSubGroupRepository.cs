﻿namespace SSWhite.Data.Interface.Master.ItemSubGroup
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemSubGroup;
    using SSWhite.Domain.Request.Master.ItemSubGroup;
    using SSWhite.Domain.Response.Master.ItemSubGroup;

    public interface IItemSubGroupRepository
    {
        Task<ServiceSearchResponse<GetAllItemSubGroupsResponse>> GetAllItemSubGroups(ServiceSearchRequest<GetAllItemSubGroups> request);

        Task<int> AddEditItemSubGroup(ServiceRequest<ItemSubGroup> request);

        Task ChangeItemSubGroupStatus(ServiceRequest<int> request);

        Task<GetItemSubGroupByIdResponse> GetItemSubGroupById(ServiceRequest<int> request);

        Task DeleteItemSubGroup(ServiceRequest<int> request);
    }
}
