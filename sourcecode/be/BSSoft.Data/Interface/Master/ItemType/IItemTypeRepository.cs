﻿namespace SSWhite.Data.Interface.Master.ItemType
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemType;
    using SSWhite.Domain.Request.Master.ItemTypes;
    using SSWhite.Domain.Response.Master.ItemType;

    public interface IItemTypeRepository
    {
        Task<ServiceSearchResponse<GetAllItemTypesResponse>> GetAllItemTypes(ServiceSearchRequest<GetAllItemTypes> request);

        Task<int> AddEditItemType(ServiceRequest<ItemType> request);

        Task ChangeItemTypeStatus(ServiceRequest<int> request);

        Task<GetItemTypeByIdResponse> GetItemTypeById(ServiceRequest<int> request);

        Task DeleteItemType(ServiceRequest<int> request);
    }
}
