﻿namespace SSWhite.Data.Interface.Master.Brand
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Master.Brand;
    using SSWhite.Domain.Response.Master.Brand;

    public interface IBrandRepository
    {
        Task<ServiceSearchResponse<GetAllBrandsResponse>> GetAllBrands(ServiceSearchRequest<GetAllBrands> request);

        Task<int> AddBrand(ServiceRequest<AddBrand> request);

        Task ChangeBrandStatus(ServiceRequest<int> request);

        Task<GetBrandByIdResponse> GetBrandById(ServiceRequest<int> request);

        Task DeleteBrand(ServiceRequest<int> request);

        Task<int> EditBrand(ServiceRequest<EditBrand> request);
    }
}
