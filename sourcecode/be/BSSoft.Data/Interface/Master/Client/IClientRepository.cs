﻿namespace SSWhite.Data.Interface.Master.Client
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Client;
    using SSWhite.Domain.Request.Master.Client;
    using SSWhite.Domain.Response.Master.Client;

    public interface IClientRepository
    {
        Task<ServiceSearchResponse<GetAllClientsResponse>> GetAllClients(ServiceSearchRequest<GetAllClients> request);

        Task<int> AddEditClient(ServiceRequest<Client> request);

        Task ChangeClientStatus(ServiceRequest<int> request);

        Task<GetClientByIdResponse> GetClientById(ServiceRequest<int> request);

        Task DeleteClient(ServiceRequest<int> request);

        Task<List<ClientOpeningBalance>> GetClientOpeningBalanceDetails(ServiceRequest<int> request);

        Task<int> AddEditOpeningBalnce(ServiceRequest<ClientOpeningBalance> request);
    }
}
