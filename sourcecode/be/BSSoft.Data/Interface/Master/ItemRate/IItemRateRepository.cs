﻿namespace SSWhite.Data.Interface.Master.ItemRate
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemRate;
    using SSWhite.Domain.Request.Master.ItemRate;
    using SSWhite.Domain.Response.Common;
    using SSWhite.Domain.Response.Master.ItemRate;

    public interface IItemRateRepository
    {
        Task<ServiceSearchResponse<GetAllItemRatesResponse>> GetAllItemRates(ServiceSearchRequest<GetAllItemRates> request);

        Task<int> AddEditItemRate(ServiceRequest<ItemRate> request);

        Task ChangeItemRateStatus(ServiceRequest<int> request);

        Task<GetItemRateByIdResponse> GetItemRateById(ServiceRequest<int> request);

        Task DeleteItemRate(ServiceRequest<int> request);
    }
}
