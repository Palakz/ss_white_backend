﻿namespace SSWhite.Data.Interface.Admin
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.HumanResource;
    using SSWhite.Domain.Response.Admin;

    public interface IAdminRepository
    {
        Task<ServiceSearchResponse<GetDocumentApprovalListResponse>> GetDocumentApprovals(ServiceSearchRequest<GetDocumentApprovalListRequest> request);

        Task<ServiceSearchResponse<GetAllEmployeeLimitsResponse>> GetAllEmployeeLimits(GetAllEmployeeLimitsRequest request);

        Task<GetEmployeeByEmployeeIdResponse> GetEmployeeByEmployeeId(GetEmployeeByEmployeeIdRequest request);

        Task UpdateEmployeeLimit(UpdateEmployeeLimitByIdRequest updateEmployeeLimitByIdRequest);

        Task ChangeEmployeeStatus(Task<ServiceRequest<ChangeEmployeeStatusRequest>> request);

        Task ChangeEmployeeDepartment(ServiceRequest<ChangeEmployeeDepartmentRequest> request);

        Task<AddOrEditEmployeeResponse> AddOrEditEmployee(ServiceRequest<AddOrEditEmployeeRequest> request);

        Task<CheckIfEmployeeIdExistsResponse> CheckIfEmployeeIdExists(CheckIfEmployeeIdExistsRequest checkIfEmployeeIdExistsRequest);

        Task<CheckIfEmailAddressExistsResponse> CheckIfEmailAddressExists(CheckIfEmailAddressExistsRequest request);

        Task UpdateDocumentApprovalList(List<UpdateDocumentApprovalListRequest> request);

        Task<List<GetUserRightsResponse>> GetUserRights(ServiceRequest request);

        Task<List<GetRolesResponse>> GetRoles();

        Task<List<GetRightsAccessByRoleIdResponse>> GetRightsAccessByRoleId(GetRightsAccessByRoleIdRequest request);

        Task UpdateRightsAccessByRoleId(UpdateRightsAccessByRoleIdRequest request);

        Task<GetEmployeeByEmployeeIdResponse> GetEmployeeDetailsById(string request);
    }
}
