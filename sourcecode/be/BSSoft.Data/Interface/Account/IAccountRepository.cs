﻿namespace SSWhite.Data.Interface.Account
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Response.Account;

    public interface IAccountRepository
    {
        Task<LogInResponse> ValidateUserForLogIn(ServiceRequest<LogInRequest> request);

        Task<string> SendForgetPasswordEmail(SendForgetPasswordEmailRequest request);

        Task<string> IsEmailValid(IsEmailValidRequest request);

        Task<string> SetNewPassword(SetNewPasswordRequest request);
    }
}
