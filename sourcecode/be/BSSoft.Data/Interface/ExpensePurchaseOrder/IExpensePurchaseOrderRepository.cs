﻿namespace SSWhite.Data.Interface.ExpensePurchaseOrder
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;

    public interface IExpensePurchaseOrderRepository
    {
        Task<GetAllEPODropDownsResponse> GetAllEPODropDowns();

        Task<GetAuthorizedLimitByUserIdResponse> GetAuthorizedLimitByUserId(ServiceRequest request);

        Task<AddOrEditResponse> AddOrEditEpo(ServiceRequest<AddOrEditEpoRequest> request);

        Task<GetEpoDetailsByEpoIdResponse> GetEpoDetailsByEpoId(ServiceRequest<GetEpoDetailsByEpoIdRequest> request);

        Task<ServiceSearchResponse<GetAllEposResponse>> GetAllEpos(ServiceSearchRequest<GetAllEposRequest> request);

        Task VoidEpoByUserId(ServiceRequest<GetEpoDetailsByEpoIdRequest> request);

        Task ApproveOrDenyEpo(ServiceRequest<ApproveOrDenyEpoRequest> request);

        Task<ServiceSearchResponse<GetMpcVoteForEpoResponse>> GetMpcVoteForEpo(ServiceSearchRequest<GetMpcVoteForEpoRequest> request);

        Task<ServiceSearchResponse<GetAllMpcVoteForEpoResponse>> GetAllMpcVoteForEpo(ServiceSearchRequest<GetAllMpcVoteForEpoRequest> request);

        Task<ServiceSearchResponse<GetMySubordinateEposResponse>> GetMySubordinateEpos(ServiceSearchRequest<GetMySubordinateEposRequest> request);

        Task<ServiceSearchResponse<GetMyApprovalForEpoResponse>> GetMyApprovalForEpo(ServiceSearchRequest<GetMyApprovalForEpoRequest> request);

        Task<GetEpoModulesCountsResponse> GetEpoModulesCounts(ServiceRequest request);

        Task<List<GetMpcVoteDetailsByEpoIdResponse>> GetMpcVoteDetailsByEpoId(GetMpcVoteDetailsByEpoIdRequest request);


        Task<List<GetAllVendorOrEmployeeDetailsByNameResponse>> GetAllVendorOrEmployeeDetailsByName(GetAllVendorOrEmployeeDetailsByNameRequest request);
    }
}
