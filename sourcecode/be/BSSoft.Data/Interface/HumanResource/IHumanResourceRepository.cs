﻿namespace SSWhite.Data.Interface.HumanResource
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.HumanResource;
    using SSWhite.Domain.Response.HumanResource;

    public interface IHumanResourceRepository
    {
        Task<ServiceSearchResponse<GetAllRecruiteesResponse>> GetAllRecruitees(ServiceSearchRequest<GetAllRecruiteesRequest> request);

        Task<ServiceSearchResponse<GetAllEmployeeOfTheMonthNomineesResponse>> GetAllEmployeeOfTheMonthNominees(ServiceSearchRequest<GetAllEmployeeOfTheMonthNomineesRequest> request);

        Task<GetAllRecruiteeDropdownsResponse> GetAllRecruiteeDropdowns();

        Task<GetRecruiteeByIdResponse> GetRecruiteeById(ServiceRequest<GetRecruiteeByIdRequest> request);

        Task<List<GetLetsKnowEachOtherByIdResponse>> GetLetsKnowEachOtherById(GetLetsKnowEachOtherByIdRequest request);

        Task<List<GetNomineeByNomineeIdResponse>> GetNomineeByNomineeId(GetNomineeByNomineeIdRequest request);

        Task<List<GetGatepassByIdResponse>> GetGatepassById(GetGatepassByIdRequest request);

        Task<AddOrEditRecruiteeResponse> AddOrEditRecruitee(ServiceRequest<AddOrEditRecruiteeRequest> request);

        Task<AddEmployeeOfTheMonthNomineeResponse> AddEmployeeOfTheMonthNominee(ServiceRequest<AddEmployeeOfTheMonthNomineeRequest> request);

        Task<AddLetsKnowEachOtherResponse> AddLetsKnowEachOther(ServiceRequest<AddLetsKnowEachOtherRequest> request);

        Task<AddGatepassResponse> AddGatepass(ServiceRequest<AddGatepassRequest> request);

        Task<ServiceSearchResponse<GetAllLetsKnowEachOtherResponse>> GetAllLetsKnowEachOther(ServiceSearchRequest<GetAllLetsKnowEachOtherRequest> request);

        Task<ServiceSearchResponse<GetAllGatepassesResponse>> GetAllGatepasses(ServiceSearchRequest<GetAllGatepassesRequest> request);

        Task<List<GetAllExtraOrdinaryAboutNomineeResponse>> GetAllExtraOrdinaryAboutNominee();

        Task<CreateNewLeaveSheetResponse> CreateNewLeaveSheet(ServiceRequest<CreateNewLeaveSheetRequest> request);

        Task<GetEmployeeLeaveDetailsByIdResponse> GetEmployeeLeaveDetailsById(ServiceRequest<GetEmployeeLeaveDetailsByIdRequest> request);

        Task<ServiceSearchResponse<GetAllLeaveSheetsResponse>> GetAllLeaveSheets(ServiceSearchRequest<GetAllLeaveSheetsRequest> request);

        Task<List<GetLeaveSheetDetailsByIdResponse>> GetLeaveSheetDetailsById(GetLeaveSheetDetailsByIdRequest request);

        Task<AddNewDepartmentCourseResponse> AddNewDepartmentCourse(ServiceRequest<AddNewDepartmentCourseRequest> request);

        Task<ServiceSearchResponse<GetAllDepartmentCoursesResponse>> GetAllDepartmentCourses(ServiceSearchRequest<GetAllDepartmentCoursesRequest> request);

        Task<List<GetDepartmentCourseDetailsByIdResponse>> GetDepartmentCourseDetailsById(GetDepartmentCourseDetailsByIdRequest request);

        Task<GetDepartmentCoursesByDepartmentIdResponse> GetDepartmentCoursesByDepartmentId(GetDepartmentCoursesByDepartmentIdRequest request);

        Task SendNewEmployeeNotification(ServiceRequest<NewEmployeeNotificationRequest> serviceRequest);

        Task<AddOrEditTrainingResponse> AddOrEditTraining(ServiceRequest<AddOrEditTrainingRequest> request);

        Task<ServiceSearchResponse<GetAllTrainingsResponse>> GetAllTrainings(ServiceSearchRequest<GetAllTrainingsRequest> request);

        Task<GetTrainingByIdResponse> GetTrainingById(ServiceRequest<GetTrainingByIdRequest> request);

        // Task<DeleteTrainingByIdResponse> DeleteTrainingById(ServiceRequest<DeleteTrainingByIdRequest> request);
        Task DeleteTrainingById(ServiceRequest<DeleteTrainingByIdRequest> request);

        Task<ServiceSearchResponse<GetInstructorByTrainingDropDownListResponse>> GetInstructorByTrainingDropDownList(ServiceSearchRequest<GetInstructorByTrainingDropDownListRequest> request);

        Task<ServiceSearchResponse<GetEmployeeTrainingDropDownListResponse>> GetEmployeeTrainingDropDownList(ServiceSearchRequest<GetEmployeeTrainingDropDownListRequest> request);

        Task<ServiceSearchResponse<GetCourseByTrainingDropDownListResponse>> GetCourseByTrainingDropDownList(ServiceSearchRequest<GetCourseByTrainingDropDownListRequest> request);

        Task<ServiceSearchResponse<GetHoursByTrainingDropDownListResponse>> GetHoursByTrainingDropDownList(ServiceSearchRequest<GetHoursByTrainingDropDownListRequest> request);

        Task<ServiceSearchResponse<GetTrainingDetailsOfCourseByCourseIdResponse>> GetTrainingDetailsOfCourseByCourseId(ServiceSearchRequest<GetTrainingDetailsOfCourseByCourseIdRequest> request);

        Task<ServiceSearchResponse<GetTrainingDetailsOfInstructorByInstructorNameResponse>> GetTrainingDetailsOfInstructorByInstructorName(ServiceSearchRequest<GetTrainingDetailsOfInstructorByInstructorNameRequest> request);

        Task<ServiceSearchResponse<GetTrainingDetailsOfEmployeeByEmployeeIdResponse>> GetTrainingDetailsOfEmployeeByEmployeeId(ServiceSearchRequest<GetTrainingDetailsOfEmployeeByEmployeeIdRequest> request);

        Task SendTrainingNotification(ServiceRequest<TrainingNotificationRequest> request);

        Task<GetRecruiteeByIdResponse> GetRecruiteeDetailsById(int id);

        Task<GetTrainingByIdResponse> GetTrainingDetailsByTrainingId(int id);
    }
}
