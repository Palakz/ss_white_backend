﻿namespace SSWhite.Data.Interface.Organization.User
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Organization.User;
    using SSWhite.Domain.Response.Organization.User;

    public interface IUserRepository
    {
        Task<ServiceSearchResponse<GetAllUsersResponse>> GetAllUsers(ServiceSearchRequest<GetAllUsers> request);

        Task<int> AddUser(ServiceRequest<AddUser> request);

        Task ChangeUserStatus(ServiceRequest<int> request);

        Task<GetUserByIdResponse> GetUserById(ServiceRequest<int> request);

        Task DeleteUser(ServiceRequest<int> request);

        Task<int> EditUser(ServiceRequest<EditUser> request);
    }
}
