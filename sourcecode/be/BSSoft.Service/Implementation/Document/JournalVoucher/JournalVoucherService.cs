﻿namespace SSWhite.Service.Implementation.Document.JournalVoucher
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Document.JournalVoucher;
    using SSWhite.Domain.Common.Document.JournalVoucher;
    using SSWhite.Domain.Request.Document.JournalVoucher;
    using SSWhite.Domain.Response.Document.JournalVoucher;
    using SSWhite.Service.Interface.Document.JournalVoucher;

    public class JournalVoucherService : IJournalVoucherService
    {
        private readonly IJournalVoucherRepository _journalVoucherRepository;

        public JournalVoucherService(IJournalVoucherRepository journalVoucherRepository)
        {
            _journalVoucherRepository = journalVoucherRepository;
        }

        public async Task<ServiceResponse<int>> AddEditJournalVoucher(ServiceRequest<JournalVoucher> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _journalVoucherRepository.AddEditJournalVoucher(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeJournalVoucherStatus(ServiceRequest<int> request)
        {
            try
            {
                await _journalVoucherRepository.ChangeJournalVoucherStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteJournalVoucher(ServiceRequest<int> request)
        {
            try
            {
                await _journalVoucherRepository.DeleteJournalVoucher(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllJournalVouchersResponse>> GetAllJournalVouchers(ServiceSearchRequest<GetAllJournalVouchers> request)
        {
            var response = new ServiceSearchResponse<GetAllJournalVouchersResponse>();
            try
            {
                response = await _journalVoucherRepository.GetAllJournalVouchers(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<JournalVoucher>> GetJournalVoucherById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<JournalVoucher>();
            try
            {
                response.Data = await _journalVoucherRepository.GetJournalVoucherById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
