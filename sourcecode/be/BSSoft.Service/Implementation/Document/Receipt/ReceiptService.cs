﻿namespace SSWhite.Service.Implementation.Document.Receipt
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Document.Receipt;
    using SSWhite.Domain.Common.Document.Receipt;
    using SSWhite.Domain.Request.Document.Receipt;
    using SSWhite.Domain.Response.Document.Receipt;
    using SSWhite.Service.Interface.Document.Receipt;

    public class ReceiptService : IReceiptService
    {
        private readonly IReceiptRepository _receiptRepository;

        public ReceiptService(IReceiptRepository receiptRepository)
        {
            _receiptRepository = receiptRepository;
        }

        public async Task<ServiceResponse<int>> AddEditReceipt(ServiceRequest<Receipt> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _receiptRepository.AddEditReceipt(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeReceiptStatus(ServiceRequest<int> request)
        {
            try
            {
                await _receiptRepository.ChangeReceiptStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteReceipt(ServiceRequest<int> request)
        {
            try
            {
                await _receiptRepository.DeleteReceipt(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllReceiptsResponse>> GetAllReceipts(ServiceSearchRequest<GetAllReceipts> request)
        {
            var response = new ServiceSearchResponse<GetAllReceiptsResponse>();
            try
            {
                response = await _receiptRepository.GetAllReceipts(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Receipt>> GetReceiptById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Receipt>();
            try
            {
                response.Data = await _receiptRepository.GetReceiptById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
