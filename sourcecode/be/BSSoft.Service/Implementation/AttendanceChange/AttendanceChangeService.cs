﻿namespace SSWhite.Service.Implementation.AttendanceChange
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.AttendanceChange;
    using SSWhite.Data.Interface.ExpensePurchaseOrder;
    using SSWhite.Domain.Enums;
    using SSWhite.Domain.Request.AttendanceChange;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.Domain.Response.AttendanceChange;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;
    using SSWhite.Service.Common;
    using SSWhite.Service.Interface.AttendanceChange;

    public class AttendanceChangeService : IAttendanceChangeService
    {
        private readonly IAttendanceChangeRepository _attendanceChangeRepository;
        private readonly ISendMailService _sendMailService;
        private readonly AppSetting _appSetting;

        public AttendanceChangeService(IAttendanceChangeRepository attendanceChangeRepository, ISendMailService sendMailService, IOptions<AppSetting> appSetting)
        {
            _attendanceChangeRepository = attendanceChangeRepository;
            _sendMailService = sendMailService;
            _appSetting = appSetting.Value;
        }

        public async Task<CreateAttendanceFtoResponse> CreateAttendanceFto(ServiceRequest<CreateAttendanceFtoRequest> request)
        {
            try
            {
                var response = await _attendanceChangeRepository.CreateAttendanceFto(request);
                if (response != null)
                {
                    await _sendMailService.SendMailAsync(PrepareMailAfterCreateFto(response, EmailType.EmailIdsForFtoCreatedNotification));
                    await _sendMailService.SendMailAsync(PrepareMailAfterCreateFto(response, EmailType.EmailIdForFtoApproval));
                }

                return response;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task ApproveOrDenyFto(ServiceRequest<ApproveOrDenyFtoRequest> request)
        {
            try
            {
                var response = await _attendanceChangeRepository.ApproveOrDenyFto(request);
                var userName = request.Session.UserName;
                if (response != null)
                {
                    await _sendMailService.SendMailAsync(PrepareMailAfterFtoApproval(response, EmailType.EmailIdsForFtoApprovedNotification, userName));
                    if (response.HrEmailAddress != null)
                    {
                        await _sendMailService.SendMailAsync(PrepareMailAfterFtoApproval(response, EmailType.EmailIdForFtoApproval));
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetDepartmentFtoByUserIdResponse>> GetDepartmentFtoByUserId(ServiceRequest<GetDepartmentFtoByUserIdRequest> request)
        {
            try
            {
                return await _attendanceChangeRepository.GetDepartmentFtoByUserId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetAttendanceFtoByIdResponse> GetAttendanceFtoById(ServiceRequest<GetAttendanceFtoByIdRequest> request)
        {
            try
            {
                return await _attendanceChangeRepository.GetAttendanceFtoById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllDepartmentOrUserFtoByUserIdResponse>> GetAllDepartmentOrUserFtoByUserId(ServiceSearchRequest<GetAllDepartmentOrUserFtoByUserIdRequest> request)
        {
            try
            {
                return await _attendanceChangeRepository.GetAllDepartmentOrUserFtoByUserId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private SendMail PrepareMailAfterCreateFto(CreateAttendanceFtoResponse response, EmailType ePOMailToManager)
        {
            var subject = string.Empty;
            var email = string.Empty;
            var body = string.Empty;
            var isHtml = true;
            switch (ePOMailToManager)
            {
                case EmailType.EmailIdsForFtoCreatedNotification:
                    subject = "*** FTO #  - " + response.FtoId + " - Created By : " + response.CreatedBy + ".";
                    email = response.EmailIdsForFtoNotification;
                    body = $"<span style = 'font-family:Arial;font-size:10pt'><p><strong> FTO Approved " +
                        $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'> " +
                        $"<p><strong>FTO Id:</strong>  { response.FtoId }" +
                        $"<br /><strong> Created By :</strong> {response.CreatedBy}" +
                        $"<br /><strong> Start Date :</strong> {response.StartDate.ToString("dd/MM/yyyy")} : {response.StartDateDayType}" +
                        $"<br /><strong> End Date :</strong> {response.EndDate.ToString("dd/MM/yyyy")} : {response.EndDateDayType} " +
                        $"<br /><strong> Leave Type :</strong> {response.LeaveTypeName}" +
                        $"</span>";
                    isHtml = true;
                    break;
                case EmailType.EmailIdForFtoApproval:
                    subject = "***Awaiting Approval for FTO #  - " + response.FtoId + " - Created By : " + response.CreatedBy + ".";
                    email = response.EmailIdsForFtoNotification;
                    body = $"<span style = 'font-family:Arial;font-size:10pt'><p><strong> New FTO Created " +
                        $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'> " +
                        $"<p><strong>FTO Id:</strong>  { response.FtoId }" +
                        $"<br /><strong> Created By :</strong> {response.CreatedBy}" +
                         $"<br /><strong> Start Date :</strong> {response.StartDate.ToString("dd/MM/yyyy")} : {response.StartDateDayType}" +
                        $"<br /><strong> End Date :</strong> {response.EndDate.ToString("dd/MM/yyyy")} : {response.EndDateDayType} " +
                        $"<br /><strong> Leave Type :</strong> {response.LeaveTypeName} <br/>" +
                        $"<a href={_appSetting.FtoApprovalLink}{response.FtoId}?case=ApprovalforFTO > Click here to approve Or Deny <br/>" +
                        $"</span>";
                    isHtml = true;
                    break;
            }

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email
            };
        }

        private SendMail PrepareMailAfterFtoApproval(ApproveOrDenyFtoResponse response, EmailType ePOMailToManager, string userName = null)
        {
            var subject = string.Empty;
            var email = string.Empty;
            var body = string.Empty;
            var isHtml = true;
            switch (ePOMailToManager)
            {
                case EmailType.EmailIdsForFtoApprovedNotification:
                    subject = "*** FTO #  - " + response.FtoId + " - Approved By : " + userName + ".";
                    email = response.UserEmailAddress;
                    body = $"<span style = 'font-family:Arial;font-size:10pt'><p><strong> New FTO Created " +
                        $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'> " +
                        $"<p><strong>FTO Id:</strong>  { response.FtoId }" +
                        $"<br /><strong> Created By :</strong> {response.CreatedBy}" +
                        $"<br /><strong> Start Date :</strong> {response.StartDate.ToString("dd/MM/yyyy")} : {response.StartDateDayType}" +
                        $"<br /><strong> End Date :</strong> {response.EndDate.ToString("dd/MM/yyyy")} : {response.EndDateDayType} " +
                        $"<br /><strong> Leave Type :</strong> {response.LeaveTypeName}" +
                        $"</span>";
                    isHtml = true;
                    break;
                case EmailType.EmailIdForFtoApproval:
                    subject = "***Awaiting Approval for FTO #  - " + response.FtoId + " - Created By : " + response.CreatedBy + ".";
                    email = response.HrEmailAddress;
                    body = $"<span style = 'font-family:Arial;font-size:10pt'><p><strong> New FTO Created " +
                        $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'> " +
                        $"<p><strong>FTO Id:</strong>  { response.FtoId }" +
                        $"<br /><strong> Created By :</strong> {response.CreatedBy}" +
                         $"<br /><strong> Start Date :</strong> {response.StartDate.ToString("dd/MM/yyyy")} : {response.StartDateDayType}" +
                        $"<br /><strong> End Date :</strong> {response.EndDate.ToString("dd/MM/yyyy")} : {response.EndDateDayType} " +
                        $"<br /><strong> Leave Type :</strong> {response.LeaveTypeName} <br/>" +
                        $"<a href={_appSetting.FtoApprovalLink}{response.FtoId}?case=ApprovalforFTO > Click here to approve Or Deny <br/>" +
                        $"</span>";
                    isHtml = true;
                    break;
            }

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email
            };
        }
    }
}
