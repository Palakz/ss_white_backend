namespace SSWhite.Service.Implementation.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Account;
    using SSWhite.Data.Interface.Admin;
    using SSWhite.Data.Interface.HumanResource;
    using SSWhite.Domain.Common.Master.Item;
    using SSWhite.Domain.Enums;
    using SSWhite.Domain.Request.HumanResource;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Domain.Response.Dashboard;
    using SSWhite.Domain.Response.HumanResource;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;
    using SSWhite.Service.Interface.HumanResource;

    public class HumanResourceService : IHumanResourceService
    {
        private readonly IHumanResourceRepository _humanResourceRepository;
        private readonly IDashboardRepository _dashboardRepository;
        private readonly IAdminRepository _adminRepository;
        private readonly ISendMailService _sendMailService;
        private readonly AppSetting _appSetting;

        public HumanResourceService(IHumanResourceRepository humanResourceRepository,
            IDashboardRepository dashboardRepository,
            IAdminRepository adminRepository,
            ISendMailService sendMailService, IOptions<AppSetting> appSetting)
        {
            _humanResourceRepository = humanResourceRepository;
            _dashboardRepository = dashboardRepository;
            _adminRepository = adminRepository;
            _sendMailService = sendMailService;
            _appSetting = appSetting.Value;
        }

        public async Task<GetAllRecruiteeDropdownsResponse> GetAllRecruiteeDropdowns()
        {
            try
            {
                var list = await _humanResourceRepository.GetAllRecruiteeDropdowns();
                foreach (var item in list.QuestionsDropDown)
                {
                    item.OptionValues = item.Options.Split(',').ToList();
                }

                return list;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetRecruiteeByIdResponse> GetRecruiteeById(ServiceRequest<GetRecruiteeByIdRequest> request)
        {
            try
            {
                var result = await _humanResourceRepository.GetRecruiteeById(request);
                return result;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllRecruiteesResponse>> GetAllRecruitees(ServiceSearchRequest<GetAllRecruiteesRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetAllRecruitees(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<AddOrEditRecruiteeResponse> AddOrEditRecruitee(ServiceRequest<AddOrEditRecruiteeRequest> request)
        {
            try
            {
                var response = await _humanResourceRepository.AddOrEditRecruitee(request);
                if (response != null)
                {
                    // await _sendMailService.SendMailAsync(PrepareMailForRecruitee(response, EmailType.PreRecruiteeNotification));
                    await _sendMailService.SendMailAsync(PrepareMailForRecruitee(response));
                }

                return response;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetAllExtraOrdinaryAboutNomineeResponse>> GetAllExtraOrdinaryAboutNominee()
        {
            try
            {
                return await _humanResourceRepository.GetAllExtraOrdinaryAboutNominee();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task AddEmployeeOfTheMonthNominee(ServiceRequest<AddEmployeeOfTheMonthNomineeRequest> request)
        {
            try
            {
                var response = await _humanResourceRepository.AddEmployeeOfTheMonthNominee(request);

                // if (response != null)
                // {
                //     await _sendMailService.SendMailAsync(PrepareMailForNewUser(response));
                // }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllEmployeeOfTheMonthNomineesResponse>> GetAllEmployeeOfTheMonthNominees(ServiceSearchRequest<GetAllEmployeeOfTheMonthNomineesRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetAllEmployeeOfTheMonthNominees(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SendNewEmployeeNotification(ServiceRequest<NewEmployeeNotificationRequest> serviceRequest)
        {
            try
            {
                await _sendMailService.SendMailAsync(PrepareNotificationMail(serviceRequest, EmailType.NewEmployeeNotification));
                await _humanResourceRepository.SendNewEmployeeNotification(serviceRequest);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetNomineeByNomineeIdResponse>> GetNomineeByNomineeId(GetNomineeByNomineeIdRequest request)
        {
            try
            {
                return await _humanResourceRepository.GetNomineeByNomineeId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetLetsKnowEachOtherByIdResponse>> GetLetsKnowEachOtherById(GetLetsKnowEachOtherByIdRequest request)
        {
            try
            {
                return await _humanResourceRepository.GetLetsKnowEachOtherById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task AddLetsKnowEachOther(ServiceRequest<AddLetsKnowEachOtherRequest> request)
        {
            try
            {
                var response = await _humanResourceRepository.AddLetsKnowEachOther(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllLetsKnowEachOtherResponse>> GetAllLetsKnowEachOther(ServiceSearchRequest<GetAllLetsKnowEachOtherRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetAllLetsKnowEachOther(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<AddGatepassResponse> AddGatepass(ServiceRequest<AddGatepassRequest> request)
        {
            try
            {
                await _sendMailService.SendMailAsync(PrepareNotificationMail(request, EmailType.GatePassNotification));

                return await _humanResourceRepository.AddGatepass(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllGatepassesResponse>> GetAllGatepasses(ServiceSearchRequest<GetAllGatepassesRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetAllGatepasses(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetGatepassByIdResponse>> GetGatepassById(GetGatepassByIdRequest request)
        {
            try
            {
                return await _humanResourceRepository.GetGatepassById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<CreateNewLeaveSheetResponse> CreateNewLeaveSheet(ServiceRequest<CreateNewLeaveSheetRequest> request)
        {
            try
            {
                await _sendMailService.SendMailAsync(PrepareNotificationMail(request, EmailType.LeaveSheetNotification));
                return await _humanResourceRepository.CreateNewLeaveSheet(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<GetEmployeeLeaveDetailsByIdResponse> GetEmployeeLeaveDetailsById(ServiceRequest<GetEmployeeLeaveDetailsByIdRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetEmployeeLeaveDetailsById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllLeaveSheetsResponse>> GetAllLeaveSheets(ServiceSearchRequest<GetAllLeaveSheetsRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetAllLeaveSheets(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetLeaveSheetDetailsByIdResponse>> GetLeaveSheetDetailsById(GetLeaveSheetDetailsByIdRequest request)
        {
            try
            {
                return await _humanResourceRepository.GetLeaveSheetDetailsById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<AddNewDepartmentCourseResponse> AddNewDepartmentCourse(ServiceRequest<AddNewDepartmentCourseRequest> request)
        {
            try
            {
                return await _humanResourceRepository.AddNewDepartmentCourse(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllDepartmentCoursesResponse>> GetAllDepartmentCourses(ServiceSearchRequest<GetAllDepartmentCoursesRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetAllDepartmentCourses(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetDepartmentCourseDetailsByIdResponse>> GetDepartmentCourseDetailsById(GetDepartmentCourseDetailsByIdRequest request)
        {
            try
            {
                return await _humanResourceRepository.GetDepartmentCourseDetailsById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<GetDepartmentCoursesByDepartmentIdResponse> GetDepartmentCoursesByDepartmentId(GetDepartmentCoursesByDepartmentIdRequest request)
        {
            try
            {
                return await _humanResourceRepository.GetDepartmentCoursesByDepartmentId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<AddOrEditTrainingResponse> AddOrEditTraining(ServiceRequest<AddOrEditTrainingRequest> request)
        {
            //try
            //{
            //    await _sendMailService.SendMailAsync(PrepareNotificationMail(request, EmailType.NewTrainingNotification));
            //    return await _humanResourceRepository.AddOrEditTraining(request);
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            var authorName = _dashboardRepository.GetUserDetailsById(request.Session.UserId).Result;
            var response = await _humanResourceRepository.AddOrEditTraining(request);
            if (response != null)
            {
                // await _sendMailService.SendMailAsync(PrepareMailForRecruitee(response, EmailType.PreRecruiteeNotification));
                await _sendMailService.SendMailAsync(PrepareMailNotificationByResponse(response, EmailType.NewTrainingNotification, authorName));
            }

            return response;
        }

        public async Task<ServiceSearchResponse<GetAllTrainingsResponse>> GetAllTrainings(ServiceSearchRequest<GetAllTrainingsRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetAllTrainings(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<GetTrainingByIdResponse> GetTrainingById(ServiceRequest<GetTrainingByIdRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetTrainingById(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteTrainingById(ServiceRequest<DeleteTrainingByIdRequest> request)
        {
            try
            {
                await _humanResourceRepository.DeleteTrainingById(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ServiceSearchResponse<GetInstructorByTrainingDropDownListResponse>> GetInstructorByTrainingDropDownList(ServiceSearchRequest<GetInstructorByTrainingDropDownListRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetInstructorByTrainingDropDownList(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ServiceSearchResponse<GetEmployeeTrainingDropDownListResponse>> GetEmployeeTrainingDropDownList(ServiceSearchRequest<GetEmployeeTrainingDropDownListRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetEmployeeTrainingDropDownList(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ServiceSearchResponse<GetCourseByTrainingDropDownListResponse>> GetCourseByTrainingDropDownList(ServiceSearchRequest<GetCourseByTrainingDropDownListRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetCourseByTrainingDropDownList(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ServiceSearchResponse<GetHoursByTrainingDropDownListResponse>> GetHoursByTrainingDropDownList(ServiceSearchRequest<GetHoursByTrainingDropDownListRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetHoursByTrainingDropDownList(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ServiceSearchResponse<GetTrainingDetailsOfCourseByCourseIdResponse>> GetTrainingDetailsOfCourseByCourseId(ServiceSearchRequest<GetTrainingDetailsOfCourseByCourseIdRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetTrainingDetailsOfCourseByCourseId(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ServiceSearchResponse<GetTrainingDetailsOfInstructorByInstructorNameResponse>> GetTrainingDetailsOfInstructorByInstructorName(ServiceSearchRequest<GetTrainingDetailsOfInstructorByInstructorNameRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetTrainingDetailsOfInstructorByInstructorName(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ServiceSearchResponse<GetTrainingDetailsOfEmployeeByEmployeeIdResponse>> GetTrainingDetailsOfEmployeeByEmployeeId(ServiceSearchRequest<GetTrainingDetailsOfEmployeeByEmployeeIdRequest> request)
        {
            try
            {
                return await _humanResourceRepository.GetTrainingDetailsOfEmployeeByEmployeeId(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SendTrainingNotification(ServiceRequest<TrainingNotificationRequest> request)
        {
            try
            {
                await _sendMailService.SendMailAsync(PrepareNotificationMail(request, EmailType.TrainingScheduleNotification));
                await _humanResourceRepository.SendTrainingNotification(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private SendMail PrepareNotificationMail<T>(ServiceRequest<T> response, EmailType emailType)
        {
            var subject = string.Empty;
            var email = string.Empty;
            var body = string.Empty;
            var isHtml = true;
            var supervisorEmail = string.Empty;
            var ccEmail = string.Empty;
            LinkedResource img = null;
            switch (emailType)
            {
                case EmailType.NewEmployeeNotification:
                    var data = response.Data as NewEmployeeNotificationRequest;
                    subject = "New Member !!";
                    Guid guid = Guid.NewGuid();
                    email = _appSetting.DefaultSendToEmail;
                    body = $"<h4>Hello All,</h4>" +
                        $"<p>Please join me in welcoming new member to S.S.White Family!</p>";
                    if (data.Image != null)
                    {
                        img = new LinkedResource($"{_appSetting.AvatarRootPathToStoreImage}{data.ImagePath}", data.Image.ContentType)
                        {
                            ContentId = guid.ToString()
                        };
                        body += $"<img src=cid:{img.ContentId} id='img' style='width:200px;height:200px;'>";
                    }

                    body = body + $"<p>{data.Name} - {data.Designation}</p>" +
                    $"<p>His hobbies are : {data.Hobbies} </p>" +
                    $"<p>He will be joining the team from {data.JoiningDate:dd-MMM-yyyy} as {data.Designation}, {data.Department} and will be reporting to {data.ReportingTo}." +
                    $"</p>Please Welcome {data.Name} !!";
                    isHtml = true;
                    break;
                case EmailType.TrainingScheduleNotification:
                    var trainingNotificationRequest = response.Data as TrainingNotificationRequest;
                    var date = trainingNotificationRequest.Date;
                    subject = $"Training On: {trainingNotificationRequest.CourseName} at {trainingNotificationRequest.Location} " +
                        $"On {date.DayOfWeek} {date:dd-MMM-yyyy} {trainingNotificationRequest.Time} by {trainingNotificationRequest.Instructor}";
                    email = string.Join(",", trainingNotificationRequest.EmailAddress);
                    body = $"<span style = 'font-family:Arial;font-size:10pt'><h4>Hello All,</h4> " +
                        $"<br/>We will be holding training on <b>{date.DayOfWeek} {date:dd-MMM-yyyy} {trainingNotificationRequest.Time}</b> at <b>{trainingNotificationRequest.Location}</b> " +
                        $"on <b>{trainingNotificationRequest.Description} </b>" +
                        $"by <b>{trainingNotificationRequest.Instructor}</b></span>";
                    isHtml = true;
                    break;
                case EmailType.GatePassNotification:
                    var gatePassData = response.Data as AddGatepassRequest;
                    var getEmployeeDetails = gatePassData.PersonId;

                    // Why this call is here it should be in some where common as it is repeating many times and pass the email as parameter in this method.
                    var employeeGatepassDetails = _adminRepository.GetEmployeeDetailsById(getEmployeeDetails);
                    subject = $"Gatepass generated for - [{gatePassData.EmployeeName} - {gatePassData.PersonId}]";
                    email = employeeGatepassDetails.Result.SupervisorEmail;
                    body = $"<span style = 'font-family:Arial;'><h4>Hello All,</h4>" +
                        "<p>Gatepass has been generated for the following details:</p>" +
                    $"<p><b>Employee Type:</b> {gatePassData.EmployeeTypeValue}</p>" +
                    $"<p><b>Employee Name:</b> {gatePassData.EmployeeName} </p>" +
                    $"<p><b>Agency Name:</b> {gatePassData.ContractAgency ?? "N/A"} </p>" +
                    $"<p><b>Employee Id:</b> {gatePassData.PersonId} </p>" +
                    $"<p><b>Department:</b> {gatePassData.Department} </p>" +
                    $"<p><b>Designation:</b> {gatePassData.Designation} </p>" +
                    $"<p><b>Mobile No:</b> {gatePassData.MobileNo} </p>" +
                    $"<p><b>Purpose:</b> {gatePassData.Purpose} </p>" +
                    $"<p><b>Time In:</b> {gatePassData.TimeIn} </p>" +
                    $"<p><b>Time Out:</b> {gatePassData.TimeOut} </p></span>";
                    isHtml = true;
                    ccEmail = "amit@aimbsystem.com";
                    break;
                case EmailType.LeaveSheetNotification:
                    var leaveSheetData = response.Data as CreateNewLeaveSheetRequest;
                    var getEmployeeLeavesheetDetails = leaveSheetData.EmployeeId;

                    // Why this call is here it should be in some where common as it is repeating many times and pass the email as parameter in this method.
                    var employeeLeaveDetails = _adminRepository.GetEmployeeDetailsById(getEmployeeLeavesheetDetails);
                    subject = $"Leave Request Notification - [{employeeLeaveDetails.Result.EmployeeName} - {leaveSheetData.EmployeeId}]";
                    email = employeeLeaveDetails.Result.SupervisorEmail;
                    body = leaveSheetData.EmployeeTypeId == 2 ?
                            PrepareLeaveSheetHtmlBodyForPayrollEmployee(leaveSheetData, employeeLeaveDetails)
                            : PrepareLeaveSheetHtmlBodyForContractEmployee(leaveSheetData, employeeLeaveDetails);
                    isHtml = true;
                    ccEmail = "amit@aimbsystem.com";
                    break;
                //case EmailType.NewTrainingNotification:
                //    AddOrEditTrainingRequest trainingData;
                //    UserDetail authorName;
                //    GetAttendanceDetailsForNotification(response, out supervisorEmail, out trainingData, out authorName);
                //    subject = $"Notice of New Training Scheduled for {trainingData.DepartmentName} Department Employees";
                //    //email = supervisorEmail;
                //    email = "amit@aimbsystem.com";
                //    body = PrepareAttendanceTrainingHtmlBody(trainingData, authorName);
                //    isHtml = true;
                //    //ccEmail = "bsr2fo1g0u@superblohey.com";
                //    ccEmail = "nehal.parmar2202@gmail.com";
                //    break;
            }

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email,
                LinkedResource = img,
                CCEmails = ccEmail
            };
        }

        private void GetAttendanceDetailsForNotification<T>(ServiceRequest<T> response, out string supervisorEmail, out AddOrEditTrainingRequest trainingData, out UserDetail authorName)
        {
            trainingData = response.Data as AddOrEditTrainingRequest;
            var supervisor = _adminRepository.GetEmployeeDetailsById(trainingData.EmployeeTraining.First().EmployeeId);
            authorName = _dashboardRepository.GetUserDetailsById(response.Session.UserId).Result;
            supervisorEmail = supervisor.Result.SupervisorEmail;
        }

        private static string PrepareAttendanceTrainingHtmlBody(Task<GetTrainingByIdResponse> trainingData, UserDetail authorName)
        {
            var htmlFileContent = $"<span style = 'font-family:Arial;'><h4>Hello All,</h4>" +
                    "<p>The details for the next scheduled training are provided below:</p>" +
                $"<p><b><u>Training Details</u></b></p>" +
                $"<p><b>Department:</b> {trainingData.Result.DepartmentName} </p>" +
                $"<p><b>Course Title:</b> {trainingData.Result.DepartmentCourseName} </p>" +
                $"<p><b>Instructor:</b> {trainingData.Result.Instructor} </p>" +
                $"<p><b>From Date:</b> {DateTime.Parse(trainingData.Result.FromDate):dd-MM-yyyy} </p>" +
                $"<p><b>To Date:</b> {DateTime.Parse(trainingData.Result.ToDate):dd-MM-yyyy} </p>" +
                $"<p><b>Hours:</b> {trainingData.Result.Hours:0.##}</p>" +
                $"<p><b>Entered By:</b> {trainingData.Result.EnteredBy} </p>" +
                $"<p><b>Location:</b> {trainingData.Result.Location} </p>" +
                $"<p><b>Author Name:</b> {authorName.FirstName + " " + authorName.LastName} </p>" +
                $"<p><b>Summary of Content:</b> {trainingData.Result.SummaryOfContent} </p></span>";
            string attendanceTableHeaderHTML = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"EmployeeTrainingDetails.html"))
            {
                attendanceTableHeaderHTML = file.ReadToEnd();
            }

            attendanceTableHeaderHTML = string.Concat(htmlFileContent, attendanceTableHeaderHTML);

            string attendanceTableItemContent = string.Empty;
            foreach (var item in trainingData.Result.EmployeeTraining)
            {
                string attendanceTableItemDetails = string.Empty;
                if (item.JobDescriptionImage != null && item.JobDescriptionImage.Length > 0)
                {
                    item.JobDescriptionImageName = ExtractFileName(item.JobDescriptionImage);
                }

                if (item.TrainingHardCopyImage != null && item.TrainingHardCopyImage.Length > 0)
                {
                     item.TrainingHardCopyImageName = ExtractFileName(item.TrainingHardCopyImage);
                }

                using (System.IO.StreamReader file = new System.IO.StreamReader(@"EmployeeTrainingItemsDetails.html"))
                {
                    attendanceTableItemDetails = file.ReadToEnd();
                }

                attendanceTableItemDetails = attendanceTableItemDetails
                                                .Replace("{EmployeeId}", item.PersonID)
                                                .Replace("{Name}", item.PersonName)
                                                .Replace("{TrainingType}", item.TrainingType)
                                                .Replace("{EvaluationDate}", (item.EvaluationDate != null) ? DateTime.Parse(item.EvaluationDate).ToString("dd-MM-yyyy") : "N/A")
                                                //.Replace("{EvaluationDate}", item.EvaluationDate?.ToString("dd-MM-yyyy") ?? "N/A")
                                                .Replace("{EvaluationMethod}", item.EvaluationMethod ?? "N/A")
                                                .Replace("{JobDescription}", item.JobDescriptionImageName ?? "N/A")
                                                .Replace("{TrainingHardCopy}", item.TrainingHardCopyImageName ?? "N/A");
                attendanceTableItemContent = string.Concat(attendanceTableItemContent, attendanceTableItemDetails);
            }

            attendanceTableItemContent += "</tbody></table> </div></div></body></html>";
            htmlFileContent = string.Concat(attendanceTableHeaderHTML, attendanceTableItemContent);
            return htmlFileContent;
        }

        private string PrepareLeaveSheetHtmlBodyForContractEmployee(CreateNewLeaveSheetRequest leaveSheetData, Task<GetEmployeeByEmployeeIdResponse> employeeLeaveDetails)
        {
            return $"<span style = 'font-family:Arial;'><h4>Hello All,</h4>" +
                                 "<p>The details for the employee's leave are provided below:</p>" +
                                $"<p><b>Employee Name:</b> {employeeLeaveDetails.Result.EmployeeName} </p>" +
                                $"<p><b>Employee Id:</b> {leaveSheetData.EmployeeId} </p>" +
                                $"<p><b>Employee Type:</b> {leaveSheetData.EmployeeTypeValue}</p>" +
                                $"<p><b>Agency Name:</b> {leaveSheetData.ContractAgency} </p>" +
                                $"<p><b>Department:</b> {employeeLeaveDetails.Result.Department} </p>" +
                                $"<p><b>Designation:</b> {employeeLeaveDetails.Result.EmpPosition} </p>" +
                                $"<p><b>Mobile No:</b> {employeeLeaveDetails.Result.PhoneNo} </p>" +
                                $"<p><b>Leave Details:</b></p>" +
                                $"<p><b>Leave Apply Date:</b> {leaveSheetData.LeaveApplyDate.ToShortDateString()} </p>" +
                                $"<p><b>Reason for Leave:</b> {leaveSheetData.LeavePurpose} </p>" +
                                $"<p><b>Start Date:</b> {leaveSheetData.FromDate.ToShortDateString()} </p>" +
                                $"<p><b>End Date:</b> {leaveSheetData.ToDate.ToShortDateString()} </p>" +
                                $"<p><b>Number of Days:</b> {leaveSheetData.CountOfDays} </p>" +
                                $"<p><b>Note:</b> {leaveSheetData.Note ?? "N/A"} </p></span>";
        }

        private string PrepareLeaveSheetHtmlBodyForPayrollEmployee(CreateNewLeaveSheetRequest leaveSheetData, Task<GetEmployeeByEmployeeIdResponse> employeeLeaveDetails)
        {
            return $"<span style = 'font-family:Arial;'><h4>Hello All,</h4>" +
                                  "<p>The details for the employee's leave are provided below:</p>" +
                                $"<p><b>Employee Name:</b> {employeeLeaveDetails.Result.EmployeeName} </p>" +
                                $"<p><b>Employee Id:</b> {leaveSheetData.EmployeeId} </p>" +
                                $"<p><b>Employee Type:</b> {leaveSheetData.EmployeeTypeValue}</p>" +
                                $"<p><b>Department:</b> {employeeLeaveDetails.Result.Department} </p>" +
                                $"<p><b>Designation:</b> {employeeLeaveDetails.Result.EmpPosition} </p>" +
                                $"<p><b>Mobile No:</b> {employeeLeaveDetails.Result.PhoneNo} </p>" +
                                $"<p><b>Leave Details:</b></p>" +
                                $"<p><b>Type Of Leave:</b> {leaveSheetData.LeaveTypeValue} </p>" +
                                $"<p><b>Leave Apply Date:</b> {leaveSheetData.LeaveApplyDate.ToShortDateString()} </p>" +
                                $"<p><b>Reason for Leave:</b> {leaveSheetData.LeavePurpose} </p>" +
                                $"<p><b>Start Date:</b> {leaveSheetData.FromDate.ToShortDateString()} </p>" +
                                $"<p><b>End Date:</b> {leaveSheetData.ToDate.ToShortDateString()} </p>" +
                                $"<p><b>Number of Days:</b> {leaveSheetData.CountOfDays} </p>" +
                                $"<p><b>Address While On Leave:</b> {leaveSheetData.AddressWhileOnLeave} </p></span>";
        }

        private SendMail PrepareMailForRecruitee(AddOrEditRecruiteeResponse response)
        {
            var subject = string.Empty;
            var email = string.Empty;
            var ccEmail = string.Empty;
            var body = string.Empty;
            var isHtml = true;
            var htmlFileContent = string.Empty;

            var recruiteeData = _humanResourceRepository.GetRecruiteeDetailsById(response.Id);
            var permanentAddress = recruiteeData.Result.RecruiteeAddresses.FirstOrDefault(x => x.AddressType == 2);
            var temporaryAddress = recruiteeData.Result.RecruiteeAddresses.FirstOrDefault(x => x.AddressType == 1) ?? permanentAddress;
            var recruiteeName = recruiteeData.Result.FirstName + " " + recruiteeData.Result.SurName;
            subject = $"Pre Recuitement Process Notification - [{recruiteeName}]";
            email = "kazumi@sswhite.net";
            if (recruiteeData.Result.RecruiteeExperiences?.Any() == true)
            {
                body = PrepareRecruiteeHtmlBodyWithExperienceDetails(recruiteeData, recruiteeName, temporaryAddress, htmlFileContent);
            }
            else
            {
                body = PrepareRecruiteeHtmlBody(recruiteeData, temporaryAddress, recruiteeName);
            }

            isHtml = true;
            ccEmail = "amit@aimbsystem.com";

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email,
                CCEmails = ccEmail
            };
        }

        private SendMail PrepareMailNotificationByResponse(AddOrEditTrainingResponse response, EmailType emailType, UserDetail authorName)
        {
            var subject = string.Empty;
            var email = string.Empty;
            var ccEmail = string.Empty;
            var body = string.Empty;
            var isHtml = true;
            switch (emailType)
            {
                case EmailType.NewTrainingNotification:
                    var trainingResponse = response as AddOrEditTrainingResponse;
                    var trainingData = _humanResourceRepository.GetTrainingDetailsByTrainingId(trainingResponse.Id);
                    var attendanceDetails = _adminRepository.GetEmployeeDetailsById(trainingData.Result.EmployeeTraining.First().PersonID);
                    subject = $"Notice of New Training Scheduled for {trainingData.Result.DepartmentName} Department Employees";
                    email = attendanceDetails.Result.SupervisorEmail;
                    body = PrepareAttendanceTrainingHtmlBody(trainingData, authorName);
                    isHtml = true;
                    ccEmail = "amit@aimbsystem.com";
                    break;
            }

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email,
                CCEmails = ccEmail
            };
        }

        private static string PrepareRecruiteeHtmlBody(Task<GetRecruiteeByIdResponse> recruiteeData, RecruiteeAddresses temporaryAddress, string recruiteeName)
        {
            return $"<span style = 'font-family:Arial;'><h4>Hello All,</h4>" +
                            "<p>The details for the Recruitee are provided below:</p>" +
                           $"<p><b><u>Personal Details</u></b></p>" +
                           $"<p><b>Recuitee Name:</b> {recruiteeName} </p>" +
                           $"<p><b>Date Of Birth:</b> {recruiteeData.Result.DOB.ToShortDateString()}</p>" +
                           $"<p><b>Gender:</b> {recruiteeData.Result.Gender} </p>" +
                           $"<p><b>Marital Status:</b> {recruiteeData.Result.MaritalStatus} </p>" +
                           $"<p><b>Mobile 1:</b> {temporaryAddress.MobileNumber1} </p>" +
                           $"<p><b>Mobile 2:</b> {temporaryAddress.MobileNumber2} </p>" +
                           $"<p><b>Email:</b> {temporaryAddress.Email} </p></span>";
        }

        private static string PrepareRecruiteeHtmlBodyWithExperienceDetails(Task<GetRecruiteeByIdResponse> recruiteeData, string recruiteeName, RecruiteeAddresses temporaryAddress, string htmlFileContent)
        {
            htmlFileContent = PrepareRecruiteeHtmlBody(recruiteeData, temporaryAddress, recruiteeName);
            string recruiteeExperienceTableHeaderHTML = string.Empty;
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"RecruiteeFormExperienceDetailsForNotification.html"))
            {
                recruiteeExperienceTableHeaderHTML = file.ReadToEnd();
            }

            recruiteeExperienceTableHeaderHTML = string.Concat(htmlFileContent, recruiteeExperienceTableHeaderHTML);

            string recruiteeExperienceItemFileContent = string.Empty;
            foreach (var item in recruiteeData.Result.RecruiteeExperiences)
            {
                string recruiteeEductionItemFileContent = string.Empty;
                using (System.IO.StreamReader file = new System.IO.StreamReader(@"RecruiteeFormExperienceItemsDetails.html"))
                {
                    recruiteeEductionItemFileContent = file.ReadToEnd();
                }

                recruiteeEductionItemFileContent = recruiteeEductionItemFileContent
                                                   .Replace("{Name}", item.CompanyName)
                                                   .Replace("{Address}", item.CompanyAddress)
                                                   .Replace("{DesignationResponsibility}", item.Designation)
                                                   .Replace("{From}", item.FromDate.ToString("MM-yyyy"))
                                                   .Replace("{To}", item.ToDate.ToString("MM-yyyy"))
                                                   .Replace("{Salary}", item.MonthlySalary.ToString());

                recruiteeExperienceItemFileContent = string.Concat(recruiteeExperienceItemFileContent, recruiteeEductionItemFileContent);
            }

            recruiteeExperienceItemFileContent += "</tbody></table> </div></div></body></html>";
            htmlFileContent = string.Concat(recruiteeExperienceTableHeaderHTML, recruiteeExperienceItemFileContent);
            return htmlFileContent;
        }

        public static string ExtractFileName(string input)
        {
            // Split the input string by underscore and take the last part
            string[] parts = input.Split('_');
            if (parts.Length > 1)
            {
                return string.Join("_", parts, 1, parts.Length - 1);
            }

            return string.Empty;
        }
    }
}
