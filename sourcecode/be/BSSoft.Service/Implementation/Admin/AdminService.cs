﻿namespace SSWhite.Service.Implementation.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Admin;
    using SSWhite.Data.Interface.Common;
    using SSWhite.Domain.Enums;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Request.HumanResource;
    using SSWhite.Domain.Response.Admin;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;
    using SSWhite.Service.Interface.Admin;

    public class AdminService : IAdminService
    {
        private readonly IAdminRepository _adminRepository;
        private readonly ICommonRepository _commonRepository;
        private readonly ISendMailService _sendMailService;
        private readonly AppSetting _appSetting;

        public AdminService(IAdminRepository adminRepository, ICommonRepository commonRepository, ISendMailService sendMailService, IOptions<AppSetting> appSetting)
        {
            _adminRepository = adminRepository;
            _commonRepository = commonRepository;
            _sendMailService = sendMailService;
            _appSetting = appSetting.Value;
        }

        public async Task AddOrEditEmployee(ServiceRequest<AddOrEditEmployeeRequest> request)
        {
            try
            {
                await _sendMailService.SendMailAsync(PrepareEmployeeNotificationMail(request, EmailType.AddNewEmployeeNotification));
                var response = await _adminRepository.AddOrEditEmployee(request);

                if (response != null)
                {
                    await _sendMailService.SendMailAsync(PrepareMailForNewUser(response));
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllEmployeeLimitsResponse>> GetAllEmployeeLimits(GetAllEmployeeLimitsRequest request)
        {
            return await _adminRepository.GetAllEmployeeLimits(request);
        }

        public async Task<GetEmployeeByEmployeeIdResponse> GetEmployeeByEmployeeId(GetEmployeeByEmployeeIdRequest request)
        {
            try
            {
                var response = await _adminRepository.GetEmployeeByEmployeeId(request);
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task UpdateEmployeeLimit(UpdateEmployeeLimitByIdRequest updateEmployeeLimitByIdRequest)
        {
            await _adminRepository.UpdateEmployeeLimit(updateEmployeeLimitByIdRequest);
        }

        public async Task ChangeEmployeeStatus(Task<ServiceRequest<ChangeEmployeeStatusRequest>> request)
        {
            await _adminRepository.ChangeEmployeeStatus(request);
        }

        public async Task<CheckIfEmployeeIdExistsResponse> CheckIfEmployeeIdExists(CheckIfEmployeeIdExistsRequest checkIfEmployeeIdExistsRequest)
        {
            return await _adminRepository.CheckIfEmployeeIdExists(checkIfEmployeeIdExistsRequest);
        }

        public async Task<CheckIfEmailAddressExistsResponse> CheckIfEmailAddressExists(CheckIfEmailAddressExistsRequest request)
        {
            return await _adminRepository.CheckIfEmailAddressExists(request);
        }

        public async Task<ServiceSearchResponse<GetDocumentApprovalListResponse>> GetDocumentApprovals(ServiceSearchRequest<GetDocumentApprovalListRequest> request)
        {
            try
            {
                return await _adminRepository.GetDocumentApprovals(request);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task UpdateDocumentApprovalList(List<UpdateDocumentApprovalListRequest> request)
        {
            await _adminRepository.UpdateDocumentApprovalList(request);
        }

        public async Task<List<GetUserRightsResponse>> GetUserRights(ServiceRequest request)
        {
            try
            {
                return await _adminRepository.GetUserRights(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetRolesResponse>> GetRoles()
        {
            try
            {
                return await _adminRepository.GetRoles();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<List<GetRightsAccessByRoleIdResponse>> GetRightsAccessByRoleId(GetRightsAccessByRoleIdRequest request)
        {
            try
            {
                return await _adminRepository.GetRightsAccessByRoleId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task UpdateRightsAccessByRoleId(UpdateRightsAccessByRoleIdRequest request)
        {
            try
            {
                await _adminRepository.UpdateRightsAccessByRoleId(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task ChangeEmployeeDepartment(ServiceRequest<ChangeEmployeeDepartmentRequest> request)
        {
            try
            {
                await _sendMailService.SendMailAsync(PrepareEmployeeNotificationMail(request, EmailType.ChangeDepartmentNotification));
                await _adminRepository.ChangeEmployeeDepartment(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private SendMail PrepareMailForNewUser(AddOrEditEmployeeResponse response)
        {
            var subject = string.Empty;
            var email = string.Empty;
            var body = string.Empty;
            var isHtml = true;

            subject = "*** New User Created #  - ";
            email = response.EmailAddress;
            body = $"<span style = 'font-family:Arial;font-size:10pt'>" +
                $"</strong></p><hr style='border-top:1px dashed black; width:330px; margin-left: 0.5px;'>" +
                ////$" <p><strong>DocumentId:</strong>  { response.DocumentId }" +
                $"<br /><strong> UserId :</strong> {response.Loginid}" +
                $" <br /> <strong>EmailAddress: </strong> {response.EmailAddress}" +
                $" <br /> <strong>Password: </strong> {_appSetting.CreateUserPassword}" +
                $"<br /> <strong>Change the password from dashboard </strong> </p> <br/>" +
                $" </span>";

            isHtml = true;

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = email
            };
        }

        private SendMail PrepareEmployeeNotificationMail<T>(ServiceRequest<T> response, EmailType emailType)
        {
            var subject = string.Empty;
            var toEmail = string.Empty;
            var ccEmail = string.Empty;
            var body = string.Empty;
            var getSupervisorEmail = string.Empty;
            var isHtml = true;
            LinkedResource img = null;
            switch (emailType)
            {
                case EmailType.ChangeDepartmentNotification:
                    var changeDepartmentNotificationRequest = response.Data as ChangeEmployeeDepartmentRequest;
                    var getEmployeeDetails = changeDepartmentNotificationRequest.EmployeeId;
                    var employeeDetails = _adminRepository.GetEmployeeDetailsById(getEmployeeDetails);
                    subject = $" Department Change For - [{employeeDetails.Result.EmployeeName} - {employeeDetails.Result.EmployeeId}] ";
                    toEmail = $"{employeeDetails.Result.SupervisorEmail},kazumi@sswhite.net";
                    body = $"<span style = 'font-family:Arial;font-size:10pt'><h4>Hello All,</h4> " +
                        $"<br/>Department of <b>{employeeDetails.Result.EmployeeName} - {employeeDetails.Result.EmployeeId}</b> is changed from <b>{changeDepartmentNotificationRequest.CurrentDepartment}</b> " +
                        $"to <b>{changeDepartmentNotificationRequest.NewDepartment}</b>. " +
                        $"<br/>Reason for change: {changeDepartmentNotificationRequest.ReasonForChangeDepartment}.</span>";
                    ccEmail = "amit@aimbsystem.com";
                    isHtml = true;
                    break;

                case EmailType.AddNewEmployeeNotification:
                    var addNewEmployeeNotificationRequest = response.Data as AddOrEditEmployeeRequest;
                    var employeeName = addNewEmployeeNotificationRequest.FirstName + " " + addNewEmployeeNotificationRequest.LastName;
                    var getSupervisorDetails = _commonRepository.GetReportingUnderDetailsByName(addNewEmployeeNotificationRequest.ReportingUnder);
                    if (getSupervisorDetails != null && getSupervisorDetails.Result.Any())
                    {
                        getSupervisorEmail = getSupervisorDetails.Result.First().DepartmentSupervisorEmail;
                    }

                    subject = $" Add New Employee Notification - [{employeeName} - {addNewEmployeeNotificationRequest.PersonID}] ";
                    toEmail = $"{getSupervisorEmail},kazumi@sswhite.net";
                    body = $"<span style = 'font-family:Arial;'><h4>Hello All,</h4>" +
                            "<p>The newly added employee's information is listed below:</p>" +
                           $"<p><b><u>Personal Details</u></b></p>" +
                    $"<p><b>Employee Name:</b> {employeeName} </p>" +
                    $"<p><b>EmployeeId:</b> {addNewEmployeeNotificationRequest.PersonID} </p>" +
                    $"<p><b>Date Of Birth:</b> {addNewEmployeeNotificationRequest.DOB.ToShortDateString()}</p>" +
                    $"<p><b>Gender:</b> {addNewEmployeeNotificationRequest.GenderValue} </p>" +
                    $"<p><b>Marital Status:</b> {addNewEmployeeNotificationRequest.MaritalStatus} </p>" +
                    $"<p><b>Mobile:</b> {addNewEmployeeNotificationRequest.PhoneNo} </p>" +
                    $"<p><b>Email:</b> {addNewEmployeeNotificationRequest.EmailAddress} </p> " +
                    $"<p><b>Date Of Joining:</b> {addNewEmployeeNotificationRequest.DOJ.ToShortDateString()} </p>" +
                    $"<p><b>Role:</b> {addNewEmployeeNotificationRequest.RoleName} </p>" +
                    $"<p><b>Designation:</b> {addNewEmployeeNotificationRequest.EmpPosition} </p>" +
                    $"<p><b>Organization:</b> {addNewEmployeeNotificationRequest.Organization} </p>" +
                    $"<p><b>Reporting Under:</b> {addNewEmployeeNotificationRequest.ReportingUnder} </p>" +
                    $"<p><b>Department:</b> {addNewEmployeeNotificationRequest.Department} </p>" +
                    $"<p><b>Buddy:</b> {addNewEmployeeNotificationRequest.Buddy} </p>" +
                    $"</span>";

                    ccEmail = "amit@aimbsystem.com";
                    isHtml = true;
                    break;
            }

            return new SendMail()
            {
                Subject = subject,
                Body = body,
                IsHtml = isHtml,
                ToEmails = toEmail,
                CCEmails = ccEmail,
                LinkedResource = img
            };
        }
    }
}
