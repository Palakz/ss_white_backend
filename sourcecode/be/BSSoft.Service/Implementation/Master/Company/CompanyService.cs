﻿namespace SSWhite.Service.Implementation.Master.Company
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Company;
    using SSWhite.Domain.Common.Master.Company;
    using SSWhite.Domain.Request.Master.Company;
    using SSWhite.Domain.Response.Master.Company;
    using SSWhite.Service.Interface.Master.Company;

    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository _companyRepository;

        public CompanyService(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        public async Task<ServiceResponse<int>> AddEditCompany(ServiceRequest<Company> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _companyRepository.AddEditCompany(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeCompanyStatus(ServiceRequest<int> request)
        {
            try
            {
                await _companyRepository.ChangeCompanyStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteCompany(ServiceRequest<int> request)
        {
            try
            {
                await _companyRepository.DeleteCompany(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllCompaniesResponse>> GetAllCompanies(ServiceSearchRequest<GetAllCompanies> request)
        {
            var response = new ServiceSearchResponse<GetAllCompaniesResponse>();
            try
            {
                response = await _companyRepository.GetAllCompanies(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<GetCompanyByIdResponse>> GetCompanyById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<GetCompanyByIdResponse>();
            try
            {
                response.Data = await _companyRepository.GetCompanyById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
