﻿namespace SSWhite.Service.Implementation.Master.Country
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Country;
    using SSWhite.Domain.Common.Master.Country;
    using SSWhite.Domain.Request.Master.Country;
    using SSWhite.Domain.Response.Master.Country;
    using SSWhite.Service.Interface.Master.Country;

    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;

        public CountryService(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
        }

        public async Task<ServiceResponse<int>> AddEditCountry(ServiceRequest<Country> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _countryRepository.AddEditCountry(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeCountryStatus(ServiceRequest<int> request)
        {
            try
            {
                await _countryRepository.ChangeCountryStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteCountry(ServiceRequest<int> request)
        {
            try
            {
                await _countryRepository.DeleteCountry(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllCountriesResponse>> GetAllCountries(ServiceSearchRequest<GetAllCountries> request)
        {
            var response = new ServiceSearchResponse<GetAllCountriesResponse>();
            try
            {
                response = await _countryRepository.GetAllCountries(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Country>> GetCountryById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Country>();
            try
            {
                response.Data = await _countryRepository.GetCountryById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
