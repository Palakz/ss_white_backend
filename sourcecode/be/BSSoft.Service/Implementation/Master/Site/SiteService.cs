﻿namespace SSWhite.Service.Implementation.Master.Site
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Site;
    using SSWhite.Domain.Common.Master.Site;
    using SSWhite.Domain.Request.Master.Site;
    using SSWhite.Domain.Response.Master.Site;
    using SSWhite.Service.Interface.Master.Site;

    public class SiteService : ISiteService
    {
        private readonly ISiteRepository _siteRepository;

        public SiteService(ISiteRepository siteRepository)
        {
            _siteRepository = siteRepository;
        }

        public async Task<ServiceResponse<int>> AddEditSite(ServiceRequest<Site> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _siteRepository.AddEditSite(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeSiteStatus(ServiceRequest<int> request)
        {
            try
            {
                await _siteRepository.ChangeSiteStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteSite(ServiceRequest<int> request)
        {
            try
            {
                await _siteRepository.DeleteSite(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllSitesResponse>> GetAllSites(ServiceSearchRequest<GetAllSites> request)
        {
            var response = new ServiceSearchResponse<GetAllSitesResponse>();
            try
            {
                response = await _siteRepository.GetAllSites(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Site>> GetSiteById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Site>();
            try
            {
                response.Data = await _siteRepository.GetSiteById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
