﻿namespace SSWhite.Service.Implementation.Master.Crate
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Crate;
    using SSWhite.Domain.Common.Master.Crate;
    using SSWhite.Domain.Request.Master.Crate;
    using SSWhite.Domain.Response.Master.Crate;
    using SSWhite.Service.Interface.Master.Crate;

    public class CrateService : ICrateService
    {
        private readonly ICrateRepository _crateRepository;

        public CrateService(ICrateRepository crateRepository)
        {
            _crateRepository = crateRepository;
        }

        public async Task<ServiceResponse<int>> AddEditCrate(ServiceRequest<Crate> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _crateRepository.AddEditCrate(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeCrateStatus(ServiceRequest<int> request)
        {
            try
            {
                await _crateRepository.ChangeCrateStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteCrate(ServiceRequest<int> request)
        {
            try
            {
                await _crateRepository.DeleteCrate(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllCratesResponse>> GetAllCrates(ServiceSearchRequest<GetAllCrates> request)
        {
            var response = new ServiceSearchResponse<GetAllCratesResponse>();
            try
            {
                response = await _crateRepository.GetAllCrates(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Crate>> GetCrateById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Crate>();
            try
            {
                response.Data = await _crateRepository.GetCrateById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
