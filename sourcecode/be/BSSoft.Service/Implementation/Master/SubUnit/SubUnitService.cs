﻿namespace SSWhite.Service.Implementation.Master.SubUnit
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.SubUnit;
    using SSWhite.Domain.Common.Master.SubUnit;
    using SSWhite.Domain.Request.Master.SubUnit;
    using SSWhite.Domain.Response.Master.SubUnit;
    using SSWhite.Service.Interface.Master.SubUnit;

    public class SubUnitService : ISubUnitService
    {
        private readonly ISubUnitRepository _subUnitRepository;

        public SubUnitService(ISubUnitRepository subUnitRepository)
        {
            _subUnitRepository = subUnitRepository;
        }

        public async Task<ServiceResponse<int>> AddEditSubUnit(ServiceRequest<SubUnit> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _subUnitRepository.AddEditSubUnit(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeSubUnitStatus(ServiceRequest<int> request)
        {
            try
            {
                await _subUnitRepository.ChangeSubUnitStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteSubUnit(ServiceRequest<int> request)
        {
            try
            {
                await _subUnitRepository.DeleteSubUnit(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllSubUnitsResponse>> GetAllSubUnits(ServiceSearchRequest<GetAllSubUnits> request)
        {
            var response = new ServiceSearchResponse<GetAllSubUnitsResponse>();
            try
            {
                response = await _subUnitRepository.GetAllSubUnits(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<SubUnit>> GetSubUnitById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<SubUnit>();
            try
            {
                response.Data = await _subUnitRepository.GetSubUnitById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
