﻿namespace SSWhite.Service.Implementation.Master.ItemType
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.ItemType;
    using SSWhite.Domain.Common.Master.ItemType;
    using SSWhite.Domain.Request.Master.ItemTypes;
    using SSWhite.Domain.Response.Master.ItemType;
    using SSWhite.Service.Interface.Master.ItemType;

    public class ItemTypeService : IItemTypeService
    {
        private readonly IItemTypeRepository _itemTypeRepository;

        public ItemTypeService(IItemTypeRepository itemTypeRepository)
        {
            _itemTypeRepository = itemTypeRepository;
        }

        public async Task<ServiceResponse<int>> AddEditItemType(ServiceRequest<ItemType> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _itemTypeRepository.AddEditItemType(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeItemTypeStatus(ServiceRequest<int> request)
        {
            try
            {
                await _itemTypeRepository.ChangeItemTypeStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteItemType(ServiceRequest<int> request)
        {
            try
            {
                await _itemTypeRepository.DeleteItemType(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllItemTypesResponse>> GetAllItemTypes(ServiceSearchRequest<GetAllItemTypes> request)
        {
            var response = new ServiceSearchResponse<GetAllItemTypesResponse>();
            try
            {
                response = await _itemTypeRepository.GetAllItemTypes(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<ItemType>> GetItemTypeById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<ItemType>();
            try
            {
                response.Data = await _itemTypeRepository.GetItemTypeById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
