﻿namespace SSWhite.Service.Implementation.Master.Tax
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.Tax;
    using SSWhite.Domain.Common.Master.Tax;
    using SSWhite.Domain.Request.Master.Tax;
    using SSWhite.Domain.Response.Master.Tax;
    using SSWhite.Service.Interface.Master.Tax;

    public class TaxService : ITaxService
    {
        private readonly ITaxRepository _taxRepository;

        public TaxService(ITaxRepository taxRepository)
        {
            _taxRepository = taxRepository;
        }

        public async Task<ServiceResponse<int>> AddEditTax(ServiceRequest<Tax> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _taxRepository.AddEditTax(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeTaxStatus(ServiceRequest<int> request)
        {
            try
            {
                await _taxRepository.ChangeTaxStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteTax(ServiceRequest<int> request)
        {
            try
            {
                await _taxRepository.DeleteTax(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllTaxesResponse>> GetAllTaxes(ServiceSearchRequest<GetAllTaxes> request)
        {
            var response = new ServiceSearchResponse<GetAllTaxesResponse>();
            try
            {
                response = await _taxRepository.GetAllTaxes(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<Tax>> GetTaxById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<Tax>();
            try
            {
                response.Data = await _taxRepository.GetTaxById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
