﻿namespace SSWhite.Service.Implementation.Master.ItemRate
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Data.Interface.Master.ItemRate;
    using SSWhite.Domain.Common.Master.ItemRate;
    using SSWhite.Domain.Request.Master.ItemRate;
    using SSWhite.Domain.Response.Master.ItemRate;
    using SSWhite.Service.Interface.Master.ItemRate;

    public class ItemRateService : IItemRateService
    {
        private readonly IItemRateRepository _itemRateRepository;

        public ItemRateService(IItemRateRepository itemRateRepository)
        {
            _itemRateRepository = itemRateRepository;
        }

        public async Task<ServiceResponse<int>> AddEditItemRate(ServiceRequest<ItemRate> request)
        {
            var response = new ServiceResponse<int>();
            try
            {
                response.Data = await _itemRateRepository.AddEditItemRate(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task ChangeItemRateStatus(ServiceRequest<int> request)
        {
            try
            {
                await _itemRateRepository.ChangeItemRateStatus(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task DeleteItemRate(ServiceRequest<int> request)
        {
            try
            {
                await _itemRateRepository.DeleteItemRate(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public async Task<ServiceSearchResponse<GetAllItemRatesResponse>> GetAllItemRates(ServiceSearchRequest<GetAllItemRates> request)
        {
            var response = new ServiceSearchResponse<GetAllItemRatesResponse>();
            try
            {
                response = await _itemRateRepository.GetAllItemRates(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }

        public async Task<ServiceResponse<ItemRate>> GetItemRateById(ServiceRequest<int> request)
        {
            var response = new ServiceResponse<ItemRate>();
            try
            {
                response.Data = await _itemRateRepository.GetItemRateById(request);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return response;
        }
    }
}
