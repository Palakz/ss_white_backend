﻿namespace SSWhite.Service.Validator.Account
{
    using System;
    using FluentValidation;
    using SSWhite.Domain.Request.TimeAndAttendance;

    public class GetInOutDetailsByUserIdRequestValidator : AbstractValidator<GetInOutDetailsByUserIdRequest>
    {
        public GetInOutDetailsByUserIdRequestValidator()
        {
            RuleFor(x => x.StartDate)
                .NotEmpty().WithMessage("StartDate Is Required");

            RuleFor(x => x.EndDate)
                .NotEmpty().WithMessage("EndDate Is Required");

            RuleFor(x => x).Must(x => x.EndDate == default(DateTime) || x.StartDate == default(DateTime) || x.EndDate > x.StartDate)
        .WithMessage("EndTime must greater than StartTime");
        }
    }
}
