﻿namespace SSWhite.Service.Validator.ExpensePurchaseOrder
{
    using FluentValidation;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.ResourceFile;

    public class AddOrEditEpoRequestValidator : AbstractValidator<AddOrEditEpoRequest>
    {
        public AddOrEditEpoRequestValidator()
        {
            RuleFor(x => x.Street1)
                .NotEmpty().WithMessage("Street1 is required")
                .MinimumLength(3)
               .MaximumLength(200);

            RuleFor(x => x.Street2)
                .MinimumLength(3)
               .MaximumLength(200);

            RuleFor(x => x.Country)
               .NotNull().WithMessage(string.Format("Country is required"))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, "Country", 0));

            RuleFor(x => x.State)
                .MinimumLength(3)
               .MaximumLength(50);

            RuleFor(x => x.City)
                .NotEmpty().WithMessage("City is required")
                .MinimumLength(3)
               .MaximumLength(50);

            RuleFor(x => x.Zip)
                .NotEmpty().WithMessage("Zip is required")
                .MinimumLength(3)
               .MaximumLength(10);

            RuleFor(x => x.Phone)
                .NotEmpty().WithMessage("Phone is required")
                .MinimumLength(6)
                .MaximumLength(10);

            RuleFor(x => x.GstNo)
                .MinimumLength(15)
               .MaximumLength(15);

            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Email Address Is Required")
               .EmailAddress().WithMessage("A valid email is required")
               .MinimumLength(10)
                .MaximumLength(100);

            RuleFor(x => x.PurposeOfPurchase)
                .NotEmpty().WithMessage("PurposeOfPurchase is required")
                .MinimumLength(3)
               .MaximumLength(1000);

            RuleFor(x => x.EpoType)
               .NotNull().WithMessage(string.Format("EpoType is required"))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, "Country", 0));

            RuleFor(x => x.ReceiptMethod)
               .NotNull().WithMessage(string.Format("ReceiptMethod is required"))
               .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, "Country", 0));

            RuleFor(x => x.PaymentType)
              .NotNull().WithMessage(string.Format("PaymentType is required"))
              .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, "Country", 0));
        }
    }
}
