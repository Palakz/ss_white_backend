﻿namespace SSWhite.Service.Validator.ExpensePurchaseOrder
{
    using FluentValidation;
    using SSWhite.Domain.Request.Account;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.Domain.Request.TimeAndAttendance;
    using SSWhite.ResourceFile;

    public class ApproveOrDenyEpoRequestValidator : AbstractValidator<ApproveOrDenyEpoRequest>
    {
        public ApproveOrDenyEpoRequestValidator()
        {
            RuleFor(x => x.EpoId)
                .NotEmpty().WithMessage("EpoId Is Required");

            RuleFor(x => x.IsApprove)
                .NotNull().WithMessage("IsApprove Is Required");
        }
    }
}
