﻿namespace SSWhite.Service.Validator.Admin
{
    using System;
    using FluentValidation;
    using SSWhite.Domain.Request.Admin;

    public class CheckIfEmailAddressExistsRequestValidator : AbstractValidator<CheckIfEmailAddressExistsRequest>
    {
        public CheckIfEmailAddressExistsRequestValidator()
        {
            RuleFor(x => x.EmailAddress)
                .NotEmpty().WithMessage("EmailAddress Is Required");
        }
    }
}
