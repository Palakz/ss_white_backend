﻿namespace SSWhite.Service.Validator.Account
{
    using System;
    using FluentValidation;
    using SSWhite.Domain.Request.Admin;

    public class GetDepartmentByDepartmentSupervisorNameRequestValidator : AbstractValidator<GetDepartmentByDepartmentSupervisorNameRequest>
    {
        public GetDepartmentByDepartmentSupervisorNameRequestValidator()
        {
            RuleFor(x => x.DepartmentSupervisorName)
                .NotEmpty().WithMessage("Department Supervisor Name Is Required");
        }
    }
}
