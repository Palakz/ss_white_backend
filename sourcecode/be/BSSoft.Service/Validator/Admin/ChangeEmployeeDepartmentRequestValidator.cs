﻿namespace SSWhite.Service.Validator.Admin
{
    using FluentValidation;
    using SSWhite.Domain.Request.Admin;

    public class ChangeEmployeeDepartmentRequestValidator : AbstractValidator<ChangeEmployeeDepartmentRequest>
    {
        public ChangeEmployeeDepartmentRequestValidator()
        {
            RuleFor(x => x.EmployeeId)
                .NotEmpty().WithMessage("Employee Id Is Required")
                .MinimumLength(3)
                .MaximumLength(50);

            RuleFor(x => x.PersonName)
                .NotEmpty().WithMessage("Employee Name Is Required")
                 .MinimumLength(3)
                .MaximumLength(500);

            RuleFor(x => x.CurrentDepartment)
                .NotEmpty().WithMessage("Current Department Name Is Required")
                 .MinimumLength(2)
                .MaximumLength(200);

            RuleFor(x => x.NewDepartment)
                .NotEmpty().WithMessage("New Department Name Is Required")
                 .MinimumLength(2)
                .MaximumLength(200);

            RuleFor(x => x.ReasonForChangeDepartment)
                .NotEmpty().WithMessage("Reason For Department Change Is Required")
                 .MinimumLength(3)
                .MaximumLength(500);
        }
    }
}
