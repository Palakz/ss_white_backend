﻿using FluentValidation;
using SSWhite.Domain.Request.Admin;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Service.Validator.Admin
{
    public class ChangeEmployeeStatusRequestValidator : AbstractValidator<ChangeEmployeeStatusRequest>
    {
        public ChangeEmployeeStatusRequestValidator()
        {
            RuleFor(x => x.EmployeeId)
                .NotEmpty().WithMessage("Employee Id Is Required")
                .MinimumLength(3)
                .MaximumLength(10);

            RuleFor(x => x.InActiveReasonType)
                .NotEmpty().WithMessage("In Active Reason Is Required");
        }
    }
}
