﻿namespace SSWhite.Service.Validator.Common
{
    using FluentValidation;
    using SSWhite.Core.Request;
    using SSWhite.ResourceFile;

    public class RequestEntityValidator : AbstractValidator<RequestEntity>
    {
        public RequestEntityValidator()
        {
            RuleFor(x => x.Id)
                .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, Labels.Id, 0));
        }
    }
}
