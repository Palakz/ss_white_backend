﻿namespace SSWhite.Service.Validator.Account
{
    using FluentValidation;
    using SSWhite.Core.Extensions;
    using SSWhite.Domain.Request.AttendanceChange;

    public class CreateAttendanceFtoRequestValidator : AbstractValidator<CreateAttendanceFtoRequest>
    {
        public CreateAttendanceFtoRequestValidator()
        {
            RuleFor(x => x.EmployeeID)
                .NotEmpty().WithMessage("EmployeeID Is Required");

            RuleFor(x => x.EmployeeName)
                .NotEmpty().WithMessage("EmployeeName Is Required");

            RuleFor(x => x.StartDate)
                .NotNull().WithMessage("StartDate Is Required");

            RuleFor(x => x.StartDateDayType)
                .NotEmpty().WithMessage("StartDateDayType Is Required");

            RuleFor(x => x.EndDate)
                .NotNull().WithMessage("EndDate Is Required");

            RuleFor(x => x.EndDateDayType)
                .NotEmpty().WithMessage("EndDateDayType Is Required");

            RuleFor(x => x.LeaveTypeID)
                .NotEmpty().WithMessage("LeaveTypeID Is Required");

            RuleFor(x => x.NumberOfDays)
                .NotEmpty().WithMessage("NumberOfDays Is Required");
        }
    }
}
