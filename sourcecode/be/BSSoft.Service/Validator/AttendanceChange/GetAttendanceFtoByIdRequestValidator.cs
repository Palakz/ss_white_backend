﻿namespace SSWhite.Service.Validator.Account
{
    using FluentValidation;
    using SSWhite.Core.Extensions;
    using SSWhite.Domain.Request.AttendanceChange;

    public class GetAttendanceFtoByIdRequestValidator : AbstractValidator<GetAttendanceFtoByIdRequest>
    {
        public GetAttendanceFtoByIdRequestValidator()
        {
            RuleFor(x => x.FtoId)
                .NotEmpty().WithMessage("FtoId Is Required");
        }
    }
}
