﻿namespace SSWhite.Service.Validator.AttendanceChange
{
    using FluentValidation;
    using SSWhite.Domain.Request.AttendanceChange;
    using SSWhite.ResourceFile;

    public class ApproveOrDenyFtoRequestValidator : AbstractValidator<ApproveOrDenyFtoRequest>
    {
        public ApproveOrDenyFtoRequestValidator()
        {
            RuleFor(x => x.FtoId)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.FtoId))
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.MinNumber, Labels.FtoId, 1));
        }
    }
}
