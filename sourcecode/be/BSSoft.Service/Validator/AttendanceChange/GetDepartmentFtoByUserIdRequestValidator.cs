﻿namespace SSWhite.Service.Validator.Account
{
    using FluentValidation;
    using SSWhite.Core.Extensions;
    using SSWhite.Domain.Request.AttendanceChange;
    using System;

    public class GetDepartmentFtoByUserIdRequestValidator : AbstractValidator<GetDepartmentFtoByUserIdRequest>
    {
        public GetDepartmentFtoByUserIdRequestValidator()
        {
            RuleFor(x => x.StartDate)
                .NotNull().WithMessage("StartDate Is Required");

            RuleFor(x => x.EndDate)
                .NotNull().WithMessage("EndDate Is Required");

            RuleFor(x => x).Must(x => x.EndDate == default(DateTime) || x.StartDate == default(DateTime) || x.EndDate >= x.StartDate)
            .WithMessage("EndTime must greater than StartTime");
        }
    }
}
