﻿namespace SSWhite.Service.Validator.Account
{
    using FluentValidation;
    using SSWhite.Domain.Request.Account;
    using SSWhite.ResourceFile;

    public class SetNewPasswordRequestValidator : AbstractValidator<SetNewPasswordRequest>
    {
        public SetNewPasswordRequestValidator()
        {
            RuleFor(x => x.EmailAddress)
                .NotEmpty().WithMessage("EmailAddress is required");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Password is required")
                .MinimumLength(8);
        }
    }
}
