﻿namespace SSWhite.Service.Validator.Account
{
    using FluentValidation;
    using SSWhite.Domain.Request.Account;
    using SSWhite.ResourceFile;

    public class SendForgetPasswordEmailRequestValidator : AbstractValidator<SendForgetPasswordEmailRequest>
    {
        public SendForgetPasswordEmailRequestValidator()
        {
            RuleFor(x => x.UserLogin)
                .NotEmpty().WithMessage(Validations.UserNameOrEmailAddressIsRequired);
        }
    }
}
