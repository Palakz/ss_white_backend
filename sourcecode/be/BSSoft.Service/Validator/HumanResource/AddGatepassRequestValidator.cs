﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class AddGatepassRequestValidator : AbstractValidator<AddGatepassRequest>
    {
        public AddGatepassRequestValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("User Id Is Required");

            RuleFor(x => x.EmployeeTypeId)
               .NotEmpty().WithMessage("Employee Type Id Is Required");

            RuleFor(x => x.Purpose)
               .NotEmpty().WithMessage("Purpose Is Required")
               .MinimumLength(3)
               .MaximumLength(500);

            RuleFor(x => x.TimeIn)
                .NotEmpty().WithMessage("Time In Is Required");

            RuleFor(x => x.TimeOut)
                .NotEmpty().WithMessage("Time Out Is Required");
        }
    }
}
