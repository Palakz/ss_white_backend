﻿using FluentValidation;
using SSWhite.Domain.Request.HumanResource;

namespace SSWhite.Service.Validator.HumanResource
{
    public class GetRecruiteeByIdRequestValidator : AbstractValidator<GetRecruiteeByIdRequest>
    {
        public GetRecruiteeByIdRequestValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty().WithMessage("Recruitee Id Is Required");
        }

    }
}