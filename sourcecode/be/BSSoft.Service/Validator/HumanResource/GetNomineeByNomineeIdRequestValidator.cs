﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class GetNomineeByNomineeIdRequestValidator : AbstractValidator<GetNomineeByNomineeIdRequest>
    {
        public GetNomineeByNomineeIdRequestValidator()
        {
            RuleFor(x => x.NomineeId)
                .NotEmpty().WithMessage("Nominee Id Is Required");
        }
    }
}