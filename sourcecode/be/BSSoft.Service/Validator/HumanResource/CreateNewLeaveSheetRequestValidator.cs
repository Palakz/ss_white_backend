﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class CreateNewLeaveSheetRequestValidator : AbstractValidator<CreateNewLeaveSheetRequest>
    {
        public CreateNewLeaveSheetRequestValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("User Id Is Required");

            RuleFor(x => x.EmployeeTypeId)
               .NotEmpty().WithMessage("Employee Type Id Is Required");

            RuleFor(x => x.FromDate)
                .NotEmpty().WithMessage("From Date Is Required");

            RuleFor(x => x.ToDate)
                .NotEmpty().WithMessage("To Date Is Required");

            RuleFor(x => x.CountOfDays)
             .NotEmpty().WithMessage("Count Of Days are Required");

            RuleFor(x => x.LeavePurpose)
               .NotEmpty().WithMessage("Leave Purpose Is Required")
               .MinimumLength(3)
               .MaximumLength(500);
        }
    }
}
