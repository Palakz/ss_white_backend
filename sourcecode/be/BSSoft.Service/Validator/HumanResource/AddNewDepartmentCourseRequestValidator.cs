﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class AddNewDepartmentCourseRequestValidator : AbstractValidator<AddNewDepartmentCourseRequest>
    {
        public AddNewDepartmentCourseRequestValidator()
        {
            RuleFor(x => x.Name)
              .NotEmpty().WithMessage("Name Is Required")
              .MinimumLength(3)
              .MaximumLength(200);

            RuleFor(x => x.DepartmentId)
                .NotEmpty().WithMessage("Department Id Is Required");

            RuleFor(x => x.SubDepartmentId)
               .NotEmpty().WithMessage("SubDepartment Id Is Required");

            RuleFor(x => x.SummaryOfContent)
               .NotEmpty().WithMessage("Summary Of Content Is Required")
               .MinimumLength(3)
               .MaximumLength(200);

            RuleFor(x => x.Hours)
                .NotEmpty().WithMessage("Hours Are Required");

            RuleFor(x => x.Location)
             .NotEmpty().WithMessage("Location Is Required")
             .MinimumLength(3)
             .MaximumLength(200);
        }
    }
}
