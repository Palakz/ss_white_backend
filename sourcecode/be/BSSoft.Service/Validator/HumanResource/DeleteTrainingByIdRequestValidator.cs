﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using FluentValidation.Validators;
    using SSWhite.Domain.Request.HumanResource;

    public class DeleteTrainingByIdRequestValidator : AbstractValidator<DeleteTrainingByIdRequest>
    {
        public DeleteTrainingByIdRequestValidator()
        {
            RuleFor(x => x.TrainingId)
           .NotEmpty().WithMessage("Training Id Is Required");

            RuleFor(x => x.IsDeleted)
                .NotEmpty().WithMessage("IsDeleted Is Required");
        }
    }
}