﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class GetLeaveSheetDetailsByIdRequestValidator : AbstractValidator<GetLeaveSheetDetailsByIdRequest>
    {
        public GetLeaveSheetDetailsByIdRequestValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty().WithMessage("Id Is Required");
        }
    }
}