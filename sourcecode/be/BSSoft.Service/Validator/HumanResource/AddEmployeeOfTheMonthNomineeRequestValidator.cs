﻿using FluentValidation;
using SSWhite.Domain.Request.HumanResource;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Service.Validator.HumanResource
{
    public class AddEmployeeOfTheMonthNomineeRequestValidator : AbstractValidator<AddEmployeeOfTheMonthNomineeRequest>
    {
        public AddEmployeeOfTheMonthNomineeRequestValidator()
        {
            // RuleFor(x => x.Id)
            //   .NotEmpty().WithMessage("Id is Required");

            RuleFor(x => x.EmployeeId)
                .NotEmpty().WithMessage("Employee Id Is Required");

            RuleFor(x => x.EmployeeTypeId)
                .NotEmpty().WithMessage("Employee Type Id Is Required");

            RuleFor(x => x.DepartmentId)
                .NotEmpty().WithMessage("DepartmentId Is Required");

            RuleFor(x => x.Supervisor)
                .NotEmpty().WithMessage("Supervisor Is Required");

            //RuleFor(x => x.NominatedBy)
            //    .NotEmpty().WithMessage("Nominated By Is Required");

            RuleFor(x => x.NominatedDetailsId)
                .NotEmpty().WithMessage("Nominated Details Id Is Required");
        }
    }
}
