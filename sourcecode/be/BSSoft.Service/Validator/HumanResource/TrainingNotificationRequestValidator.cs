﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using FluentValidation.Validators;
    using SSWhite.Domain.Request.HumanResource;

    public class TrainingNotificationRequestValidator : AbstractValidator<TrainingNotificationRequest>
    {
        public TrainingNotificationRequestValidator()
        {
            RuleFor(x => x.CourseId)
                .NotEmpty().WithMessage("Course Id Is Required");

            RuleFor(x => x.CourseName)
              .NotEmpty().WithMessage("Course Name Is Required");

            RuleFor(x => x.Location)
               .NotEmpty().WithMessage("Location Is Required");

            RuleFor(x => x.Instructor)
                .NotEmpty().WithMessage("Instructor Is Required")
                .MinimumLength(3)
                .MaximumLength(200);

            RuleFor(x => x.Date)
             .NotEmpty().WithMessage("Date Is Required");

            RuleFor(x => x.Time)
             .NotEmpty().WithMessage("Time Is Required");

            RuleFor(x => x.TrainingTypeId)
               .NotEmpty().WithMessage("Training Type Id Is Required");

            RuleFor(x => x.TrainingType)
              .NotEmpty().WithMessage("Training Type Is Required");

            RuleFor(x => x.EmailAddress)
               .NotEmpty().WithMessage("Email Address IS Required");

            RuleFor(x => x.Description)
              .NotEmpty().WithMessage("Description Is Required")
              .MinimumLength(3)
              .MaximumLength(200);
        }
    }
}