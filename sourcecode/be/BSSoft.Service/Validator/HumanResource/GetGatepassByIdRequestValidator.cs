﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class GetGatepassByIdRequestValidator : AbstractValidator<GetGatepassByIdRequest>
    {
        public GetGatepassByIdRequestValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty().WithMessage("Id Is Required");
        }
    }
}