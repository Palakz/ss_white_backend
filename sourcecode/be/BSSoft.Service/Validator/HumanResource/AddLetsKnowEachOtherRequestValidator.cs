﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class AddLetsKnowEachOtherRequestValidator : AbstractValidator<AddLetsKnowEachOtherRequest>
    {
        public AddLetsKnowEachOtherRequestValidator()
        {
            // RuleFor(x => x.UserId)
            //    .NotEmpty().WithMessage("User Id Is Required");

            RuleFor(x => x.SSWGoals)
                .NotEmpty().WithMessage("SSWGoals Are Required")
                .MinimumLength(3)
                .MaximumLength(500);
        }
    }
}
