﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class GetTrainingByIdRequestValidator : AbstractValidator<GetTrainingByIdRequest>
    {
        public GetTrainingByIdRequestValidator()
        {
            RuleFor(x => x.TrainingId)
                .NotEmpty().WithMessage("Training Id Is Required");
        }
    }
}