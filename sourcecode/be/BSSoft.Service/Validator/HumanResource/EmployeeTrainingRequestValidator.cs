﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using FluentValidation.Validators;
    using SSWhite.Domain.Request.HumanResource;

    public class EmployeeTrainingRequestValidator : AbstractValidator<EmployeeTrainingRequest>
    {
        public EmployeeTrainingRequestValidator()
        {
            RuleFor(x => x.TrainingTypeId)
                .NotEmpty().WithMessage("Training Type Id Is Required");

            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("User Id(Attendence Id) Is Required");

            //RuleFor(x => x.IsEvaluated)
            //    .NotEmpty().WithMessage("Is Evaluated Is Required");

            //RuleFor(x => x.HasAttachment)
            //    .NotEmpty().WithMessage("Has Attachment Is Required");
        }
    }
}