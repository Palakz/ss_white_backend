﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using SSWhite.Domain.Request.HumanResource;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Service.Validator.HumanResource
{
    public class AddOrEditTrainingRequestValidator : AbstractValidator<AddOrEditTrainingRequest>
    {
        public AddOrEditTrainingRequestValidator()
        {
            RuleFor(x => x.DepartmentId)
                .NotEmpty().WithMessage("Department Id Is Required");

            RuleFor(x => x.DepartmentCourseId)
                .NotEmpty().WithMessage("Department Course Id Is Required");

            RuleFor(x => x.Instructor)
                .NotEmpty().WithMessage("Instructor Is Required");

            RuleFor(x => x.FromDate)
                .NotEmpty().WithMessage("FromDate Is Required");

            RuleFor(x => x.ToDate)
                .NotEmpty().WithMessage("ToDate Is Required");

            RuleFor(x => x.EnteredBy)
                .NotEmpty().WithMessage("Entered By Is Required");

            RuleForEach(x => x.EmployeeTraining)
                .SetValidator(new EmployeeTrainingRequestValidator());
        }
    }
}