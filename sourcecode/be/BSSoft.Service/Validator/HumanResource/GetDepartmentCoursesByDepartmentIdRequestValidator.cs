﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class GetDepartmentCoursesByDepartmentIdRequestValidator : AbstractValidator<GetDepartmentCoursesByDepartmentIdRequest>
    {
        public GetDepartmentCoursesByDepartmentIdRequestValidator()
        {
            RuleFor(x => x.DepartmentId)
                .NotEmpty().WithMessage("Department Id Is Required");
        }
    }
}