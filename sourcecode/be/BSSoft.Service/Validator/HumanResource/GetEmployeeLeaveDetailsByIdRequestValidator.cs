﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class GetEmployeeLeaveDetailsByIdRequestValidator : AbstractValidator<GetEmployeeLeaveDetailsByIdRequest>
    {
        public GetEmployeeLeaveDetailsByIdRequestValidator()
        {
            RuleFor(x => x.UserId)
                .NotEmpty().WithMessage("User Id Is Required");
        }
    }
}