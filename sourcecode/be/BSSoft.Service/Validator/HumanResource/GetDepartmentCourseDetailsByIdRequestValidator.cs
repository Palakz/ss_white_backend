﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class GetDepartmentCourseDetailsByIdRequestValidator : AbstractValidator<GetDepartmentCourseDetailsByIdRequest>
    {
        public GetDepartmentCourseDetailsByIdRequestValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty().WithMessage("Id Is Required");
        }
    }
}