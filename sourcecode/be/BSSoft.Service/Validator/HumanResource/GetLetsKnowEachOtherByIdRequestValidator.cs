﻿namespace SSWhite.Service.Validator.HumanResource
{
    using FluentValidation;
    using SSWhite.Domain.Request.HumanResource;

    public class GetLetsKnowEachOtherByIdRequestValidator : AbstractValidator<GetLetsKnowEachOtherByIdRequest>
    {
        public GetLetsKnowEachOtherByIdRequestValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty().WithMessage("Id Is Required");
        }

    }
}