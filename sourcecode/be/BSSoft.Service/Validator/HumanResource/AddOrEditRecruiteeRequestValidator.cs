﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using SSWhite.Domain.Request.HumanResource;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Service.Validator.HumanResource
{
    public class AddOrEditRecruiteeRequestValidator : AbstractValidator<AddOrEditRecruiteeRequest>
    {
        public AddOrEditRecruiteeRequestValidator()
        {
            //RuleFor(x => x.Id)
            //    .NotEmpty().WithMessage("Recruitee Id is required");

            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage("FirstName is required");

            RuleFor(x => x.MiddleName)
                .NotEmpty().WithMessage("MiddleName is required");

            RuleFor(x => x.SurName)
                .NotEmpty().WithMessage("SurName is required");

            RuleFor(x => x.DOB)
                .NotEmpty().WithMessage("DOB is required");

            RuleFor(x => x.GenderId)
                .NotEmpty().WithMessage("GenderId is required");

            RuleFor(x => x.MaritalStatusId)
                .NotEmpty().WithMessage("MaritalStatusId is required");

            //RuleFor(x => x.ImagePath)
            //    .NotEmpty().WithMessage("ImagePath is required");

            //RuleFor(x => x.DoAnyRelative)
            //    .NotEmpty().WithMessage("DoAnyRelative is required");

            //RuleFor(x => x.IsDisabilityOrSickness)
            //    .NotEmpty().WithMessage("IsDisabilityOrSickness is required");

            //RuleFor(x => x.DisabilityDescription)
            //    .NotEmpty().WithMessage("DisabilityDescription is required");

            //RuleFor(x => x.IsWorkExperience)
            //    .NotEmpty().WithMessage("IsWorkExperience is required");

            //RuleFor(x => x.IsFromReference)
            //    .NotEmpty().WithMessage("IsFromReference is required");

            //RuleFor(x => x.IsSameAsCurrentAddress)
            //    .NotEmpty().WithMessage("IsSameAsCurrentAddress is required");

            //RuleForEach(x => x.RecruiteeAddresses)
            //  .SetValidator(new RecruiteeAddressesValidator());

            //RuleForEach(x => x.RecruiteeEducations)
            //    .SetValidator(new RecruiteeEducationsValidator());

            //RuleForEach(x => x.RecruiteeFamilyDetails)
            //    .SetValidator(new RecruiteeFamilyDetailsValidator());

            //RuleForEach(x => x.RecruiteeExperiences)
            //    .SetValidator(new RecruiteeExperiencesValidator());

            //RuleForEach(x => x.RecruiteeQuestions)
            //    .SetValidator(new RecruiteeQuestionsValidator());

            //RuleForEach(x => x.RecruiteeReferences)
            //    .SetValidator(new RecruiteeReferencesValidator());
        }
    }
}