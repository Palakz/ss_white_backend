﻿namespace SSWhite.Service.Validator.Document
{
    using FluentValidation;
    using SSWhite.Domain.Request.Document;

    public class ApproveDocumentRequestValidator : AbstractValidator<ApproveDocumentRequest>
    {
        public ApproveDocumentRequestValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty().WithMessage("EpoId Is Required");

            RuleFor(x => x.ApprovalType)
                .NotEmpty().WithMessage("IsApprove Is Required");
        }
    }
}
