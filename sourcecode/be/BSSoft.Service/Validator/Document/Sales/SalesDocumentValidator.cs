﻿namespace SSWhite.Service.Validator.Document.Sales
{
    using FluentValidation;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Common.Document.Sales;
    using SSWhite.ResourceFile;

    public class SalesDocumentValidator : AbstractValidator<SalesDocument>
    {
        public SalesDocumentValidator()
        {
            RuleFor(x => x.VoucherNumber)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.VoucherNumber));

            RuleFor(x => x.Date)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Date));

            When(x => x.Date != null, () =>
            {
                RuleFor(x => x.Date)
                    .Custom((context, arguments) =>
                    {
                        if (context.Value.ToFinancialYear() != SessionGetterSetter.Get().CurrentFinancialYear)
                        {
                            arguments.AddFailure(nameof(SalesDocument.Date), string.Format(Validations.DateNotInSelectedFinancialYear));
                        }
                    });
            });

            RuleFor(x => x.VoucherType)
                 .IsInEnum().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.VoucherType));

            RuleFor(x => x.ReceivedBy)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.ReceivingPerson))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.ReceivingPerson, 50));
        }
    }
}
