﻿namespace SSWhite.Service.Validator.Document.JournalVoucher
{
    using FluentValidation;
    using SSWhite.Core.Common.Session;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Common.Document.JournalVoucher;
    using SSWhite.ResourceFile;

    public class JournalVoucherValidator : AbstractValidator<JournalVoucher>
    {
        public JournalVoucherValidator()
        {
            RuleFor(x => x.VoucherNumber)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.VoucherNumber));

            RuleFor(x => x.Date)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Date));

            RuleFor(x => x.TotalAmount)
                .GreaterThan(0).WithMessage(string.Format(Validations.MinNumber, Labels.TotalAmount, 0));

            RuleFor(x => x.Remarks)
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Remarks, 100));

            RuleFor(x => x.CreditedClientId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.CRCompany));

            RuleFor(x => x.DebitedClientId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.DRCompany));

            RuleFor(x => x.CreditedClientId)
                .NotEqual(x => x.DebitedClientId).WithMessage(string.Format(Validations.NotEqualTo, Labels.CRCompany, Labels.DRCompany))
                .When(x => x.DebitedClientId > 0 && x.CreditedClientId > 0);

            When(x => x.Date != null, () =>
            {
                RuleFor(x => x.Date)
                    .Custom((context, arguments) =>
                    {
                        if (context.Value.ToFinancialYear() != SessionGetterSetter.Get().CurrentFinancialYear)
                        {
                            arguments.AddFailure(nameof(JournalVoucher.Date), string.Format(Validations.DateNotInSelectedFinancialYear));
                        }
                    });
            });
        }
    }
}
