﻿namespace SSWhite.Service.Validator.Master.Client
{
    using FluentValidation;
    using SSWhite.Core.Common;
    using SSWhite.Core.Extensions;
    using SSWhite.Domain.Common.Master.Client;
    using SSWhite.ResourceFile;

    public class ClientOpeningBalanceValidator : AbstractValidator<ClientOpeningBalance>
    {
        public ClientOpeningBalanceValidator()
        {
            RuleFor(x => x.FinancialYear)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.FinancialYear))
                .GreaterThan(0).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.FinancialYear));

            RuleFor(x => x.Balance)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.OpeningBalance));

            RuleFor(x => x.TransactionType)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.TransactionType))
                .IsInEnum().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.TransactionType))
                .When(x => x.Balance != null);
        }
    }
}
