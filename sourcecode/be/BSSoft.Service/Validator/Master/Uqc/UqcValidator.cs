﻿namespace SSWhite.Service.Validator.Master.Uqc
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.Uqc;
    using SSWhite.ResourceFile;

    public class UqcValidator : AbstractValidator<Uqc>
    {
        public UqcValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));

            RuleFor(x => x.Code)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.ShortName))
                .MaximumLength(20).WithMessage(string.Format(Validations.MaxLength, Labels.ShortName, 20));

            RuleFor(x => x.PackingTypeId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.PackingType));

            RuleFor(x => x.SubUnitId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.SubUnit));
        }
    }
}
