﻿namespace SSWhite.Service.Validator.Master.City
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.City;
    using SSWhite.ResourceFile;

    public class CityValidator : AbstractValidator<City>
    {
        public CityValidator()
        {
            RuleFor(x => x.Name)
            .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
            .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));

            RuleFor(x => x.CountryId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Country));

            RuleFor(x => x.StateId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.State));
        }
    }
}
