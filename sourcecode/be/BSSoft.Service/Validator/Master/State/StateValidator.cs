﻿namespace SSWhite.Service.Validator.Master.State
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.State;
    using SSWhite.ResourceFile;

    public class StateValidator : AbstractValidator<State>
    {
        public StateValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 50));

            RuleFor(x => x.CountryId)
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Country));
        }
    }
}
