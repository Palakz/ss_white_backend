﻿namespace SSWhite.Service.Validator.Master.ItemRate
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.ItemRate;
    using SSWhite.ResourceFile;

    public class ItemRateValidator : AbstractValidator<ItemRate>
    {
        public ItemRateValidator()
        {
            RuleFor(x => x.Id)
                .GreaterThanOrEqualTo(1)
                .When(x => x.Id != null);

            RuleFor(x => x.ItemGroupId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Group))
                .GreaterThan(0).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Group));

            RuleFor(x => x.ItemId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Item))
                .GreaterThan(0).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Item));

            RuleFor(x => x.Rate)
                .NotNull().WithMessage(string.Format(Validations.IsRequired, Labels.Rate))
                .GreaterThanOrEqualTo(0).WithMessage(string.Format(Validations.IsRequired, Labels.Rate));

            RuleFor(x => x.ToDate)
                .GreaterThanOrEqualTo(x => x.FromDate).WithMessage(Validations.ToDateLessThanFromDate);

            RuleFor(x => x.FromDate)
                .NotNull()
                .When(x => x.ToDate != null).WithMessage(Validations.FromDateIsRequired);
        }
    }
}
