﻿namespace SSWhite.Service.Validator.Master.ItemType
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.ItemType;
    using SSWhite.ResourceFile;

    public class ItemTypeValidator : AbstractValidator<ItemType>
    {
        public ItemTypeValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 50));
        }
    }
}
