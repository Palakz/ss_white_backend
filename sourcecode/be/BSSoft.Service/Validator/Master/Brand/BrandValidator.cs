﻿namespace SSWhite.Service.Validator.Master.Brand
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.Brand;
    using SSWhite.ResourceFile;

    public class BrandValidator : AbstractValidator<Brand>
    {
        public BrandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));
        }
    }
}
