﻿namespace SSWhite.Service.Validator.Master.Company
{
    using FluentValidation;
    using SSWhite.Core.Common;
    using SSWhite.Core.Extensions;
    using SSWhite.Domain.Common.Master.Company;
    using SSWhite.ResourceFile;

    public class CompanyValidator : AbstractValidator<Company>
    {
        public CompanyValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));

            RuleFor(x => x.Gstin)
                .Length(15).WithMessage(string.Format(Validations.ExactLength, Labels.GSTNo, 15))
                .When(x => !x.Gstin.IsNullOrWhiteSpace());

            RuleFor(x => x.Pan)
                .Length(10).WithMessage(string.Format(Validations.ExactLength, Labels.Pan, 10))
                .When(x => !x.Pan.IsNullOrWhiteSpace());

            RuleFor(x => x.Address)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Address))
                .MaximumLength(500).WithMessage(string.Format(Validations.MaxLength, Labels.Address, 500));

            RuleFor(x => x.CountryId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Country))
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.Country));

            RuleFor(x => x.CityId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.City))
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.City));

            RuleFor(x => x.StateId)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.State))
                .GreaterThanOrEqualTo(1).WithMessage(string.Format(Validations.DropDownIsRequired, Labels.State));

            RuleFor(x => x.Pincode)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Pincode))
                .Length(6).WithMessage(string.Format(Validations.ExactLength, Labels.Pincode, 6));

            RuleFor(x => x.EmailAddress)
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Email, 100))
                .Matches(RegexExpression.EMAIL).WithMessage(string.Format(Validations.Invalid, Labels.Email))
                .When(x => !x.EmailAddress.IsNullOrWhiteSpace());

            RuleFor(x => x.ContactPerson)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.ContactPerson))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.ContactPerson, 100));

            RuleFor(x => x.ContactNumber)
                .MaximumLength(10).WithMessage(string.Format(Validations.MaxLength, Labels.ContactNumber, 10))
                .When(x => !x.ContactNumber.IsNullOrWhiteSpace());

            RuleFor(x => x.Website)
                .MaximumLength(80).WithMessage(string.Format(Validations.MaxLength, Labels.Website, 80))
                .When(x => !x.Website.IsNullOrWhiteSpace());

            RuleFor(x => x.OtherLicense)
                .MaximumLength(200).WithMessage(string.Format(Validations.MaxLength, Labels.OtherLicense, 200))
                .When(x => !x.OtherLicense.IsNullOrWhiteSpace());

            RuleFor(x => x.TagLine)
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.BusinessLine, 100))
                .When(x => !x.TagLine.IsNullOrWhiteSpace());
        }
    }
}
