﻿namespace SSWhite.Service.Validator.Master.LedgerGroup
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.LedgerGroup;
    using SSWhite.ResourceFile;

    public class LedgerGroupValidator : AbstractValidator<LedgerGroup>
    {
        public LedgerGroupValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));

            RuleFor(x => x.Code)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Code))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Code, 100));

            RuleFor(x => x.LedgerGroupType)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.LedgerGroupType))
                .IsInEnum().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.LedgerGroupType));

            RuleFor(x => x.AccountType)
                .NotNull().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.LedgerAccountType))
                .IsInEnum().WithMessage(string.Format(Validations.DropDownIsRequired, Labels.LedgerAccountType));
        }
    }
}
