﻿namespace SSWhite.Service.Validator.Master.ItemGroup
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.ItemGroup;
    using SSWhite.ResourceFile;

    public class ItemGroupValidator : AbstractValidator<ItemGroup>
    {
        public ItemGroupValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(50).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 50));
        }
    }
}
