﻿namespace SSWhite.Service.Validator.Master.Header
{
    using FluentValidation;
    using SSWhite.Domain.Common.Master.Header;
    using SSWhite.ResourceFile;

    public class HeaderValidator : AbstractValidator<Header>
    {
        public HeaderValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty().WithMessage(string.Format(Validations.IsRequired, Labels.Name))
                .MaximumLength(100).WithMessage(string.Format(Validations.MaxLength, Labels.Name, 100));
        }
    }
}
