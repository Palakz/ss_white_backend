﻿namespace SSWhite.Service.Interface.Master.State
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.State;
    using SSWhite.Domain.Request.Master.State;
    using SSWhite.Domain.Response.Master.State;

    public interface IStateService
    {
        Task<ServiceSearchResponse<GetAllStatesResponse>> GetAllStates(ServiceSearchRequest<GetAllStates> request);

        Task<ServiceResponse<int>> AddEditState(ServiceRequest<State> request);

        Task ChangeStateStatus(ServiceRequest<int> request);

        Task<ServiceResponse<State>> GetStateById(ServiceRequest<int> request);

        Task DeleteState(ServiceRequest<int> request);
    }
}
