﻿namespace SSWhite.Service.Interface.Master.ItemCategory
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemCategory;
    using SSWhite.Domain.Request.Master.ItemCategory;
    using SSWhite.Domain.Response.Master.ItemCategory;

    public interface IItemCategoryService
    {
        Task<ServiceSearchResponse<GetAllItemCategoriesResponse>> GetAllItemCategories(ServiceSearchRequest<GetAllItemCategories> request);

        Task<ServiceResponse<int>> AddEditItemCategory(ServiceRequest<ItemCategory> request);

        Task ChangeItemCategoryStatus(ServiceRequest<int> request);

        Task<ServiceResponse<ItemCategory>> GetItemCategoryById(ServiceRequest<int> request);

        Task DeleteItemCategory(ServiceRequest<int> request);
    }
}
