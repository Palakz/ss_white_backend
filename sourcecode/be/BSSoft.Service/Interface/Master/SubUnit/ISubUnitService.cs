﻿namespace SSWhite.Service.Interface.Master.SubUnit
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.SubUnit;
    using SSWhite.Domain.Request.Master.SubUnit;
    using SSWhite.Domain.Response.Master.SubUnit;

    public interface ISubUnitService
    {
        Task<ServiceSearchResponse<GetAllSubUnitsResponse>> GetAllSubUnits(ServiceSearchRequest<GetAllSubUnits> request);

        Task<ServiceResponse<int>> AddEditSubUnit(ServiceRequest<SubUnit> request);

        Task ChangeSubUnitStatus(ServiceRequest<int> request);

        Task<ServiceResponse<SubUnit>> GetSubUnitById(ServiceRequest<int> request);

        Task DeleteSubUnit(ServiceRequest<int> request);
    }
}
