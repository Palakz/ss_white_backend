﻿namespace SSWhite.Service.Interface.Master.PackingType
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.PackingType;
    using SSWhite.Domain.Request.Master.PackingType;
    using SSWhite.Domain.Response.Master.PackingType;

    public interface IPackingTypeService
    {
        Task<ServiceSearchResponse<GetAllPackingTypesResponse>> GetAllPackingTypes(ServiceSearchRequest<GetAllPackingTypes> request);

        Task<ServiceResponse<int>> AddEditPackingType(ServiceRequest<PackingType> request);

        Task ChangePackingTypeStatus(ServiceRequest<int> request);

        Task<ServiceResponse<PackingType>> GetPackingTypeById(ServiceRequest<int> request);

        Task DeletePackingType(ServiceRequest<int> request);
    }
}
