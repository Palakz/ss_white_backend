﻿namespace SSWhite.Service.Interface.Master.Client
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.Client;
    using SSWhite.Domain.Request.Master.Client;
    using SSWhite.Domain.Response.Master.Client;

    public interface IClientService
    {
        Task<ServiceSearchResponse<GetAllClientsResponse>> GetAllClients(ServiceSearchRequest<GetAllClients> request);

        Task<ServiceResponse<int>> AddEditClient(ServiceRequest<Client> request);

        Task ChangeClientStatus(ServiceRequest<int> request);

        Task<ServiceResponse<Client>> GetClientById(ServiceRequest<int> request);

        Task DeleteClient(ServiceRequest<int> request);

        Task<ServiceResponse<ClientOpeningBalance>> GetClientOpeningBalanceDetails(ServiceRequest<int> request);

        Task<ServiceResponse<int>> AddEditOpeningBalnce(ServiceRequest<ClientOpeningBalance> request);
    }
}
