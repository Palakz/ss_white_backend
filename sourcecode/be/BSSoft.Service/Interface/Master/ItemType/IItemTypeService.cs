﻿namespace SSWhite.Service.Interface.Master.ItemType
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemType;
    using SSWhite.Domain.Request.Master.ItemTypes;
    using SSWhite.Domain.Response.Master.ItemType;

    public interface IItemTypeService
    {
        Task<ServiceSearchResponse<GetAllItemTypesResponse>> GetAllItemTypes(ServiceSearchRequest<GetAllItemTypes> request);

        Task<ServiceResponse<int>> AddEditItemType(ServiceRequest<ItemType> request);

        Task ChangeItemTypeStatus(ServiceRequest<int> request);

        Task<ServiceResponse<ItemType>> GetItemTypeById(ServiceRequest<int> request);

        Task DeleteItemType(ServiceRequest<int> request);
    }
}
