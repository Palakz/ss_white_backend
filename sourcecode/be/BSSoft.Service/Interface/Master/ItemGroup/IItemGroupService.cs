﻿namespace SSWhite.Service.Interface.Master.ItemGroup
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Master.ItemGroup;
    using SSWhite.Domain.Request.Master.ItemGroup;
    using SSWhite.Domain.Response.Master.ItemGroup;

    public interface IItemGroupService
    {
        Task<ServiceSearchResponse<GetAllItemGroupsResponse>> GetAllItemGroups(ServiceSearchRequest<GetAllItemGroups> request);

        Task<ServiceResponse<int>> AddEditItemGroup(ServiceRequest<ItemGroup> request);

        Task ChangeItemGroupStatus(ServiceRequest<int> request);

        Task<ServiceResponse<ItemGroup>> GetItemGroupById(ServiceRequest<int> request);

        Task DeleteItemGroup(ServiceRequest<int> request);
    }
}
