﻿namespace SSWhite.Service.Interface.Report.InwardSlip
{
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Report.InwardSlip;
    using SSWhite.Domain.Response.Report.InwardSlip;

    public interface IInwardSlipReportService
    {
        Task<ServiceSearchResponse<GetAllInwardSlipsForReportResponse>> GetAllInwardSlipsForReport(ServiceSearchRequest<GetAllInwardSlipsForReport> request);
    }
}
