﻿namespace SSWhite.Service.Interface.Document.Purchase
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Common.Document.Purchase;
    using SSWhite.Domain.Request.Document.Purchase;
    using SSWhite.Domain.Response.Document.Purchase;

    public interface IPurchaseService
    {
        Task<ServiceSearchResponse<GetAllPurchaseDocumentsResponse>> GetAllPurchaseDocuments(ServiceSearchRequest<GetAllPurchaseDocuments> request);

        Task<ServiceResponse<List<InsertOrUpdatePurchaseDocumentResponse>>> AddEditPurchaseDocument(ServiceRequest<PurchaseDocument> request);

        Task ChangePurchaseDocumentStatus(ServiceRequest<int> request);

        Task<ServiceResponse<PurchaseDocument>> GetPurchaseDocumentById(ServiceRequest<int> request);

        Task<ServiceResponse<int>> DeletePurchaseDocument(ServiceRequest<int> request);

        Task<ServiceSearchResponse<GetAllPurchaseDocumentsResponse>> GetAllPendingRatePurchaseDocuments(ServiceSearchRequest<GetAllPurchaseDocuments> request);
    }
}
