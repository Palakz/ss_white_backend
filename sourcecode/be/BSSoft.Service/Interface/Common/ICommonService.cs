﻿namespace SSWhite.Service.Interface.Common
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SSWhite.Domain.Request.Admin;
    using SSWhite.Domain.Response.Admin;

    public interface ICommonService
    {
        Task<List<GetOrganizationResponse>> GetOrganization();

        Task<List<GetEmployeeJobDescriptionResponse>> GetEmployeeJobDescription();

        Task<List<GetReportingUnderResponse>> GetReportingUnder();

        Task<GetAllEmployeeDetailsDropdownsResponse> GetAllEmployeeDetailsDropdowns();

        Task<List<GetAllEmployeeTypesResponse>> GetAllEmployeeTypes();

        Task<List<GetDepartmentByDepartmentSupervisorNameResponse>> GetDepartmentByDepartmentSupervisorName(GetDepartmentByDepartmentSupervisorNameRequest getDepartmentByDepartmentSupervisorNameRequest);
    }
}
