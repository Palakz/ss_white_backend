﻿namespace SSWhite.Service.Common
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
    using Newtonsoft.Json;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;

    public static class Common
    {
        //public static decimal GetDollarValue()
        //{

        //    string apiUrl = "https://api.exchangerate.host/latest?base=USD&symbols=INR";
        //    CurrencyRateResponse currencyRateResponse = new CurrencyRateResponse()
        //    {
        //        Rates = new Rates()
        //    };
        //    HttpClient client = new HttpClient();
        //    HttpResponseMessage httpResponseMessage = client.GetAsync(apiUrl).Result;
        //    if (httpResponseMessage.IsSuccessStatusCode)
        //    {
        //        currencyRateResponse = JsonConvert.DeserializeObject<CurrencyRateResponse>(httpResponseMessage.Content.ReadAsStringAsync().Result);
        //        return currencyRateResponse.Rates.INR;
        //    }

        //    return currencyRateResponse.Rates.INR = 83.00m;
        //}
        public static decimal GetDollarValue()
        {
            string apiUrl = "https://api.apilayer.com/fixer/latest?base=USD&symbols=INR";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("apiKey", "IAcXjpMSNQbXSY933W5AawnfSIOOXpaj");
            HttpResponseMessage httpResponseMessage = client.GetAsync(apiUrl).Result;
            var response = JsonConvert.DeserializeObject<CurrencyRateResponse>(httpResponseMessage.Content.ReadAsStringAsync().Result);
            if (httpResponseMessage.IsSuccessStatusCode && response.Rates != null)
            {
                return response.Rates.INR;
            }

            return 83.00m;
        }
    }
}
