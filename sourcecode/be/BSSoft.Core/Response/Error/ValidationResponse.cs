﻿namespace SSWhite.Core.Response.Error
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    public class ValidationResponse
    {
        public string Message { get; }

        public List<ValidationError> Errors { get; }

        public ValidationResponse(ModelStateDictionary modelState)
        {
            Message = "UnProcessable Entity";
            Errors = modelState.Keys
                    .SelectMany(key => modelState[key].Errors.Select(x => new ValidationError(key, x.ErrorMessage)))
                    .ToList();
        }
    }
}
