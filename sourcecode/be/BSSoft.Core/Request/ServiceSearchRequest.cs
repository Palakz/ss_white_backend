﻿namespace SSWhite.Core.Request
{
    using SSWhite.Core.Attributes;

    /// <summary>
    /// This Class is used to Create GenericSearchRequest.
    /// It will bind any search request parameter of any Viewmodel.
    /// </summary>
    public class ServiceSearchRequest<T> : ServiceRequest
    {
        public T Data { get; set; }

        public int Start { get; set; }

        public int Length { get; set; }

        public string SortExpression { get; set; }

        [Trim]
        public string SearchKeyword { get; set; }
    }
}
