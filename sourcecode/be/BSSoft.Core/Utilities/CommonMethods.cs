﻿namespace SSWhite.Core.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Linq;
    using System.Reflection;
    using System.Resources;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;

    public static class CommonMethods
    {
        /// <summary>
        /// This method takes enum value and returns string value from the resource files.
        /// </summary>
        public static string GetDisplayValue<T>(T type)
        {
            if (type == null)
            {
                return string.Empty;
            }

            var fieldInfo = type.GetType().GetField(type.ToString());
            if (fieldInfo == null)
            {
                return string.Empty;
            }

            var descriptionAttributes = fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false) as DisplayAttribute[];

            if (descriptionAttributes == null || descriptionAttributes.Length == 0)
            {
                return type.ToString();
            }

            if (descriptionAttributes[0].ResourceType != null)
            {
                var resource = descriptionAttributes[0].ResourceType.GetProperty("ResourceManager").GetValue(null) as ResourceManager;
                return resource.GetString(descriptionAttributes[0].Name);
            }
            else
            {
                return descriptionAttributes[0].Name;
            }
        }

        public static string YesOrNo(byte value)
        {
            return Convert.ToBoolean(value) ? "Yes" : "No";
        }

        public static DataTable ToDataTable<T>(this List<T> items)
        {
            if (typeof(T).IsValueType)
            {
                DataTable dataTable = new DataTable(typeof(T).Name);
                dataTable.Columns.Add("Id");
                foreach (var item in items)
                {
                    if (item.GetType().IsEnum)
                    {
                        dataTable.Rows.Add(Convert.ToInt32(item));
                    }
                    else
                    {
                        dataTable.Rows.Add(item);
                    }
                }

                return dataTable;
            }
            else
            {
                DataTable dataTable = new DataTable(typeof(T).Name);

                // Get all the properties by using reflection
                PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => !x.GetCustomAttributes(typeof(ExcludeColumn)).Any()).ToArray();

                foreach (PropertyInfo prop in props)
                {
                    // Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }

                foreach (T item in items)
                {
                    var values = new object[props.Length];
                    for (int i = 0; i < props.Length; i++)
                    {
                        values[i] = props[i].GetValue(item, null);
                    }

                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }

        public static (DataTable dataTable, PropertyInfo[] propertyInfo) ToExportDataTable<T>(this List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            // Get all the properties by using reflection
            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.GetCustomAttributes(typeof(ExportMappingAttribute)).Any()).ToArray();

            foreach (PropertyInfo prop in props)
            {
                // Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }

            foreach (T item in items)
            {
                var values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }

                dataTable.Rows.Add(values);
            }

            return (dataTable, props);
        }

        public static int ToFinancialYear(this DateTime date)
        {
            var year = date.Year;
            if (date.Month > 3)
            {
                return Convert.ToInt32(string.Concat(year, (year + 1).ToString().Substring(2, 2)));
            }
            else
            {
                return Convert.ToInt32(string.Concat(year - 1, year.ToString().Substring(2, 2)));
            }
        }

        public static int? ToFinancialYear(this DateTime? date)
        {
            if (date != null)
            {
                var year = date.Value.Year;
                if (date.Value.Month > 3)
                {
                    return Convert.ToInt32(string.Concat(year, (year + 1).ToString().Substring(2, 2)));
                }
                else
                {
                    return Convert.ToInt32(string.Concat(year - 1, year.ToString().Substring(2, 2)));
                }
            }

            return null;
        }

        public static (decimal totalAmountIncTax, decimal roundOffAmount) PerformRoundOff(decimal totalAmountIncTax, bool roundOffSettingValue)
        {
            var taxAmountAfterRoundOff = roundOffSettingValue ? Math.Ceiling(totalAmountIncTax) : Math.Floor(totalAmountIncTax);
            return (taxAmountAfterRoundOff, taxAmountAfterRoundOff - totalAmountIncTax);
        }

        public static List<int> GetFinancialYearList()
        {
            var financialYearList = new List<int>();
            var startDate = new DateTime(2017, 4, 1);
            for (int yearRange = DateTime.Now.Year; yearRange >= startDate.Year; yearRange--)
            {
                var year = (yearRange + 1).ToString().Substring(2, 2);
                if (DateTime.Now.Month <= 3 && yearRange != DateTime.Now.Year)
                {
                    financialYearList.Add(Convert.ToInt32(string.Concat(yearRange, year)));
                }
                else if (DateTime.Now.Month > 3)
                {
                    financialYearList.Add(Convert.ToInt32(string.Concat(yearRange, year)));
                }
            }

            return financialYearList;
        }

        public static string SerializeObject<T>(this T data)
        {
            return data == null ? null : JsonConvert.SerializeObject(data);
        }

        public static T DeserializeJson<T>(this string data)
        {
            return JsonConvert.DeserializeObject<T>(data, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Ignore, NullValueHandling = NullValueHandling.Ignore });
        }
    }
}
