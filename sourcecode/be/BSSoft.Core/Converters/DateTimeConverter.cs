﻿namespace SSWhite.Core.Converters
{
    using System;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using Newtonsoft.Json.Linq;
    using JsonSerializer = Newtonsoft.Json.JsonSerializer;

    public class DateTimeConverter : IsoDateTimeConverter
    {
        private readonly string _dateFormat;

        public DateTimeConverter(string dateFormat)
        {
            _dateFormat = dateFormat;
        }

        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null)
            {
                return null;
            }

            return existingValue;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JToken jToken = DateTime.Parse(value?.ToString()).ToString(_dateFormat);
            jToken.WriteTo(writer);
        }
    }
}
