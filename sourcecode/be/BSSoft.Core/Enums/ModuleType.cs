﻿namespace SSWhite.Core.Enums
{
    public enum ModuleType : short
    {
        Item = 1,
        Client = 2,
        Sales = 3,
        Purchase = 4,
        Receipt = 5,
        LotNumber = 6,
        Payment = 7,
        JournalVoucher = 8
    }
}
