﻿namespace SSWhite.Core.Common.Application
{
    public class AppSetting
    {
        public int Pagination { get; set; }

        public string DefaultPath { get; set; }

        public string DefaultCompanyLogoPath { get; set; }

        public string DefaultExceptionFilePath { get; set; }

        public bool IsLogRequest { get; set; }

        public string AvatarRootPathToStoreImage { get; set; }

        public string AttachmentRootPathToStoreImage { get; set; }

        public string AttachmentRootPathToGetImage { get; set; }

        public string DocumentAttachmentRootPathImage { get; set; }

        public string DocumentAttachmentPathToGetImage { get; set; }

        public string FtoApprovalLink { get; set; }

        public string EpoApprovalLink { get; set; }

        public string DocumentApprovalLink { get; set; }

        public string CreateUserPassword { get; set; }

        public string ForgetPasswordLink { get; set; }

        public Pdf Pdf { get; set; }

        public NotificationCredentials NotificationCredentials { get; set; }

        public bool IsRedisCacheSession { get; set; }

        public string DefaultSendToEmail { get; set; }

        public string DefaultChangeDepartmentSendToEmail { get; set; }

        public string DefaultLeaveSheetSendToEmail { get; set; }

        public string DefaultGatepassSendToEmail { get; set; }

        public string DefaultPreRecruitementSendToEmail { get; set; }

        public string DefaultAddNewEmployeeSendToEmail { get; set; }

        public string DefaultTrainingSendToEmail { get; set; }

        public string DefaultEmployeeOfTheMonthSendToEmail { get; set; }

        public string DefaultChangeDepartmentSendCCEmail { get; set; }

        public string DefaultLeaveSheetSendCCEmail { get; set; }

        public string DefaultGatepassSendCCEmail { get; set; }

        public string DefaultPreRecruitementSendCCEmail { get; set; }

        public string DefaultAddNewEmployeeSendCCEmail { get; set; }

        public string DefaultTrainingSendCCEmail { get; set; }

        public string DefaultEmployeeOfTheMonthSendCCEmail { get; set; }
    }
}
