﻿namespace SSWhite.Notification.Implementation
{
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Utilities;
    using SSWhite.Notification.Interface;
    using SSWhite.Notification.Model;

    public class SendMailService : ISendMailService
    {

        private readonly AppSetting _appSetting;

        public SendMailService(IOptions<AppSetting> appSetting)
        {
            _appSetting = appSetting.Value;
        }

        public async Task SendMailAsync(SendMail sendMail)
        {
            var message = new MailMessage();
            var toEmails = sendMail.ToEmails.ToString().Split(',');
            message.From = new MailAddress(_appSetting.NotificationCredentials.From);
            message.Subject = sendMail.Subject;
            GetMailingName(toEmails, message.To);

            if (sendMail.LinkedResource != null)
            {
                var alternateViews =
                AlternateView.CreateAlternateViewFromString(sendMail.Body, null, MediaTypeNames.Text.Html);
                alternateViews.LinkedResources.Add(sendMail.LinkedResource);
                message.AlternateViews.Add(alternateViews);
            }
            else
            {
                message.BodyEncoding = Encoding.UTF8;
                message.Body = sendMail.Body;
            }

            message.IsBodyHtml = sendMail.IsHtml;
            message.Priority = MailPriority.High;

            if (!string.IsNullOrEmpty(sendMail.CCEmails))
                message.CC.Add(sendMail.CCEmails);

            var smpt = new SmtpClient
            {
                Host = _appSetting.NotificationCredentials.Host,
                Port = _appSetting.NotificationCredentials.Port,
                EnableSsl = true,
                Credentials = new System.Net.NetworkCredential(_appSetting.NotificationCredentials.UserName, EncryptDecrypt.Decrypt(_appSetting.NotificationCredentials.Password))
            };

            smpt.SendMailAsync(message);
        }

        private static void GetMailingName(string[] emails, MailAddressCollection mailAddresses)
        {
            foreach (string multiemailid in emails)
            {
                mailAddresses.Add(new MailAddress(multiemailid));
            }
        }
    }
}
