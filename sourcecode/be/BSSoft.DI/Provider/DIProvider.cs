﻿namespace SSWhite.DI.Provider
{
    using System;
    using DryIoc;
    using DryIoc.Microsoft.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection;
    using SSWhite.DI.Registrator;

    public class DIProvider : IServiceProviderFactory<IContainer>
    {
        public IContainer CreateBuilder(IServiceCollection services)
        {
            return new Container().WithDependencyInjectionAdapter(services);
        }

        public IServiceProvider CreateServiceProvider(IContainer containerBuilder)
        {
            return containerBuilder.ConfigureServiceProvider<DIRegistrator>();
        }
    }
}
