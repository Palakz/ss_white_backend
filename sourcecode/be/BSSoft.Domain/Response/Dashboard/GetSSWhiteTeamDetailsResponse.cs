﻿namespace SSWhite.Domain.Response.Dashboard
{
    public class GetSSWhiteTeamDetailsResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Designation { get; set; }

        public string Email { get; set; }

        public string Image { get; set; }
    }
}
