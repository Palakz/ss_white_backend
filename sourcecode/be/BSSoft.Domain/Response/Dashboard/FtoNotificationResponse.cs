﻿using Newtonsoft.Json;
using SSWhite.Core.Converters;
using SSWhite.Core.Enums;
using System;

namespace SSWhite.Domain.Response.Dashboard
{
    public class FtoNotificationResponse
    {
        public int Id { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int NumberOfDays { get; set; }

        [JsonConverter(typeof(EnumStringConverter))]
        public ApprovalType SupervisorApproval { get; set; }

        [JsonConverter(typeof(EnumStringConverter))]
        public ApprovalType HRApproval { get; set; }

        public string PersonName { get; set; }
    }
}
