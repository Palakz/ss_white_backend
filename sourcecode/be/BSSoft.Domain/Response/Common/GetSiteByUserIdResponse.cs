﻿namespace SSWhite.Domain.Response.Common
{
    public class GetSiteByUserIdResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int UserId { get; set; }
    }
}
