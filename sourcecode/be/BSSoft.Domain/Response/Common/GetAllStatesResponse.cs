﻿namespace SSWhite.Domain.Response.Common
{
    using SSWhite.Core.Enums;

    public class GetAllStatesResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public StatusType Status { get; set; }
    }
}
