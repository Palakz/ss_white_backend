﻿namespace SSWhite.Domain.Response.Common
{
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums;

    public class GetHeadersResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public CalculationType CalculationType { get; set; }

        public string Module { get; set; }

        public int ModuleId { get; set; }

        public string CalculationTypeString => CommonMethods.GetDisplayValue<CalculationType>(CalculationType);
    }
}
