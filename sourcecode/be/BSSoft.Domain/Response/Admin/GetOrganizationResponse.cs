﻿namespace SSWhite.Domain.Response.Admin
{
    public class GetOrganizationResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
