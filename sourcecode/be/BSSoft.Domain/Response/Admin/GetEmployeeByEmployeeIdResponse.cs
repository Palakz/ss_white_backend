﻿namespace SSWhite.Domain.Response.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Newtonsoft.Json;
    using SSWhite.Core.Common;
    using SSWhite.Core.Converters;

    public class GetEmployeeByEmployeeIdResponse
    {
        public int Id { get; set; }

        public string EmployeeId { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public int GenderId { get; set; }

        public string Gender { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]

        public string DOB { get; set; }

        public string Cast { get; set; }

        public string BloodGroup { get; set; }

        public int MaritalStatusesId { get; set; }

        public string MaritalStatus { get; set; }

        public string PhoneNo { get; set; }

        public string EmailAddress { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public string AvatarImage { get; set; }

        public string SignatureImage { get; set; }

        public string EmpPosition { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]

        public DateTime DOJ { get; set; }

        public string Organization { get; set; }

        public string ReportingUnder { get; set; }

        public string Department { get; set; }

        public string PreviousDepartment { get; set; }

        public string WorkEmail { get; set; }

        public string WorkPhoneNumber { get; set; }

        public string PFNumber { get; set; }

        public string UANNumber { get; set; }

        public string BankAccountNumber { get; set; }

        public string Buddy { get; set; }

        public string EmergencyContactName { get; set; }

        public string EmergencyMobileNumber { get; set; }

        public string EmergencyPhoneNumber { get; set; }

        public string EmergencyAddress { get; set; }

        public string EmergencyEmail { get; set; }

        public string SupervisorEmail { get; set; }

        public string SupervisorName { get; set; }

        public string ReasonForChangeDepartment { get; set; }

        public string? EmployeeName { get; set; }

        public List<DocumentsResponse>? DocumentsList { get; set; }
    }
}
