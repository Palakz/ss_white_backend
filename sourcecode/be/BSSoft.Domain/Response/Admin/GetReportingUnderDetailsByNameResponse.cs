﻿namespace SSWhite.Domain.Response.Admin
{
    public class GetReportingUnderDetailsByNameResponse
    {
        public string DepartmentName { get; set; }

        public string DepartmentSupervisorEmail { get; set; }

        public string DepartmentSupervisorName { get; set; }
    }
}
