﻿namespace SSWhite.Domain.Response.Admin
{
    using System.Collections.Generic;
    using SSWhite.Domain.Response.HumanResource;

    public class GetAllEmployeeDetailsDropdownsResponse
    {
        public List<GenderDropDown> GenderDropDown { get; set; }

        public List<MaritalStatusDropDown> MaritalStatusDropDown { get; set; }

        public List<DocumentTypesDropDown> DocumentTypesDropDown { get; set; }

        public List<DepartmentSupervisorDropDown> DepartmentSupervisorDropDown { get; set; }

        public List<BuddyDropDown> BuddyDropDown { get; set; }

        public List<OrganizationDropDown> OrganizationDropDown { get; set; }

        public List<EmployeeLeaveTypesDropDown> EmployeeLeaveTypesDropDown { get; set; }

        public List<DepartmentCoursesDropDown> DepartmentCoursesDropDown { get; set; }
    }

    public class GenderDropDown
    {
        public string Id { get; set; }

        public string Gender { get; set; }
    }

    public class MaritalStatusDropDown
    {
        public string Id { get; set; }

        public string MaritalStatus { get; set; }
    }

    public class DocumentTypesDropDown
    {
        public string Id { get; set; }

        public string DocumentType { get; set; }
    }

    public class DepartmentSupervisorDropDown
    {
        public string DepartmentSupervisorName { get; set; }
    }

    public class BuddyDropDown
    {
        public int UserID { get; set; }

        public string BuddyName { get; set; }

        public string PersonID { get; set; }
    }

    public class OrganizationDropDown
    {
        public string Id { get; set; }

        public string Organization { get; set; }
    }

    public class EmployeeLeaveTypesDropDown
    {
        public int LeaveTypeId { get; set; }

        public string LeaveType { get; set; }
    }

    public class DepartmentCoursesDropDown
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public string Location { get; set; }
    }
}
