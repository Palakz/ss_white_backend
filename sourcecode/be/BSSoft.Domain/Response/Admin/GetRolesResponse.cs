﻿namespace SSWhite.Domain.Response.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetRolesResponse
    {
        public int RoleID { get; set; }

        public string RoleName { get; set; }
    }
}
