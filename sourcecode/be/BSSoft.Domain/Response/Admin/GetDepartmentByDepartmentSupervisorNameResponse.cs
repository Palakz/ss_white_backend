﻿namespace SSWhite.Domain.Response.Admin
{
    public class GetDepartmentByDepartmentSupervisorNameResponse
    {
        public int Id { get; set; }

        public string DepartmentName { get; set; }
    }
}
