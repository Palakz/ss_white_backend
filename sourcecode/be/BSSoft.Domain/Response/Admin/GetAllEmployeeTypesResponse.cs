﻿namespace SSWhite.Domain.Response.Admin
{
    public class GetAllEmployeeTypesResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
