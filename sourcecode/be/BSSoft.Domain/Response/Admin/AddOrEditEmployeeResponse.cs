﻿namespace SSWhite.Domain.Response.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class AddOrEditEmployeeResponse
    {
        public string Loginid { get; set; }

        public string EmailAddress { get; set; }
    }
}
