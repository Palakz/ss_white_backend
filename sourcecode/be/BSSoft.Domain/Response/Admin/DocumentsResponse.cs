﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Response.Admin
{
    public class DocumentsResponse
    {
        public int? DocumentId { get; set; }

        public string Name { get; set; }

        public int UserId { get; set; }

        public int DocumentTypeId { get; set; }

        public string DocumentImage { get; set; }

        public string DocumentType { get; set; }

        public bool IsDeleted { get; set; }

        // public IFormFile Image { get; set; }
    }
}
