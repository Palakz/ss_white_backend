﻿namespace SSWhite.Domain.Response.TimeAndAttendance
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Core.Common;
    using SSWhite.Core.Enums;

    public class GetUsersInOutStatusResponse
    {
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Department { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNo { get; set; }

        public DateTime PunchTime { get; set; }

        public int AttendanceInOutStatusId { get; set; }

        public string Picture { get; set; }
    }
}
