﻿namespace SSWhite.Domain.Response.TimeAndAttendance
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Core.Common;
    using SSWhite.Core.Enums;

    public class GetUsersInOutStatus
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        ////public double TotalHours { get; set; }

        public DateTime PunchTime { get; set; }

        public string InOutStatus { get; set; }

        public int PunchType { get; set; }
    }
}
