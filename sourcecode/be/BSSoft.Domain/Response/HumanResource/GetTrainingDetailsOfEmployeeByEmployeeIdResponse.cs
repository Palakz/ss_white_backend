﻿namespace SSWhite.Domain.Response.HumanResource
{
    public class GetTrainingDetailsOfEmployeeByEmployeeIdResponse
    {
        //public int Id { get; set; }

        public string CourseName { get; set; }

        public string SummaryOfContent { get; set; }

        public string Location { get; set; }

        public string JobDescriptionImage { get; set; }

        public string TrainingHardCopyImage { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string Hours { get; set; }
    }
}