﻿namespace SSWhite.Domain.Response.HumanResource
{
    public class GetTrainingDetailsOfInstructorByInstructorNameResponse
    {
        //public int TrainingId { get; set; }

        public string Instructor { get; set; }

        public string CourseName { get; set; }

        public string Hours { get; set; }

        public string Location { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }
    }
}