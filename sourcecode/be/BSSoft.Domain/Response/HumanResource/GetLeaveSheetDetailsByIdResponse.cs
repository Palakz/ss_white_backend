﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System;

    public class GetLeaveSheetDetailsByIdResponse
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        //public string EmployeeName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string EmployeeId { get; set; }

        public int EmployeeTypeId { get; set; }

        public string EmployeeType { get; set; }

        public string? ContractAgency { get; set; }

        public string Department { get; set; }

        public string Designation { get; set; }

        public int? EVL { get; set; }

        public int? EFL { get; set; }

        public int? LeaveTypeId { get; set; }

        public string? LeaveType { get; set; }

        public DateTime LeaveApplyDate { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public float CountOfDays { get; set; }

        public string LeavePurpose { get; set; }

        public string? AddressWhileOnLeave { get; set; }

        public string? Note { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? CreatedBy { get; set; }
    }
}
