﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System;

    public class GetGatepassByIdResponse
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        //public string EmployeeName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string PersonID { get; set; }

        public int EmployeeTypeId { get; set; }

        public string EmployeeType { get; set; }

        public string ContractAgency { get; set; }

        public string Department { get; set; }

        public string Designation { get; set; }

        public string PhoneNo { get; set; }

        public string Purpose { get; set; }

        public DateTime TimeIn { get; set; }

        public DateTime TimeOut { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? CreatedBy { get; set; }

        public string? CreatedByName { get; set; }
    }
}
