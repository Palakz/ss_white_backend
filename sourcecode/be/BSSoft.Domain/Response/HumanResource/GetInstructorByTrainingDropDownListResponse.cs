﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System;

    public class GetInstructorByTrainingDropDownListResponse
    {
        public int InstructorId { get; set; }

        public string Instructor { get; set; }
    }
}