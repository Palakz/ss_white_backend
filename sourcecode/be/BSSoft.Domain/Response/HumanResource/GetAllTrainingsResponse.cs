﻿namespace SSWhite.Domain.Response.HumanResource
{
    public class GetAllTrainingsResponse
    {
        public int Id { get; set; }

        public int DepartmentCourseId { get; set; }

        public string DepartmentCourseName { get; set; }

        public string Location { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string Hours { get; set; }
        public bool IsDeleted { get; set; }
    }
}