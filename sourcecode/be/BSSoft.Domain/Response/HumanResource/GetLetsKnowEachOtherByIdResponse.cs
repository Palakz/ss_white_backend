﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System;

    public class GetLetsKnowEachOtherByIdResponse
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        //public string EmployeeName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string PersonID { get; set; }

        public string SSWGoals { get; set; }

        public string InterestingFacts { get; set; }

        public string Hobbies { get; set; }

        public string FavouriteQuote { get; set; }

        public string FavouriteMovie { get; set; }

        public string FavouriteSong { get; set; }

        public string FavouriteBook { get; set; }

        public string PersonalGoals { get; set; }

        public string FamilyIntroduction { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? CreatedBy { get; set; }

        //public string? CreatedByName { get; set; }
    }
}
