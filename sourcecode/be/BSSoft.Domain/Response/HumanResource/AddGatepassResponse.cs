﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class AddGatepassResponse
    {
        public int Id { get; set; }
    }
}
