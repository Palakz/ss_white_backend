﻿using System.Collections.Generic;

namespace SSWhite.Domain.Response.HumanResource
{
    public class GetAllRecruiteeDropdownsResponse
    {
        public List<GendersDropDown> GendersDropDown { get; set; }

        public List<IdentitiesDropDown> IdentitiesDropDown { get; set; }

        public List<MaritalStatusesDropDown> MaritalStatusesDropDown { get; set; }

        public List<RelationTypesDropDown> RelationTypesDropDown { get; set; }

        public List<QuestionsDropDown> QuestionsDropDown { get; set; }
    }

    public class GendersDropDown
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }

    public class IdentitiesDropDown
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }

    public class MaritalStatusesDropDown
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }

    public class RelationTypesDropDown
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }

    public class QuestionsDropDown
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string QuestionCategoriesId { get; set; }

        public string QuestionTypesId { get; set; }

        public string Options { get; set; }

        public List<string> OptionValues { get; set; }

        public string Attachment { get; set; }

        public string Answer { get; set; }
    }
}
