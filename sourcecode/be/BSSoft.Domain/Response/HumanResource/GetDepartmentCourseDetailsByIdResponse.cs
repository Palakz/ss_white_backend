﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System;

    public class GetDepartmentCourseDetailsByIdResponse
    {
        public int Id { get; set; }

        public string CourseName { get; set; }

        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public int SubDepartmentId { get; set; }

        public string SubDepartmentName { get; set; }

        public string SummaryOfContent { get; set; }

        public decimal Hours { get; set; }

        public string Location { get; set; }

        public DateTime CreatedDate { get; set; }

        public int? CreatedBy { get; set; }
    }
}
