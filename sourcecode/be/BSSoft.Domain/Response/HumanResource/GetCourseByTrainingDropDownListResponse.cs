﻿namespace SSWhite.Domain.Response.HumanResource
{
    public class GetCourseByTrainingDropDownListResponse
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }
    }
}