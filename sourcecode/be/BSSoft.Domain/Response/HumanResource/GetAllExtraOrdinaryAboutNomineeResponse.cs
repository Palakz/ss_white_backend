﻿using System;

namespace SSWhite.Domain.Response.HumanResource
{
    public class GetAllExtraOrdinaryAboutNomineeResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
