﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System.Collections.Generic;
    using SSWhite.Domain.Response.Admin;

    public class GetDepartmentCoursesByDepartmentIdResponse
    {
        public List<DepartmentCoursesDetails> DepartmentCoursesDetails { get; set; }

        public List<BuddyDropDown> BuddyDropDowns { get; set; }
    }

    public class DepartmentCoursesDetails
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public int DepartmentId { get; set; }

        public string SummaryOfContent { get; set; }

        public decimal Hours { get; set; }

        public string Location { get; set; }
    }
}
