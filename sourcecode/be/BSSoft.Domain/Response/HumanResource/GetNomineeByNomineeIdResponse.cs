﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System;

    public class GetNomineeByNomineeIdResponse
    {
        public int NomineeId { get; set; }

        public int EmployeeId { get; set; }

        //public string EmployeeName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public int EmployeeTypeId { get; set; }

        public string EmployeeType { get; set; }

        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public string Supervisor { get; set; }

        public string AdditionalDetails { get; set; }

        public int NominatedById { get; set; }

        public string NominatedBy { get; set; }

        public DateTime NominatedDate { get; set; }

        public string NominatedDetailsId { get; set; }

        public int Status { get; set; }
    }
}
