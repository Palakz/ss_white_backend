﻿namespace SSWhite.Domain.Response.HumanResource
{
    public class GetEmployeeTrainingDropDownListResponse
    {
        public int UserId { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string EmployeeName { get; set; }  
    }
}