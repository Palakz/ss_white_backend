﻿namespace SSWhite.Domain.Response.HumanResource
{
    public class GetHoursByTrainingDropDownListResponse
    {
        public int Id { get; set; }

        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public string Location { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string Hours { get; set; }
    }
}