﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Common;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Request.HumanResource;

    public class GetTrainingByIdResponse
    {
        public int TrainingId { get; set; }

        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public int DepartmentCourseId { get; set; }

        public string DepartmentCourseName { get; set; }

        public string Instructor { get; set; }

        public int AuthorId { get; set; }

        public string AuthorFirstName { get; set; }

        public string AuthorMiddleName { get; set; }

        public string AuthorLastName { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]
        public string FromDate { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]
        public string ToDate { get; set; }

        public decimal Hours { get; set; }

        public int EnteredById { get; set; }

        public string EnteredBy { get; set; }

        public string Location { get; set; }

        public string SummaryOfContent { get; set; }

        public int TrainingStatus { get; set; }

        public bool IsDeleted { get; set; }

        public List<EmployeeTrainingResponse> EmployeeTraining { get; set; }
    }
}
