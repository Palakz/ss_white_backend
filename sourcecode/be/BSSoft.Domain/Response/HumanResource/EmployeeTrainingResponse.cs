﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Common;
    using SSWhite.Core.Converters;

    public class EmployeeTrainingResponse
    {
        public int EmployeeTrainingId { get; set; }

        public int UserId { get; set; }

        public string PersonID { get; set; }

        public string PersonName { get; set; }

        public int TrainingId { get; set; }

        public int TrainingTypeId { get; set; }

        public string TrainingType { get; set; }

        public bool IsEvaluated { get; set; }
        //public string IsEvaluated { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]
        public string EvaluationDate { get; set; }

        public string EvaluationMethod { get; set; }

        public bool HasAttachment { get; set; }
        //public string HasAttachment { get; set; }

        public string JobDescriptionImage { get; set; }

        public string TrainingHardCopyImage { get; set; }

        public int EmployeeTrainingStatus { get; set; }

        public bool IsDeleted { get; set; }

        public string? JobDescriptionImageName { get; set; }

        public string? TrainingHardCopyImageName { get; set; }
    }
}