﻿namespace SSWhite.Domain.Response.HumanResource
{
    public class GetTrainingDetailsOfCourseByCourseIdResponse
    {
        public string EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public string CourseName { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string Hours { get; set; }
    }
}
