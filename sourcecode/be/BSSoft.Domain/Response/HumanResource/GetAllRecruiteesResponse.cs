﻿using System;

namespace SSWhite.Domain.Response.HumanResource
{
    public class GetAllRecruiteesResponse
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string SurName { get; set; }

        public DateTime DOB { get; set; }

        public int GendersId { get; set; }

        public int MaritalStatusesId { get; set; }

        public string ImagePath { get; set; }

        public int Status { get; set; }

        public bool DoAnyRelative { get; set; }

        public bool IsDisabilityOrSickness { get; set; }

        public string DisabilityDescription { get; set; }

        public bool IsWorkExperience { get; set; }

        public bool IsFromReference { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public string Gender { get; set; }

        public string MaritalStatus { get; set; }
    }
}
