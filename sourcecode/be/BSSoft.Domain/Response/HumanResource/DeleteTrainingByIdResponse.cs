﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Domain.Request.HumanResource;

    public class DeleteTrainingByIdResponse
    {
        public int TrainingId { get; set; }
    }
}
