﻿using SSWhite.Domain.Request.HumanResource;
using System;
using System.Collections.Generic;

namespace SSWhite.Domain.Response.HumanResource
{
    public class GetRecruiteeByIdResponse
    {
        //public RecruiteeBasicDetails RecruiteeBasicDetails { get; set; }
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string SurName { get; set; }

        public DateTime DOB { get; set; }

        public int GendersId { get; set; }

        public int MaritalStatusesId { get; set; }

        public string ImagePath { get; set; }

        public int Status { get; set; }

        public bool DoAnyRelative { get; set; }

        public string RelativeName { get; set; }

        public string RelationWithRelative { get; set; }

        public string IdentityProofs { get; set; }

        public string IdentityProofsValue { get; set; }

        public bool IsDisabilityOrSickness { get; set; }

        public string DisabilityDescription { get; set; }

        public bool IsWorkExperience { get; set; }

        public bool IsFromReference { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CreatedBy { get; set; }

        public string Gender { get; set; }

        public string MaritalStatus { get; set; }

        public List<RecruiteeAddresses> RecruiteeAddresses { get; set; }

        public List<RecruiteeEducations> RecruiteeEducations { get; set; }

        public List<RecruiteeExperiences> RecruiteeExperiences { get; set; }

        public List<RecruiteeFamilyDetails> RecruiteeFamilyDetails { get; set; }

        public List<RecruiteeQuestions> RecruiteeQuestions { get; set; }

        public List<RecruiteeReferences> RecruiteeReferences { get; set; }
    }
}
