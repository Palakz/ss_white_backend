﻿namespace SSWhite.Domain.Response.HumanResource
{
    using System.Collections.Generic;

    public class GetEmployeeLeaveDetailsByIdResponse
    {
        public List<EmployeeDetailsForLeaveSheet> EmployeeDetailsForLeaveSheet { get; set; }

        public List<EmployeeCurrentMonthLeaveDetails>? EmployeeCurrentMonthLeaveDetails { get; set; }
    }

    public class EmployeeDetailsForLeaveSheet
    {
        public string PersonName { get; set; }

        public string EmployeeId { get; set; }

        public string Department { get; set; }

        public string Designation { get; set; }
    }

    public class EmployeeCurrentMonthLeaveDetails
    {
        public int LeaveTypeId { get; set; }

        public string LeaveType { get; set; }

        public int NoOfDaysForLeave { get; set; }
    }
}
