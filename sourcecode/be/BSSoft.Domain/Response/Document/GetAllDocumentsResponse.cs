﻿namespace SSWhite.Domain.Response.Document
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllDocumentsResponse
    {
        public int Id { get; set; }

        public string DocumentId { get; set; }

        public string DocumentTitle { get; set; }

        public string Rev { get; set; }

        public string PersonName { get; set; }

        public DateTime CreatedDate { get; set; }

        [JsonConverter(typeof(EnumStringConverter))]
        public ApprovalType? ApprovalStatus { get; set; }

        public int DocumentType { get; set; }

        public bool? CanEdit { get; set; }

        public bool? IsSendForApproval { get; set; }
    }
}