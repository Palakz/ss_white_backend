﻿namespace SSWhite.Domain.Response.Document
{
    public class ApproveDocumentResponse
    {
        public string ApprovedPerson { get; set; }

        public string DocumentTitle { get; set; }

        public string DocumentId { get; set; }

        public string EmailAddress { get; set; }

        public int Result { get; set; }

    }
}
