﻿namespace SSWhite.Domain.Response.Document
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Domain.Request.Document;

    public class DocumentEcnChange
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}