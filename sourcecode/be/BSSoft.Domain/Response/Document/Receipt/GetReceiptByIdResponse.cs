﻿namespace SSWhite.Domain.Response.Document.Receipt
{
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Common.Document.Receipt;
    using SSWhite.Domain.Enums.Document;

    public class GetReceiptByIdResponse : Receipt
    {
        public string ClientName { get; set; }

        public string DayBook { get; set; }

        public string Bank { get; set; }

        public string ReceiptTypeString => CommonMethods.GetDisplayValue<DocumentType?>(ReceiptType);
    }
}
