﻿namespace SSWhite.Domain.Response.Document.Payment
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums.Document;

    public class GetAllPaymentsResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string PaymentNumber { get; set; }

        public DateTime Date { get; set; }

        public string DayBook { get; set; }

        public string ClientName { get; set; }

        public DocumentType PaymentType { get; set; }

        public decimal PaidAmount { get; set; }

        public decimal TotalAmount { get; set; }

        public StatusType? Status { get; set; }

        public string PaymentTypeString => CommonMethods.GetDisplayValue(PaymentType);
    }
}
