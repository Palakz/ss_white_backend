﻿namespace SSWhite.Domain.Response.Master.ItemType
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllItemTypesResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public StatusType? Status { get; set; }
    }
}
