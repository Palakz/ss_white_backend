﻿namespace SSWhite.Domain.Response.Master.LedgerGroup
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums.Master.LedgerGroup;

    public class GetAllLedgerGroupsResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public LedgerGroupType LedgerGroupType { get; set; }

        public LedgerAccountType LedgerAccountType { get; set; }

        public StatusType Status { get; set; }

        public string LedgerGroupTypeString => CommonMethods.GetDisplayValue<LedgerGroupType>(LedgerGroupType);

        public string LedgerAccountTypeString => CommonMethods.GetDisplayValue<LedgerAccountType>(LedgerAccountType);

        public string StatusTypeString => CommonMethods.GetDisplayValue<StatusType>(Status);
    }
}
