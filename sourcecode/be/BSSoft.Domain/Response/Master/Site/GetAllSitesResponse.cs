﻿namespace SSWhite.Domain.Response.Master.Site
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllSitesResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public string Name { get; set; }

        public string City { get; set; }

        public string Address { get; set; }

        public StatusType Status { get; set; }
    }
}
