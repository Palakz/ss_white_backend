﻿namespace SSWhite.Domain.Response.Master.City
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllCitiesResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public string State { get; set; }

        public StatusType Status { get; set; }
    }
}
