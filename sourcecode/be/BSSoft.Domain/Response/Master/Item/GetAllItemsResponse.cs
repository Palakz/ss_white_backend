﻿namespace SSWhite.Domain.Response.Master.Item
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllItemsResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Group { get; set; }

        public string Unit { get; set; }

        public string Category { get; set; }

        public string Crate { get; set; }

        public StatusType? Status { get; set; }
    }
}
