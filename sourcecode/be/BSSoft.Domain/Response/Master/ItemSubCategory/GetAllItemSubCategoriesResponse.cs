﻿namespace SSWhite.Domain.Response.Master.ItemSubCategory
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllItemSubCategoriesResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public string ItemCategory { get; set; }

        public StatusType? Status { get; set; }
    }
}
