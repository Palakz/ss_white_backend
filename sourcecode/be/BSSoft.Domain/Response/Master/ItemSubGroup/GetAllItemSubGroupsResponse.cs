﻿namespace SSWhite.Domain.Response.Master.ItemSubGroup
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;

    public class GetAllItemSubGroupsResponse
    {
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int Id { get; set; }

        public string Name { get; set; }

        public string ItemGroup { get; set; }

        public StatusType? Status { get; set; }
    }
}
