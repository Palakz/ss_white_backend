﻿namespace SSWhite.Domain.Response.AttendanceChange
{
    using System;
    using SSWhite.Domain.Enums;

    public class CreateAttendanceFtoResponse
    {
        public int FtoId { get; set; }

        public DateTime StartDate { get; set; }

        public DayType StartDateDayType { get; set; }

        public DateTime EndDate { get; set; }

        public DayType EndDateDayType { get; set; }

        public string LeaveTypeName { get; set; }

        public string CreatedBy { get; set; }

        public string EmailIdsForFtoNotification { get; set; }

        public string EmailIdForFtoApproval { get; set; }
    }
}
