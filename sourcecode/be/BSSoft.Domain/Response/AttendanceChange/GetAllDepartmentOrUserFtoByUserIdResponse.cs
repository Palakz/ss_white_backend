﻿namespace SSWhite.Domain.Response.AttendanceChange
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Core.Enums;
    using SSWhite.Domain.Enums;

    public class GetAllDepartmentOrUserFtoByUserIdResponse
    {
        public int Id { get; set; }

        public string PersonName { get; set; }

        public string PersonId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string LeaveType { get; set; }

        public string EmployeeComment { get; set; }

        [JsonConverter(typeof(EnumStringConverter))]
        public ApprovalType SupervisorApproval { get; set; }

        [JsonConverter(typeof(EnumStringConverter))]
        public ApprovalType HrApproval { get; set; }
    }
}
