﻿namespace SSWhite.Domain.Response.AttendanceChange
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Common;
    using SSWhite.Core.Converters;

    public class GetDepartmentFtoByUserIdResponse
    {
        public int Id { get; set; }

        public string PersonName { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]
        public DateTime StartDate { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]
        public DateTime EndDate { get; set; }
    }
}
