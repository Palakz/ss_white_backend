﻿namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    using System;

    public class GetMpcVoteDetailsByEpoIdResponse
    {
        public string PersonName { get; set; }

        public string MpcNumber { get; set; }

        public int? MpcApproval { get; set; }

        public DateTime? MpcVoteDate { get; set; }
    }
}
