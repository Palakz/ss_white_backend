﻿namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    public class GetEpoModulesCountsResponse
    {
        public string MpcVoteForEpoCount { get; set; }

        public string MpcAllVoteForEpoCount { get; set; }

        public string MySubordinateEpoCount { get; set; }
    }
}
