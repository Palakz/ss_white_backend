﻿namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    using Newtonsoft.Json;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Enums;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class AddOrEditResponse
    {
        public string EpoApprovalMessage { get; set; }

        public string EpoID { get; set; }

        public string CreatedBy { get; set; }

        public decimal GrandTotal { get; set; }

        public string PurposeOfPurchase { get; set; }

        public string PersonEmailId { get; set; }

        public string ApprovalEmailIds { get; set; }

        public string AwaitingForApproval { get; set; }

        public bool? IsAttachmentPending { get; set; }

        [JsonConverter(typeof(EnumStringConverter))]
        public CurrencyType CurrencyType { get; set; }
    }
}
