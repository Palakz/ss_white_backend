﻿namespace SSWhite.Domain.Response.ExpensePurchaseOrder
{
    using System.Collections.Generic;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;

    public class GetEpoDetailsByEpoIdResponse
    {
        public EpoBasicDetailsResponse EpoBasicDetailsResponse { get; set; }

        public List<EpoLineItemsList> EpoLineItemsList { get; set; }

        public List<EpoAttachmentMst> EpoAttachmentMst { get; set; }

        public bool? CanEdit { get; set; }

        public string ApprovedBY { get; set; }
    }
}
