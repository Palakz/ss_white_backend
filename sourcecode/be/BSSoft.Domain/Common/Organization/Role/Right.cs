﻿namespace SSWhite.Domain.Common.Organization.Role
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class Right
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public int? RightsGroupId { get; set; }

        public string Name { get; set; }

        public int DisplayOrder { get; set; }

        public string Acryonym { get; set; }
    }
}
