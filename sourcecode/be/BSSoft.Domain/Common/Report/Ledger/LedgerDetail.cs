﻿namespace SSWhite.Domain.Common.Report.Ledger
{
    using System;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums;

    public class LedgerDetail
    {
        public DateTime? Date { get; set; }

        public string Particulars { get; set; }

        public string Type { get; set; }

        public string ReferenceNumber { get; set; }

        public decimal? Debit { get; set; }

        public decimal? Credit { get; set; }

        public TransactionType TransactionType { get; set; }

        public string TransactionTypeString => CommonMethods.GetDisplayValue(TransactionType);
    }
}
