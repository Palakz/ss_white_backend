﻿namespace SSWhite.Domain.Common.Document
{
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums;

    public class DocumentHeader
    {
        public int? HeaderId { get; set; }

        public string Name { get; set; }

        public CalculationType CalculationType { get; set; }

        public string CalculationTypeString => CommonMethods.GetDisplayValue(CalculationType);

        public decimal? Percentage { get; set; }

        public decimal? Value { get; set; }
    }
}
