﻿namespace SSWhite.Domain.Common.Document.Purchase
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Enums.Document;

    public class PurchaseDocument
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public int? CompanyId { get; set; }

        public int? ClientId { get; set; }

        public string VoucherNumber { get; set; }

        public DateTime? Date { get; set; }

        public VoucherType VoucherType { get; set; }

        public string ChallanNumber { get; set; }

        public DateTime? ChallanDate { get; set; }

        public string VehicleNumber { get; set; }

        public string MarkNumber { get; set; }

        public int Month { get; set; }

        public int FinancialYear { get; set; }

        public string ReceivingPerson { get; set; }

        public decimal? TotalAmount { get; set; }

        public string Remarks { get; set; }

        public List<PurchaseDocumentItem> PurchaseDocumentItems { get; set; }

        public List<DocumentHeader> DocumentHeaders { get; set; }

        public string Prefix { get; set; }

        public string Suffix { get; set; }
    }
}
