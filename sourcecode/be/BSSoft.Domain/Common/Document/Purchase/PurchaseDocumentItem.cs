﻿namespace SSWhite.Domain.Common.Document.Purchase
{
    using SSWhite.Core.Attributes;

    public class PurchaseDocumentItem
    {
        public int? ItemId { get; set; }

        public int? CrateId { get; set; }

        public int? UnitId { get; set; }

        [ExcludeColumn]
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal? Quantity { get; set; }

        public decimal? CurrentQuantity { get; set; }

        public decimal? Weight { get; set; }

        public decimal? CurrentWeight { get; set; }

        public decimal? FreeQuantity { get; set; }

        public decimal? Rate { get; set; }

        [ExcludeColumn]
        public string Unit { get; set; }

        public decimal? Amount { get; set; }

        public int? LotNumber { get; set; }

        public string CrateName { get; set; }
    }
}
