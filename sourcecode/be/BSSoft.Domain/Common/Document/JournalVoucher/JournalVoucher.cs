﻿namespace SSWhite.Domain.Common.Document.JournalVoucher
{
    using System;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class JournalVoucher
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public int? CompanyId { get; set; }

        public int CreditedClientId { get; set; }

        public string CreditedClientName { get; set; }

        public int DebitedClientId { get; set; }

        public string DebitedClientName { get; set; }

        public string VoucherNumber { get; set; }

        public DateTime? Date { get; set; }

        public string CreditedReferenceNumber { get; set; }

        public DateTime? CreditedReferenceDate { get; set; }

        public string DebitedReferenceNumber { get; set; }

        public DateTime? DebitedReferenceDate { get; set; }

        public int Month { get; set; }

        public int FinancialYear { get; set; }

        public decimal TotalAmount { get; set; }

        public string Remarks { get; set; }

        public string Suffix { get; set; }

        public string Prefix { get; set; }
    }
}
