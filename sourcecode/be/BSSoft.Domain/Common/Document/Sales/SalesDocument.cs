﻿namespace SSWhite.Domain.Common.Document.Sales
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Enums.Document;

    public class SalesDocument
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public int? CompanyId { get; set; }

        public int? ClientId { get; set; }

        public string VoucherNumber { get; set; }

        public DateTime? Date { get; set; }

        public VoucherType VoucherType { get; set; }

        public int Month { get; set; }

        public int FinancialYear { get; set; }

        public string ReceivedBy { get; set; }

        public decimal? TotalAmount { get; set; }

        public decimal? AdvanceAmount { get; set; }

        public string AdvanceReceivedBy { get; set; }

        public string Remarks { get; set; }

        public List<SalesDocumentItem> SalesDocumentItems { get; set; }

        public List<DocumentHeader> DocumentHeaders { get; set; }

        public string Prefix { get; set; }

        public string Suffix { get; set; }
    }
}
