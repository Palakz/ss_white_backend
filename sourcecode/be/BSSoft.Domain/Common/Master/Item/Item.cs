﻿namespace SSWhite.Domain.Common.Master.Item
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class Item
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string HsnOrSac { get; set; }

        public int ItemGroupId { get; set; }

        public int ItemSubGroupId { get; set; }

        public int ItemCategoryId { get; set; }

        public int ItemSubCategoryId { get; set; }

        public List<ItemUnit> ItemUnits { get; set; }

        public List<ItemCrate> ItemCrates { get; set; }

        public string Remarks { get; set; }

        public string Suffix { get; set; }

        public string Prefix { get; set; }
    }
}
