﻿namespace SSWhite.Domain.Common.Master.Uqc
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class Uqc
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public int SubUnitId { get; set; }

        public int PackingTypeId { get; set; }

        public decimal? ConversionUnit { get; set; }
    }
}
