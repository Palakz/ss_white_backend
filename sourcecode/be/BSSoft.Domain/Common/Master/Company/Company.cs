﻿namespace SSWhite.Domain.Common.Master.Company
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Common.Master.Common;

    public class Company
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Gstin { get; set; }

        public string Pan { get; set; }

        public string Address { get; set; }

        public int? CountryId { get; set; }

        public int? StateId { get; set; }

        public int? CityId { get; set; }

        public string Pincode { get; set; }

        public string EmailAddress { get; set; }

        public string ContactPerson { get; set; }

        public string ContactNumber { get; set; }

        public string Website { get; set; }

        public string OtherLicense { get; set; }

        public string TagLine { get; set; }

        public string TermsAndConditions { get; set; }

        public List<IFormFile> Logo { get; set; }

        public string LogoPath { get; set; }

        public List<BankDetail> BankDetails { get; set; }
    }
}
