﻿namespace SSWhite.Domain.Common.Master.Client
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Common.Master.Common;
    using SSWhite.Domain.Enums;

    public class Client
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public string Name { get; set; }

        public string CompanyName { get; set; }

        public string Code { get; set; }

        public string Gstin { get; set; }

        public string Pan { get; set; }

        public string Address { get; set; }

        public int? CountryId { get; set; }

        public int? StateId { get; set; }

        public int? CityId { get; set; }

        public string EmailAddress { get; set; }

        public string MobileNumber { get; set; }

        public string ContactNumber { get; set; }

        public string Website { get; set; }

        public decimal? CreditLimit { get; set; }

        public decimal? OpeningBalance { get; set; }

        public TransactionType? TransactionType { get; set; }

        public int? LedgerGroupId { get; set; }

        public int? RegionId { get; set; }

        public List<ClientHeaderDetail> ClientHeaderDetails { get; set; }

        public List<BankDetail> BankDetails { get; set; }

        public List<ContactDetail> ContactDetails { get; set; }

        public string Suffix { get; set; }

        public string Prefix { get; set; }

        public int ActualLastNumber { get; set; }

        public bool IsStatutoryDetails { get; set; }

        public bool IsAddressDetails { get; set; }
    }
}
