﻿namespace SSWhite.Domain.Common.Master.Client
{
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums;

    public class ClientHeaderDetail
    {
        public int? HeaderId { get; set; }

        public string Name { get; set; }

        public CalculationType CalculationType { get; set; }

        public string Module { get; set; }

        public int ModuleId { get; set; }

        public string CalculationTypeString => CommonMethods.GetDisplayValue<CalculationType>(CalculationType);

        public decimal? Value { get; set; }
    }
}
