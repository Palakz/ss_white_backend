﻿namespace SSWhite.Domain.Common.Master.Client
{
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Utilities;
    using SSWhite.Domain.Enums;

    public class ClientOpeningBalance
    {
        [Decrypt]
        public int ClientId { get; set; }

        public int FinancialYear { get; set; }

        public decimal? Balance { get; set; }

        public TransactionType? TransactionType { get; set; }

        public string TransactionTypeString => CommonMethods.GetDisplayValue(TransactionType);
    }
}
