﻿namespace SSWhite.Domain.Common.Master.LedgerGroup
{
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;
    using SSWhite.Domain.Enums.Master.LedgerGroup;

    public class LedgerGroup
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public LedgerGroupType? LedgerGroupType { get; set; }

        public LedgerAccountType? AccountType { get; set; }

        public int Order { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<LedgerSectionType> SectionTypes { get; set; }

        public string Sections { get; set; }
    }
}
