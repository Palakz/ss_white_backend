﻿namespace SSWhite.Domain.Common.Master.Site
{
    using Newtonsoft.Json;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Converters;

    public class Site
    {
        [Decrypt]
        [JsonConverter(typeof(EncryptPropertyConverter))]
        public int? Id { get; set; }

        [UpperCase]
        public string Name { get; set; }

        [UpperCase]
        public string Address { get; set; }

        public int? StateId { get; set; }

        public int? CityId { get; set; }
    }
}
