﻿namespace SSWhite.Domain.Request.Document.Receipt
{
    using System;
    using SSWhite.Core.Enums;
    using SSWhite.Domain.Enums.Document;

    public class GetAllReceipts
    {
        public int? CompanyId { get; set; }

        public int? ClientId { get; set; }

        public int? Month { get; set; }

        public int? FinancialYear { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public StatusType? Status { get; set; }

        public DocumentType? ReceiptType { get; set; }
    }
}
