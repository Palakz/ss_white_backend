﻿namespace SSWhite.Domain.Request.Document
{
    public class GetDocumentByIdRequest
    {
        public int Id { get; set; }

        public string? DocumentId { get; set; }
    }
}
