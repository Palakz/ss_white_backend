﻿using SSWhite.Core.Converters;
using SSWhite.Domain.Enums.Document;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace SSWhite.Domain.Request.Document
{
    public class RevisionLog
    {
        public string? Rev { get; set; }

        public string? SectionParaChanged { get; set; }

        public string? ChangeMade { get; set; }

        //[JsonConverter(typeof(EnumStringConverter))]
        public string? TrainingRequired { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string ChangedBy { get; set; }
    }
}
