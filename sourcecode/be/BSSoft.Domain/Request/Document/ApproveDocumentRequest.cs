﻿namespace SSWhite.Domain.Request.Document
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Enums;

    public class ApproveDocumentRequest
    {
        public int Id { get; set; }

        public ApprovalType? ApprovalType { get; set; }
    }
}
