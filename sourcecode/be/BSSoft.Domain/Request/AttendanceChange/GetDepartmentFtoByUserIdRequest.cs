﻿namespace SSWhite.Domain.Request.AttendanceChange
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Domain.Enums;

    public class GetDepartmentFtoByUserIdRequest
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
