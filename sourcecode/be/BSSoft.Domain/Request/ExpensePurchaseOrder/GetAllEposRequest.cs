﻿namespace SSWhite.Domain.Request.ExpensePurchaseOrder
{
    public class GetAllEposRequest
    {
        public int? ApprovalType { get; set; }

        public bool? IsMyEpos { get; set; }
    }
}
