﻿namespace SSWhite.Domain.Request.ExpensePurchaseOrder
{
    using Newtonsoft.Json;
    using SSWhite.Core.Common;
    using SSWhite.Core.Converters;
    using System;

    public class EpoLineItemsList
    {
        public int? Id { get; set; }

        public int LineNo { get; set; }

        public string ProductDescription { get; set; }

        public int ProductGroup { get; set; }

        public decimal Qty { get; set; }

        public decimal UnitPrice { get; set; }

        public int Uom { get; set; }

        public string? UomOther { get; set; }


        public int Department { get; set; }

        public int AccountNo { get; set; }

        public bool Gujarat { get; set; }

        public decimal Tax { get; set; }

        public decimal Discount { get; set; }

        public decimal? UnitPriceQty { get; set; }

        public decimal? QtyUnitPriceTax { get; set; }

        public decimal? TotalCostDiscount { get; set; }

        public decimal Gst { get; set; }

        public decimal? Cgst { get; set; }

        public decimal? Sgst { get; set; }

        public decimal? Igst { get; set; }

        public decimal ExtendedCost { get; set; }

        public string Purchase { get; set; }

        [JsonConverter(typeof(DateTimeConverter), DateFormat.DATE_FORMAT_YEAR)]
        public DateTime DueDate { get; set; }

        public decimal? CgstValue { get; set; }

        public decimal? SgstValue { get; set; }

        public decimal TaxValue { get; set; }

        public decimal? IgstValue { get; set; }

        public int? CurrencyType { get; set; }

        public string UOMName { get; set; }
    }
}
