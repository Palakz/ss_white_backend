﻿namespace SSWhite.Domain.Request.Admin
{
    using SSWhite.Core.Enums;

    public class GetDepartmentByDepartmentSupervisorNameRequest
    {
        public string DepartmentSupervisorName { get; set; }
    }
}
