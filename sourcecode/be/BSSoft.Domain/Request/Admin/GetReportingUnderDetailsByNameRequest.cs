﻿namespace SSWhite.Domain.Request.Admin
{
    using SSWhite.Core.Enums;

    public class GetReportingUnderDetailsByNameRequest
    {
        public string DepartmentSupervisorName { get; set; }
    }
}
