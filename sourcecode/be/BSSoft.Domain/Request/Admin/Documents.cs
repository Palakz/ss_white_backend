﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.AspNetCore.Http;

    public class Documents
    {
        public int? DocumentId { get; set; }

        public string Name { get; set; }

        public int DocumentTypeId { get; set; }

        public string? DocumentImage { get; set; }

        public bool IsDeleted { get; set; }

        public IFormFile Image { get; set; }
    }
}
