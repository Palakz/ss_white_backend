﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class AddOrEditEmployeeRequest
    {
        public int Id { get; set; }

        public string PersonID { get; set; }

        public string Organization { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public int Gender { get; set; }

        public string Cast { get; set; }

        public int MaritalStatusesId { get; set; }

        public DateTime DOB { get; set; }

        public DateTime DOJ { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string BloodGroup { get; set; }

        public string EmailAddress { get; set; }

        public string EmpPosition { get; set; }

        public string ReportingUnder { get; set; }

        public string Department { get; set; }

        public string Signature { get; set; }

        public string Picture { get; set; }

        public string Password { get; set; }

        public string PhoneNo { get; set; }

        public string WorkEmailAddress { get; set; }

        public string WorkPhoneNo { get; set; }

        public string PFNumber { get; set; }

        public string UANNunber { get; set; }

        public string BankAccountNumber { get; set; }

        public string Buddy { get; set; }

        public string EmergencyContactName { get; set; }

        public string EmergencyMobileNumber { get; set; }

        public string EmergencyPhoneNumber { get; set; }

        public string EmergencyAddress { get; set; }

        public string EmergencyEmailAddress { get; set; }

        public int Role { get; set; }

        public List<Documents> Documents { get; set; }

        [Internal]
        public int? CreatedBy { get; set; }

        public IFormFile AvatarImage { get; set; }

        public IFormFile SignatureImage { get; set; }

        public string GenderValue { get; set; }

        public string MaritalStatus { get; set; }

        public string RoleName { get; set; }
    }
}
