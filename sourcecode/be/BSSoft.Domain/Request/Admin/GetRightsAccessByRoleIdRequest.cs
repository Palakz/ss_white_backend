﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetRightsAccessByRoleIdRequest
    {
        public int? RoleId { get; set; }
    }
}
