﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetEmployeeByEmployeeIdRequest
    {
        public string EmployeeId { get; set; }
    }
}
