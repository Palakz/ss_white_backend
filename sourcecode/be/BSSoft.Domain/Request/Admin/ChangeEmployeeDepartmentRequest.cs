﻿namespace SSWhite.Domain.Request.Admin
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class ChangeEmployeeDepartmentRequest
    {
            public string EmployeeId { get; set; }

            public string PersonName { get; set; }

            public string CurrentDepartment { get; set; }

            public string NewDepartment { get; set; }

            public string ReasonForChangeDepartment { get; set; }
    }
}
