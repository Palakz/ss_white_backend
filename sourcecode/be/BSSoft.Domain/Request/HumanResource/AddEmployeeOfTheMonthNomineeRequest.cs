﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;

    public class AddEmployeeOfTheMonthNomineeRequest
    {
        public int? Id { get; set; }

        public int EmployeeId { get; set; }

        public int EmployeeTypeId { get; set; }

        public int DepartmentId { get; set; }

        public string Supervisor { get; set; }

        public string AdditionalDetails { get; set; }

        public int NominatedBy { get; set; }

        public DateTime NominatedDate { get; set; }

        public List<string> NominatedDetailsId { get; set; }

        public int CreatedBy { get; set; }
    }
}
