﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;

    public class CreateNewLeaveSheetRequest
    {
        public int? Id { get; set; }

        public int? UserId { get; set; }

        public int EmployeeTypeId { get; set; }

        public string? ContractAgency { get; set; }

        public int? LeaveTypeId { get; set; }

        public int? EVL { get; set; }

        public int? EFL { get; set; }

        //public string AlreadyTakenLeaveInCurrentMonth { get; set; }

        public DateTime LeaveApplyDate { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public float CountOfDays { get; set; }

        public string LeavePurpose { get; set; }

        public string? AddressWhileOnLeave { get; set; }

        public string? Note { get; set; }

        public int? ApprovedBY { get; set; }

        //public int? CreatedBy { get; set; }

        public string EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public string Designation { get; set; }

        public string EmployeeTypeValue { get; set; }

        public string LeaveTypeValue { get; set; }
    }
}
