﻿namespace SSWhite.Domain.Request.HumanResource
{
    using SSWhite.Core.Attributes;

    public class GetTrainingDetailsOfInstructorByInstructorNameRequest
    {
        public string InstructorName { get; set; }
    }
}
