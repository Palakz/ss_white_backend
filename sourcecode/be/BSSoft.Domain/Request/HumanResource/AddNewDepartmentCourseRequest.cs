﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;

    public class AddNewDepartmentCourseRequest
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int DepartmentId { get; set; }

        public int SubDepartmentId { get; set; }

        public string SummaryOfContent { get; set; }

        public decimal Hours { get; set; }

        public string Location { get; set; }

        //public int? CreatedBy { get; set; }
    }
}
