﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using SSWhite.Core.Attributes;
    using SSWhite.Core.Request;

    public class GetHoursByTrainingDropDownListRequest
    {
        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }
    }
}
