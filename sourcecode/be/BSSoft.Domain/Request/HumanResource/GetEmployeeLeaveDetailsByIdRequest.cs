﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetEmployeeLeaveDetailsByIdRequest
    {
        public int UserId { get; set; }
    }
}
