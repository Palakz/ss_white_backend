﻿namespace SSWhite.Domain.Request.HumanResource
{
    public class RecruiteeFamilyDetails
    {
        public string Relation { get; set; }

        public int RelationTypesId { get; set; }

        public string Name { get; set; }

        public string Occupation { get; set; }

        public string? OrganizationName { get; set; }
    }
}
