﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;

    public class AddGatepassRequest
    {
        public int? Id { get; set; }

        public int UserId { get; set; }

        public string EmployeeName { get; set; }

        public string PersonId { get; set; }

        public string SupervisorName { get; set; }

        public string SupervisorEmail { get; set; }

        public string Department { get; set; }

        public string Designation { get; set; }

        public string MobileNo { get; set; }

        public int EmployeeTypeId { get; set; }

        public string EmployeeTypeValue { get; set; }

        public string ContractAgency { get; set; }

        public string Purpose { get; set; }

        public DateTime TimeIn { get; set; }

        public DateTime TimeOut { get; set; }

        public int? CreatedBy { get; set; }
    }
}
