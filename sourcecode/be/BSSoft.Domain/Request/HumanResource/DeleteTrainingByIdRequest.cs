﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class DeleteTrainingByIdRequest
    {
        public int TrainingId { get; set; }

        public bool IsDeleted { get; set; }
    }
}
