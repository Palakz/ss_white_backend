﻿namespace SSWhite.Domain.Request.HumanResource
{
    public class RecruiteeReferences
    {
        public string Name { get; set; }

        public string Designation { get; set; }

        public string Address { get; set; }

        public string ContactNumber { get; set; }
    }
}
