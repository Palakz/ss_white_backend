﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetTrainingByIdRequest
    {
        public int TrainingId { get; set; }
    }
}
