﻿namespace SSWhite.Domain.Request.HumanResource
{
    using SSWhite.Core.Attributes;

    public class GetTrainingDetailsOfEmployeeByEmployeeIdRequest
    {
        public int UserId { get; set; }
    }
}
