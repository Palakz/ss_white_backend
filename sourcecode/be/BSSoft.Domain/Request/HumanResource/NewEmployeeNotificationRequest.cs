﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using Microsoft.AspNetCore.Http;
    using SSWhite.Core.Attributes;

    public class NewEmployeeNotificationRequest
    {
        public string Name { get; set; }

        public string Designation { get; set; }

        public string Education { get; set; }

        public string Department { get; set; }

        public string ReportingTo { get; set; }

        public string Experience { get; set; }

        public string Hobbies { get; set; }

        public DateTime JoiningDate { get; set; }

        public IFormFile Image { get; set; }

        [Internal]
        public string ImagePath { get; set; }
    }
}
