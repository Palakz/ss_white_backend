﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetDepartmentCoursesByDepartmentIdRequest
    {
        public int DepartmentId { get; set; }
    }
}
