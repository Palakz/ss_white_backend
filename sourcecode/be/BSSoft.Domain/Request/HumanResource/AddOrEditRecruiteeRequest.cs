﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;

    public class AddOrEditRecruiteeRequest
    {
        public int? Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string SurName { get; set; }

        public DateTime DOB { get; set; }

        public int GenderId { get; set; }

        public int MaritalStatusId { get; set; }

        public string ImagePath { get; set; }

        public bool DoAnyRelative { get; set; }

        public string RelativeName { get; set; }

        public string RelationWithRelative { get; set; }

        public List<int> IdentityProofs { get; set; }

        public bool IsDisabilityOrSickness { get; set; }

        public string DisabilityDescription { get; set; }

        public bool IsWorkExperience { get; set; }

        public bool IsFromReference { get; set; }

        public bool IsSameAsCurrentAddress { get; set; }

        public IFormFile AvatarImage { get; set; }

        public List<RecruiteeAddresses> RecruiteeAddresses { get; set; }

        public List<RecruiteeEducations> RecruiteeEducations { get; set; }

        public List<RecruiteeFamilyDetails> RecruiteeFamilyDetails { get; set; }

        public List<RecruiteeExperiences> RecruiteeExperiences { get; set; }

        public List<RecruiteeQuestions> RecruiteeQuestions { get; set; }

        public List<RecruiteeReferences> RecruiteeReferences { get; set; }

        public int CreatedBy { get; set; }
    }
}
