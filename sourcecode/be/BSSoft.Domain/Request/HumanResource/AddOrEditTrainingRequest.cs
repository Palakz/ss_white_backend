﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;

    public class AddOrEditTrainingRequest
    {
        public int? Id { get; set; }

        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public int DepartmentCourseId { get; set; }

        public string DepartmentCourseName { get; set; }

        public string Instructor { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public int EnteredBy { get; set; }

        public string EnteredByName { get; set; }

        public string AuthorName { get; set; }

        public string SummaryOfContent { get; set; }

        public decimal Hours { get; set; }

        public string Location { get; set; }

        public List<EmployeeTrainingRequest> EmployeeTraining { get; set; }
    }
}
