﻿namespace SSWhite.Domain.Request.HumanResource
{
    public class RecruiteeQuestions
    {
        //public int RecruiteeId { get; set; }

        public int QuestionsId { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public int QuestionCategoryId { get; set; }

        public string QuestionCategory { get; set; }

        public bool IsCorrect { get; set; }

        public string Base64Image { get; set; }
    }
}
