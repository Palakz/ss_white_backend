﻿namespace SSWhite.Domain.Request.HumanResource
{
    using SSWhite.Core.Attributes;

    public class GetAllTrainingsRequest
    {
        public int Start { get; set; }

        public int Length { get; set; }

        public string SortExpression { get; set; }

        [Trim]
        public string SearchKeyword { get; set; }
    }
}