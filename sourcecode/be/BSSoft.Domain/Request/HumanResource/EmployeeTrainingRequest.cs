﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using Microsoft.AspNetCore.Http;

    public class EmployeeTrainingRequest
    {
        public int? EmployeeTrainingId { get; set; }

        public string? EmployeeTrainingName { get; set; }

        public int TrainingTypeId { get; set; }

        public string TrainingType { get; set; }

        public int UserId { get; set; }

        public string EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public bool IsEvaluated { get; set; }

        public DateTime? EvaluationDate { get; set; }

        public string? EvaluationMethod { get; set; }

        public bool HasAttachment { get; set; }

        public string? JobDescriptionImage { get; set; }

        public string? TrainingHardCopyImage { get; set; }

        public bool IsDeleted { get; set; }

        public IFormFile JobDescriptionImageFile { get; set; }

        public IFormFile TrainingHardCopyImageFile { get; set; }

        public string? JobDescriptionImageName { get; set; }

        public string? TrainingHardCopyImageName { get; set; }
    }
}