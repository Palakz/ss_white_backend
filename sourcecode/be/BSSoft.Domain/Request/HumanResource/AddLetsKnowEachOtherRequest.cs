﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Http;

    public class AddLetsKnowEachOtherRequest
    {
        public int? Id { get; set; }

        public int? UserId { get; set; }

        public string SSWGoals { get; set; }

        public string InterestingFacts { get; set; }

        public string Hobbies { get; set; }

        public string FavouriteQuote { get; set; }

        public string FavouriteMovie { get; set; }

        public string FavouriteSong { get; set; }

        public string FavouriteBook { get; set; }

        public string PersonalGoals { get; set; }

        public string FamilyIntroduction { get; set; }

        public int? CreatedBy { get; set; }
    }
}
