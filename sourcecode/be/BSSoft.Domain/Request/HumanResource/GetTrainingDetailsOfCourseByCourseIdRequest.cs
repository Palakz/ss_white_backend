﻿namespace SSWhite.Domain.Request.HumanResource
{
    using SSWhite.Core.Attributes;

    public class GetTrainingDetailsOfCourseByCourseIdRequest
    {
        public int CourseId { get; set; }
    }
}
