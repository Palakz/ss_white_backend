﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;

    public class TrainingNotificationRequest
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }

        public string Location { get; set; }

        public string Instructor { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan Time { get; set; }

        public int TrainingTypeId { get; set; }

        public string TrainingType { get; set; }

        public List<string> EmailAddress { get; set; }

        public string Description { get; set; }
    }
}
