﻿using SSWhite.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.HumanResource
{
    public class GetAllEmployeeOfTheMonthNomineesRequest
    {
        public int Start { get; set; }

        public int Length { get; set; }

        public string SortExpression { get; set; }

        [Trim]
        public string SearchKeyword { get; set; }
    }
}
