﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;

    public class RecruiteeEducations
    {
        public DateTime AcademicYear { get; set; }

        public string Course { get; set; }

        public string University { get; set; }

        public string MainSubject { get; set; }

        public decimal Percentage { get; set; }

    }
}
