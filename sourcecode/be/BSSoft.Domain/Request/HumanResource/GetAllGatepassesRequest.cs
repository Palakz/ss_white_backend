﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SSWhite.Core.Attributes;

    public class GetAllGatepassesRequest
    {
        public int Start { get; set; }

        public int Length { get; set; }

        public string SortExpression { get; set; }

        [Trim]
        public string SearchKeyword { get; set; }
    }
}
