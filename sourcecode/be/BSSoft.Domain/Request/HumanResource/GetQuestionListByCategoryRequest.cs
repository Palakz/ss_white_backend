﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.HumanResource
{
    public class GetQuestionListByCategoryRequest
    {
        public int @CategoryId { get; set; }
    }
}
