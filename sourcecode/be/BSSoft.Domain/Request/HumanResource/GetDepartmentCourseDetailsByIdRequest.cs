﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetDepartmentCourseDetailsByIdRequest
    {
        public int Id { get; set; }
    }
}
