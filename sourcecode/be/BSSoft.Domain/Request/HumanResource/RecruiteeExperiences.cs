﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;

    public class RecruiteeExperiences
    {
        //public int RecruiteeId { get; set; }
        public string CompanyName { get; set; }

        public string CompanyAddress { get; set; }

        public string Designation { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public decimal MonthlySalary { get; set; }

    }
}
