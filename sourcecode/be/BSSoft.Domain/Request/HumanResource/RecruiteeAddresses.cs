﻿namespace SSWhite.Domain.Request.HumanResource
{
    public class RecruiteeAddresses
    {
        //public int RecruiteeId { get; set; }

        public int AddressType { get; set; }

        public bool IsSameAsCurrentAddress { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string Taluka { get; set; }

        public string District { get; set; }

        public string State { get; set; }

        public string PinCode { get; set; }

        public string MobileNumber1 { get; set; }

        public string MobileNumber2 { get; set; }

        public string Email { get; set; }
    }
}
