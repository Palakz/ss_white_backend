﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.HumanResource
{
    public class GetNomineeByNomineeIdRequest
    {
        public int NomineeId { get; set; }
    }
}
