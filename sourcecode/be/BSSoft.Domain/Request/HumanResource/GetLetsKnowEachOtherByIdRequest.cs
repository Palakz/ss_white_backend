﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Request.HumanResource
{
    public class GetLetsKnowEachOtherByIdRequest
    {
        public int Id { get; set; }
    }
}
