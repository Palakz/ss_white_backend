﻿namespace SSWhite.Domain.Request.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class GetGatepassByIdRequest
    {
        public int Id { get; set; }
    }
}
