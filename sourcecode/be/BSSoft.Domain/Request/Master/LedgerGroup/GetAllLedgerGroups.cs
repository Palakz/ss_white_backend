﻿namespace SSWhite.Domain.Request.Master.LedgerGroup
{
    using SSWhite.Core.Enums;
    using SSWhite.Domain.Enums.Master.LedgerGroup;

    public class GetAllLedgerGroups
    {
        public LedgerGroupType? LedgerGroupType { get; set; }

        public LedgerAccountType? AccountType { get; set; }

        public StatusType? Status { get; set; }
    }
}
