﻿namespace SSWhite.Domain.Request.Master.Tax
{
    using SSWhite.Core.Enums;

    public class GetAllTaxes
    {
        public StatusType? Status { get; set; }
    }
}
