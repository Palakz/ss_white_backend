﻿namespace SSWhite.Domain.Request.Master.Country
{
    using SSWhite.Core.Enums;

    public class GetAllCountries
    {
        public StatusType? Status { get; set; }
    }
}
