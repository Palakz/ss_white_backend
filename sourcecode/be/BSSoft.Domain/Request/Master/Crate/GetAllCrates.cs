﻿namespace SSWhite.Domain.Request.Master.Crate
{
    using SSWhite.Core.Enums;

    public class GetAllCrates
    {
        public StatusType? Status { get; set; }
    }
}
