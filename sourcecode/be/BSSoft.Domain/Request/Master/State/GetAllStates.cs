﻿namespace SSWhite.Domain.Request.Master.State
{
    using SSWhite.Core.Enums;

    public class GetAllStates
    {
        public int? CountryId { get; set; }

        public StatusType? Status { get; set; }
    }
}
