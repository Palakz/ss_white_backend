﻿namespace SSWhite.Domain.Request.Master.ItemSubCategory
{
    using SSWhite.Core.Enums;

    public class GetAllItemSubCategories
    {
        public int? ItemCategoryId { get; set; }

        public StatusType? Status { get; set; }
    }
}
