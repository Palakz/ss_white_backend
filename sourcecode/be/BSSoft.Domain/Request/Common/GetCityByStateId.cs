﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetCityByStateId
    {
        public int? StateId { get; set; }

        public StatusType? StatusType { get; set; }
    }
}
