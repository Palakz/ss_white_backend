﻿namespace SSWhite.Domain.Request.Common
{
    using System.Collections.Generic;
    using SSWhite.Core.Enums;
    using SSWhite.Domain.Enums.Master.LedgerGroup;

    public class GetClientsForDropdown
    {
        public GetClientsForDropdown()
        {
            AccountType = new List<LedgerAccountType>();
        }

        public StatusType? Status { get; set; }

        public List<LedgerAccountType> AccountType { get; set; }
    }
}
