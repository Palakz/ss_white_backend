﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetItemsForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
