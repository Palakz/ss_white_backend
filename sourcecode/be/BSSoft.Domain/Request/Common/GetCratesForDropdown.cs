﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetCratesForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
