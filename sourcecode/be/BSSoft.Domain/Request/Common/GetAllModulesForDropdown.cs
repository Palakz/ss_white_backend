﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetAllModulesForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
