﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetUqcsForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
