﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetItemCategoriesForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
