﻿namespace SSWhite.Domain.Request.Common
{
    public class GetDataForListByItemId
    {
        public int ItemId { get; set; }
    }
}
