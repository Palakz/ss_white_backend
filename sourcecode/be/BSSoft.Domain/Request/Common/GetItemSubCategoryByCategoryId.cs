﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetItemSubCategoryByCategoryId
    {
        public int? CategoryId { get; set; }

        public StatusType? Status { get; set; }
    }
}
