﻿namespace SSWhite.Domain.Request.Common
{
    public class GetItemDetailByLotNumber
    {
        public int LotNumber { get; set; }
    }
}
