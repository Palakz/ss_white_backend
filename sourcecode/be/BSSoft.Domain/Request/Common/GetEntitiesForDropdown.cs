﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetEntitiesForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
