﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetStateByCountryId
    {
        public int? CountryId { get; set; }

        public StatusType? StatusType { get; set; }
    }
}
