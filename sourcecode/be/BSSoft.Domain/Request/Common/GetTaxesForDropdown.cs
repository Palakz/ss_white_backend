﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetTaxesForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
