﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetItemByGroupId
    {
        public int? GroupId { get; set; }

        public StatusType? Status { get; set; }
    }
}
