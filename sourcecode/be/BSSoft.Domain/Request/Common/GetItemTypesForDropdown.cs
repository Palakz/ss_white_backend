﻿namespace SSWhite.Domain.Request.Common
{
    using SSWhite.Core.Enums;

    public class GetItemTypesForDropdown
    {
        public StatusType? Status { get; set; }
    }
}
