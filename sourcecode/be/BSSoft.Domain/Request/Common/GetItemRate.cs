﻿namespace SSWhite.Domain.Request.Common
{
    using System;
    using System.Collections.Generic;
    using SSWhite.Domain.Response.Common;

    public class GetItemRate
    {
        public int? ItemId { get; set; }

        public int? SiteId { get; set; }

        public DateTime DocumentDate { get; set; }

        public List<GetItemRatesResponse> ItemRates { get; set; }
    }
}
