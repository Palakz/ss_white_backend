﻿namespace SSWhite.Domain.Request.Dashboard
{
    public class UpdateWorkingStatusRequest
    {
        public bool? Status { get; set; }
    }
}
