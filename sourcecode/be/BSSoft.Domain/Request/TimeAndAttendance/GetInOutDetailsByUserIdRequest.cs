﻿namespace SSWhite.Domain.Request.TimeAndAttendance
{
    using System;
    using SSWhite.Core.Attributes;

    public class GetInOutDetailsByUserIdRequest
    {
        [Internal]
        public int UserId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
