﻿namespace SSWhite.Domain.Request.Report.LedgerReport
{
    using System;

    public class GetLedgerReport
    {
        public int? ClientId { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}
