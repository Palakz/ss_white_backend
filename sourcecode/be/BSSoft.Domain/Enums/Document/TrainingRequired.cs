﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWhite.Domain.Enums.Document
{
    public enum TrainingRequired : short
    {

        Yes = 1,
        No = 2,
        Optional = 3,
        NA = 4
    }
}
