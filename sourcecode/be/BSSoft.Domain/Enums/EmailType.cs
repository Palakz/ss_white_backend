﻿namespace SSWhite.Domain.Enums
{
    public enum EmailType : short
    {
        EPOMailToManager = 1,
        EPOMailToSelfForApproval = 2,
        EPOMailCreatedToSelf = 3,
        EmailIdsForFtoCreatedNotification = 4,
        EmailIdForFtoApproval = 5,
        EmailIdForFtoApproved = 6,
        EmailIdsForFtoApprovedNotification = 7,
        DocumentNotification = 8,
        DocumentApproval = 9,
        DocumentApprovedNotification = 10,
        NewEmployeeNotification = 11,
        TrainingScheduleNotification = 12,

        ChangeDepartmentNotification = 13,
        GatePassNotification = 14,
        LeaveSheetNotification = 15,
        PreRecruiteeNotification = 16,
        AddNewEmployeeNotification = 17,
        NewTrainingNotification = 18,
    }
}
