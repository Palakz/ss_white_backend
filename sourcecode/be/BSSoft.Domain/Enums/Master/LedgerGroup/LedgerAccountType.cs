﻿namespace SSWhite.Domain.Enums.Master.LedgerGroup
{
    using System.ComponentModel.DataAnnotations;
    using SSWhite.ResourceFile;

    public enum LedgerAccountType : short
    {
        [Display(Name = nameof(EnumLabels.CustomerOrSupplier), ResourceType = typeof(EnumLabels))]
        CustomerOrSupplier = 1,
        [Display(Name = nameof(EnumLabels.IncomeOrExpense), ResourceType = typeof(EnumLabels))]
        IncomeOrExpense = 2,
        [Display(Name = nameof(EnumLabels.BankOrCash), ResourceType = typeof(EnumLabels))]
        BankOrCash = 3,
        Broker = 4,
        Other = 5,
    }
}
