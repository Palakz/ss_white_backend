﻿namespace SSWhite.Domain.Enums.Master.LedgerGroup
{
    using System.ComponentModel.DataAnnotations;
    using SSWhite.ResourceFile;

    public enum LedgerSectionType : short
    {
        [Display(Name = nameof(EnumLabels.AddressDetails), ResourceType = typeof(EnumLabels))]
        AddressDetails = 1,

        [Display(Name = nameof(EnumLabels.StatutoryDetails), ResourceType = typeof(EnumLabels))]
        StatutoryDetails = 2,

        [Display(Name = nameof(EnumLabels.HeaderDetails), ResourceType = typeof(EnumLabels))]
        HeaderDetails = 3,

        [Display(Name = nameof(EnumLabels.BankDetails), ResourceType = typeof(EnumLabels))]
        BankDetails = 4,

        [Display(Name = nameof(EnumLabels.ContactDetails), ResourceType = typeof(EnumLabels))]
        ContactDetails = 5,
    }
}
