﻿namespace SSWhite.Domain.Enums.Master.Client
{
    public enum ClientType : short
    {
        Labour = 1,
        Supplier = 2,
        Other = 3,
        Machinery = 4
    }
}
