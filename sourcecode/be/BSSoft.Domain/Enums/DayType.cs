﻿namespace SSWhite.Domain.Enums
{
    using System.ComponentModel.DataAnnotations;
    using SSWhite.ResourceFile;

    public enum DayType : short
    {
        [Display(Name = nameof(EnumLabels.FullDayOff), ResourceType = typeof(EnumLabels))]
        FullDayOff = 1,

        [Display(Name = nameof(EnumLabels.FirstHalfOff), ResourceType = typeof(EnumLabels))]
        FirstHalfOff = 2,

        [Display(Name = nameof(EnumLabels.SecondHalfOff), ResourceType = typeof(EnumLabels))]
        SecondHalfOff = 3
    }
}
