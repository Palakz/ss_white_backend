﻿namespace SSWhite.Api.Controllers.V1.HumanResource
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using NReco.PdfGenerator;
    using SSWhite.Api.Common;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.Document;
    using SSWhite.Domain.Request.HumanResource;
    using SSWhite.Domain.Response.HumanResource;
    using SSWhite.Service.Interface.HumanResource;

    [Route("v1.0/[controller]")]
    [ApiController]
    public class HumanResourceController : BaseController
    {
        private readonly IHumanResourceService _humanResourceService;

        public HumanResourceController(IHumanResourceService humanResourceService, IOptions<AppSetting> appsetting)
        {
            _humanResourceService = humanResourceService;
        }

        [HttpGet("GetAllRecruiteeDropdowns")]
        public async Task<GetAllRecruiteeDropdownsResponse> GetAllRecruiteeDropdowns()
        {
            return await _humanResourceService.GetAllRecruiteeDropdowns();
        }

        [HttpGet("GetAllExtraOrdinaryAboutNominee")]
        public async Task<List<GetAllExtraOrdinaryAboutNomineeResponse>> GetAllExtraOrdinaryAboutNominee()
        {
            return await _humanResourceService.GetAllExtraOrdinaryAboutNominee();
        }

        [HttpGet("GetRecruiteeById")]
        public async Task<GetRecruiteeByIdResponse> GetRecruiteeById([FQuery] GetRecruiteeByIdRequest request)
        {
            var response = await _humanResourceService.GetRecruiteeById(await CreateServiceRequest(request));
            return response;
        }

        [HttpPost("GetAllRecruitees")]
        public async Task<ServiceSearchResponse<GetAllRecruiteesResponse>> GetAllRecruitees(ServiceSearchRequest<GetAllRecruiteesRequest> request)
        {
            return await _humanResourceService.GetAllRecruitees(request);
        }

        [HttpPost("GetAllEmployeeOfTheMonthNominees")]
        public async Task<ServiceSearchResponse<GetAllEmployeeOfTheMonthNomineesResponse>> GetAllRecruitees(ServiceSearchRequest<GetAllEmployeeOfTheMonthNomineesRequest> request)
        {
            return await _humanResourceService.GetAllEmployeeOfTheMonthNominees(request);
        }

        [HttpPost("AddOrEditRecruitee")]
        public async Task<AddOrEditRecruiteeResponse> AddOrEditRecruitee([FromForm] AddOrEditRecruiteeRequest request)
        {
            if (request.AvatarImage?.Length > 0)
            {
                Guid guid = Guid.NewGuid();
                string path = _appSetting.AvatarRootPathToStoreImage;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                request.ImagePath = $"{guid}_{request.AvatarImage.FileName}";

                using (FileStream fileStream = System.IO.File.Create(path + guid + '_' + request.AvatarImage.FileName))
                {
                    request.AvatarImage.CopyTo(fileStream);
                }
            }

            return await _humanResourceService.AddOrEditRecruitee(await CreateServiceRequest(request));
        }

        [HttpPost("AddEmployeeOfTheMonthNominee")]
        public async Task AddEmployeeOfTheMonthNominee([FromForm] AddEmployeeOfTheMonthNomineeRequest request)
        {
            await _humanResourceService.AddEmployeeOfTheMonthNominee(await CreateServiceRequest(request));
        }

        [HttpPost("AddLetsKnowEachOther")]
        public async Task AddLetsKnowEachOther([FromForm] AddLetsKnowEachOtherRequest request)
        {
            await _humanResourceService.AddLetsKnowEachOther(await CreateServiceRequest(request));
        }

        [HttpPost("GetAllLetsKnowEachOther")]
        public async Task<ServiceSearchResponse<GetAllLetsKnowEachOtherResponse>> GetAllLetsKnowEachOther(ServiceSearchRequest<GetAllLetsKnowEachOtherRequest> request)
        {
            return await _humanResourceService.GetAllLetsKnowEachOther(request);
        }

        [HttpPost("Notification/SendNewEmployeeNotification", Name = "SendNewEmployeeNotification")]
        public async Task<ActionResult> SendNewEmployeeNotification([FromForm] NewEmployeeNotificationRequest request)
        {
            if (request.Image?.Length > 0)
            {
                Guid guid = Guid.NewGuid();
                string path = _appSetting.AvatarRootPathToStoreImage;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                request.ImagePath = $"{guid}_{request.Image.FileName}";

                using (FileStream fileStream = System.IO.File.Create(path + guid + '_' + request.Image.FileName))
                {
                    request.Image.CopyTo(fileStream);
                    MemoryStream memoryStream = new MemoryStream();
                    request.Image.CopyTo(memoryStream);
                }
            }

            await _humanResourceService.SendNewEmployeeNotification(await CreateServiceRequest(request));

            return Ok();
        }

        [HttpGet("GetNomineeByNomineeId")]
        public async Task<List<GetNomineeByNomineeIdResponse>> GetNomineeByNomineeId([FromQuery] GetNomineeByNomineeIdRequest request)
        {
            return await _humanResourceService.GetNomineeByNomineeId(request);
        }

        [HttpGet("GetLetsKnowEachOtherById")]
        public async Task<List<GetLetsKnowEachOtherByIdResponse>> GetLetsKnowEachOtherById([FromQuery] GetLetsKnowEachOtherByIdRequest request)
        {
            return await _humanResourceService.GetLetsKnowEachOtherById(request);
        }

        [HttpGet("GetGatepassById")]
        public async Task<List<GetGatepassByIdResponse>> GetGatepassById([FromQuery] GetGatepassByIdRequest request)
        {
            return await _humanResourceService.GetGatepassById(request);
        }

        [HttpPost("AddGatepass")]
        public async Task AddGatepass([FromForm] AddGatepassRequest request)
        {
            await _humanResourceService.AddGatepass(await CreateServiceRequest(request));
        }

        [HttpPost("GetAllGatepasses")]
        public async Task<ServiceSearchResponse<GetAllGatepassesResponse>> GetAllGatepasses(ServiceSearchRequest<GetAllGatepassesRequest> request)
        {
            return await _humanResourceService.GetAllGatepasses(request);
        }

        [HttpPost("CreateNewLeaveSheet")]
        public async Task CreateNewLeaveSheet([FromForm] CreateNewLeaveSheetRequest request)
        {
            await _humanResourceService.CreateNewLeaveSheet(await CreateServiceRequest(request));
        }

        [HttpGet("GetEmployeeLeaveDetailsById")]
        public async Task<GetEmployeeLeaveDetailsByIdResponse> GetEmployeeLeaveDetailsById([FQuery] GetEmployeeLeaveDetailsByIdRequest request)
        {
            var response = await _humanResourceService.GetEmployeeLeaveDetailsById(await CreateServiceRequest(request));
            return response;
        }

        [HttpGet("GetLeaveSheetDetailsById")]
        public async Task<List<GetLeaveSheetDetailsByIdResponse>> GetLeaveSheetDetailsById([FromQuery] GetLeaveSheetDetailsByIdRequest request)
        {
            return await _humanResourceService.GetLeaveSheetDetailsById(request);
        }

        [HttpPost("GetAllLeaveSheets")]
        public async Task<ServiceSearchResponse<GetAllLeaveSheetsResponse>> GetAllLeaveSheets(ServiceSearchRequest<GetAllLeaveSheetsRequest> request)
        {
            return await _humanResourceService.GetAllLeaveSheets(request);
        }

        [HttpPost("AddNewDepartmentCourse")]
        public async Task AddNewDepartmentCourse([FromForm] AddNewDepartmentCourseRequest request)
        {
            await _humanResourceService.AddNewDepartmentCourse(await CreateServiceRequest(request));
        }

        [HttpPost("GetAllDepartmentCourses")]
        public async Task<ServiceSearchResponse<GetAllDepartmentCoursesResponse>> GetAllDepartmentCourses(ServiceSearchRequest<GetAllDepartmentCoursesRequest> request)
        {
            return await _humanResourceService.GetAllDepartmentCourses(request);
        }

        [HttpGet("GetDepartmentCourseDetailsById")]
        public async Task<List<GetDepartmentCourseDetailsByIdResponse>> GetDepartmentCourseDetailsById([FromQuery] GetDepartmentCourseDetailsByIdRequest request)
        {
            return await _humanResourceService.GetDepartmentCourseDetailsById(request);
        }

        [HttpGet("GetDepartmentCoursesByDepartmentId")]
        public async Task<GetDepartmentCoursesByDepartmentIdResponse> GetDepartmentCoursesByDepartmentId([FromQuery] GetDepartmentCoursesByDepartmentIdRequest request)
        {
            return await _humanResourceService.GetDepartmentCoursesByDepartmentId(request);
        }

        [HttpPost("AddOrEditTraining")]
        public async Task<AddOrEditTrainingResponse> AddOrEditTraining([FromForm] AddOrEditTrainingRequest request)
        {
            if (request.EmployeeTraining.Count > 0)
            {
                foreach (var item in request.EmployeeTraining)
                {
                    var jobDescriptionImage = item.JobDescriptionImageFile;
                    if (jobDescriptionImage != null)
                    {
                        if (jobDescriptionImage.Length > 0)
                        {
                            Guid guid = Guid.NewGuid();
                            string path = _appSetting.AvatarRootPathToStoreImage;

                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }

                            item.JobDescriptionImage = $"{guid}_{item.JobDescriptionImageFile.FileName}";

                            using (FileStream fileStream = System.IO.File.Create(path + guid + "_" + jobDescriptionImage.FileName))
                            {
                                jobDescriptionImage.CopyTo(fileStream);
                            }
                        }
                    }

                    var trainingCopyImage = item.TrainingHardCopyImageFile;
                    if (trainingCopyImage != null)
                    {
                        if (trainingCopyImage.Length > 0)
                        {
                            Guid guid = Guid.NewGuid();
                            string path = _appSetting.AvatarRootPathToStoreImage;

                            if (!Directory.Exists(path))
                            {
                                Directory.CreateDirectory(path);
                            }

                            item.TrainingHardCopyImage = $"{guid}_{item.TrainingHardCopyImageFile.FileName}";

                            using (FileStream fileStream = System.IO.File.Create(path + guid + "_" + trainingCopyImage.FileName))
                            {
                                trainingCopyImage.CopyTo(fileStream);
                            }
                        }
                    }
                }
            }

            return await _humanResourceService.AddOrEditTraining(await CreateServiceRequest(request));
        }

        [HttpPost("GetAllTrainings")]
        public async Task<ServiceSearchResponse<GetAllTrainingsResponse>> GetAllTrainings(ServiceSearchRequest<GetAllTrainingsRequest> request)
        {
            return await _humanResourceService.GetAllTrainings(request);
        }

        [HttpGet("GetTrainingById")]
        public async Task<GetTrainingByIdResponse> GetTrainingById([FQuery] GetTrainingByIdRequest request)
        {
            var response = await _humanResourceService.GetTrainingById(await CreateServiceRequest(request));
            return response;
        }

        [HttpPost("DeleteTrainingById")]
        public async Task DeleteTrainingById([FQuery] DeleteTrainingByIdRequest request)
        {
            await _humanResourceService.DeleteTrainingById(await CreateServiceRequest(request));
        }

        [HttpPost("GetAllInstructorByTrainingDropDownList")]
        public async Task<ServiceSearchResponse<GetInstructorByTrainingDropDownListResponse>> GetInstructorByTrainingDropDownList(ServiceSearchRequest<GetInstructorByTrainingDropDownListRequest> request)
        {
            return await _humanResourceService.GetInstructorByTrainingDropDownList(request);
        }

        [HttpPost("GetAllEmployeeTrainingDropDownList")]
        public async Task<ServiceSearchResponse<GetEmployeeTrainingDropDownListResponse>> GetEmployeeTrainingDropDownList(ServiceSearchRequest<GetEmployeeTrainingDropDownListRequest> request)
        {
            return await _humanResourceService.GetEmployeeTrainingDropDownList(request);
        }

        [HttpPost("GetAllCourseByTrainingDropDownList")]
        public async Task<ServiceSearchResponse<GetCourseByTrainingDropDownListResponse>> GetCourseByTrainingDropDownList(ServiceSearchRequest<GetCourseByTrainingDropDownListRequest> request)
        {
            return await _humanResourceService.GetCourseByTrainingDropDownList(request);
        }

        [HttpPost("GetAllHoursByTrainingDropDownList")]
        public async Task<ServiceSearchResponse<GetHoursByTrainingDropDownListResponse>> GetHoursByTrainingDropDownList(ServiceSearchRequest<GetHoursByTrainingDropDownListRequest> request)
        {
            return await _humanResourceService.GetHoursByTrainingDropDownList(request);
        }

        [HttpPost("GetTrainingDetailsOfCourseByCourseId")]
        public async Task<ServiceSearchResponse<GetTrainingDetailsOfCourseByCourseIdResponse>> GetTrainingDetailsOfCourseByCourseId(ServiceSearchRequest<GetTrainingDetailsOfCourseByCourseIdRequest> request)
        {
            return await _humanResourceService.GetTrainingDetailsOfCourseByCourseId(request);
        }

        [HttpPost("GetTrainingDetailsOfInstructorByInstructorName")]
        public async Task<ServiceSearchResponse<GetTrainingDetailsOfInstructorByInstructorNameResponse>> GetTrainingDetailsOfInstructorByInstructorName(ServiceSearchRequest<GetTrainingDetailsOfInstructorByInstructorNameRequest> request)
        {
            return await _humanResourceService.GetTrainingDetailsOfInstructorByInstructorName(request);
        }

        [HttpPost("GetTrainingDetailsOfEmployeeByEmployeeId")]
        public async Task<ServiceSearchResponse<GetTrainingDetailsOfEmployeeByEmployeeIdResponse>> GetTrainingDetailsOfEmployeeByEmployeeId(ServiceSearchRequest<GetTrainingDetailsOfEmployeeByEmployeeIdRequest> request)
        {
            return await _humanResourceService.GetTrainingDetailsOfEmployeeByEmployeeId(request);
        }

        [HttpPost("Notification/SendTrainingNotification", Name = "SendTrainingNotification")]
        public async Task<ActionResult> SendTrainingNotification(TrainingNotificationRequest request)
        {
            await _humanResourceService.SendTrainingNotification(await CreateServiceRequest(request));
            return Ok();
        }

        [HttpGet("DownloadPdf")]
        [ProducesResponseType(typeof(byte[]), StatusCodes.Status200OK)]
        public async Task<byte[]> DownloadPdf([FromQuery] GetRecruiteeByIdRequest request)
        {
            var response = await _humanResourceService.GetRecruiteeById(await CreateServiceRequest(request));

            string htmlContent;

            htmlContent = TemplateGenerator.GenerateRecruiteeFormTemplate(response);

            var htmlToPdf = new HtmlToPdfConverter();
            var margins = new PageMargins() { Top = 30, Bottom = 30, Left = 5, Right = 5 };
            htmlToPdf.CustomWkHtmlArgs = "  --print-media-type ";
            htmlToPdf.Margins = margins;
            ////htmlToPdf.Margins.Left = 5;
            ////htmlToPdf.Margins.Right = 5;
            htmlToPdf.Orientation = PageOrientation.Portrait;
            htmlToPdf.Size = PageSize.A4;
            htmlToPdf.PageHeaderHtml = TemplateGenerator.GenerateRecruiteeHeaderHtml();
            ////htmlToPdf.PageFooterHtml = "<div style='position: relative;bottom: 200; left: 0;font-color:grey;'>Page <span class='page'></span> of <span class='topage'></span></div>";
            var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);
            return pdfBytes;
        }
    }
}
