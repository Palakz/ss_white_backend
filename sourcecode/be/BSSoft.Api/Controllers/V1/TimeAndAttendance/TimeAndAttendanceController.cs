﻿namespace SSWhite.Api.Controllers.V1.TimeAndAttendance
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Domain.Request.TimeAndAttendance;
    using SSWhite.Domain.Response.TimeAndAttendance;
    using SSWhite.Service.Interface.TimeAndAttendance;

    [Route("v1.0/[controller]")]
    [ApiController]
    public class TimeAndAttendanceController : BaseController
    {
        private readonly ITimeAndAttendanceService _timeAndAttendanceService;

        public TimeAndAttendanceController(ITimeAndAttendanceService timeAndAttendanceService, ISession session, IOptions<AppSetting> appsetting, IOptions<IPAddress> ipAddress)
            : base(session, appsetting, ipAddress)
        {
            _timeAndAttendanceService = timeAndAttendanceService;
        }

        [HttpPost("InsertInOutDetailsByPunchType")]
        public async Task InsertPunchDetails(InsertPunchDetailsRequest insertPunchDetailsRequest)
        {
            await _timeAndAttendanceService.InsertPunchDetails(await CreateServiceRequest(insertPunchDetailsRequest));
        }

        ////[HttpPost("InsertInOutDetailsByPunchType")]
        ////public async Task InsertInOutDetailsForLunch(InsertInOutDetailsRequest insertInOutDetailsRequest)
        ////{
        ////    var session = await GetSessionDetails();
        ////    ////insertInOutDetailsRequest.PunchType = 2;
        ////    insertInOutDetailsRequest.UserId = session.UserId;
        ////    await _timeAndAttendanceService.InsertInOutDetailsForLunch(insertInOutDetailsRequest);
        ////}

        [HttpPost("GetInOutDetailsByUserId")]
        public async Task<List<GetInOutDetailsByUserIdFinalResponse>> GetInOutDetailsByUserId(GetInOutDetailsByUserIdRequest request)
        {
            var response = await _timeAndAttendanceService.GetInOutDetailsByUserId(await CreateServiceRequest(request));
            ////foreach (var item in res)
            ////{
            ////    item.TotalHours = 10;
            ////}

            return response;
            ////return new List<GetInOutDetailsByUserIdResponse>();
        }

        [HttpGet("GetUsersInOutStatus")]
        public async Task<List<GetUsersInOutStatusResponse>> GetUsersInOutStatus()
        {
            return await _timeAndAttendanceService.GetUsersInOutStatus();
        }

        [HttpGet("GetInOutStatusByPunchTypeUserId")]
        public async Task<GetInOutStatusByPunchTypeByUserIdResponse> GetInOutStatusByPunchTypeUserId([FQuery] GetInOutStatusByPunchTypeUserIdRequest request)
        {
            return await _timeAndAttendanceService.GetInOutStatusByPunchTypeUserId(await CreateServiceRequest(request));
        }

        ////[HttpPost("InsertMissingPunchDetails")]
        ////public async Task InsertMissingPunchDetails(DateTime date, DateTime punch, string type, string inOutStatus)
        ////{
        ////    ////var session = await GetSessionDetails();
        ////    ////insertInOutDetailsRequest.PunchType = 4;
        ////    ////insertInOutDetailsRequest.UserId = session.UserId;
        ////    ////await _timeAndAttendanceService.InsertInOutDetailsForLunch(insertInOutDetailsRequest);

        ////    ////return new List<GetInOutDetailsByUserId>()
        ////    ////{
        ////    ////    new GetInOutDetailsByUserId
        ////    ////    {
        ////    ////        ////Date = DateTime.ParseExact("2022/04/13", "yyyy/dd/MM", CultureInfo.InvariantCulture),
        ////    ////        Date = new DateTime(2022, 04, 13).Date,
        ////    ////        FirstPunch = new DateTime(2022, 04, 13, 09, 15, 00).ToString(),
        ////    ////        LastPunch = new DateTime(2022, 04, 13, 22, 15, 00).ToString(),
        ////    ////        PunchType = "Normal",
        ////    ////        TotalHours = new DateTime(2022, 04, 13, 22, 15, 00).Subtract(new DateTime(2022, 04, 13, 10, 15, 00)).TotalHours
        ////    ////    },
        ////    ////    new GetInOutDetailsByUserId
        ////    ////    {
        ////    ////        ////Date = DateTime.ParseExact("2022/04/13", "yyyy/dd/MM", CultureInfo.InvariantCulture),
        ////    ////        Date = new DateTime(2022, 04, 13).Date,
        ////    ////        FirstPunch = new DateTime(2022, 04, 13, 13, 15, 00).ToString(),
        ////    ////        LastPunch = new DateTime(2022, 04, 13, 14, 15, 00).ToString(),
        ////    ////        PunchType = "Lunch",
        ////    ////        TotalHours = new DateTime(2022, 04, 13, 13, 15, 00).Subtract(new DateTime(2022, 14, 13, 10, 15, 00)).TotalHours
        ////    ////    }
        ////    ////};
        ////}
    }
}
