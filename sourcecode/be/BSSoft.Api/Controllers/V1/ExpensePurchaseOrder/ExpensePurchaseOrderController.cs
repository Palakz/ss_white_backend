﻿namespace SSWhite.Api.Controllers.V1.ExpensePurchaseOrder
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Common;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Attributes;
    using SSWhite.Core.Common.Application;
    using SSWhite.Core.Request;
    using SSWhite.Core.Response;
    using SSWhite.Domain.Request.ExpensePurchaseOrder;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;
    using SSWhite.Service.Interface.ExpensePurchaseOrder;

    [Route("v1.0/[controller]")]
    [ApiController]
    public class ExpensePurchaseOrderController : BaseController
    {
        private readonly IExpensePurchaseOrderService _expensePurchaseOrderService;
        private readonly AppSetting _appSetting;

        public ExpensePurchaseOrderController(IExpensePurchaseOrderService expensePurchaseOrderService, IOptions<AppSetting> appSetting)
        {
            _expensePurchaseOrderService = expensePurchaseOrderService;

            _appSetting = appSetting.Value;
        }

        [HttpGet("GetAllEPODropDowns")]
        public async Task<GetAllEPODropDownsResponse> GetAllEPODropDowns()
        {
            return await _expensePurchaseOrderService.GetAllEPODropDowns();
        }

        [HttpGet("GetAuthorizedLimitByUserId")]
        public async Task<GetAuthorizedLimitByUserIdResponse> GetAuthorizedLimitByUserId()
        {
            return await _expensePurchaseOrderService.GetAuthorizedLimitByUserId(await CreateServiceRequest());
        }

        [HttpPost("AddOrEditEpo")]
        public async Task<AddOrEditResponse> AddOrEditEpo([FromForm] AddOrEditEpoRequest request)
        {
            request.EpoAttachmentMst = new List<EpoAttachmentMst>();
            if (request.AttachmentImage?.Any() == true)
            {
                foreach (var item in request.AttachmentImage)
                {
                    if (item.Length > 0)
                    {
                        var epoAttachmentMst = new EpoAttachmentMst();

                        Guid guid = Guid.NewGuid();
                        ////string path = _webHostEnvironment.WebRootPath + "\\uploads\\";
                        ////string path = $"{_configuration.GetValue<string>("AppSetting:AttachmentRootPathToStoreImage")}";
                        string path = _appSetting.AttachmentRootPathToStoreImage;

                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        var attachmentPath = $"{guid}_{item.FileName}";
                        request.EpoAttachmentMst.Add(new EpoAttachmentMst
                        {
                            AttachedFileName = attachmentPath,
                        });

                        using (FileStream fileStream = System.IO.File.Create(path + guid + "_" + item.FileName))
                        {
                            item.CopyTo(fileStream);
                        }

                        request.IsAttachmentPending = null;
                    }
                }
            }

            return await _expensePurchaseOrderService.AddOrEditEpo(await CreateServiceRequest(request));
        }

        [HttpGet("GetEpoDetailsByEpoId")]
        public async Task<GetEpoDetailsByEpoIdResponse> GetEpoDetailsByEpoId([FromQuery] GetEpoDetailsByEpoIdRequest request)
        {
            return await _expensePurchaseOrderService.GetEpoDetailsByEpoId(await CreateServiceRequest(request));
        }

        [HttpGet("GetAllVendorOrEmployeeDetailsByName")]

        public async Task<List<GetAllVendorOrEmployeeDetailsByNameResponse>> GetAllVendorOrEmployeeDetailsByName([FQuery] GetAllVendorOrEmployeeDetailsByNameRequest request)
        {
            return await _expensePurchaseOrderService.GetAllVendorOrEmployeeDetailsByName(request);
        }

        [HttpPost("GetAllEpos")]
        public async Task<ServiceSearchResponse<GetAllEposResponse>> GetAllEpos(ServiceSearchRequest<GetAllEposRequest> request)
        {
            await SetSessionDetails(request);
            return await _expensePurchaseOrderService.GetAllEpos(request);
        }

        [HttpPost("VoidEpoByUserId")]
        public async Task VoidEpoByUserId(GetEpoDetailsByEpoIdRequest request)
        {
            await _expensePurchaseOrderService.VoidEpoByUserId(await CreateServiceRequest(request));
        }

        [HttpPost("ApproveOrDenyEpo")]
        public async Task ApproveOrDenyEpo([FQuery] ApproveOrDenyEpoRequest request)
        {
            await _expensePurchaseOrderService.ApproveOrDenyEpo(await CreateServiceRequest(request));
        }

        [HttpPost("GetMpcVoteForEpo")]
        public async Task<ServiceSearchResponse<GetMpcVoteForEpoResponse>> GetMpcVoteForEpo(ServiceSearchRequest<GetMpcVoteForEpoRequest> request)
        {
            await SetSessionDetails(request);
            return await _expensePurchaseOrderService.GetMpcVoteForEpo(request);
        }

        [HttpPost("GetAllMpcVoteForEpo")]
        public async Task<ServiceSearchResponse<GetAllMpcVoteForEpoResponse>> GetAllMpcVoteForEpo(ServiceSearchRequest<GetAllMpcVoteForEpoRequest> request)
        {
            await SetSessionDetails(request);
            return await _expensePurchaseOrderService.GetAllMpcVoteForEpo(request);
        }

        [HttpPost("GetMySubordinateEpos")]
        public async Task<ServiceSearchResponse<GetMySubordinateEposResponse>> GetMySubordinateEpos(ServiceSearchRequest<GetMySubordinateEposRequest> request)
        {
            await SetSessionDetails(request);
            return await _expensePurchaseOrderService.GetMySubordinateEpos(request);
        }

        [HttpPost("GetMyApprovalForEpo")]
        public async Task<ServiceSearchResponse<GetMyApprovalForEpoResponse>> GetMyApprovalForEpo(ServiceSearchRequest<GetMyApprovalForEpoRequest> request)
        {
            await SetSessionDetails(request);
            return await _expensePurchaseOrderService.GetMyApprovalForEpo(request);
        }

        [HttpGet("GetEpoModulesCounts")]
        public async Task<GetEpoModulesCountsResponse> GetEpoModulesCounts()
        {
            var response = await _expensePurchaseOrderService.GetEpoModulesCounts(await CreateServiceRequest());
            return response;
        }

        [HttpGet("GetMpcVoteDetailsByEpoId")]
        public async Task<List<GetMpcVoteDetailsByEpoIdResponse>> GetMpcVoteDetailsByEpoId([FromQuery] GetMpcVoteDetailsByEpoIdRequest request)
        {
            var response = await _expensePurchaseOrderService.GetMpcVoteDetailsByEpoId(request);
            return response;
        }

        [HttpGet("DownloadPdf")]
        [ProducesResponseType(typeof(byte[]), StatusCodes.Status200OK)]
        public async Task<byte[]> DownloadPdf([FromQuery] GetEpoDetailsByEpoIdRequest request)
        {
            var epoDetails = await _expensePurchaseOrderService.GetEpoDetailsByEpoId(await CreateServiceRequest(request));
            var htmlContent = TemplateGenerator.GenerateEPODetailsTemplate(epoDetails);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            htmlToPdf.Margins.Top = 85;
            htmlToPdf.CustomWkHtmlArgs = "  --print-media-type ";
            htmlToPdf.Margins.Left = 5;
            htmlToPdf.Margins.Right = 5;
            htmlToPdf.Orientation = NReco.PdfGenerator.PageOrientation.Portrait;
            htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            htmlToPdf.PageHeaderHtml = TemplateGenerator.GenerateEpoHeaderDetails(epoDetails);
            htmlToPdf.PageFooterHtml = TemplateGenerator.GenerateEpoFooterDetails(epoDetails);
            var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);
            return pdfBytes;
        }
    }
}
