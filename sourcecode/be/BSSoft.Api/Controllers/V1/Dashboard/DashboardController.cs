﻿namespace SSWhite.Api.Controllers.V1.Dashboard
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using SSWhite.Api.Controllers.Base;
    using SSWhite.Component.Interface.Session;
    using SSWhite.Core.Common.Application;
    using SSWhite.Domain.Request.Dashboard;
    using SSWhite.Domain.Response.Dashboard;
    using SSWhite.Domain.Response.ExpensePurchaseOrder;
    using SSWhite.ResourceFile;
    using SSWhite.Service.Interface.Account;
    using SSWhite.Service.Interface.Dashboard;

    [Route("v1.0/[controller]")]
    [ApiController]
    public class DashboardController : BaseController
    {
        private readonly IDashboardService _dashboardService;

        public DashboardController(IDashboardService dashboardService, ISession session, IOptions<AppSetting> appsetting, IOptions<IPAddress> ipAddress)
            : base(session, appsetting, ipAddress)
        {
            _dashboardService = dashboardService;
        }

        [HttpGet("GetSSWhiteTeamDetails")]
        public async Task<List<GetSSWhiteTeamDetailsResponse>> GetSSWhiteTeamDetails()
        {
            return await _dashboardService.GetSSWhiteTeamDetails();
        }

        [HttpGet("GetUserDetailsById")]
        public async Task<UserDetail> GetUserDetailsById()
        {
            var session = await GetSessionDetails();
            ////var serviceRequest = await CreateServiceRequest(session.UserId);
            var response = await _dashboardService.GetUserDetailsById(session.UserId);
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(response.Picture);
            request.Method = "HEAD";

            try
            {
                request.GetResponse();
                response.IsPictureExists = true;
            }
            catch (Exception e)
            {
                response.IsPictureExists = false;
            }

            return response;
        }

        [HttpGet("UserNotification")]
        public async Task<UserNotificationResponse> UserNotification()
        {
            return await _dashboardService.UserNotification(await CreateServiceRequest());
        }

        [HttpGet("GetNotifications")]
        public async Task<List<GetNotificationsResponse>> GetNotifications()
        {
            var response = await _dashboardService.GetNotifications(await CreateServiceRequest());
            return response;
        }

        [HttpGet("GetModulesCounts")]
        public async Task<GetModulesCountsResponse> GetModulesCounts()
        {
            return await _dashboardService.GetModulesCounts(await CreateServiceRequest());
        }

        [HttpPost("UpdateWorkingStatus")]
        public async Task<bool> UpdateWorkingStatus(UpdateWorkingStatusRequest request)
        {
            return await _dashboardService.UpdateWorkingStatus(await CreateServiceRequest(request));
        }

        [HttpPost("ChangePassword")]
        public async Task<ActionResult> ChangePassword(ChangePasswordRequest request)
        {
            var response = await _dashboardService.ChangePassword(await CreateServiceRequest(request));

            if (response == -1)
            {
                Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status406NotAcceptable;

                return new JsonResult(new { error = Validations.CurrentPasswordIsNotCorrect });
            }
            else
            {
                await DeleteSession();
                Response.StatusCode = Microsoft.AspNetCore.Http.StatusCodes.Status200OK;
                return new JsonResult(new { message = "Success" });
            }
        }
    }
}
