﻿namespace SSWhite.Logger.Interface.ExceptionLogger
{
    using System;
    using System.Threading.Tasks;
    using SSWhite.Core.Request;

    public interface IExceptionLogger
    {
        public Task LogToDatabase<T>(ServiceRequest<T> request, Exception exception);

        public Task LogToFile(Exception appException, Exception exception);

        public Task LogToDatabase(Exception exception);
    }
}
